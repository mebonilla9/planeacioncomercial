/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package co.movistar.planeacioncomercial.negocio.rest.servicios;

import co.movistar.planeacioncomercial.modelo.conexion.ConexionBD;
import co.movistar.planeacioncomercial.modelo.vo.AppMetas;
import co.movistar.planeacioncomercial.negocio.delegado.AppMetasDelegado;
import co.movistar.planeacioncomercial.negocio.excepciones.PlaneacionComercialException;
import co.movistar.planeacioncomercial.negocio.util.TokenUtil;

import javax.ws.rs.GET;
import javax.ws.rs.HeaderParam;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.UriInfo;
import java.sql.Connection;
import java.util.List;

/**
 * REST Web Service
 *
 * @author Lord_Nightmare
 */
@Path("meta")
public class AppMetaRest {

  @Context
  private UriInfo context;

  /**
   * Creates a new instance of AppMetaRest
   */
  public AppMetaRest() {
  }

  /**
   * Retrieves representation of an instance of co.movistar.planeacioncomercial.negocio.rest.servicios.AppMetaRest
   *
   * @param token
   * @return an instance of co.movistar.planeacioncomercial.modelo.vo.AppMetas
   * @throws co.movistar.planeacioncomercial.negocio.excepciones.PlaneacionComercialException
   */
  @GET
  @Path("consultar")
  @Produces(MediaType.APPLICATION_JSON)
  public List<AppMetas> consultarIndiceMetas(@HeaderParam("Authorization") String token) throws PlaneacionComercialException {
    Connection cnn = null;
    try {
      cnn = ConexionBD.conectar();
      TokenUtil.obtenerUsuarioToken(token, cnn);
      List<AppMetas> consultar = new AppMetasDelegado(cnn, null).consultar();
      AppMetas meta = new AppMetas();
      meta.setNombre("Incapacidad");
      consultar.add(meta);
      return consultar;
    } finally {
      ConexionBD.desconectar(cnn);
    }
  }
}
