/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package co.movistar.planeacioncomercial.negocio.rest.servicios;

import co.movistar.planeacioncomercial.modelo.conexion.ConexionBD;
import co.movistar.planeacioncomercial.modelo.vo.AppCiudad;
import co.movistar.planeacioncomercial.negocio.delegado.AppCiudadDelegado;
import co.movistar.planeacioncomercial.negocio.excepciones.PlaneacionComercialException;
import co.movistar.planeacioncomercial.negocio.util.TokenUtil;

import javax.ws.rs.*;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.UriInfo;
import java.sql.Connection;
import java.util.ArrayList;
import java.util.List;

/**
 * REST Web Service
 *
 * @author Lord_Nightmare
 */
@Path("ciudad")
public class AppCiudadRest {

  @Context
  private UriInfo context;

  /**
   * Creates a new instance of AppCiudadRest
   */
  public AppCiudadRest() {
  }

  /**
   * Retrieves representation of an instance of co.movistar.planeacioncomercial.negocio.rest.servicios.AppCiudadRest
   *
   * @param token
   * @param idRegional
   * @return an instance of co.movistar.planeacioncomercial.modelo.vo.AppCiudad
   * @throws co.movistar.planeacioncomercial.negocio.excepciones.PlaneacionComercialException
   */
  @GET
  @Path("consultar/{idRegional}")
  @Produces(MediaType.APPLICATION_JSON)
  public List<AppCiudad> consultarPorRegional(@HeaderParam("Authorization") String token, @PathParam("idRegional") long idRegional) throws PlaneacionComercialException {
    List<AppCiudad> lista = new ArrayList<>();
    Connection cnn = null;
    try {
      cnn = ConexionBD.conectar();
      TokenUtil.obtenerUsuarioToken(token, cnn);
      lista = new AppCiudadDelegado(cnn, null).consultarPorRegional(idRegional);
    } finally {
      ConexionBD.desconectar(cnn);
    }
    return lista;
  }
}
