/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package co.movistar.planeacioncomercial.negocio.rest.servicios;

import co.movistar.planeacioncomercial.modelo.conexion.ConexionBD;
import co.movistar.planeacioncomercial.modelo.vo.AppArticulos;
import co.movistar.planeacioncomercial.negocio.delegado.AppArticulosDelegado;
import co.movistar.planeacioncomercial.negocio.excepciones.PlaneacionComercialException;
import co.movistar.planeacioncomercial.negocio.util.TokenUtil;

import javax.ws.rs.*;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.UriInfo;
import java.sql.Connection;
import java.util.ArrayList;
import java.util.List;

/**
 * REST Web Service
 *
 * @author lord_nightmare
 */
@Path("articulos")
public class AppArticulosRest {

  @Context
  private UriInfo context;

  /**
   * Creates a new instance of AppArticulosRest
   */
  public AppArticulosRest() {
  }

  /**
   * Retrieves representation of an instance of co.movistar.planeacioncomercial.negocio.rest.servicios.AppArticulosRest
   *
   * @param token
   * @param id
   * @return an instance of co.movistar.planeacioncomercial.modelo.vo.AppArticulos
   * @throws co.movistar.planeacioncomercial.negocio.excepciones.PlaneacionComercialException
   */
  @GET
  @Path("consultar/{id}")
  @Produces(MediaType.APPLICATION_JSON)
  public List<AppArticulos> obtenerArticulosPunto(@HeaderParam("Authorization") String token, @PathParam("id") String id) throws PlaneacionComercialException {
    List<AppArticulos> articulos = new ArrayList<>();
    Connection cnn = null;
    try {
      cnn = ConexionBD.conectar();
      TokenUtil.obtenerUsuarioToken(token, cnn);
      articulos = new AppArticulosDelegado(cnn, null).consultarPorPunto(Long.parseLong(id));
    } finally {
      ConexionBD.desconectar(cnn);
    }
    return articulos;
  }
}
