/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package co.movistar.planeacioncomercial.negocio.rest.autoridad;

import javax.ws.rs.core.Application;
import java.util.Set;

/**
 * @author Manuel Ernesto Bonilla Muñoz <mebonilla9@gmail.com>
 */
@javax.ws.rs.ApplicationPath("rest")
public class ApplicationConfig extends Application {

  @Override
  public Set<Class<?>> getClasses() {
    Set<Class<?>> resources = new java.util.HashSet<>();
    addRestResourceClasses(resources);
    return resources;
  }


  /**
   * Do not modify addRestResourceClasses() method.
   * It is automatically populated with
   * all resources defined in the project.
   * If required, comment out calling this method in getClasses().
   */
  private void addRestResourceClasses(Set<Class<?>> resources) {
    resources.add(co.movistar.planeacioncomercial.negocio.rest.autoridad.AutenticacionRest.class);
    resources.add(co.movistar.planeacioncomercial.negocio.rest.proveedor.PlaneacionComercialExceptionHandler.class);
    resources.add(co.movistar.planeacioncomercial.negocio.rest.proveedor.RESTCorsRequestFilter.class);
    resources.add(co.movistar.planeacioncomercial.negocio.rest.proveedor.RESTCorsResponseFilter.class);
    resources.add(co.movistar.planeacioncomercial.negocio.rest.servicios.AppAdminCanalRest.class);
    resources.add(co.movistar.planeacioncomercial.negocio.rest.servicios.AppAltasRest.class);
    resources.add(co.movistar.planeacioncomercial.negocio.rest.servicios.AppArchivosRest.class);
    resources.add(co.movistar.planeacioncomercial.negocio.rest.servicios.AppArticulosRest.class);
    resources.add(co.movistar.planeacioncomercial.negocio.rest.servicios.AppBajasRest.class);
    resources.add(co.movistar.planeacioncomercial.negocio.rest.servicios.AppCalculadorIpRest.class);
    resources.add(co.movistar.planeacioncomercial.negocio.rest.servicios.AppCanalesRest.class);
    resources.add(co.movistar.planeacioncomercial.negocio.rest.servicios.AppCargosRest.class);
    resources.add(co.movistar.planeacioncomercial.negocio.rest.servicios.AppCiudadRest.class);
    resources.add(co.movistar.planeacioncomercial.negocio.rest.servicios.AppCruceVialRest.class);
    resources.add(co.movistar.planeacioncomercial.negocio.rest.servicios.AppDireccionesFoRest.class);
    resources.add(co.movistar.planeacioncomercial.negocio.rest.servicios.AppDisponibilidadFoRest.class);
    resources.add(co.movistar.planeacioncomercial.negocio.rest.servicios.AppDisponibilidadRest.class);
    resources.add(co.movistar.planeacioncomercial.negocio.rest.servicios.AppEstadosRest.class);
    resources.add(co.movistar.planeacioncomercial.negocio.rest.servicios.AppFinancieroRest.class);
    resources.add(co.movistar.planeacioncomercial.negocio.rest.servicios.AppJefaturaRest.class);
    resources.add(co.movistar.planeacioncomercial.negocio.rest.servicios.AppLiquidacionHistRest.class);
    resources.add(co.movistar.planeacioncomercial.negocio.rest.servicios.AppLlavesRest.class);
    resources.add(co.movistar.planeacioncomercial.negocio.rest.servicios.AppMenuCargoRest.class);
    resources.add(co.movistar.planeacioncomercial.negocio.rest.servicios.AppMenuRest.class);
    resources.add(co.movistar.planeacioncomercial.negocio.rest.servicios.AppMetaRest.class);
    resources.add(co.movistar.planeacioncomercial.negocio.rest.servicios.AppMetaUsuarioRest.class);
    resources.add(co.movistar.planeacioncomercial.negocio.rest.servicios.AppMetasRest.class);
    resources.add(co.movistar.planeacioncomercial.negocio.rest.servicios.AppNotificacionRest.class);
    resources.add(co.movistar.planeacioncomercial.negocio.rest.servicios.AppNovedadRest.class);
    resources.add(co.movistar.planeacioncomercial.negocio.rest.servicios.AppOficinaCeRest.class);
    resources.add(co.movistar.planeacioncomercial.negocio.rest.servicios.AppProyeccionRest.class);
    resources.add(co.movistar.planeacioncomercial.negocio.rest.servicios.AppPuntosRest.class);
    resources.add(co.movistar.planeacioncomercial.negocio.rest.servicios.AppRegionalRest.class);
    resources.add(co.movistar.planeacioncomercial.negocio.rest.servicios.AppRolRest.class);
    resources.add(co.movistar.planeacioncomercial.negocio.rest.servicios.AppSegmentacionRest.class);
    resources.add(co.movistar.planeacioncomercial.negocio.rest.servicios.AppServicioRest.class);
    resources.add(co.movistar.planeacioncomercial.negocio.rest.servicios.AppUsuarioGeografiaRest.class);
    resources.add(co.movistar.planeacioncomercial.negocio.rest.servicios.AppUsuariosRest.class);
    resources.add(com.fasterxml.jackson.jaxrs.json.JacksonJaxbJsonProvider.class);
    resources.add(com.fasterxml.jackson.jaxrs.json.JacksonJsonProvider.class);
    resources.add(org.codehaus.jackson.jaxrs.JacksonJaxbJsonProvider.class);
    resources.add(org.codehaus.jackson.jaxrs.JacksonJsonProvider.class);
    resources.add(org.codehaus.jackson.jaxrs.JsonMappingExceptionMapper.class);
    resources.add(org.codehaus.jackson.jaxrs.JsonParseExceptionMapper.class);
    resources.add(org.jboss.resteasy.core.AcceptHeaderByFileSuffixFilter.class);
    resources.add(org.jboss.resteasy.plugins.providers.SerializableProvider.class);
    resources.add(org.jboss.resteasy.plugins.providers.jackson.Jackson2JsonpInterceptor.class);
    resources.add(org.jboss.resteasy.plugins.providers.jackson.JacksonJsonpInterceptor.class);
    resources.add(org.jboss.resteasy.plugins.stats.RegistryStatsResource.class);
  }

}
