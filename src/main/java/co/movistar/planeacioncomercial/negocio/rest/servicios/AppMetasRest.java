/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package co.movistar.planeacioncomercial.negocio.rest.servicios;

import co.movistar.planeacioncomercial.modelo.conexion.ConexionBD;
import co.movistar.planeacioncomercial.modelo.vo.AppPresupuestoComercial;
import co.movistar.planeacioncomercial.modelo.vo.AppUsuarios;
import co.movistar.planeacioncomercial.negocio.delegado.AppPresupuestoComercialDelegado;
import co.movistar.planeacioncomercial.negocio.excepciones.PlaneacionComercialException;
import co.movistar.planeacioncomercial.negocio.util.TokenUtil;

import javax.ws.rs.*;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.UriInfo;
import java.sql.Connection;
import java.util.ArrayList;
import java.util.List;

/**
 * REST Web Service
 *
 * @author Lord_Nightmare
 */
@Path("metas")
public class AppMetasRest {

  @Context
  private UriInfo context;

  /**
   * Creates a new instance of MetasRest
   */
  public AppMetasRest() {
  }

  /**
   * Retrieves representation of an instance of
   * co.movistar.planeacioncomercial.negocio.rest.servicios.AppMetasRest
   *
   * @param token
   * @return an instance of
   * co.movistar.planeacioncomercial.modelo.vo.AppPresupuestoComercial
   * @throws co.movistar.planeacioncomercial.negocio.excepciones.PlaneacionComercialException
   */
  @GET
  @Path("categorias")
  @Produces(MediaType.APPLICATION_JSON)
  public List<AppPresupuestoComercial> consultarCategoriasUsuario(@HeaderParam("Authorization") String token) throws PlaneacionComercialException {
    List<AppPresupuestoComercial> listaCategorias = new ArrayList<>();
    Connection cnn = null;
    try {
      cnn = ConexionBD.conectar();
      AppUsuarios usuarioAutorizado = TokenUtil.obtenerUsuarioToken(token, cnn);
      listaCategorias = new AppPresupuestoComercialDelegado(cnn, null).consultarCategoriasUsuario(usuarioAutorizado.getIdUsuario());
    } finally {
      ConexionBD.desconectar(cnn);
    }
    return listaCategorias;
  }

  /**
   * @param token
   * @return
   * @throws PlaneacionComercialException
   */
  @GET
  @Path("periodos")
  @Produces(MediaType.APPLICATION_JSON)
  public List<AppPresupuestoComercial> consultarPeriodosUsuario(@HeaderParam("Authorization") String token) throws PlaneacionComercialException {
    List<AppPresupuestoComercial> listaPeriodos = new ArrayList<>();
    Connection cnn = null;
    try {
      cnn = ConexionBD.conectar();
      AppUsuarios usuarioAutorizado = TokenUtil.obtenerUsuarioToken(token, cnn);
      listaPeriodos = new AppPresupuestoComercialDelegado(cnn, null).consultarPeriodosUsuario(usuarioAutorizado.getIdUsuario());
    } finally {
      ConexionBD.desconectar(cnn);
    }
    return listaPeriodos;
  }

  /**
   * @param categoria
   * @param periodo
   * @param token
   * @return
   * @throws PlaneacionComercialException
   */
  @GET
  @Path("consultar/{categoria}/{periodo}")
  @Produces(MediaType.APPLICATION_JSON)
  public List<AppPresupuestoComercial> consultarMetaUsuario(@PathParam("categoria") String categoria, @PathParam("periodo") String periodo, @HeaderParam("Authorization") String token) throws PlaneacionComercialException {
    List<AppPresupuestoComercial> listaCategorias = new ArrayList<>();
    Connection cnn = null;
    try {
      cnn = ConexionBD.conectar();
      AppUsuarios usuarioAutorizado = TokenUtil.obtenerUsuarioToken(token, cnn);
      listaCategorias = new AppPresupuestoComercialDelegado(cnn, null).consultarMetaUsuario(usuarioAutorizado.getIdUsuario(), categoria, periodo);
    } finally {
      ConexionBD.desconectar(cnn);
    }
    return listaCategorias;
  }
}
