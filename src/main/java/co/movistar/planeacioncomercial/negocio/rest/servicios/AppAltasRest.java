/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package co.movistar.planeacioncomercial.negocio.rest.servicios;

import co.movistar.planeacioncomercial.modelo.conexion.ConexionBD;
import co.movistar.planeacioncomercial.modelo.dto.GeneracionAltasDTO;
import co.movistar.planeacioncomercial.modelo.dto.InformacionAltasGeneralDTO;
import co.movistar.planeacioncomercial.modelo.vo.AppAltas;
import co.movistar.planeacioncomercial.modelo.vo.AppPorcentajeCorte;
import co.movistar.planeacioncomercial.modelo.vo.AppUsuarios;
import co.movistar.planeacioncomercial.negocio.delegado.AppAltasDelegado;
import co.movistar.planeacioncomercial.negocio.delegado.AppPorcentajeCorteDelegado;
import co.movistar.planeacioncomercial.negocio.excepciones.PlaneacionComercialException;
import co.movistar.planeacioncomercial.negocio.util.TokenUtil;

import javax.ws.rs.*;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.UriInfo;
import java.sql.Connection;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * REST Web Service
 *
 * @author Lord_Nightmare
 */
@Path("altas")
public class AppAltasRest {

  @Context
  private UriInfo context;

  /**
   * Creates a new instance of AppAltasRest
   */
  public AppAltasRest() {
  }

  /**
   * Retrieves representation of an instance of
   * co.movistar.planeacioncomercial.negocio.rest.servicios.AppAltasRest
   *
   * @param tipo
   * @param idUsuario
   * @param token
   * @return an instance of
   * co.movistar.planeacioncomercial.modelo.dto.GeneracionAltasDTO
   * @throws co.movistar.planeacioncomercial.negocio.excepciones.PlaneacionComercialException
   */
  @GET
  @Path("consultar/{tipo}/{idUsuario}")
  @Produces(MediaType.APPLICATION_JSON)
  public InformacionAltasGeneralDTO consultarInformacionAltas(@PathParam("tipo") String tipo, @PathParam("idUsuario") String idUsuario, @HeaderParam("Authorization") String token) throws PlaneacionComercialException {
    InformacionAltasGeneralDTO infoAltas = new InformacionAltasGeneralDTO();
    Connection cnn = null;
    try {
      List<List<GeneracionAltasDTO>> listaAltas = new ArrayList<>();
      cnn = ConexionBD.conectar();
      AppUsuarios usuarioAutorizado = TokenUtil.obtenerUsuarioToken(token, cnn);
      Long idConsulta = Long.parseLong(idUsuario) > 0 ? Long.parseLong(idUsuario) : usuarioAutorizado.getIdUsuario();
      listaAltas.add(new AppAltasDelegado(cnn, null).consultarInformacionAltasCanal(tipo, idConsulta));
      listaAltas.add(new AppAltasDelegado(cnn, null).consultarInformacionAltasRegional(tipo, idConsulta));
      listaAltas.add(new AppAltasDelegado(cnn, null).consultarInformacionAltasSegmento(tipo, idConsulta));
      infoAltas.setListaAltas(listaAltas);
      AppPorcentajeCorte appPorcentajeCorte = new AppPorcentajeCorteDelegado(cnn, null).consultar().get(0);
      infoAltas.setPorcentajeCorte(appPorcentajeCorte);
    } finally {
      ConexionBD.desconectar(cnn);
    }
    return infoAltas;
  }

  /**
   * @param idUsuario
   * @param token
   * @return
   * @throws PlaneacionComercialException
   */
  @GET
  @Path("productos/{idUsuario}")
  @Produces(MediaType.APPLICATION_JSON)
  public List<String> listaProductosDisponibles(@PathParam("idUsuario") String idUsuario, @HeaderParam("Authorization") String token) throws PlaneacionComercialException {
    List<String> productos = new ArrayList<>();
    Connection cnn = null;
    try {
      cnn = ConexionBD.conectar();
      AppUsuarios usuarioAutorizado = TokenUtil.obtenerUsuarioToken(token, cnn);
      List<AppAltas> listaProductos = new AppAltasDelegado(cnn, null).consultarProductosPorUsuario(Long.parseLong(idUsuario) > 0 ? Long.parseLong(idUsuario) : usuarioAutorizado.getIdUsuario());
      for (int i = 0; i < listaProductos.size(); i++) {
        productos.add(listaProductos.get(i).getProducto());
      }
      Collections.sort(productos);
    } finally {
      ConexionBD.desconectar(cnn);
    }
    return productos;
  }

}
