/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package co.movistar.planeacioncomercial.negocio.rest.servicios;

import co.movistar.planeacioncomercial.modelo.conexion.ConexionBD;
import co.movistar.planeacioncomercial.modelo.vo.AppBajas;
import co.movistar.planeacioncomercial.negocio.delegado.AppBajasDelegado;
import co.movistar.planeacioncomercial.negocio.excepciones.PlaneacionComercialException;
import co.movistar.planeacioncomercial.negocio.util.TokenUtil;

import javax.ws.rs.*;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.UriInfo;
import java.sql.Connection;
import java.util.ArrayList;
import java.util.List;

/**
 * REST Web Service
 *
 * @author lord_nightmare
 */
@Path("bajas")
public class AppBajasRest {

  @Context
  private UriInfo context;

  /**
   * Creates a new instance of AppBajasRest
   */
  public AppBajasRest() {
  }

  /**
   * Retrieves representation of an instance of
   * co.movistar.planeacioncomercial.negocio.rest.servicios.AppBajasRest
   *
   * @param producto
   * @param regionalCanal
   * @param token
   * @return an instance of co.movistar.planeacioncomercial.modelo.vo.AppBajas
   * @throws co.movistar.planeacioncomercial.negocio.excepciones.PlaneacionComercialException
   */
  @GET
  @Path("consultar/{producto}/{regionalcanal}")
  @Produces(MediaType.APPLICATION_JSON)
  public List<AppBajas> consultarBajas(@PathParam("producto") String producto, @PathParam("regionalcanal") String regionalCanal, @HeaderParam("Authorization") String token) throws PlaneacionComercialException {
    List<AppBajas> listaBajas = new ArrayList<>();
    Connection cnn = null;
    try {
      cnn = ConexionBD.conectar();
      TokenUtil.obtenerUsuarioToken(token, cnn);
      listaBajas = new AppBajasDelegado(cnn, null).consultar(producto, regionalCanal);
    } finally {
      ConexionBD.desconectar(cnn);
    }
    return listaBajas;
  }

  /**
   * Retrieves representation of an instance of
   * co.movistar.planeacioncomercial.negocio.rest.servicios.AppBajasRest
   *
   * @param token
   * @return an instance of co.movistar.planeacioncomercial.modelo.vo.AppBajas
   * @throws co.movistar.planeacioncomercial.negocio.excepciones.PlaneacionComercialException
   */
  @GET
  @Path("producto")
  @Produces(MediaType.APPLICATION_JSON)
  public List<AppBajas> consultarProductos(@HeaderParam("Authorization") String token) throws PlaneacionComercialException {
    List<AppBajas> listaBajas = new ArrayList<>();
    Connection cnn = null;
    try {
      cnn = ConexionBD.conectar();
      TokenUtil.obtenerUsuarioToken(token, cnn);
      listaBajas = new AppBajasDelegado(cnn, null).consultarProductos();
    } finally {
      ConexionBD.desconectar(cnn);
    }
    return listaBajas;
  }

  /**
   * Retrieves representation of an instance of
   * co.movistar.planeacioncomercial.negocio.rest.servicios.AppBajasRest
   *
   * @param token
   * @return an instance of co.movistar.planeacioncomercial.modelo.vo.AppBajas
   * @throws co.movistar.planeacioncomercial.negocio.excepciones.PlaneacionComercialException
   */
  @GET
  @Path("regionalcanal")
  @Produces(MediaType.APPLICATION_JSON)
  public List<AppBajas> consultarRegionalCanal(@HeaderParam("Authorization") String token) throws PlaneacionComercialException {
    List<AppBajas> listaBajas = new ArrayList<>();
    Connection cnn = null;
    try {
      cnn = ConexionBD.conectar();
      TokenUtil.obtenerUsuarioToken(token, cnn);
      listaBajas = new AppBajasDelegado(cnn, null).consultarRegionalCanal();
    } finally {
      ConexionBD.desconectar(cnn);
    }
    return listaBajas;
  }
}
