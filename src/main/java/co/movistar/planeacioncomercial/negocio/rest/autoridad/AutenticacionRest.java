/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package co.movistar.planeacioncomercial.negocio.rest.autoridad;

import co.movistar.planeacioncomercial.modelo.conexion.ConexionBD;
import co.movistar.planeacioncomercial.negocio.delegado.AppUsuariosDelegado;
import co.movistar.planeacioncomercial.negocio.excepciones.PlaneacionComercialException;
import co.movistar.planeacioncomercial.negocio.util.CryptoUtil;
import co.movistar.planeacioncomercial.negocio.util.TokenUtil;

import javax.ws.rs.*;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriInfo;
import java.sql.Connection;
import java.util.Date;

/**
 * REST Web Service
 *
 * @author Manuel Ernesto Bonilla Muñoz <mebonilla9@gmail.com>
 */
@Path("autenticacion")
public class AutenticacionRest {

  @Context
  private UriInfo context;

  /**
   * Creates a new instance of AutenticacionRest
   */
  public AutenticacionRest() {
  }

  /**
   * PUT method for updating or creating an instance of AutenticacionRest
   *
   * @param correo
   * @param contrasena
   * @param macAddress
   * @return
   */
  @POST
  @Consumes(MediaType.APPLICATION_FORM_URLENCODED)
  @Produces(MediaType.TEXT_PLAIN)
  public Response autenticarUsuario(@FormParam("correo") String correo, @FormParam("contrasena") String contrasena, @FormParam("mac_address") String macAddress) {
    Connection cnn = null;
    try {
      cnn = ConexionBD.conectar();
      String tokenGenerado = "";
      if (macAddress != null) {
        tokenGenerado = TokenUtil.generarToken(
                new AppUsuariosDelegado(
                        cnn,
                        null
                ).autenticarUsuario(
                        correo,
                        CryptoUtil.cifrarContrasena(contrasena),
                        macAddress
                )
        );
      } else {
        tokenGenerado = TokenUtil.generarToken(
                new AppUsuariosDelegado(
                        cnn,
                        null
                ).autenticarUsuario(
                        correo,
                        CryptoUtil.cifrarContrasena(contrasena)
                )
        );
      }
      ConexionBD.commit(cnn);
      return Response.ok(tokenGenerado).build();
    } catch (PlaneacionComercialException ex) {
      return Response.status(Response.Status.UNAUTHORIZED).entity(ex.getMensaje()).build();
    } finally {
      ConexionBD.desconectar(cnn);
    }
  }

  @GET
  public Response obtenerFechaToken(@HeaderParam("Authorization") String token) throws PlaneacionComercialException {
    Date fechaExpiracion = TokenUtil.obtenerFechaExpiracionToken(token);
    return Response.ok(fechaExpiracion.getTime()).build();
  }
}
