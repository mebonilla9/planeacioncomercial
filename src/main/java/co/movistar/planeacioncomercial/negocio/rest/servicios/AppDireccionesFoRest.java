/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package co.movistar.planeacioncomercial.negocio.rest.servicios;

import co.movistar.planeacioncomercial.modelo.conexion.ConexionBD;
import co.movistar.planeacioncomercial.negocio.delegado.AppDireccionesFoDelegado;
import co.movistar.planeacioncomercial.negocio.excepciones.PlaneacionComercialException;
import co.movistar.planeacioncomercial.negocio.util.TokenUtil;

import javax.ws.rs.*;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriInfo;
import java.sql.Connection;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * REST Web Service
 *
 * @author lord_nightmare
 */
@Path("fibraoptica")
public class AppDireccionesFoRest {

  @Context
  private UriInfo context;

  /**
   * Creates a new instance of AppDireccionesFoRest
   */
  public AppDireccionesFoRest() {
  }

  /**
   * Retrieves representation of an instance of co.movistar.planeacioncomercial.negocio.rest.servicios.AppDireccionesFoRest
   *
   * @param token
   * @param tipo
   * @return an instance of co.movistar.planeacioncomercial.modelo.vo.AppDireccionesFo
   * @throws co.movistar.planeacioncomercial.negocio.excepciones.PlaneacionComercialException
   */
  @GET
  @Path("municipios/{tipo}")
  @Produces(MediaType.APPLICATION_JSON)
  public List<String> consultarMunicipios(@HeaderParam("Authorization") String token, @PathParam("tipo") String tipo) throws PlaneacionComercialException {
    List<String> municipios = new ArrayList<>();
    Connection cnn = null;
    try {
      cnn = ConexionBD.conectar();
      TokenUtil.obtenerUsuarioToken(token, cnn);
      municipios = new AppDireccionesFoDelegado(cnn, null).consultarMunicipios(tipo);
      Collections.sort(municipios);
    } finally {
      ConexionBD.desconectar(cnn);
    }
    return municipios;
  }

  /**
   * @param token
   * @param municipio
   * @param tipo
   * @return
   * @throws PlaneacionComercialException
   */
  @GET
  @Path("conjuntos/{municipio}/{tipo}")
  @Produces(MediaType.APPLICATION_JSON)
  public List<String> consultarConjuntos(@HeaderParam("Authorization") String token, @PathParam("municipio") String municipio, @PathParam("tipo") String tipo) throws PlaneacionComercialException {
    List<String> conjuntos = new ArrayList<>();
    Connection cnn = null;
    try {
      cnn = ConexionBD.conectar();
      TokenUtil.obtenerUsuarioToken(token, cnn);
      conjuntos = new AppDireccionesFoDelegado(cnn, null).consultarConjunto(municipio, tipo);
      Collections.sort(conjuntos);
    } finally {
      ConexionBD.desconectar(cnn);
    }
    return conjuntos;
  }

  /**
   * @param token
   * @param conjunto
   * @param tipo
   * @return
   * @throws PlaneacionComercialException
   */
  @GET
  @Path("complementos/{conjunto}/{tipo}")
  @Produces(MediaType.APPLICATION_JSON)
  public List<String> consultarComplementos(@HeaderParam("Authorization") String token, @PathParam("conjunto") String conjunto, @PathParam("tipo") String tipo) throws PlaneacionComercialException {
    List<String> complementos = new ArrayList<>();
    Connection cnn = null;
    try {
      cnn = ConexionBD.conectar();
      TokenUtil.obtenerUsuarioToken(token, cnn);
      complementos = new AppDireccionesFoDelegado(cnn, null).consultarComplemento(conjunto, tipo);
      Collections.sort(complementos);
    } finally {
      ConexionBD.desconectar(cnn);
    }
    return complementos;
  }

  @GET
  @Path("direccioncomp/{conjunto}/{tipo}")
  @Produces(MediaType.APPLICATION_JSON)
  public List<String> consultarDireccionConjunto(@HeaderParam("Authorization") String token, @PathParam("conjunto") String conjunto, @PathParam("tipo") String tipo) throws PlaneacionComercialException {
    List<String> direcciones = new ArrayList<>();
    Connection cnn = null;
    try {
      cnn = ConexionBD.conectar();
      TokenUtil.obtenerUsuarioToken(token, cnn);
      direcciones = new AppDireccionesFoDelegado(cnn, null).consultarDireccionConjunto(conjunto, tipo);
      Collections.sort(direcciones);
    } finally {
      ConexionBD.desconectar(cnn);
    }
    return direcciones;
  }

  @GET
  @Path("direccion/{complemento}/{tipo}")
  @Produces(MediaType.APPLICATION_JSON)
  public Response consultarDireccionComplemento(@HeaderParam("Authorization") String token, @PathParam("complemento") String complemento, @PathParam("tipo") String tipo) throws PlaneacionComercialException {
    String direccion = "";
    Connection cnn = null;
    try {
      cnn = ConexionBD.conectar();
      TokenUtil.obtenerUsuarioToken(token, cnn);
      direccion = new AppDireccionesFoDelegado(cnn, null).consultarDireccionComplemento(complemento, tipo);
    } finally {
      ConexionBD.desconectar(cnn);
    }
    return Response.ok(direccion, MediaType.TEXT_PLAIN).build();
  }
}
