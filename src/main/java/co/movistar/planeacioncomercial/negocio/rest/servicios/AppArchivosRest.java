/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package co.movistar.planeacioncomercial.negocio.rest.servicios;

import co.movistar.planeacioncomercial.modelo.conexion.ConexionBD;
import co.movistar.planeacioncomercial.modelo.vo.AppArchivos;
import co.movistar.planeacioncomercial.negocio.delegado.AppArchivosDelegado;
import co.movistar.planeacioncomercial.negocio.excepciones.PlaneacionComercialException;
import co.movistar.planeacioncomercial.negocio.util.TokenUtil;

import javax.servlet.ServletContext;
import javax.ws.rs.*;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.UriInfo;
import java.sql.Connection;
import java.util.ArrayList;
import java.util.List;

/**
 * REST Web Service
 *
 * @author Lord_Nightmare
 */
@Path("archivos")
public class AppArchivosRest {

  @Context
  private UriInfo context;

  @Context
  private ServletContext servletContext;

  /**
   * Creates a new instance of AppArchivosRest
   */
  public AppArchivosRest() {
  }

  /**
   * @param modulo
   * @param token
   * @return
   * @throws PlaneacionComercialException
   */
  @GET
  @Path("consultar/{modulo}")
  @Produces(MediaType.APPLICATION_JSON)
  public List<String> consultarCategoriasModulo(@PathParam("modulo") String modulo, @HeaderParam("Authorization") String token) throws PlaneacionComercialException {
    List<String> listaCategorias = new ArrayList<>();
    Connection cnn = null;
    try {
      System.out.println("ruta server: " + servletContext.getRealPath("/"));
      cnn = ConexionBD.conectar();
      TokenUtil.obtenerUsuarioToken(token, cnn);
      listaCategorias = new AppArchivosDelegado(cnn, null).consultarCategoriasModulo(modulo);
    } finally {
      ConexionBD.desconectar(cnn);
    }
    return listaCategorias;
  }

  /**
   * Retrieves representation of an instance of
   * co.movistar.planeacioncomercial.negocio.rest.servicios.AppArchivosRest
   *
   * @param modulo
   * @param categoria
   * @param token
   * @return an instance of
   * co.movistar.planeacioncomercial.modelo.vo.AppArchivos
   * @throws co.movistar.planeacioncomercial.negocio.excepciones.PlaneacionComercialException
   */
  @GET
  @Path("consultar/{modulo}/{categoria}")
  @Produces(MediaType.APPLICATION_JSON)
  public List<AppArchivos> consultarArchivosModuloCategoria(@PathParam("modulo") String modulo, @PathParam("categoria") String categoria, @HeaderParam("Authorization") String token) throws PlaneacionComercialException {
    List<AppArchivos> listaArchivos = new ArrayList<>();
    Connection cnn = null;
    try {
      cnn = ConexionBD.conectar();
      TokenUtil.obtenerUsuarioToken(token, cnn);
      listaArchivos = new AppArchivosDelegado(cnn, null).consultarArchivosModuloCategoria(modulo, categoria);
    } finally {
      ConexionBD.desconectar(cnn);
    }
    return listaArchivos;
  }
}
