/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package co.movistar.planeacioncomercial.negocio.rest.servicios;

import co.movistar.planeacioncomercial.modelo.conexion.ConexionBD;
import co.movistar.planeacioncomercial.modelo.dto.InformacionComisionesDTO;
import co.movistar.planeacioncomercial.modelo.vo.AppCostoxAlta;
import co.movistar.planeacioncomercial.modelo.vo.AppUsuarios;
import co.movistar.planeacioncomercial.negocio.delegado.AppCostoxAltaDelegado;
import co.movistar.planeacioncomercial.negocio.excepciones.PlaneacionComercialException;
import co.movistar.planeacioncomercial.negocio.util.TokenUtil;

import javax.ws.rs.*;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.UriInfo;
import java.sql.Connection;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * REST Web Service
 *
 * @author lord_nightmare
 */
@Path("financiero")
public class AppFinancieroRest {

  @Context
  private UriInfo context;

  /**
   * Creates a new instance of AppFinancieroRest
   */
  public AppFinancieroRest() {
  }

  @GET
  @Path("periodo")
  @Produces(MediaType.APPLICATION_JSON)
  public List<String> consultarPeriodos(@HeaderParam("Authorization") String token) throws PlaneacionComercialException {
    List<String> periodos = new ArrayList<>();
    Connection cnn = null;
    try {
      cnn = ConexionBD.conectar();
      AppUsuarios usuarioAutorizado = TokenUtil.obtenerUsuarioToken(token, cnn);
      List<AppCostoxAlta> listaPeriodos = new AppCostoxAltaDelegado(cnn, null).consultarPeriodos(usuarioAutorizado.getIdUsuario());
      for (int i = 0; i < listaPeriodos.size(); i++) {
        periodos.add(listaPeriodos.get(i).getPeriodo());
      }
    } finally {
      ConexionBD.desconectar(cnn);
    }
    return periodos;
  }

  @GET
  @Path("productos/{periodo}")
  @Produces(MediaType.APPLICATION_JSON)
  public List<String> listaProductosDisponibles(@PathParam("periodo") String periodo, @HeaderParam("Authorization") String token) throws PlaneacionComercialException {
    List<String> productos = new ArrayList<>();
    Connection cnn = null;
    try {
      cnn = ConexionBD.conectar();
      AppUsuarios usuarioAutorizado = TokenUtil.obtenerUsuarioToken(token, cnn);
      List<AppCostoxAlta> listaProductos = new AppCostoxAltaDelegado(cnn, null).consultarCostoxAltaProducto(usuarioAutorizado.getIdUsuario(), periodo);
      for (int i = 0; i < listaProductos.size(); i++) {
        productos.add(listaProductos.get(i).getProducto());
      }
      Collections.sort(productos);
    } finally {
      ConexionBD.desconectar(cnn);
    }
    return productos;
  }

  @GET
  @Path("consultar/{producto}/{periodo}")
  @Produces(MediaType.APPLICATION_JSON)
  public InformacionComisionesDTO listaCostoxAltaProductos(@PathParam("periodo") String periodo, @PathParam("producto") String producto, @HeaderParam("Authorization") String token) throws PlaneacionComercialException {
    InformacionComisionesDTO comisionesDto = new InformacionComisionesDTO();
    comisionesDto.setListaComisiones(new ArrayList<>());
    Connection cnn = null;
    try {
      cnn = ConexionBD.conectar();
      AppUsuarios usuarioAutorizado = TokenUtil.obtenerUsuarioToken(token, cnn);
      AppCostoxAltaDelegado appCostoxAltaDelegado = new AppCostoxAltaDelegado(cnn, null);
      List<AppCostoxAlta> listaCanales = appCostoxAltaDelegado.consultarCanalesDisponibles(usuarioAutorizado.getIdUsuario(), periodo, producto);
      for (int i = 0; i < listaCanales.size(); i++) {
        AppCostoxAlta canal = listaCanales.get(i);
        comisionesDto.getListaComisiones().add(appCostoxAltaDelegado.consultarCostoxAltaCanalUsuario(usuarioAutorizado.getIdUsuario(), producto, periodo, canal.getCanal()));
      }
    } finally {
      ConexionBD.desconectar(cnn);
    }
    return comisionesDto;
  }
}
