/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package co.movistar.planeacioncomercial.negocio.rest.servicios;

import co.movistar.planeacioncomercial.modelo.conexion.ConexionBD;
import co.movistar.planeacioncomercial.modelo.vo.AppCruceVial;
import co.movistar.planeacioncomercial.negocio.delegado.AppCruceVialDelegado;
import co.movistar.planeacioncomercial.negocio.excepciones.PlaneacionComercialException;
import co.movistar.planeacioncomercial.negocio.util.TokenUtil;

import javax.ws.rs.GET;
import javax.ws.rs.HeaderParam;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.UriInfo;
import java.sql.Connection;
import java.util.List;

/**
 * REST Web Service
 *
 * @author lord_nightmare
 */
@Path("crucevial")
public class AppCruceVialRest {

  @Context
  private UriInfo context;

  /**
   * Creates a new instance of AppCruceVialRest
   */
  public AppCruceVialRest() {
  }

  /**
   * Retrieves representation of an instance of
   * co.movistar.planeacioncomercial.negocio.rest.servicios.AppCruceVialRest
   *
   * @param token
   * @return an instance of
   * co.movistar.planeacioncomercial.modelo.vo.AppCruceVial
   * @throws co.movistar.planeacioncomercial.negocio.excepciones.PlaneacionComercialException
   */
  @GET
  @Path("consultar")
  @Produces(MediaType.APPLICATION_JSON)
  public List<AppCruceVial> consultarCruceVial(@HeaderParam("Authorization") String token) throws PlaneacionComercialException {
    List<AppCruceVial> listaCruceVial = null;
    Connection cnn = null;
    try {
      cnn = ConexionBD.conectar();
      TokenUtil.obtenerUsuarioToken(token, cnn);
      listaCruceVial = new AppCruceVialDelegado(cnn, null).consultar();
      ConexionBD.commit(cnn);
    } finally {
      ConexionBD.desconectar(cnn);
    }
    return listaCruceVial;
  }
}
