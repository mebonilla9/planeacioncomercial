/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package co.movistar.planeacioncomercial.negocio.rest.servicios;

import co.movistar.planeacioncomercial.modelo.conexion.ConexionBD;
import co.movistar.planeacioncomercial.modelo.vo.AppLiquidacionHist;
import co.movistar.planeacioncomercial.modelo.vo.AppUsuarios;
import co.movistar.planeacioncomercial.negocio.delegado.AppLiquidacionHistDelegado;
import co.movistar.planeacioncomercial.negocio.excepciones.PlaneacionComercialException;
import co.movistar.planeacioncomercial.negocio.util.TokenUtil;

import javax.ws.rs.*;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.UriInfo;
import java.sql.Connection;
import java.util.ArrayList;
import java.util.List;

/**
 * REST Web Service
 *
 * @author Lord_Nightmare
 */
@Path("liquidacion")
public class AppLiquidacionHistRest {

  @Context
  private UriInfo context;

  /**
   * Creates a new instance of AppLiquidacionHistRest
   */
  public AppLiquidacionHistRest() {
  }

  /**
   * Retrieves representation of an instance of
   * co.movistar.planeacioncomercial.negocio.rest.servicios.AppLiquidacionHistRest
   *
   * @param periodo
   * @param token
   * @return an instance of
   * co.movistar.planeacioncomercial.modelo.vo.AppLiquidacionHist
   * @throws co.movistar.planeacioncomercial.negocio.excepciones.PlaneacionComercialException
   */
  @GET
  @Path("/resumen/{periodo}")
  @Produces(MediaType.APPLICATION_JSON)
  public AppLiquidacionHist obtenerResumenLiquidacion(@PathParam("periodo") String periodo, @HeaderParam("Authorization") String token) throws PlaneacionComercialException {
    AppLiquidacionHist liquidacionHist = new AppLiquidacionHist();
    Connection cnn = null;
    try {
      cnn = ConexionBD.conectar();
      AppUsuarios usuarioAutorizado = TokenUtil.obtenerUsuarioToken(token, cnn);
      liquidacionHist = new AppLiquidacionHistDelegado(cnn, null).obtenerResumenLiquidacion(usuarioAutorizado.getIdUsuario(), periodo);
    } finally {
      ConexionBD.desconectar(cnn);
    }
    return liquidacionHist;
  }

  /**
   * @param token
   * @return
   * @throws PlaneacionComercialException
   */
  @GET
  @Path("/categoria")
  @Produces(MediaType.APPLICATION_JSON)
  public List<AppLiquidacionHist> consultarCategoria(@HeaderParam("Authorization") String token) throws PlaneacionComercialException {
    List<AppLiquidacionHist> listaCategorias = new ArrayList<>();
    Connection cnn = null;
    try {
      cnn = ConexionBD.conectar();
      AppUsuarios usuarioAutorizado = TokenUtil.obtenerUsuarioToken(token, cnn);
      listaCategorias = new AppLiquidacionHistDelegado(cnn, null).consultarCategoria(usuarioAutorizado.getIdUsuario());
    } finally {
      ConexionBD.desconectar(cnn);
    }
    return listaCategorias;
  }

  @GET
  @Path("/periodo")
  @Produces(MediaType.APPLICATION_JSON)
  public List<AppLiquidacionHist> consultarPeriodos(@HeaderParam("Authorization") String token) throws PlaneacionComercialException {
    List<AppLiquidacionHist> listaPeriodos = new ArrayList<>();
    Connection cnn = null;
    try {
      cnn = ConexionBD.conectar();
      AppUsuarios usuarioAutorizado = TokenUtil.obtenerUsuarioToken(token, cnn);
      listaPeriodos = new AppLiquidacionHistDelegado(cnn, null).consultarPeriodos(usuarioAutorizado.getIdUsuario());
    } finally {
      ConexionBD.desconectar(cnn);
    }
    return listaPeriodos;
  }

  /**
   * @param token
   * @param categoria
   * @param periodo
   * @return
   * @throws PlaneacionComercialException
   */
  @GET
  @Path("/detalle/{categoria}/{periodo}")
  @Produces(MediaType.APPLICATION_JSON)
  public List<AppLiquidacionHist> consultarDetalleLiquidacion(@HeaderParam("Authorization") String token, @PathParam("categoria") String categoria, @PathParam("periodo") String periodo) throws PlaneacionComercialException {
    List<AppLiquidacionHist> listaDetalles = new ArrayList<>();
    Connection cnn = null;
    try {
      cnn = ConexionBD.conectar();
      AppUsuarios usuarioAutorizado = TokenUtil.obtenerUsuarioToken(token, cnn);
      listaDetalles = new AppLiquidacionHistDelegado(cnn, null).consultarDetalleLiquidacion(usuarioAutorizado.getIdUsuario(), categoria, periodo);
    } finally {
      ConexionBD.desconectar(cnn);
    }
    return listaDetalles;
  }
}
