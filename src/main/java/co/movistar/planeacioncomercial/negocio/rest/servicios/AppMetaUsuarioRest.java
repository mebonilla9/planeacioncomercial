/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package co.movistar.planeacioncomercial.negocio.rest.servicios;

import co.movistar.planeacioncomercial.modelo.conexion.ConexionBD;
import co.movistar.planeacioncomercial.modelo.dto.InfoMetaDTO;
import co.movistar.planeacioncomercial.modelo.vo.AppUsuarios;
import co.movistar.planeacioncomercial.negocio.delegado.AppMetaUsuarioDelegado;
import co.movistar.planeacioncomercial.negocio.excepciones.PlaneacionComercialException;
import co.movistar.planeacioncomercial.negocio.util.TokenUtil;

import javax.ws.rs.*;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriInfo;
import java.sql.Connection;
import java.util.ArrayList;
import java.util.List;

/**
 * REST Web Service
 *
 * @author Lord_Nightmare
 */
@Path("metausuario")
public class AppMetaUsuarioRest {

  @Context
  private UriInfo context;

  /**
   * Creates a new instance of AppMetaUsuarioRest
   */
  public AppMetaUsuarioRest() {
  }

  /**
   * Retrieves representation of an instance of co.movistar.planeacioncomercial.negocio.rest.servicios.AppMetaUsuarioRest
   *
   * @param token
   * @return
   * @throws PlaneacionComercialException
   */
  @GET
  @Path("consultar")
  @Produces(MediaType.APPLICATION_JSON)
  public List<InfoMetaDTO> consultarMetasSubordinados(@HeaderParam("Authorization") String token) throws PlaneacionComercialException {
    Connection cnn = null;
    List<InfoMetaDTO> listaMetas = new ArrayList<>();
    try {
      cnn = ConexionBD.conectar();
      AppUsuarios usuarioAutorizado = TokenUtil.obtenerUsuarioToken(token, cnn);
      listaMetas = new AppMetaUsuarioDelegado(cnn, null).consultarInfoCoordinador(usuarioAutorizado);
    } finally {
      ConexionBD.desconectar(cnn);
    }
    return listaMetas;
  }

  @POST
  @Path("cargar")
  @Consumes(MediaType.APPLICATION_JSON)
  public Response cargarMetasSubordinados(@HeaderParam("Authorization") String token, List<InfoMetaDTO> content) throws PlaneacionComercialException {
    Connection cnn = null;
    try {
      cnn = ConexionBD.conectar();
      TokenUtil.obtenerUsuarioToken(token, cnn);
      new AppMetaUsuarioDelegado(cnn, null).cargarMetas(content);
    } finally {
      ConexionBD.desconectar(cnn);
    }
    return Response.ok("Información de metas cargada correctamente").build();
  }
}
