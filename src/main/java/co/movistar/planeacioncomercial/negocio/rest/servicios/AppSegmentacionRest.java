/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package co.movistar.planeacioncomercial.negocio.rest.servicios;

import co.movistar.planeacioncomercial.modelo.conexion.ConexionBD;
import co.movistar.planeacioncomercial.modelo.vo.AppConsultaSegmento;
import co.movistar.planeacioncomercial.modelo.vo.AppSegmentacion;
import co.movistar.planeacioncomercial.modelo.vo.AppUsuarios;
import co.movistar.planeacioncomercial.negocio.delegado.AppConsultaSegmentoDelegado;
import co.movistar.planeacioncomercial.negocio.delegado.AppSegmentacionDelegado;
import co.movistar.planeacioncomercial.negocio.excepciones.PlaneacionComercialException;
import co.movistar.planeacioncomercial.negocio.util.DateUtil;
import co.movistar.planeacioncomercial.negocio.util.TokenUtil;

import javax.ws.rs.*;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.UriInfo;
import java.sql.Connection;

/**
 * REST Web Service
 *
 * @author lord_nightmare
 */
@Path("consultanit")
public class AppSegmentacionRest {

  @Context
  private UriInfo context;

  /**
   * Creates a new instance of AppSegmentacionRest
   */
  public AppSegmentacionRest() {
  }

  /**
   * Retrieves representation of an instance of
   * co.movistar.planeacioncomercial.negocio.rest.servicios.AppSegmentacionRest
   *
   * @param nit
   * @param idAgente
   * @param token
   * @return an instance of
   * co.movistar.planeacioncomercial.modelo.vo.AppSegmentacion
   * @throws co.movistar.planeacioncomercial.negocio.excepciones.PlaneacionComercialException
   */
  @GET
  @Path("consultar/{nit}/{idAgente}")
  @Produces(MediaType.APPLICATION_JSON)
  public AppSegmentacion consultaInformacion(@PathParam("nit") String nit, @PathParam("idAgente") String idAgente, @HeaderParam("Authorization") String token) throws PlaneacionComercialException {
    AppSegmentacion segmento = new AppSegmentacion();
    Connection cnn = null;
    try {
      cnn = ConexionBD.conectar();
      AppUsuarios usuarioAutorizado = TokenUtil.obtenerUsuarioToken(token, cnn);
      AppConsultaSegmento acs = new AppConsultaSegmento();
      acs.setIdUsuario(usuarioAutorizado.getIdUsuario());
      acs.setIdAgente(Long.parseLong(idAgente));
      acs.setNit(nit);
      acs.setFecha(DateUtil.obtenerFechaActualTexto());
      new AppConsultaSegmentoDelegado(cnn, null).insertar(acs);
      ConexionBD.commit(cnn);
      segmento = new AppSegmentacionDelegado(cnn, null).consultarInformacionSegmento(nit);
    } finally {
      ConexionBD.desconectar(cnn);
    }
    return segmento;
  }
}
