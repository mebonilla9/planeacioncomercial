/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package co.movistar.planeacioncomercial.negocio.rest.servicios;

import co.movistar.planeacioncomercial.modelo.conexion.ConexionBD;
import co.movistar.planeacioncomercial.modelo.vo.AppNotificacion;
import co.movistar.planeacioncomercial.modelo.vo.AppUsuarios;
import co.movistar.planeacioncomercial.negocio.delegado.AppNotificacionDelegado;
import co.movistar.planeacioncomercial.negocio.excepciones.PlaneacionComercialException;
import co.movistar.planeacioncomercial.negocio.util.TokenUtil;

import javax.ws.rs.GET;
import javax.ws.rs.HeaderParam;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.UriInfo;
import java.sql.Connection;

/**
 * REST Web Service
 *
 * @author lord_nightmare
 */
@Path("notificacion")
public class AppNotificacionRest {

  @Context
  private UriInfo context;

  /**
   * Creates a new instance of AppNotificacionResource
   */
  public AppNotificacionRest() {
  }

  /**
   * @param token
   * @return
   * @throws PlaneacionComercialException
   */
  @GET
  @Path("consultar")
  @Produces(MediaType.APPLICATION_JSON)
  public AppNotificacion consultarUltimaNotificacion(@HeaderParam("Authorization") String token) throws PlaneacionComercialException {
    AppNotificacion ultimaNotificacion = new AppNotificacion();
    Connection cnn = null;
    try {
      cnn = ConexionBD.conectar();
      AppUsuarios usuarioAutorizado = TokenUtil.obtenerUsuarioToken(token, cnn);
      AppNotificacionDelegado appNotificacionDelegado = new AppNotificacionDelegado(cnn, null);
      ultimaNotificacion = appNotificacionDelegado.obtenerUltimaNotificacion(usuarioAutorizado.getCargo().getIdCargo());
      if (ultimaNotificacion == null) {
        ultimaNotificacion = appNotificacionDelegado.obtenerUltimaNotificacion();
      }
    } finally {
      ConexionBD.desconectar(cnn);
    }
    return ultimaNotificacion;
  }
}
