/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package co.movistar.planeacioncomercial.negocio.rest.servicios;

import co.movistar.planeacioncomercial.modelo.conexion.ConexionBD;
import co.movistar.planeacioncomercial.modelo.dto.InformacionUsuarioDTO;
import co.movistar.planeacioncomercial.modelo.vo.AppUsuarios;
import co.movistar.planeacioncomercial.negocio.delegado.AppCargoDelegado;
import co.movistar.planeacioncomercial.negocio.delegado.AppMenuDelegado;
import co.movistar.planeacioncomercial.negocio.excepciones.PlaneacionComercialException;
import co.movistar.planeacioncomercial.negocio.util.TokenUtil;

import javax.ws.rs.GET;
import javax.ws.rs.HeaderParam;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.UriInfo;
import java.sql.Connection;

/**
 * REST Web Service
 *
 * @author Manuel Ernesto Bonilla Muñoz <mebonilla9@gmail.com>
 */
@Path("menu")
public class AppMenuRest {

  @Context
  private UriInfo context;

  /**
   * Creates a new instance of AppMenuRest
   */
  public AppMenuRest() {
  }

  /**
   * @param token
   * @return
   * @throws PlaneacionComercialException
   */
  @GET
  @Path("consultar")
  @Produces(MediaType.APPLICATION_JSON)
  public InformacionUsuarioDTO consultarMenu(@HeaderParam("Authorization") String token) throws PlaneacionComercialException {
    InformacionUsuarioDTO infoUsuario = new InformacionUsuarioDTO();
    Connection cnn = null;
    try {
      cnn = ConexionBD.conectar();
      AppUsuarios usuarioAutorizado = TokenUtil.obtenerUsuarioToken(token, cnn);
      infoUsuario.setMenu(new AppMenuDelegado(cnn, null).consultarMenuUsuario(usuarioAutorizado.getIdUsuario()));
      usuarioAutorizado.setCargo(new AppCargoDelegado(cnn, null).consultar(usuarioAutorizado.getCargo().getIdCargo()));
      infoUsuario.setUsuario(usuarioAutorizado);
    } finally {
      ConexionBD.desconectar(cnn);
    }
    return infoUsuario;
  }
}
