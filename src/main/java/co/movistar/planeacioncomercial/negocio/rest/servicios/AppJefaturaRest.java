/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package co.movistar.planeacioncomercial.negocio.rest.servicios;

import co.movistar.planeacioncomercial.modelo.conexion.ConexionBD;
import co.movistar.planeacioncomercial.modelo.vo.AppJefatura;
import co.movistar.planeacioncomercial.negocio.constantes.EMensajes;
import co.movistar.planeacioncomercial.negocio.delegado.AppJefaturaDelegado;
import co.movistar.planeacioncomercial.negocio.excepciones.PlaneacionComercialException;
import co.movistar.planeacioncomercial.negocio.util.TokenUtil;

import javax.ws.rs.*;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriInfo;
import java.sql.Connection;
import java.util.List;

/**
 * REST Web Service
 *
 * @author Manuel Ernesto Bonilla Muñoz <mebonilla9@gmail.com>
 */
@Path("jefatura")
public class AppJefaturaRest {

  @Context
  private UriInfo context;

  /**
   * Creates a new instance of AppJefaturaRest
   */
  public AppJefaturaRest() {
  }

  /**
   * PUT method for updating or creating an instance of AppCanalesRest
   *
   * @param content representation for the resource
   * @param token
   * @return
   */
  @PUT
  @Path("insertar")
  @Consumes(MediaType.APPLICATION_JSON)
  public Response insertarJefaturas(AppJefatura content, @HeaderParam("Authorization") String token) {
    Connection cnn = null;
    try {
      TokenUtil.obtenerUsuarioToken(token, cnn);
      new AppJefaturaDelegado(cnn, null).insertar(content);
    } catch (PlaneacionComercialException ex) {
      return Response.status(Response.Status.BAD_REQUEST).entity(ex.getMensaje()).build();
    } finally {
      ConexionBD.desconectar(cnn);
    }
    return Response.ok(EMensajes.INSERTO).build();
  }

  /**
   * PUT method for updating or creating an instance of AppCanalesRest
   *
   * @param content representation for the resource
   * @param token
   * @return
   */
  @PUT
  @Path("modificar")
  @Consumes(MediaType.APPLICATION_JSON)
  public Response actualizarJefaturas(AppJefatura content, @HeaderParam("Authorization") String token) {
    Connection cnn = null;
    try {
      TokenUtil.obtenerUsuarioToken(token, cnn);
      new AppJefaturaDelegado(cnn, null).editar(content);
    } catch (PlaneacionComercialException ex) {
      return Response.status(Response.Status.BAD_REQUEST).entity(ex.getMensaje()).build();
    } finally {
      ConexionBD.desconectar(cnn);
    }
    return Response.ok(EMensajes.INSERTO).build();
  }

  /**
   * Retrieves representation of an instance of co.movistar.planeacioncomercial.negocio.rest.servicios.AppCanalesRest
   *
   * @param token
   * @return an instance of AppCanales
   * @throws co.movistar.planeacioncomercial.negocio.excepciones.PlaneacionComercialException
   */
  @GET
  @Path("consultar")
  @Produces(MediaType.APPLICATION_JSON)
  public List<AppJefatura> consultarJefaturas(@HeaderParam("Authorization") String token) throws PlaneacionComercialException {
    List<AppJefatura> listaMenus = null;
    Connection cnn = null;
    try {
      TokenUtil.obtenerUsuarioToken(token, cnn);
      listaMenus = new AppJefaturaDelegado(cnn, null).consultar();
    } finally {
      ConexionBD.desconectar(cnn);
    }
    return listaMenus;
  }

  /**
   * Retrieves representation of an instance of co.movistar.planeacioncomercial.negocio.rest.servicios.AppCanalesRest
   *
   * @param id
   * @param token
   * @return an instance of AppCanales
   * @throws co.movistar.planeacioncomercial.negocio.excepciones.PlaneacionComercialException
   */
  @GET
  @Path("consultar/{id}")
  @Produces(MediaType.APPLICATION_JSON)
  public AppJefatura consultarJefaturas(@PathParam("id") long id, @HeaderParam("Authorization") String token) throws PlaneacionComercialException {
    AppJefatura jefatura = null;
    Connection cnn = null;
    try {
      TokenUtil.obtenerUsuarioToken(token, cnn);
      jefatura = new AppJefaturaDelegado(cnn, null).consultar(id);
    } finally {
      ConexionBD.desconectar(cnn);
    }
    return jefatura;
  }
}
