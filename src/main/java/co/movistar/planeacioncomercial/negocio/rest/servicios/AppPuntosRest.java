/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package co.movistar.planeacioncomercial.negocio.rest.servicios;

import co.movistar.planeacioncomercial.modelo.conexion.ConexionBD;
import co.movistar.planeacioncomercial.modelo.vo.AppPuntos;
import co.movistar.planeacioncomercial.modelo.vo.AppUsuarios;
import co.movistar.planeacioncomercial.negocio.delegado.AppPuntosDelegado;
import co.movistar.planeacioncomercial.negocio.delegado.AppUsuariosDelegado;
import co.movistar.planeacioncomercial.negocio.excepciones.PlaneacionComercialException;
import co.movistar.planeacioncomercial.negocio.util.TokenUtil;

import javax.ws.rs.*;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.UriInfo;
import java.sql.Connection;
import java.util.ArrayList;
import java.util.List;

/**
 * REST Web Service
 *
 * @author lord_nightmare
 */
@Path("puntos")
public class AppPuntosRest {

  @Context
  private UriInfo context;

  /**
   * Creates a new instance of AppPuntosRest
   */
  public AppPuntosRest() {
  }

  /**
   * Retrieves representation of an instance of
   * co.movistar.planeacioncomercial.negocio.rest.servicios.AppPuntosRest
   *
   * @param token
   * @param id
   * @return an instance of
   * co.movistar.planeacioncomercial.modelo.vo.AppPuntos
   */
  @GET
  @Path("consultar/{id}")
  @Produces(MediaType.APPLICATION_JSON)
  public List<AppPuntos> obtenerPuntosPorUsuario(@HeaderParam("Authorization") String token, @PathParam("id") String id) throws PlaneacionComercialException {
    List<AppPuntos> puntos = new ArrayList<>();
    Connection cnn = null;
    try {
      cnn = ConexionBD.conectar();
      TokenUtil.obtenerUsuarioToken(token, cnn);
      puntos = new AppPuntosDelegado(cnn, null).consultarPuntosUsuario(Long.parseLong(id));
    } finally {
      ConexionBD.desconectar(cnn);
    }
    return puntos;
  }

  @GET
  @Path("subordinados")
  @Produces(MediaType.APPLICATION_JSON)
  public List<AppUsuarios> consultarSubordinados(@HeaderParam("Authorization") String token) throws PlaneacionComercialException {
    List<AppUsuarios> listaSubordinados = new ArrayList<>();
    Connection cnn = null;
    try {
      cnn = ConexionBD.conectar();
      AppUsuarios usuario = TokenUtil.obtenerUsuarioToken(token, cnn);
      listaSubordinados = new AppUsuariosDelegado(cnn, null).consultarSubordinadosPuntos(usuario.getIdUsuario());
    } finally {
      ConexionBD.desconectar(cnn);
    }
    return listaSubordinados;
  }

  /**
   * Retrieves representation of an instance of
   * co.movistar.planeacioncomercial.negocio.rest.servicios.AppPuntosRest
   *
   * @param token
   * @return an instance of
   * co.movistar.planeacioncomercial.modelo.vo.AppPuntos
   */
  @GET
  @Path("consultar")
  @Produces(MediaType.APPLICATION_JSON)
  public List<AppPuntos> obtenerPuntosCoordinador(@HeaderParam("Authorization") String token) throws PlaneacionComercialException {
    List<AppPuntos> puntos = new ArrayList<>();
    Connection cnn = null;
    try {
      cnn = ConexionBD.conectar();
      AppUsuarios usuario = TokenUtil.obtenerUsuarioToken(token, cnn);
      puntos = new AppPuntosDelegado(cnn, null).consultarPuntosNovedad(usuario.getIdUsuario());
    } finally {
      ConexionBD.desconectar(cnn);
    }
    return puntos;
  }
}
