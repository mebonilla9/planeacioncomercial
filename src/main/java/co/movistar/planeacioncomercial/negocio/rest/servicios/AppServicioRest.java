/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package co.movistar.planeacioncomercial.negocio.rest.servicios;

import co.movistar.planeacioncomercial.modelo.conexion.ConexionBD;
import co.movistar.planeacioncomercial.modelo.vo.AppServicio;
import co.movistar.planeacioncomercial.modelo.vo.AppUsuarios;
import co.movistar.planeacioncomercial.negocio.delegado.AppServicioDelegado;
import co.movistar.planeacioncomercial.negocio.excepciones.PlaneacionComercialException;
import co.movistar.planeacioncomercial.negocio.util.TokenUtil;

import javax.ws.rs.GET;
import javax.ws.rs.HeaderParam;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.UriInfo;
import java.sql.Connection;
import java.util.ArrayList;
import java.util.List;

/**
 * REST Web Service
 *
 * @author lord_nightmare
 */
@Path("servicio")
public class AppServicioRest {

  @Context
  private UriInfo context;

  /**
   * Creates a new instance of AppServicioRest
   */
  public AppServicioRest() {
  }

  /**
   * Retrieves representation of an instance of
   * co.movistar.planeacioncomercial.negocio.rest.servicios.AppServicioRest
   *
   * @param authToken
   * @return an instance of
   * co.movistar.planeacioncomercial.modelo.vo.AppServicio
   * @throws co.movistar.planeacioncomercial.negocio.excepciones.PlaneacionComercialException
   */
  @GET
  @Path("consultar")
  @Produces(MediaType.APPLICATION_JSON)
  public List<AppServicio> consultarServicios(@HeaderParam("Authorization") String authToken) throws PlaneacionComercialException {
    List<AppServicio> listaServicio = new ArrayList<>();
    Connection cnn = null;
    try {
      cnn = ConexionBD.conectar();
      AppUsuarios usuarioAutorizado = TokenUtil.obtenerUsuarioToken(authToken, cnn);
      listaServicio = new AppServicioDelegado(cnn, null).consultarServiciosUsuario(usuarioAutorizado.getIdUsuario());
    } finally {
      ConexionBD.desconectar(cnn);
    }
    return listaServicio;
  }
}
