/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package co.movistar.planeacioncomercial.negocio.rest.servicios;

import co.movistar.planeacioncomercial.modelo.conexion.ConexionBD;
import co.movistar.planeacioncomercial.modelo.vo.AppOficinasCe;
import co.movistar.planeacioncomercial.modelo.vo.AppUsuarios;
import co.movistar.planeacioncomercial.negocio.delegado.AppOficinasCeDelegado;
import co.movistar.planeacioncomercial.negocio.excepciones.PlaneacionComercialException;
import co.movistar.planeacioncomercial.negocio.util.TokenUtil;

import javax.ws.rs.GET;
import javax.ws.rs.HeaderParam;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.UriInfo;
import java.sql.Connection;
import java.util.List;

/**
 * REST Web Service
 *
 * @author Lord_Nightmare
 */
@Path("oficina")
public class AppOficinaCeRest {

  @Context
  private UriInfo context;

  /**
   * Creates a new instance of AppOficinaCeResource
   */
  public AppOficinaCeRest() {
  }

  /**
   * Retrieves representation of an instance of co.movistar.planeacioncomercial.negocio.rest.servicios.AppOficinaCeRest
   *
   * @param token
   * @return an instance of co.movistar.planeacioncomercial.modelo.vo.AppOficinasCe
   */
  @GET
  @Path("consultar")
  @Produces(MediaType.APPLICATION_JSON)
  public List<AppOficinasCe> consultarOficinas(@HeaderParam("Authorization") String token) throws PlaneacionComercialException {
    List<AppOficinasCe> listaOficinas = null;
    Connection cnn = null;
    try {
      cnn = ConexionBD.conectar();
      AppUsuarios autorizado = TokenUtil.obtenerUsuarioToken(token, cnn);
      listaOficinas = new AppOficinasCeDelegado(cnn, null).consultarPorCoordinador(autorizado.getCedula());
    } finally {
      ConexionBD.desconectar(cnn);
    }
    return listaOficinas;
  }
}
