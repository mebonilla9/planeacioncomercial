/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package co.movistar.planeacioncomercial.negocio.rest.servicios;

import co.movistar.planeacioncomercial.modelo.conexion.ConexionBD;
import co.movistar.planeacioncomercial.modelo.vo.AppAdminCanal;
import co.movistar.planeacioncomercial.negocio.constantes.EMensajes;
import co.movistar.planeacioncomercial.negocio.delegado.AppAdminCanalDelegado;
import co.movistar.planeacioncomercial.negocio.excepciones.PlaneacionComercialException;
import co.movistar.planeacioncomercial.negocio.util.TokenUtil;

import javax.ws.rs.*;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriInfo;
import java.sql.Connection;

/**
 * REST Web Service
 *
 * @author Lord_Nightmare
 */
@Path("admincanal")
public class AppAdminCanalRest {

  @Context
  private UriInfo context;

  /**
   * Creates a new instance of AppAdminCanalRest
   */
  public AppAdminCanalRest() {
  }

  /**
   * Retrieves representation of an instance of co.movistar.planeacioncomercial.negocio.rest.servicios.AppAdminCanalRest
   *
   * @param token
   * @param restriccion
   * @return an instance of AppAdminCanal
   */
  @POST
  @Path("insertar")
  @Consumes(MediaType.APPLICATION_JSON)
  public Response agregarRestriccion(@HeaderParam("Authorization") String token, AppAdminCanal restriccion) {
    Connection cnn = null;
    try {
      cnn = ConexionBD.conectar();
      TokenUtil.obtenerUsuarioToken(token, cnn);
      new AppAdminCanalDelegado(cnn, null).insertar(restriccion);
      ConexionBD.commit(cnn);
    } catch (PlaneacionComercialException ex) {
      ConexionBD.rollback(cnn);
      return Response.status(Response.Status.BAD_REQUEST).entity(ex.getMensaje()).build();
    } finally {
      ConexionBD.desconectar(cnn);
    }
    return Response.ok(EMensajes.INSERTO.getDescripcion()).build();
  }

  /**
   * PUT method for updating or creating an instance of AppAdminCanalRest
   *
   * @param token
   * @return
   * @throws PlaneacionComercialException
   */
  @GET
  @Path("consultar/{modulo}")
  @Produces(MediaType.APPLICATION_JSON)
  public AppAdminCanal evaluarRestriccion(@HeaderParam("Authorization") String token, @PathParam("modulo") String modulo) throws PlaneacionComercialException {
    AppAdminCanal restriccion = null;
    Connection cnn = null;
    try {
      cnn = ConexionBD.conectar();
      TokenUtil.obtenerUsuarioToken(token, cnn);
      restriccion = new AppAdminCanalDelegado(cnn, null).consultarPorModulo(modulo);
    } finally {
      ConexionBD.desconectar(cnn);
    }
    return restriccion;
  }
}
