/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package co.movistar.planeacioncomercial.negocio.rest.servicios;

import co.movistar.planeacioncomercial.modelo.conexion.ConexionBD;
import co.movistar.planeacioncomercial.modelo.vo.AppRegional;
import co.movistar.planeacioncomercial.negocio.constantes.EMensajes;
import co.movistar.planeacioncomercial.negocio.delegado.AppRegionalDelegado;
import co.movistar.planeacioncomercial.negocio.excepciones.PlaneacionComercialException;
import co.movistar.planeacioncomercial.negocio.util.TokenUtil;

import javax.ws.rs.*;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriInfo;
import java.sql.Connection;
import java.util.List;

/**
 * REST Web Service
 *
 * @author Manuel Ernesto Bonilla Muñoz <mebonilla9@gmail.com>
 */
@Path("regional")
public class AppRegionalRest {

  @Context
  private UriInfo context;

  /**
   * Creates a new instance of AppRegionalRest
   */
  public AppRegionalRest() {
  }

  /**
   * PUT method for updating or creating an instance of AppCanalesRest
   *
   * @param content representation for the resource
   * @param token
   * @return
   */
  @PUT
  @Path("insertar")
  @Consumes(MediaType.APPLICATION_JSON)
  public Response insertarRegionales(AppRegional content, @HeaderParam("Authorization") String token) {
    Connection cnn = null;
    try {
      cnn = ConexionBD.conectar();
      TokenUtil.obtenerUsuarioToken(token, cnn);
      new AppRegionalDelegado(cnn, null).insertar(content);
      ConexionBD.commit(cnn);
    } catch (PlaneacionComercialException ex) {
      return Response.status(Response.Status.BAD_REQUEST).entity(ex.getMensaje()).build();
    } finally {
      ConexionBD.commit(cnn);
    }
    return Response.ok(EMensajes.INSERTO).build();
  }

  /**
   * PUT method for updating or creating an instance of AppCanalesRest
   *
   * @param content representation for the resource
   * @param token
   * @return
   */
  @PUT
  @Path("modificar")
  @Consumes(MediaType.APPLICATION_JSON)
  public Response actualizarRegionales(AppRegional content, @HeaderParam("Authorization") String token) {
    Connection cnn = null;
    try {
      cnn = ConexionBD.conectar();
      TokenUtil.obtenerUsuarioToken(token, cnn);
      new AppRegionalDelegado(cnn, null).editar(content);
      ConexionBD.commit(cnn);
    } catch (PlaneacionComercialException ex) {
      return Response.status(Response.Status.BAD_REQUEST).entity(ex.getMensaje()).build();
    } finally {
      ConexionBD.desconectar(cnn);
    }
    return Response.ok(EMensajes.INSERTO).build();
  }

  /**
   * Retrieves representation of an instance of
   * co.movistar.planeacioncomercial.negocio.rest.servicios.AppCanalesRest
   *
   * @param token
   * @return an instance of AppCanales
   * @throws co.movistar.planeacioncomercial.negocio.excepciones.PlaneacionComercialException
   */
  @GET
  @Path("consultar")
  @Produces(MediaType.APPLICATION_JSON)
  public List<AppRegional> consultarRegionales(@HeaderParam("Authorization") String token) throws PlaneacionComercialException {
    List<AppRegional> listaRegionales = null;
    Connection cnn = null;
    try {
      cnn = ConexionBD.conectar();
      TokenUtil.obtenerUsuarioToken(token, cnn);
      listaRegionales = new AppRegionalDelegado(ConexionBD.conectar(), null).consultar();
      ConexionBD.commit(cnn);
    } finally {
      ConexionBD.desconectar(cnn);
    }
    return listaRegionales;
  }

  /**
   * Retrieves representation of an instance of
   * co.movistar.planeacioncomercial.negocio.rest.servicios.AppCanalesRest
   *
   * @param id
   * @param token
   * @return an instance of AppCanales
   * @throws co.movistar.planeacioncomercial.negocio.excepciones.PlaneacionComercialException
   */
  @GET
  @Path("consultar/{id}")
  @Produces(MediaType.APPLICATION_JSON)
  public AppRegional consultarRegionales(@PathParam("id") long id, @HeaderParam("Authorization") String token) throws PlaneacionComercialException {
    AppRegional regional = null;
    Connection cnn = null;
    try {
      cnn = ConexionBD.conectar();
      TokenUtil.obtenerUsuarioToken(token, cnn);
      regional = new AppRegionalDelegado(cnn, null).consultar(id);
    } finally {
      ConexionBD.desconectar(cnn);
    }
    return regional;
  }

  @GET
  @Path("consultar/ciudad/{idCiudad}")
  @Produces(MediaType.APPLICATION_JSON)
  public AppRegional consultarRegionalCiudad(@PathParam("idCiudad") Long idCiudad, @HeaderParam("Authorization") String token) throws PlaneacionComercialException {
    AppRegional regional = null;
    Connection cnn = null;
    try {
      cnn = ConexionBD.conectar();
      TokenUtil.obtenerUsuarioToken(token, cnn);
      regional = new AppRegionalDelegado(cnn, null).consultarCiudad(idCiudad);
    } finally {
      ConexionBD.desconectar(cnn);
    }
    return regional;
  }
}
