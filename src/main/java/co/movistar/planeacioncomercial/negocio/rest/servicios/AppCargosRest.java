/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package co.movistar.planeacioncomercial.negocio.rest.servicios;

import co.movistar.planeacioncomercial.modelo.conexion.ConexionBD;
import co.movistar.planeacioncomercial.modelo.vo.AppCargo;
import co.movistar.planeacioncomercial.negocio.constantes.EMensajes;
import co.movistar.planeacioncomercial.negocio.delegado.AppCargoDelegado;
import co.movistar.planeacioncomercial.negocio.excepciones.PlaneacionComercialException;
import co.movistar.planeacioncomercial.negocio.util.TokenUtil;

import javax.ws.rs.*;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriInfo;
import java.sql.Connection;
import java.util.List;

/**
 * REST Web Service
 *
 * @author Manuel Ernesto Bonilla Muñoz <mebonilla9@gmail.com>
 */
@Path("cargo")
public class AppCargosRest {

  @Context
  private UriInfo context;

  /**
   * Creates a new instance of AppCargosRest
   */
  public AppCargosRest() {
  }

  /**
   * PUT method for updating or creating an instance of AppCanalesRest
   *
   * @param content representation for the resource
   * @param token
   * @return
   */
  @PUT
  @Path("insertar")
  @Consumes(MediaType.APPLICATION_JSON)
  public Response insertarCargos(AppCargo content, @HeaderParam("Authorization") String token) {
    Connection cnn = null;
    try {
      cnn = ConexionBD.conectar();
      TokenUtil.obtenerUsuarioToken(token, cnn);
      new AppCargoDelegado(cnn, null).insertar(content);
      ConexionBD.commit(cnn);
    } catch (PlaneacionComercialException ex) {
      return Response.status(Response.Status.BAD_REQUEST).entity(ex.getMensaje()).build();
    } finally {
      ConexionBD.desconectar(cnn);
    }
    return Response.ok(EMensajes.INSERTO).build();
  }

  /**
   * PUT method for updating or creating an instance of AppCanalesRest
   *
   * @param content representation for the resource
   * @param token
   * @return
   */
  @PUT
  @Path("modificar")
  @Consumes(MediaType.APPLICATION_JSON)
  public Response actualizarCargos(AppCargo content, @HeaderParam("Authorization") String token) {
    Connection cnn = null;
    try {
      cnn = ConexionBD.conectar();
      TokenUtil.obtenerUsuarioToken(token, cnn);
      new AppCargoDelegado(cnn, null).editar(content);
      ConexionBD.commit(cnn);
    } catch (PlaneacionComercialException ex) {
      return Response.status(Response.Status.BAD_REQUEST).entity(ex.getMensaje()).build();
    } finally {
      ConexionBD.desconectar(cnn);
    }
    return Response.ok(EMensajes.INSERTO).build();
  }

  /**
   * Retrieves representation of an instance of
   * co.movistar.planeacioncomercial.negocio.rest.servicios.AppCanalesRest
   *
   * @param token
   * @return an instance of AppCanales
   * @throws co.movistar.planeacioncomercial.negocio.excepciones.PlaneacionComercialException
   */
  @GET
  @Path("consultar")
  @Produces(MediaType.APPLICATION_JSON)
  public List<AppCargo> consultarCargos(@HeaderParam("Authorization") String token) throws PlaneacionComercialException {
    List<AppCargo> listaCargos = null;
    Connection cnn = null;
    try {
      cnn = ConexionBD.conectar();
      TokenUtil.obtenerUsuarioToken(token, cnn);
      listaCargos = new AppCargoDelegado(cnn, null).consultarAsesor();
    } finally {
      ConexionBD.desconectar(cnn);
    }
    return listaCargos;
  }

  /**
   * Retrieves representation of an instance of
   * co.movistar.planeacioncomercial.negocio.rest.servicios.AppCanalesRest
   *
   * @param id
   * @param token
   * @return an instance of AppCanales
   * @throws co.movistar.planeacioncomercial.negocio.excepciones.PlaneacionComercialException
   */
  @GET
  @Path("consultar/{id}")
  @Produces(MediaType.APPLICATION_JSON)
  public AppCargo consultarCargos(@PathParam("id") long id, @HeaderParam("Authorization") String token) throws PlaneacionComercialException {
    AppCargo cargo = null;
    Connection cnn = null;
    try {
      cnn = ConexionBD.conectar();
      TokenUtil.obtenerUsuarioToken(token, cnn);
      cargo = new AppCargoDelegado(cnn, null).consultar(id);
    } finally {
      ConexionBD.desconectar(cnn);
    }
    return cargo;
  }

  @GET
  @Path("consultar/asesor")
  @Produces(MediaType.APPLICATION_JSON)
  public List<AppCargo> consultarCargoAsesor(@HeaderParam("Authorization") String token) throws PlaneacionComercialException {
    List<AppCargo> lista = null;
    Connection cnn = null;
    try {
      cnn = ConexionBD.conectar();
      TokenUtil.obtenerUsuarioToken(token, cnn);
      lista = new AppCargoDelegado(cnn, null).consultarAsesor();
    } finally {
      ConexionBD.desconectar(cnn);
    }
    return lista;
  }
}
