/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package co.movistar.planeacioncomercial.negocio.rest.servicios;

import co.movistar.planeacioncomercial.modelo.conexion.ConexionBD;
import co.movistar.planeacioncomercial.modelo.dto.InfoMetaDTO;
import co.movistar.planeacioncomercial.modelo.dto.InsertarUsuarioDto;
import co.movistar.planeacioncomercial.modelo.dto.RespuestaDTO;
import co.movistar.planeacioncomercial.modelo.vo.AppDatosCe;
import co.movistar.planeacioncomercial.modelo.vo.AppUsuarios;
import co.movistar.planeacioncomercial.modelo.vo.AppVentasCe;
import co.movistar.planeacioncomercial.negocio.constantes.ECanales;
import co.movistar.planeacioncomercial.negocio.constantes.EMensajes;
import co.movistar.planeacioncomercial.negocio.delegado.AppUsuariosDelegado;
import co.movistar.planeacioncomercial.negocio.excepciones.PlaneacionComercialException;
import co.movistar.planeacioncomercial.negocio.util.CryptoUtil;
import co.movistar.planeacioncomercial.negocio.util.JavaMailUtil;
import co.movistar.planeacioncomercial.negocio.util.TokenUtil;
import com.fasterxml.jackson.databind.ObjectMapper;

import javax.ws.rs.*;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriInfo;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.List;

/**
 * REST Web Service
 *
 * @author Manuel Ernesto Bonilla Muñoz <mebonilla9@gmail.com>
 */
@Path("usuario")
public class AppUsuariosRest {

  @Context
  private UriInfo context;

  /**
   * Creates a new instance of AppUsuariosRest
   */
  public AppUsuariosRest() {
  }

  /**
   * PUT method for updating or creating an instance of AppCanalesRest
   *
   * @param content representation for the resource
   * @param token
   * @return
   */
  @POST
  @Path("insertar/{canal}")
  @Consumes(MediaType.APPLICATION_JSON)
  public RespuestaDTO insertarUsuarios(InsertarUsuarioDto content, @HeaderParam("Authorization") String token, @PathParam("canal") ECanales canal) {
    Connection cnn = null;
    RespuestaDTO respuestaDTO = null;
    try {
      cnn = ConexionBD.conectar();
      AppUsuarios usuarioActual = TokenUtil.obtenerUsuarioToken(token, cnn);
      String mensajeAdd = "El usuario " + content.getUsuario().getNombre() + " identificado con C.C. " + content.getUsuario().getCedula().longValue() + " Ha sido puesto a tu cargo!";
      AppUsuariosDelegado aud = new AppUsuariosDelegado(cnn, null);
      asignarDatosAdicionales(content, canal);
      aud.insertarUsuario(content, usuarioActual, canal);
      //JavaMailUtil.envioCorreo(aud.consultar(content.getUsuario().getIdJefe()).getCorreo(), "correo", mensajeAdd);
      respuestaDTO = new RespuestaDTO(EMensajes.INSERTO);
    } catch (PlaneacionComercialException ex) {
      ConexionBD.rollback(cnn);
      respuestaDTO = new RespuestaDTO(ex);
    } finally {
      ConexionBD.desconectar(cnn);
    }
    return respuestaDTO;
  }

  private void asignarDatosAdicionales(InsertarUsuarioDto content, ECanales canal) {
    switch (canal){
      case CENTROS_EXPERIENCIA:
        AppDatosCe datosCe = new ObjectMapper().convertValue(content.getDatosAdicionales(), AppDatosCe.class);
        content.setDatosAdicionales(datosCe);
        break;
      case FUERZA_VENTA_DIRECTA:
        AppVentasCe ventasCe = new ObjectMapper().convertValue(content.getDatosAdicionales(), AppVentasCe.class);
        content.setDatosAdicionales(ventasCe);
        break;
    }
  }

  /**
   * @param content
   * @param token
   * @param canal
   * @return
   */
  @POST
  @Path("modificar/{canal}")
  @Consumes(MediaType.APPLICATION_JSON)
  public RespuestaDTO modificarUsuario(InsertarUsuarioDto content, @HeaderParam("Authorization") String token, @PathParam("canal") ECanales canal) {
    Connection cnn = null;
    RespuestaDTO respuestaDTO = null;
    try {
      cnn = ConexionBD.conectar();
      AppUsuarios usuarioActual = TokenUtil.obtenerUsuarioToken(token, cnn);
      asignarDatosAdicionales(content, canal);
      new AppUsuariosDelegado(cnn, null).insertarUsuario(content, usuarioActual, canal);
      respuestaDTO = new RespuestaDTO(EMensajes.MODIFICO);
    } catch (PlaneacionComercialException ex) {
      ConexionBD.rollback(cnn);
      respuestaDTO = new RespuestaDTO(EMensajes.ERROR_MODIFICAR);
    } finally {
      ConexionBD.desconectar(cnn);
    }
    return respuestaDTO;
  }

  /**
   * PUT method for updating or creating an instance of AppCanalesRest
   *
   * @param content representation for the resource
   * @param token
   * @return
   */
  @POST
  @Path("modificar2")
  @Consumes(MediaType.APPLICATION_JSON)
  public Response actualizarUsuarios(AppUsuarios content, @HeaderParam("Authorization") String token) {
    Connection cnn = null;
    try {
      cnn = ConexionBD.conectar();
      TokenUtil.obtenerUsuarioToken(token, cnn);
      new AppUsuariosDelegado(cnn, null).editar(content);
    } catch (PlaneacionComercialException ex) {
      return Response.status(Response.Status.BAD_REQUEST).entity(ex.getMensaje()).build();
    } finally {
      ConexionBD.desconectar(cnn);
    }
    return Response.ok(EMensajes.INSERTO).build();
  }

  /**
   * Retrieves representation of an instance of
   * co.movistar.planeacioncomercial.negocio.rest.servicios.AppCanalesRest
   *
   * @param token
   * @return an instance of AppCanales
   * @throws co.movistar.planeacioncomercial.negocio.excepciones.PlaneacionComercialException
   */
  @GET
  @Path("consultar")
  @Produces(MediaType.APPLICATION_JSON)
  public List<AppUsuarios> consultarUsuarios(@HeaderParam("Authorization") String token) throws PlaneacionComercialException {
    List<AppUsuarios> listaUsuarios = null;
    Connection cnn = null;
    try {
      cnn = ConexionBD.conectar();
      TokenUtil.obtenerUsuarioToken(token, cnn);
      listaUsuarios = new AppUsuariosDelegado(cnn, null).consultar();
    } finally {
      ConexionBD.desconectar(cnn);
    }
    return listaUsuarios;
  }

  /**
   * Retrieves representation of an instance of
   * co.movistar.planeacioncomercial.negocio.rest.servicios.AppCanalesRest
   *
   * @param id
   * @param token
   * @return an instance of AppCanales
   * @throws co.movistar.planeacioncomercial.negocio.excepciones.PlaneacionComercialException
   */
  @GET
  @Path("consultar/{id}")
  @Produces(MediaType.APPLICATION_JSON)
  public AppUsuarios consultarUsuarios(@PathParam("id") long id, @HeaderParam("Authorization") String token) throws PlaneacionComercialException {
    AppUsuarios usuario = null;
    Connection cnn = null;
    try {
      cnn = ConexionBD.conectar();
      TokenUtil.obtenerUsuarioToken(token, cnn);
      usuario = new AppUsuariosDelegado(cnn, null).consultar(id);
    } finally {
      ConexionBD.desconectar(cnn);
    }
    return usuario;
  }

  @GET
  @Path("cedula/{cedula}/{canal}")
  @Produces(MediaType.APPLICATION_JSON)
  public InsertarUsuarioDto consultarUsuariosCedula(@PathParam("cedula") double cedula, @PathParam("canal") ECanales canal, @HeaderParam("Authorization") String token) throws PlaneacionComercialException {
    InsertarUsuarioDto usuario;
    Connection cnn = null;
    try {
      cnn = ConexionBD.conectar();
      AppUsuarios usuarioAutorizado = TokenUtil.obtenerUsuarioToken(token, cnn);
      usuario = new AppUsuariosDelegado(cnn, null).consultarPorCedula(cedula, canal, usuarioAutorizado.getIdUsuario());
    } finally {
      ConexionBD.desconectar(cnn);
    }
    return usuario;
  }

  public Response consultarUsuarioToken(@HeaderParam("Authorization") String token) throws PlaneacionComercialException {
    AppUsuarios usuario;
    Connection cnn = null;
    try {
      cnn = ConexionBD.conectar();
      usuario = TokenUtil.obtenerUsuarioToken(token, cnn);
    } finally {
      ConexionBD.desconectar(cnn);
    }
    return Response.ok(usuario.getNombre() + "," + usuario.getCorreo()).build();
  }

  @POST
  @Path("reestablecer")
  public Response reestablecerContrasenaUsuario(String context, @HeaderParam("Authorization") String token) throws PlaneacionComercialException, SQLException {
    AppUsuarios usuario;
    Connection cnn = null;
    try {
      cnn = ConexionBD.conectar();
      usuario = TokenUtil.obtenerUsuarioToken(token, cnn);
      usuario.setPassword(CryptoUtil.cifrarContrasena(context.substring(1, context.length() - 1)));
      new AppUsuariosDelegado(cnn, null).editar(usuario);
      cnn.commit();
    } finally {
      ConexionBD.desconectar(cnn);
    }
    return Response.ok(EMensajes.MODIFICO.getDescripcion()).build();
  }

  @GET
  @Path("subordinados")
  @Produces(MediaType.APPLICATION_JSON)
  public List<AppUsuarios> consultarSubordinados(@HeaderParam("Authorization") String token) throws PlaneacionComercialException {
    List<AppUsuarios> listaSubordinados;
    Connection cnn = null;
    try {
      cnn = ConexionBD.conectar();
      AppUsuarios usuario = TokenUtil.obtenerUsuarioToken(token, cnn);
      listaSubordinados = new AppUsuariosDelegado(cnn, null).consultarSubordinados(usuario.getIdUsuario());
    } finally {
      ConexionBD.desconectar(cnn);
    }
    return listaSubordinados;
  }

  @GET
  @Path("nov/subordinados/{canal}")
  @Produces(MediaType.APPLICATION_JSON)
  public List<InfoMetaDTO> consultarSubordinadosNov(@HeaderParam("Authorization") String token, @PathParam("canal") ECanales canal) throws PlaneacionComercialException {
    List<InfoMetaDTO> listaSubordinados;
    Connection cnn = null;
    try {
      cnn = ConexionBD.conectar();
      AppUsuarios usuario = TokenUtil.obtenerUsuarioToken(token, cnn);
      listaSubordinados = new AppUsuariosDelegado(cnn, null).consultarSubordinadosNov(usuario.getIdUsuario(), canal);
    } finally {
      ConexionBD.desconectar(cnn);
    }
    return listaSubordinados;
  }

  @GET
  @Path("reestablecer/{correo}")
  @Produces(MediaType.APPLICATION_JSON)
  public Response reestablecerContrasenaCorreo(@PathParam("correo") String correo) throws PlaneacionComercialException {
    Connection cnn = null;
    try {
      cnn = ConexionBD.conectar();
      AppUsuarios usuario = new AppUsuariosDelegado(cnn, null).reestablecerContrasenaCorreo(correo);
      ConexionBD.commit(cnn);
      JavaMailUtil.envioCorreo(usuario.getCorreo(), "contraseña", null);
    } finally {
      ConexionBD.desconectar(cnn);
    }
    return Response.ok(EMensajes.REESTABLECER_CONTRASENA.getDescripcion()).build();
  }

  @GET
  @Path("jefes")
  @Produces(MediaType.APPLICATION_JSON)
  public List<AppUsuarios> consultarJefes(@HeaderParam("Authorization") String token) throws PlaneacionComercialException {
    Connection cnn = null;
    List<AppUsuarios> listaJefes;
    try {
      cnn = ConexionBD.conectar();
      listaJefes = new AppUsuariosDelegado(cnn, null).consultar();
    } finally {
      ConexionBD.desconectar(cnn);
    }
    return listaJefes;
  }
}
