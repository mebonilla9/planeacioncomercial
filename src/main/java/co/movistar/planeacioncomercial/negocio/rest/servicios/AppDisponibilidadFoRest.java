/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package co.movistar.planeacioncomercial.negocio.rest.servicios;

import co.movistar.planeacioncomercial.modelo.conexion.ConexionBD;
import co.movistar.planeacioncomercial.modelo.vo.AppDisponibilidadFo;
import co.movistar.planeacioncomercial.negocio.constantes.EMensajes;
import co.movistar.planeacioncomercial.negocio.delegado.AppDisponibilidadFoDelegado;
import co.movistar.planeacioncomercial.negocio.excepciones.PlaneacionComercialException;
import co.movistar.planeacioncomercial.negocio.util.TokenUtil;

import javax.ws.rs.*;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.UriInfo;
import java.sql.Connection;
import java.util.ArrayList;
import java.util.List;

/**
 * REST Web Service
 *
 * @author Manuel Ernesto Bonilla Muñoz <mebonilla9@gmail.com>
 */
@Path("disponibilidadfibra")
public class AppDisponibilidadFoRest {

  @Context
  private UriInfo context;

  /**
   * Creates a new instance of AppDisponibilidadFoRest
   */
  public AppDisponibilidadFoRest() {
  }

  /**
   * Retrieves representation of an instance of
   * co.movistar.planeacioncomercial.negocio.rest.servicios.AppDisponibilidadFoRest
   *
   * @param token
   * @param disFibra
   * @return an instance of
   * co.movistar.planeacioncomercial.modelo.vo.AppDisponibilidadFo
   * @throws co.movistar.planeacioncomercial.negocio.excepciones.PlaneacionComercialException
   */
  @POST
  @Path("consultar")
  @Consumes(MediaType.APPLICATION_JSON)
  @Produces(MediaType.APPLICATION_JSON)
  public AppDisponibilidadFo consultarPorDireccion(@HeaderParam("Authorization") String token, AppDisponibilidadFo disFibra) throws PlaneacionComercialException {
    AppDisponibilidadFo disponibilidadFibra = new AppDisponibilidadFo();
    Connection cnn = null;
    try {
      cnn = ConexionBD.conectar();
      TokenUtil.obtenerUsuarioToken(token, cnn);
      disponibilidadFibra = new AppDisponibilidadFoDelegado(cnn, null).consultarPorDireccion(disFibra);
    } catch (NullPointerException e) {
      throw new PlaneacionComercialException(EMensajes.NO_RESULTADOS);
    } finally {
      ConexionBD.desconectar(cnn);
    }
    return disponibilidadFibra;
  }

  @POST
  @Path("consultar/noparam")
  @Consumes(MediaType.APPLICATION_JSON)
  @Produces(MediaType.APPLICATION_JSON)
  public AppDisponibilidadFo consultarNoParametrizada(@HeaderParam("Authorization") String token, AppDisponibilidadFo disFibra) throws PlaneacionComercialException {
    AppDisponibilidadFo disponibilidadFibra = new AppDisponibilidadFo();
    Connection cnn = null;
    try {
      cnn = ConexionBD.conectar();
      TokenUtil.obtenerUsuarioToken(token, cnn);
      disponibilidadFibra = new AppDisponibilidadFoDelegado(cnn, null).consultarDireccionNoParametrizada(disFibra);
    } catch (NullPointerException e) {
      throw new PlaneacionComercialException(EMensajes.NO_RESULTADOS);
    } finally {
      ConexionBD.desconectar(cnn);
    }
    return disponibilidadFibra;
  }

  @POST
  @Path("direccion/noparam")
  @Consumes(MediaType.APPLICATION_JSON)
  @Produces(MediaType.APPLICATION_JSON)
  public List<AppDisponibilidadFo> consultarDireccionesNoParametrizada(@HeaderParam("Authorization") String token, AppDisponibilidadFo disFibra) throws PlaneacionComercialException {
    List<AppDisponibilidadFo> direccionesNoParam = new ArrayList<>();
    Connection cnn = null;
    try {
      cnn = ConexionBD.conectar();
      TokenUtil.obtenerUsuarioToken(token, cnn);
      direccionesNoParam = new AppDisponibilidadFoDelegado(cnn, null).consultarDireccionesNoParametrizada(disFibra);
    } catch (NullPointerException e) {
      throw new PlaneacionComercialException(EMensajes.NO_RESULTADOS);
    } finally {
      ConexionBD.desconectar(cnn);
    }
    return direccionesNoParam;
  }

  /**
   * Retrieves representation of an instance of
   * co.movistar.planeacioncomercial.negocio.rest.servicios.AppDisponibilidadFoRest
   *
   * @param token
   * @param param
   * @return an instance of
   * co.movistar.planeacioncomercial.modelo.vo.AppDisponibilidadFo
   * @throws co.movistar.planeacioncomercial.negocio.excepciones.PlaneacionComercialException
   */
  @GET
  @Path("regional/{param}")
  @Produces(MediaType.APPLICATION_JSON)
  public List<AppDisponibilidadFo> consultarRegionales(@HeaderParam("Authorization") String token, @PathParam("param") Boolean param) throws PlaneacionComercialException {
    List<AppDisponibilidadFo> listaRegionales = new ArrayList<>();
    Connection cnn = null;
    try {
      cnn = ConexionBD.conectar();
      TokenUtil.obtenerUsuarioToken(token, cnn);
      listaRegionales = new AppDisponibilidadFoDelegado(cnn, null).consultarRegionales(param);
    } catch (NullPointerException e) {
      throw new PlaneacionComercialException(EMensajes.NO_RESULTADOS);
    } finally {
      ConexionBD.desconectar(cnn);
    }
    return listaRegionales;
  }

  /**
   * Retrieves representation of an instance of
   * co.movistar.planeacioncomercial.negocio.rest.servicios.AppDisponibilidadFoRest
   *
   * @param token
   * @param regional
   * @return an instance of
   * co.movistar.planeacioncomercial.modelo.vo.AppDisponibilidadFo
   * @throws co.movistar.planeacioncomercial.negocio.excepciones.PlaneacionComercialException
   */
  @GET
  @Path("departamento/{regional}/{param}")
  @Produces(MediaType.APPLICATION_JSON)
  public List<AppDisponibilidadFo> consultarDepartamentos(@HeaderParam("Authorization") String token, @PathParam("regional") String regional, @PathParam("param") Boolean param) throws PlaneacionComercialException {
    List<AppDisponibilidadFo> listaDepartamentos = new ArrayList<>();
    Connection cnn = null;
    try {
      cnn = ConexionBD.conectar();
      TokenUtil.obtenerUsuarioToken(token, cnn);
      listaDepartamentos = new AppDisponibilidadFoDelegado(cnn, null).consultarDepartamentos(regional, param);
    } catch (NullPointerException e) {
      throw new PlaneacionComercialException(EMensajes.NO_RESULTADOS);
    } finally {
      ConexionBD.desconectar(cnn);
    }
    return listaDepartamentos;
  }

  /**
   * Retrieves representation of an instance of
   * co.movistar.planeacioncomercial.negocio.rest.servicios.AppDisponibilidadFoRest
   *
   * @param token
   * @param departamento
   * @return an instance of
   * co.movistar.planeacioncomercial.modelo.vo.AppDisponibilidadFo
   * @throws co.movistar.planeacioncomercial.negocio.excepciones.PlaneacionComercialException
   */
  @GET
  @Path("localidad/{departamento}/{param}")
  @Produces(MediaType.APPLICATION_JSON)
  public List<AppDisponibilidadFo> consultarLocalidades(@HeaderParam("Authorization") String token, @PathParam("departamento") String departamento, @PathParam("param") Boolean param) throws PlaneacionComercialException {
    List<AppDisponibilidadFo> listaLocalidades = new ArrayList<>();
    Connection cnn = null;
    try {
      cnn = ConexionBD.conectar();
      TokenUtil.obtenerUsuarioToken(token, cnn);
      listaLocalidades = new AppDisponibilidadFoDelegado(cnn, null).consultarLocalidades(departamento, param);
    } catch (NullPointerException e) {
      throw new PlaneacionComercialException(EMensajes.NO_RESULTADOS);
    } finally {
      ConexionBD.desconectar(cnn);
    }
    return listaLocalidades;
  }

  /**
   * Retrieves representation of an instance of
   * co.movistar.planeacioncomercial.negocio.rest.servicios.AppDisponibilidadFoRest
   *
   * @param token
   * @param localidad
   * @return an instance of
   * co.movistar.planeacioncomercial.modelo.vo.AppDisponibilidadFo
   * @throws co.movistar.planeacioncomercial.negocio.excepciones.PlaneacionComercialException
   */
  @GET
  @Path("barrio/{localidad}/{param}")
  @Produces(MediaType.APPLICATION_JSON)
  public List<AppDisponibilidadFo> consultarBarrios(@HeaderParam("Authorization") String token, @PathParam("localidad") String localidad, @PathParam("param") Boolean param) throws PlaneacionComercialException {
    List<AppDisponibilidadFo> listaBarrios = new ArrayList<>();
    Connection cnn = null;
    try {
      cnn = ConexionBD.conectar();
      TokenUtil.obtenerUsuarioToken(token, cnn);
      listaBarrios = new AppDisponibilidadFoDelegado(cnn, null).consultarBarrios(localidad, param);
    } catch (NullPointerException e) {
      throw new PlaneacionComercialException(EMensajes.NO_RESULTADOS);
    } finally {
      ConexionBD.desconectar(cnn);
    }
    return listaBarrios;
  }

  /**
   * Retrieves representation of an instance of
   * co.movistar.planeacioncomercial.negocio.rest.servicios.AppDisponibilidadFoRest
   *
   * @param token
   * @param localidad
   * @param barrio
   * @return an instance of
   * co.movistar.planeacioncomercial.modelo.vo.AppDisponibilidadFo
   * @throws co.movistar.planeacioncomercial.negocio.excepciones.PlaneacionComercialException
   */
  @GET
  @Path("principal/{localidad}/{barrio}")
  @Produces(MediaType.APPLICATION_JSON)
  public List<AppDisponibilidadFo> consultarPrincipal(@HeaderParam("Authorization") String token, @PathParam("localidad") String localidad, @PathParam("barrio") String barrio) throws PlaneacionComercialException {
    List<AppDisponibilidadFo> listaBarrios = new ArrayList<>();
    Connection cnn = null;
    try {
      cnn = ConexionBD.conectar();
      TokenUtil.obtenerUsuarioToken(token, cnn);
      listaBarrios = new AppDisponibilidadFoDelegado(cnn, null).consultarPrincipal(localidad, barrio);
    } catch (NullPointerException e) {
      throw new PlaneacionComercialException(EMensajes.NO_RESULTADOS);
    } finally {
      ConexionBD.desconectar(cnn);
    }
    return listaBarrios;
  }

  /**
   * Retrieves representation of an instance of
   * co.movistar.planeacioncomercial.negocio.rest.servicios.AppDisponibilidadFoRest
   *
   * @param token
   * @param localidad
   * @param barrio
   * @param principal
   * @return an instance of
   * co.movistar.planeacioncomercial.modelo.vo.AppDisponibilidadFo
   * @throws co.movistar.planeacioncomercial.negocio.excepciones.PlaneacionComercialException
   */
  @GET
  @Path("principal/{localidad}/{barrio}/{principal}")
  @Produces(MediaType.APPLICATION_JSON)
  public List<AppDisponibilidadFo> consultarReferenciaPrincipal(@HeaderParam("Authorization") String token, @PathParam("localidad") String localidad, @PathParam("barrio") String barrio, @PathParam("principal") String principal) throws PlaneacionComercialException {
    List<AppDisponibilidadFo> listaBarrios = new ArrayList<>();
    Connection cnn = null;
    try {
      cnn = ConexionBD.conectar();
      TokenUtil.obtenerUsuarioToken(token, cnn);
      listaBarrios = new AppDisponibilidadFoDelegado(cnn, null).consultarReferenciaPrincipal(localidad, barrio, principal);
    } catch (NullPointerException e) {
      throw new PlaneacionComercialException(EMensajes.NO_RESULTADOS);
    } finally {
      ConexionBD.desconectar(cnn);
    }
    return listaBarrios;
  }

  /**
   * Retrieves representation of an instance of
   * co.movistar.planeacioncomercial.negocio.rest.servicios.AppDisponibilidadFoRest
   *
   * @param token
   * @param localidad
   * @param barrio
   * @param principal
   * @param referenciaPrincipal
   * @return an instance of
   * co.movistar.planeacioncomercial.modelo.vo.AppDisponibilidadFo
   * @throws co.movistar.planeacioncomercial.negocio.excepciones.PlaneacionComercialException
   */
  @GET
  @Path("principal/{localidad}/{barrio}/{principal}/{referenciaPrincipal}")
  @Produces(MediaType.APPLICATION_JSON)
  public List<AppDisponibilidadFo> consultarCruce(@HeaderParam("Authorization") String token, @PathParam("localidad") String localidad, @PathParam("barrio") String barrio, @PathParam("principal") String principal, @PathParam("referenciaPrincipal") String referenciaPrincipal) throws PlaneacionComercialException {
    List<AppDisponibilidadFo> listaBarrios = new ArrayList<>();
    Connection cnn = null;
    try {
      cnn = ConexionBD.conectar();
      TokenUtil.obtenerUsuarioToken(token, cnn);
      listaBarrios = new AppDisponibilidadFoDelegado(cnn, null).consultarCruce(localidad, barrio, principal, referenciaPrincipal);
    } catch (NullPointerException e) {
      throw new PlaneacionComercialException(EMensajes.NO_RESULTADOS);
    } finally {
      ConexionBD.desconectar(cnn);
    }
    return listaBarrios;
  }

  /**
   * Retrieves representation of an instance of
   * co.movistar.planeacioncomercial.negocio.rest.servicios.AppDisponibilidadFoRest
   *
   * @param token
   * @param localidad
   * @param barrio
   * @param principal
   * @param referenciaPrincipal
   * @param cruce
   * @return an instance of
   * co.movistar.planeacioncomercial.modelo.vo.AppDisponibilidadFo
   * @throws co.movistar.planeacioncomercial.negocio.excepciones.PlaneacionComercialException
   */
  @GET
  @Path("principal/{localidad}/{barrio}/{principal}/{referenciaPrincipal}/{cruce}")
  @Produces(MediaType.APPLICATION_JSON)
  public List<AppDisponibilidadFo> consultarReferenciaCruce(@HeaderParam("Authorization") String token, @PathParam("localidad") String localidad, @PathParam("barrio") String barrio, @PathParam("principal") String principal, @PathParam("referenciaPrincipal") String referenciaPrincipal, @PathParam("cruce") String cruce) throws PlaneacionComercialException {
    List<AppDisponibilidadFo> listaBarrios = new ArrayList<>();
    Connection cnn = null;
    try {
      cnn = ConexionBD.conectar();
      TokenUtil.obtenerUsuarioToken(token, cnn);
      listaBarrios = new AppDisponibilidadFoDelegado(cnn, null).consultarReferenciaCruce(localidad, barrio, principal, referenciaPrincipal, cruce);
    } catch (NullPointerException e) {
      throw new PlaneacionComercialException(EMensajes.NO_RESULTADOS);
    } finally {
      ConexionBD.desconectar(cnn);
    }
    return listaBarrios;
  }

  /**
   * Retrieves representation of an instance of
   * co.movistar.planeacioncomercial.negocio.rest.servicios.AppDisponibilidadFoRest
   *
   * @param token
   * @param localidad
   * @param barrio
   * @param principal
   * @param referenciaPrincipal
   * @param cruce
   * @param referenciaCruce
   * @return an instance of
   * co.movistar.planeacioncomercial.modelo.vo.AppDisponibilidadFo
   * @throws co.movistar.planeacioncomercial.negocio.excepciones.PlaneacionComercialException
   */
  @GET
  @Path("principal/{localidad}/{barrio}/{principal}/{referenciaPrincipal}/{cruce}/{referenciaCruce}")
  @Produces(MediaType.APPLICATION_JSON)
  public List<AppDisponibilidadFo> consultarPlaca(@HeaderParam("Authorization") String token, @PathParam("localidad") String localidad, @PathParam("barrio") String barrio, @PathParam("principal") String principal, @PathParam("referenciaPrincipal") String referenciaPrincipal, @PathParam("cruce") String cruce, @PathParam("referenciaCruce") String referenciaCruce) throws PlaneacionComercialException {
    List<AppDisponibilidadFo> listaBarrios = new ArrayList<>();
    Connection cnn = null;
    try {
      cnn = ConexionBD.conectar();
      TokenUtil.obtenerUsuarioToken(token, cnn);
      listaBarrios = new AppDisponibilidadFoDelegado(cnn, null).consultarPlaca(localidad, barrio, principal, referenciaPrincipal, cruce, referenciaCruce);
    } catch (NullPointerException e) {
      throw new PlaneacionComercialException(EMensajes.NO_RESULTADOS);
    } finally {
      ConexionBD.desconectar(cnn);
    }
    return listaBarrios;
  }

  /**
   * Retrieves representation of an instance of
   * co.movistar.planeacioncomercial.negocio.rest.servicios.AppDisponibilidadFoRest
   *
   * @param token
   * @param localidad
   * @param barrio
   * @param principal
   * @param referenciaPrincipal
   * @param cruce
   * @param referenciaCruce
   * @param placa
   * @return an instance of
   * co.movistar.planeacioncomercial.modelo.vo.AppDisponibilidadFo
   * @throws co.movistar.planeacioncomercial.negocio.excepciones.PlaneacionComercialException
   */
  @GET
  @Path("principal/{localidad}/{barrio}/{principal}/{referenciaPrincipal}/{cruce}/{referenciaCruce}/{placa}")
  @Produces(MediaType.APPLICATION_JSON)
  public List<AppDisponibilidadFo> consultarComplemento(@HeaderParam("Authorization") String token, @PathParam("localidad") String localidad, @PathParam("barrio") String barrio, @PathParam("principal") String principal, @PathParam("referenciaPrincipal") String referenciaPrincipal, @PathParam("cruce") String cruce, @PathParam("referenciaCruce") String referenciaCruce, @PathParam("placa") String placa) throws PlaneacionComercialException {
    List<AppDisponibilidadFo> listaBarrios = new ArrayList<>();
    Connection cnn = null;
    try {
      cnn = ConexionBD.conectar();
      TokenUtil.obtenerUsuarioToken(token, cnn);
      listaBarrios = new AppDisponibilidadFoDelegado(cnn, null).consultarComplemento(localidad, barrio, principal, referenciaPrincipal, cruce, referenciaCruce, placa);
    } catch (NullPointerException e) {
      throw new PlaneacionComercialException(EMensajes.NO_RESULTADOS);
    } finally {
      ConexionBD.desconectar(cnn);
    }
    return listaBarrios;
  }
}
