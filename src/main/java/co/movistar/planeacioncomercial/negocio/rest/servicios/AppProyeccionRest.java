/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package co.movistar.planeacioncomercial.negocio.rest.servicios;

import co.movistar.planeacioncomercial.modelo.conexion.ConexionBD;
import co.movistar.planeacioncomercial.modelo.vo.AppProyeccion;
import co.movistar.planeacioncomercial.modelo.vo.AppUsuarios;
import co.movistar.planeacioncomercial.negocio.delegado.AppProyeccionDelegado;
import co.movistar.planeacioncomercial.negocio.excepciones.PlaneacionComercialException;
import co.movistar.planeacioncomercial.negocio.util.TokenUtil;

import javax.ws.rs.*;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.UriInfo;
import java.sql.Connection;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * REST Web Service
 *
 * @author lord_nightmare
 */
@Path("proyeccion")
public class AppProyeccionRest {

  @Context
  private UriInfo context;

  /**
   * Creates a new instance of AppProyeccionRest
   */
  public AppProyeccionRest() {
  }

  @GET
  @Path("productos")
  @Produces(MediaType.APPLICATION_JSON)
  public List<String> listaProductosDisponibles(@HeaderParam("Authorization") String token) throws PlaneacionComercialException {
    List<String> productos = new ArrayList<>();
    Connection cnn = null;
    try {
      cnn = ConexionBD.conectar();
      AppUsuarios usuarioAutorizado = TokenUtil.obtenerUsuarioToken(token, cnn);
      List<AppProyeccion> listaProductos = new AppProyeccionDelegado(cnn, null).consultarProyeccionProducto(usuarioAutorizado.getIdUsuario());
      for (int i = 0; i < listaProductos.size(); i++) {
        productos.add(listaProductos.get(i).getProducto());
      }
      Collections.sort(productos);
    } finally {
      ConexionBD.desconectar(cnn);
    }
    return productos;
  }

  @GET
  @Path("consultar/{producto}")
  @Produces(MediaType.APPLICATION_JSON)
  public List<AppProyeccion> listaProyeccionProductos(@PathParam("producto") String producto, @HeaderParam("Authorization") String token) throws PlaneacionComercialException {
    List<AppProyeccion> listaProyeccion = new ArrayList<>();
    Connection cnn = null;
    try {
      cnn = ConexionBD.conectar();
      AppUsuarios usuarioAutorizado = TokenUtil.obtenerUsuarioToken(token, cnn);
      listaProyeccion = new AppProyeccionDelegado(cnn, null).consultarProyeccionProductoUsuario(usuarioAutorizado.getIdUsuario(), producto);
    } finally {
      ConexionBD.desconectar(cnn);
    }
    return listaProyeccion;
  }
}
