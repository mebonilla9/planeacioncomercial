/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package co.movistar.planeacioncomercial.negocio.rest.proveedor;

import co.movistar.planeacioncomercial.negocio.excepciones.PlaneacionComercialException;

import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.ext.ExceptionMapper;
import javax.ws.rs.ext.Provider;

/**
 * @author Lord_Nightmare
 */
@Provider
public class PlaneacionComercialExceptionHandler implements ExceptionMapper<PlaneacionComercialException> {

  @Override
  public Response toResponse(PlaneacionComercialException exception) {
    return Response.serverError().entity(exception.getMensaje()).type(MediaType.APPLICATION_JSON).build();
  }

}
