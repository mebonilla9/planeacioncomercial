/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package co.movistar.planeacioncomercial.negocio.rest.servicios;

import co.movistar.planeacioncomercial.modelo.conexion.ConexionBD;
import co.movistar.planeacioncomercial.modelo.vo.AppUsuarioGeografia;
import co.movistar.planeacioncomercial.negocio.constantes.EMensajes;
import co.movistar.planeacioncomercial.negocio.delegado.AppUsuarioGeografiaDelegado;
import co.movistar.planeacioncomercial.negocio.excepciones.PlaneacionComercialException;
import co.movistar.planeacioncomercial.negocio.util.TokenUtil;

import javax.ws.rs.*;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriInfo;
import java.sql.Connection;
import java.util.List;

/**
 * REST Web Service
 *
 * @author Manuel Ernesto Bonilla Muñoz <mebonilla9@gmail.com>
 */
@Path("usuario_geografia")
public class AppUsuarioGeografiaRest {

  @Context
  private UriInfo context;

  /**
   * Creates a new instance of AppUsuarioGeografiaRest
   */
  public AppUsuarioGeografiaRest() {
  }

  /**
   * PUT method for updating or creating an instance of AppCanalesRest
   *
   * @param content representation for the resource
   * @param token
   * @return
   */
  @PUT
  @Path("insertar")
  @Consumes(MediaType.APPLICATION_JSON)
  public Response insertarUsuarioGeografia(AppUsuarioGeografia content, @HeaderParam("Authorization") String token) {
    Connection cnn = null;
    try {
      cnn = ConexionBD.conectar();
      TokenUtil.obtenerUsuarioToken(token, cnn);
      new AppUsuarioGeografiaDelegado(cnn, null).insertar(content);
      ConexionBD.commit(cnn);
    } catch (PlaneacionComercialException ex) {
      ConexionBD.rollback(cnn);
      return Response.status(Response.Status.BAD_REQUEST).entity(ex.getMensaje()).build();
    } finally {
      ConexionBD.desconectar(cnn);
    }
    return Response.ok(EMensajes.INSERTO).build();
  }

  /**
   * PUT method for updating or creating an instance of AppCanalesRest
   *
   * @param content representation for the resource
   * @param token
   * @return
   */
  @PUT
  @Path("modificar")
  @Consumes(MediaType.APPLICATION_JSON)
  public Response actualizarUsuarioGeografia(AppUsuarioGeografia content, @HeaderParam("Authorization") String token) {
    Connection cnn = null;
    try {
      cnn = ConexionBD.conectar();
      TokenUtil.obtenerUsuarioToken(token, cnn);
      new AppUsuarioGeografiaDelegado(cnn, null).editar(content);
      ConexionBD.commit(cnn);
    } catch (PlaneacionComercialException ex) {
      ConexionBD.rollback(cnn);
      return Response.status(Response.Status.BAD_REQUEST).entity(ex.getMensaje()).build();
    } finally {
      ConexionBD.desconectar(cnn);
    }
    return Response.ok(EMensajes.INSERTO).build();
  }

  /**
   * Retrieves representation of an instance of
   * co.movistar.planeacioncomercial.negocio.rest.servicios.AppCanalesRest
   *
   * @param token
   * @return an instance of AppCanales
   * @throws co.movistar.planeacioncomercial.negocio.excepciones.PlaneacionComercialException
   */
  @GET
  @Path("consultar")
  @Produces(MediaType.APPLICATION_JSON)
  public List<AppUsuarioGeografia> consultarUsuarioGeografia(@HeaderParam("Authorization") String token) throws PlaneacionComercialException {
    List<AppUsuarioGeografia> listaUsuarioGeografia = null;
    Connection cnn = null;
    try {
      cnn = ConexionBD.conectar();
      TokenUtil.obtenerUsuarioToken(token, cnn);
      listaUsuarioGeografia = new AppUsuarioGeografiaDelegado(ConexionBD.conectar(), null).consultar();
      ConexionBD.commit(cnn);
    } finally {
      ConexionBD.desconectar(cnn);
    }
    return listaUsuarioGeografia;
  }

  /**
   * Retrieves representation of an instance of
   * co.movistar.planeacioncomercial.negocio.rest.servicios.AppCanalesRest
   *
   * @param id
   * @param token
   * @return an instance of AppCanales
   * @throws co.movistar.planeacioncomercial.negocio.excepciones.PlaneacionComercialException
   */
  @GET
  @Path("consultar/{id}")
  @Produces(MediaType.APPLICATION_JSON)
  public AppUsuarioGeografia consultarUsuarioGeografia(@PathParam("id") long id, @HeaderParam("Authorization") String token) throws PlaneacionComercialException {
    AppUsuarioGeografia usuarioGeografia = null;
    Connection cnn = null;
    try {
      cnn = ConexionBD.conectar();
      TokenUtil.obtenerUsuarioToken(token, cnn);
      usuarioGeografia = new AppUsuarioGeografiaDelegado(cnn, null).consultar(id);
    } finally {
      ConexionBD.desconectar(cnn);
    }
    return usuarioGeografia;
  }
}
