/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package co.movistar.planeacioncomercial.negocio.rest.servicios;

import co.movistar.planeacioncomercial.modelo.conexion.ConexionBD;
import co.movistar.planeacioncomercial.modelo.dto.DireccionDTO;
import co.movistar.planeacioncomercial.modelo.dto.InfoDisponibilidadDTO;
import co.movistar.planeacioncomercial.modelo.vo.AppDisponibilidad;
import co.movistar.planeacioncomercial.modelo.vo.AppHistoricoDirecciones;
import co.movistar.planeacioncomercial.modelo.vo.AppUsuarios;
import co.movistar.planeacioncomercial.negocio.constantes.EMensajes;
import co.movistar.planeacioncomercial.negocio.delegado.AppDisponibilidadDelegado;
import co.movistar.planeacioncomercial.negocio.delegado.AppHistoricoDireccionesDelegado;
import co.movistar.planeacioncomercial.negocio.excepciones.PlaneacionComercialException;
import co.movistar.planeacioncomercial.negocio.util.GeoCodingUtil;
import co.movistar.planeacioncomercial.negocio.util.TokenUtil;
import com.google.maps.model.LatLng;

import javax.ws.rs.*;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.UriInfo;
import java.sql.Connection;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Pattern;

/**
 * REST Web Service
 *
 * @author Lord_Nightmare
 */
@Path("disponibilidad")
public class AppDisponibilidadRest {

  @Context
  private UriInfo context;

  /**
   * Creates a new instance of AppDisponibilidadRest
   */
  public AppDisponibilidadRest() {
  }

  /**
   * Retrieves representation of an instance of
   * co.movistar.planeacioncomercial.negocio.rest.servicios.AppDisponibilidadRest
   *
   * @param content
   * @param token
   * @return an instance of
   * co.movistar.planeacioncomercial.modelo.vo.AppDisponibilidad
   * @throws co.movistar.planeacioncomercial.negocio.excepciones.PlaneacionComercialException
   */
  @POST
  @Path("consultar")
  @Produces(MediaType.APPLICATION_JSON)
  public InfoDisponibilidadDTO consultarInformacionDisponibilidad(String content, @HeaderParam("Authorization") String token) throws PlaneacionComercialException {
    InfoDisponibilidadDTO infoDisponibilidad = new InfoDisponibilidadDTO();
    Connection cnn = null;
    try {
      cnn = ConexionBD.conectar();
      TokenUtil.obtenerUsuarioToken(token, cnn);
      String[] valores = content.substring(1, content.length() - 1).split(Pattern.quote("?"));
      for (int i = 0; i < valores.length; i++) {
        valores[i] = valores[i].replace("[", "").replace("]", "");
      }
      infoDisponibilidad = new AppDisponibilidadDelegado(cnn, null).consultarInformacionDisponibilidad(valores);
    } finally {
      ConexionBD.desconectar(cnn);
    }
    return infoDisponibilidad;
  }

  /**
   * @param token
   * @return an instance of
   * co.movistar.planeacioncomercial.modelo.vo.AppDisponibilidad
   * @throws co.movistar.planeacioncomercial.negocio.excepciones.PlaneacionComercialException
   */
  @GET
  @Path("departamentos")
  @Produces(MediaType.APPLICATION_JSON)
  public List<AppDisponibilidad> consultarInformacionDepartamentos(@HeaderParam("Authorization") String token) throws PlaneacionComercialException {
    List<AppDisponibilidad> listaDisponibilidad = new ArrayList<>();
    Connection cnn = null;
    try {
      cnn = ConexionBD.conectar();
      TokenUtil.obtenerUsuarioToken(token, cnn);
      listaDisponibilidad = new AppDisponibilidadDelegado(cnn, null).consultarDepartamentos();
    } finally {
      ConexionBD.desconectar(cnn);
    }
    return listaDisponibilidad;
  }

  /**
   * @param token
   * @param departamento
   * @return an instance of
   * co.movistar.planeacioncomercial.modelo.vo.AppDisponibilidad
   * @throws co.movistar.planeacioncomercial.negocio.excepciones.PlaneacionComercialException
   */
  @POST
  @Path("localidades/{departamento}")
  @Produces(MediaType.APPLICATION_JSON)
  public List<AppDisponibilidad> consultarInformacionLocalidades(@HeaderParam("Authorization") String token, @PathParam("departamento") String departamento) throws PlaneacionComercialException {
    List<AppDisponibilidad> listaDisponibilidad = new ArrayList<>();
    Connection cnn = null;
    try {
      cnn = ConexionBD.conectar();
      TokenUtil.obtenerUsuarioToken(token, cnn);
      listaDisponibilidad = new AppDisponibilidadDelegado(cnn, null).consultarLocalidades(departamento);
    } finally {
      ConexionBD.desconectar(cnn);
    }
    return listaDisponibilidad;
  }

  /**
   * @param token
   * @param departamento
   * @param localidad
   * @return an instance of
   * co.movistar.planeacioncomercial.modelo.vo.AppDisponibilidad
   * @throws co.movistar.planeacioncomercial.negocio.excepciones.PlaneacionComercialException
   */
  @GET
  @Path("distritos/{departamento}/{localidad}")
  @Produces(MediaType.APPLICATION_JSON)
  public List<AppDisponibilidad> consultarInformacionDistritos(@HeaderParam("Authorization") String token, @PathParam("departamento") String departamento, @PathParam("localidad") String localidad) throws PlaneacionComercialException {
    List<AppDisponibilidad> listaDisponibilidad = new ArrayList<>();
    Connection cnn = null;
    try {
      cnn = ConexionBD.conectar();
      TokenUtil.obtenerUsuarioToken(token, cnn);
      listaDisponibilidad = new AppDisponibilidadDelegado(cnn, null).consultarDistritos(departamento, localidad);
    } finally {
      ConexionBD.desconectar(cnn);
    }
    return listaDisponibilidad;
  }

  /**
   * @param token
   * @param distrito
   * @return an instance of
   * co.movistar.planeacioncomercial.modelo.vo.AppDisponibilidad
   * @throws co.movistar.planeacioncomercial.negocio.excepciones.PlaneacionComercialException
   */
  @GET
  @Path("armarios/{distrito}")
  @Produces(MediaType.APPLICATION_JSON)
  public List<AppDisponibilidad> consultarInformacionArmarios(@HeaderParam("Authorization") String token, @PathParam("distrito") String distrito) throws PlaneacionComercialException {
    List<AppDisponibilidad> listaDisponibilidad = new ArrayList<>();
    Connection cnn = null;
    try {
      cnn = ConexionBD.conectar();
      TokenUtil.obtenerUsuarioToken(token, cnn);
      listaDisponibilidad = new AppDisponibilidadDelegado(cnn, null).consultarArmarios(distrito);
    } finally {
      ConexionBD.desconectar(cnn);
    }
    return listaDisponibilidad;
  }

  /**
   * @param token
   * @param distrito
   * @param armario
   * @return an instance of
   * co.movistar.planeacioncomercial.modelo.vo.AppDisponibilidad
   * @throws co.movistar.planeacioncomercial.negocio.excepciones.PlaneacionComercialException
   */
  @GET
  @Path("cajas/{distrito}/{armario}")
  @Produces(MediaType.APPLICATION_JSON)
  public List<AppDisponibilidad> consultarInformacionCajas(@HeaderParam("Authorization") String token, @PathParam("distrito") String distrito, @PathParam("armario") String armario) throws PlaneacionComercialException {
    List<AppDisponibilidad> listaDisponibilidad = new ArrayList<>();
    Connection cnn = null;
    try {
      cnn = ConexionBD.conectar();
      TokenUtil.obtenerUsuarioToken(token, cnn);
      listaDisponibilidad = new AppDisponibilidadDelegado(cnn, null).consultarCajas(distrito, armario);
    } finally {
      ConexionBD.desconectar(cnn);
    }
    return listaDisponibilidad;
  }

  @POST
  @Path("cajas/direccion")
  @Consumes(MediaType.APPLICATION_JSON)
  @Produces(MediaType.APPLICATION_JSON)
  public InfoDisponibilidadDTO consultarInformacionDirecciones(@HeaderParam("Authorization") String token, DireccionDTO direccion) throws PlaneacionComercialException {
    InfoDisponibilidadDTO infoDisponibilidad = new InfoDisponibilidadDTO();
    Connection cnn = null;
    try {
      cnn = ConexionBD.conectar();
      AppUsuarios usuarioAutorizado = TokenUtil.obtenerUsuarioToken(token, cnn);
      LatLng coordenadas = GeoCodingUtil.obtenerCoordenadas(direccion.toString());
      AppHistoricoDirecciones registroDirUsauario = new AppHistoricoDirecciones();
      registroDirUsauario.setIdUsuario(usuarioAutorizado.getIdUsuario());
      registroDirUsauario.setCiudad(direccion.getCiudad());
      registroDirUsauario.setDireccion(direccion.toString());
      registroDirUsauario.setCoordenadaX(coordenadas.lng + "");
      registroDirUsauario.setCoordenadaY(coordenadas.lat + "");
      new AppHistoricoDireccionesDelegado(cnn, null).insertar(registroDirUsauario);
      ConexionBD.commit(cnn);
      infoDisponibilidad = new AppDisponibilidadDelegado(cnn, null).consultarDisponibilidadDireccion(coordenadas);
    } catch (NullPointerException e) {
      throw new PlaneacionComercialException(EMensajes.NO_RESULTADOS);
    } finally {
      ConexionBD.desconectar(cnn);
    }
    return infoDisponibilidad;
  }

  @POST
  @Path("localidades/coor/{departamento}")
  @Produces(MediaType.APPLICATION_JSON)
  public List<AppDisponibilidad> consultarInformacionLocalidadesCoordenadas(@HeaderParam("Authorization") String token, @PathParam("departamento") String departamento) throws PlaneacionComercialException {
    List<AppDisponibilidad> listaDisponibilidad = new ArrayList<>();
    Connection cnn = null;
    try {
      cnn = ConexionBD.conectar();
      TokenUtil.obtenerUsuarioToken(token, cnn);
      listaDisponibilidad = new AppDisponibilidadDelegado(cnn, null).consultarLocalidadCoordenadas(departamento);
    } finally {
      ConexionBD.desconectar(cnn);
    }
    return listaDisponibilidad;
  }
}
