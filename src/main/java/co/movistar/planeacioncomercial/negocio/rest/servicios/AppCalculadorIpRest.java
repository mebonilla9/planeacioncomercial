/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package co.movistar.planeacioncomercial.negocio.rest.servicios;

import co.movistar.planeacioncomercial.modelo.conexion.ConexionBD;
import co.movistar.planeacioncomercial.modelo.dto.InformacionCalcularIpDTO;
import co.movistar.planeacioncomercial.modelo.vo.AppCabecera;
import co.movistar.planeacioncomercial.modelo.vo.AppGestionado;
import co.movistar.planeacioncomercial.modelo.vo.AppNoGestionado;
import co.movistar.planeacioncomercial.modelo.vo.AppUsuarios;
import co.movistar.planeacioncomercial.negocio.constantes.EMensajes;
import co.movistar.planeacioncomercial.negocio.delegado.AppCabeceraDelegado;
import co.movistar.planeacioncomercial.negocio.delegado.AppGestionadoDelegado;
import co.movistar.planeacioncomercial.negocio.delegado.AppNoGestionadoDelegado;
import co.movistar.planeacioncomercial.negocio.excepciones.PlaneacionComercialException;
import co.movistar.planeacioncomercial.negocio.util.TokenUtil;

import javax.ws.rs.*;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.UriInfo;
import java.sql.Connection;
import java.util.List;

/**
 * REST Web Service
 *
 * @author Lord_Nightmare
 */
@Path("calculadorip")
public class AppCalculadorIpRest {

  @Context
  private UriInfo context;

  /**
   * Creates a new instance of CalculadorIpRest
   */
  public AppCalculadorIpRest() {
  }

  /**
   * Retrieves representation of an instance of
   * co.movistar.planeacioncomercial.negocio.rest.servicios.AppCalculadorIpRest
   *
   * @param authToken
   * @return an instance of java.lang.String
   * @throws co.movistar.planeacioncomercial.negocio.excepciones.PlaneacionComercialException
   */
  @GET
  @Path("consultar")
  @Produces(MediaType.APPLICATION_JSON)
  public InformacionCalcularIpDTO consultarInformacionCalculador(@HeaderParam("Authorization") String authToken) throws PlaneacionComercialException {
    InformacionCalcularIpDTO infoCalcularIp = new InformacionCalcularIpDTO();
    Connection cnn = null;
    try {
      cnn = ConexionBD.conectar();
      AppUsuarios usuarioAutorizado = TokenUtil.obtenerUsuarioToken(authToken, cnn);
      AppCabecera cabecera = new AppCabeceraDelegado(cnn, null).consultarInfoCabecera(usuarioAutorizado.getIdUsuario());
      infoCalcularIp.setListaAppGestionado(new AppGestionadoDelegado(cnn, null).consultarInfoGestionado(cabecera.getIdUsuario()));
      //infoCalcularIp.setListaAppNoGestionado(new AppNoGestionadoDelegado(cnn, null).consultarInfoNoGestionado(cabecera.getIdUsuario()));
      infoCalcularIp.setAppCabecera(cabecera);
    } catch (NullPointerException e) {
      throw new PlaneacionComercialException(EMensajes.NO_RESULTADOS);
    } finally {
      ConexionBD.desconectar(cnn);
    }
    return infoCalcularIp;
  }

  /**
   * @param infoCalcular
   * @param authToken
   * @return
   * @throws PlaneacionComercialException
   */
  @POST
  @Path("actualizar")
  @Produces(MediaType.APPLICATION_JSON)
  public InformacionCalcularIpDTO actualizarInformacionCalculador(InformacionCalcularIpDTO infoCalcular, @HeaderParam("Authorization") String authToken) throws PlaneacionComercialException {
    InformacionCalcularIpDTO infoCalcularActualizado = null;
    Connection cnn = null;
    try {
      cnn = ConexionBD.conectar();
      AppUsuarios usuarioAutorizado = TokenUtil.obtenerUsuarioToken(authToken, cnn);

      List<AppGestionado> listaGestionadoActualizar = infoCalcular.getListaAppGestionado();
      AppGestionadoDelegado gestionadoDelegado = new AppGestionadoDelegado(cnn, null);
      for (int i = 0; i < listaGestionadoActualizar.size(); i++) {
        gestionadoDelegado.editarEjecucion(listaGestionadoActualizar.get(i));
      }

      /*List<AppNoGestionado> listaNoGestionadoActualizar = infoCalcular.getListaAppNoGestionado();
      AppNoGestionadoDelegado noGestionadoDelegado = new AppNoGestionadoDelegado(cnn, null);
      for (int i = 0; i < listaNoGestionadoActualizar.size(); i++) {
        noGestionadoDelegado.editarEjecucion(listaNoGestionadoActualizar.get(i));
      }*/

      new AppCabeceraDelegado(cnn, null).ejecutarActualizacionCalculador(usuarioAutorizado.getIdUsuario());
      ConexionBD.commit(cnn);
      System.out.println("------------- Confirmado la base de datos ----------------------");
      infoCalcularActualizado = new InformacionCalcularIpDTO();

      AppCabecera cabecera = new AppCabeceraDelegado(cnn, null).consultarInfoCabecera(usuarioAutorizado.getIdUsuario());
      infoCalcularActualizado.setListaAppGestionado(new AppGestionadoDelegado(cnn, null).consultarInfoGestionado(cabecera.getIdUsuario()));
      //infoCalcularActualizado.setListaAppNoGestionado(new AppNoGestionadoDelegado(cnn, null).consultarInfoNoGestionado(cabecera.getIdUsuario()));
      infoCalcularActualizado.setAppCabecera(cabecera);
    } finally {
      ConexionBD.desconectar(cnn);
    }
    return infoCalcularActualizado;
  }
}
