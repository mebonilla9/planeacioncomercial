/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package co.movistar.planeacioncomercial.negocio.rest.servicios;

import co.movistar.planeacioncomercial.modelo.conexion.ConexionBD;
import co.movistar.planeacioncomercial.modelo.vo.AppNovedad;
import co.movistar.planeacioncomercial.negocio.constantes.ECanales;
import co.movistar.planeacioncomercial.negocio.delegado.AppNovedadDelegado;
import co.movistar.planeacioncomercial.negocio.excepciones.PlaneacionComercialException;

import javax.ws.rs.*;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.UriInfo;
import java.sql.Connection;
import java.util.List;

/**
 * REST Web Service
 *
 * @author Lord_Nightmare
 */
@Path("novedad")
public class AppNovedadRest {

  @Context
  private UriInfo context;

  /**
   * Creates a new instance of AppNovedadRest
   */
  public AppNovedadRest() {
  }

  /**
   * Retrieves representation of an instance of
   * co.movistar.planeacioncomercial.negocio.rest.servicios.AppNovedadRest
   *
   * @param token
   * @return an instance of
   * co.movistar.planeacioncomercial.modelo.vo.AppNovedad
   */
  @GET
  @Path("centros/consultar")
  @Produces(MediaType.APPLICATION_JSON)
  public List<AppNovedad> consultarNovedades(@HeaderParam("Authorization") String token) throws PlaneacionComercialException {
    List<AppNovedad> listaNovedades = null;
    Connection cnn = null;
    try {
      cnn = ConexionBD.conectar();
      listaNovedades = new AppNovedadDelegado(cnn, null).consultarCentroExperiencia();
    } finally {
      ConexionBD.desconectar(cnn);
    }
    return listaNovedades;
  }

  @GET
  @Path("venta/consultar")
  @Produces(MediaType.APPLICATION_JSON)
  public List<AppNovedad> consultarNovedadesVenta(@HeaderParam("Authorization") String token) throws PlaneacionComercialException {
    List<AppNovedad> listaNovedades = null;
    Connection cnn = null;
    try {
      cnn = ConexionBD.conectar();
      listaNovedades = new AppNovedadDelegado(cnn, null).consultarVentaDirecta();
    } finally {
      ConexionBD.desconectar(cnn);
    }
    return listaNovedades;
  }

  /**
   * Retrieves representation of an instance of
   * co.movistar.planeacioncomercial.negocio.rest.servicios.AppNovedadRest
   *
   * @param token
   * @return an instance of
   * co.movistar.planeacioncomercial.modelo.vo.AppNovedad
   */
  @GET
  @Path("centros/consultar/{modulo}")
  @Produces(MediaType.APPLICATION_JSON)
  public List<AppNovedad> consultarNovedadesCanal(@HeaderParam("Authorization") String token, @PathParam("modulo") String modulo) throws PlaneacionComercialException {
    List<AppNovedad> listaNovedades = null;
    Connection cnn = null;
    try {
      cnn = ConexionBD.conectar();
      listaNovedades = new AppNovedadDelegado(cnn, null).consultarCentroExperiencia();
    } finally {
      ConexionBD.desconectar(cnn);
    }
    return listaNovedades;
  }

  @GET
  @Path("consultar/{modulo}")
  @Produces(MediaType.APPLICATION_JSON)
  public List<AppNovedad> listarNovedades(@HeaderParam("Authorization") String token, @PathParam("modulo") ECanales canal) throws PlaneacionComercialException{
    List<AppNovedad> listaNovedades = null;
    Connection cnn = null;
    try {
      cnn = ConexionBD.conectar();
      listaNovedades = new AppNovedadDelegado(cnn, null).consultarNovedadesModulo(canal);
    } finally {
      ConexionBD.desconectar(cnn);
    }
    return listaNovedades;
  }
}
