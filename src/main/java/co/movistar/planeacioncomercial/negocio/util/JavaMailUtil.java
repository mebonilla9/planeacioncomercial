/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package co.movistar.planeacioncomercial.negocio.util;

import co.movistar.planeacioncomercial.negocio.constantes.EMensajes;
import co.movistar.planeacioncomercial.negocio.excepciones.PlaneacionComercialException;

import javax.mail.*;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import java.util.Properties;

/**
 * @author lord_nightmare
 */
public final class JavaMailUtil {

  //private final static String CUENTA_CORREO = "planeacioncomercial@telefonica.co";
  private final static String CUENTA_CORREO = "planeacion.comercial.co@gmail.com";

  public static void envioCorreo(String destinatario, String tipo, String mensajeAdd) throws PlaneacionComercialException {
    enviarCorreo(destinatario, FileLoadUtil.obtenerContrasenaArchivo(), tipo, mensajeAdd);
  }

  private static void enviarCorreo(String destino, String contrasena, String tipo, String mensajeAdd) throws PlaneacionComercialException {

    Properties props = new Properties();

    props.put("mail.smtp.host", "smtp.gmail.com");
    props.put("mail.smtp.socketFactory.port", "465");
    props.put("mail.smtp.socketFactory.class",
            "javax.net.ssl.SSLSocketFactory");
    props.put("mail.smtp.auth", "true");
    props.put("mail.smtp.port", "465");

    Session session = Session.getInstance(props,
            new javax.mail.Authenticator() {
              protected PasswordAuthentication getPasswordAuthentication() {
                return new PasswordAuthentication(CUENTA_CORREO, contrasena);
              }
            }
    );
    try {

      MimeMessage message = new MimeMessage(session);
      message.setFrom(new InternetAddress(CUENTA_CORREO));
      message.setRecipients(Message.RecipientType.TO, InternetAddress.parse(destino));
      message.setSubject(tipo.equals("correo") ? "Nuevo usuario a cargo" : "Reestablecimiento de contraseña");
      message.setContent(tipo.equals("correo") ? HtmlUtil.leerPlantillaUsuario().replace("__MENSAJE__", mensajeAdd) : HtmlUtil.leerPlantillaCorreo(), "text/html; charset=utf-8");
      Transport.send(message);

    } catch (MessagingException e) {
      e.printStackTrace(System.err);
      throw new PlaneacionComercialException(EMensajes.ERROR_DIRECCION_ENVIO_CORREO);
    }
  }
}
