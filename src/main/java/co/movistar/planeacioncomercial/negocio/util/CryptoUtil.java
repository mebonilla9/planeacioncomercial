/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package co.movistar.planeacioncomercial.negocio.util;

import co.movistar.planeacioncomercial.negocio.constantes.EMensajes;
import co.movistar.planeacioncomercial.negocio.excepciones.PlaneacionComercialException;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

/**
 * @author lord_nightmare
 */
public final class CryptoUtil {

  private static final String CONTRASENA_DEFECTO = "Abcd123456!";

  public static String generarContrasenaDefecto() throws PlaneacionComercialException {
    String cifrado = "";
    try {
      String cifradoUno = cifrarSha384(CONTRASENA_DEFECTO);
      cifrado = cifrarSha384(cifradoUno);

    } catch (NoSuchAlgorithmException e) {
      throw new PlaneacionComercialException(EMensajes.ERROR_CIFRADO_NO_ENCONTRADO);
    }
    return cifrado;
  }

  private static String cifrarSha384(String password) throws NoSuchAlgorithmException {
    MessageDigest md = MessageDigest.getInstance("SHA-384");
    md.update(password.getBytes());

    byte[] byteData = md.digest();

    StringBuilder sb = new StringBuilder();
    for (int i = 0; i < byteData.length; i++) {
      sb.append(Integer.toString((byteData[i] & 0xff) + 0x100, 16).substring(1));
    }
    return sb.toString();
  }

  public static String cifrarContrasena(String contrasena) throws PlaneacionComercialException {
    String contrasenaCifrada = "";
    try {
      contrasenaCifrada = cifrarSha384(contrasena);
    } catch (NoSuchAlgorithmException e) {
      throw new PlaneacionComercialException(EMensajes.ERROR_CIFRADO_NO_ENCONTRADO);
    }
    return contrasenaCifrada;
  }
}
