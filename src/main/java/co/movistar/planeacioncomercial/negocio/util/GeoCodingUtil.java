/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package co.movistar.planeacioncomercial.negocio.util;

import co.movistar.planeacioncomercial.negocio.constantes.EMensajes;
import co.movistar.planeacioncomercial.negocio.excepciones.PlaneacionComercialException;
import com.google.maps.GeoApiContext;
import com.google.maps.GeocodingApi;
import com.google.maps.errors.ApiException;
import com.google.maps.model.GeocodingResult;
import com.google.maps.model.LatLng;

import java.io.IOException;

/**
 * @author lord_nightmare
 */
public final class GeoCodingUtil {

  private final static String API_KEY = "AIzaSyDhwwrPEkul8Da0UrcjJGvydVSGsyPIcg8";

  public static LatLng obtenerCoordenadas(String direccion) throws PlaneacionComercialException {
    LatLng coordenadaEncontrada = new LatLng(0, 0);
    try {
      GeoApiContext contexto = new GeoApiContext().setApiKey(API_KEY);
      GeocodingResult[] resultados = GeocodingApi.geocode(contexto, direccion).await();
      if (resultados.length >= 1) {
        coordenadaEncontrada = retornarObjetoCoordenadas(resultados);
      }
    } catch (ApiException | IOException | InterruptedException e) {
      throw new PlaneacionComercialException(EMensajes.ERROR_DIRECCION_GOOGLE_GEOCODING);
    }
    return coordenadaEncontrada;
  }

  private static LatLng retornarObjetoCoordenadas(GeocodingResult[] resultados) {
    LatLng coordenada = null;
    for (GeocodingResult resultado : resultados) {
      coordenada = resultado.geometry.location;
    }
    return coordenada;
  }

}
