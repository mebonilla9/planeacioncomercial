/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package co.movistar.planeacioncomercial.negocio.util;

import co.movistar.planeacioncomercial.negocio.constantes.EMensajes;
import co.movistar.planeacioncomercial.negocio.excepciones.PlaneacionComercialException;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URL;

/**
 * @author lord_nightmare
 */
public final class HtmlUtil {

  public static String leerPlantillaCorreo() throws PlaneacionComercialException {
    StringBuilder sb = new StringBuilder();
    try {
      //URL appreactor = new URL("http://186.116.14.117:8080/planeacioncomercial/email-password.html");
      URL appreactor = new URL("http://localhost:8080/planeacioncomercial/email-password.html");
      BufferedReader in = new BufferedReader(
              new InputStreamReader(appreactor.openStream())
      );
      String str;
      while ((str = in.readLine()) != null) {
        sb.append(str);
      }
      in.close();
    } catch (IOException e) {
      throw new PlaneacionComercialException(EMensajes.ERROR_PLANTILLA_CORREO);
    }
    return sb.toString();
  }

  public static String leerPlantillaUsuario() throws PlaneacionComercialException {
    StringBuilder sb = new StringBuilder();
    try {
      URL appreactor = new URL("http://localhost:8080/planeacioncomercial/email.jefe.min.html");
      BufferedReader in = new BufferedReader(
              new InputStreamReader(appreactor.openStream())
      );
      String str;
      while ((str = in.readLine()) != null) {
        sb.append(str);
      }
      in.close();
    } catch (IOException e) {
      throw new PlaneacionComercialException(EMensajes.ERROR_PLANTILLA_CORREO);
    }
    return sb.toString();
  }

}
