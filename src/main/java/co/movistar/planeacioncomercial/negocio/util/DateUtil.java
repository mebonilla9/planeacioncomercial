package co.movistar.planeacioncomercial.negocio.util;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.ZoneOffset;
import java.util.Calendar;
import java.util.Date;

/**
 * @author Manuel Ernesto Bonilla Muñoz <mebonilla9@gmail.com>
 */
public final class DateUtil {

  public static java.sql.Date parseDate(java.util.Date fecha) {
    if (fecha == null) {
      return null;
    }
    return new java.sql.Date(fecha.getTime());
  }

  public static java.sql.Timestamp parseTimestamp(java.util.Date fecha) {
    if (fecha == null) {
      return null;
    }
    return new java.sql.Timestamp(fecha.getTime());
  }

  public static java.util.Date obtenerFechaExpiracionToken(java.util.Date fechaActual) {
    LocalDateTime fechaExpiracion = LocalDateTime.from(fechaActual.toInstant().atZone(ZoneId.of("UTC")).plusHours(12));
    return Date.from(fechaExpiracion.atZone(ZoneId.systemDefault()).toInstant());
  }

  public static String obtenerFechaActualTexto() {
    Date today = new Date();
    Calendar cal = Calendar.getInstance();
    cal.setTime(today);
    StringBuilder sb = new StringBuilder();
    sb.append(cal.get(Calendar.DAY_OF_MONTH));
    sb.append("-");
    sb.append(cal.get(Calendar.MONTH));
    sb.append("-");
    sb.append(cal.get(Calendar.YEAR));
    return sb.toString();
  }

  public static Date balancearFecha(Date fecha) {
    LocalDateTime nuevaFecha = LocalDateTime.ofInstant(fecha.toInstant(), ZoneId.systemDefault()).plusDays(1);
    return Date.from(nuevaFecha.toInstant(ZoneOffset.UTC));
  }

  public static LocalDate obtenerFechaLocal(java.util.Date fecha) {
    LocalDate fechaLocal;

    if (fecha instanceof java.sql.Date) {
      fechaLocal = ((java.sql.Date) fecha).toLocalDate();
    } else {
      fechaLocal = fecha.toInstant().atZone(ZoneId.systemDefault()).toLocalDate();
    }
    return fechaLocal;
  }

  public static Date obtenerFechaClasica(LocalDate fecha) {
    return Date.from(fecha.atStartOfDay(ZoneId.systemDefault()).toInstant());
  }
}
