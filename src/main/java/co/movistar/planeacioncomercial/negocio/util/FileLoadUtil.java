/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package co.movistar.planeacioncomercial.negocio.util;

import co.movistar.planeacioncomercial.negocio.constantes.EMensajes;
import co.movistar.planeacioncomercial.negocio.excepciones.PlaneacionComercialException;
import org.apache.commons.io.FileUtils;

import java.io.*;

/**
 * @author lord_nightmare
 */
public final class FileLoadUtil {

  public static void copiarCarpetaPublica(String rutaCargaApp, String homeUploadDir) throws IOException {
    FileUtils.deleteDirectory(new File(rutaCargaApp));
    File origen = new File(homeUploadDir);
    File destino = new File(rutaCargaApp);
    FileUtils.copyDirectory(origen, destino);
  }

  public static String obtenerContrasenaArchivo() throws PlaneacionComercialException {
    StringBuilder cna = new StringBuilder();
    try {
      BufferedReader br = new BufferedReader(
              new InputStreamReader(
                      new FileInputStream(
                              new File(System.getProperty("user.home") + File.separator + "contrasena.txt")
                      )
              )
      );
      String str;
      while ((str = br.readLine()) != null) {
        cna.append(str);
      }
      br.close();
    } catch (IOException e) {
      e.printStackTrace(System.err);
      throw new PlaneacionComercialException(EMensajes.ERROR_PLANTILLA_CORREO);
    }
    return cna.toString();
  }
}
