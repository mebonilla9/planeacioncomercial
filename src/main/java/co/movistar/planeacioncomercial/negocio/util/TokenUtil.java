/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package co.movistar.planeacioncomercial.negocio.util;

import co.movistar.planeacioncomercial.modelo.vo.AppUsuarios;
import co.movistar.planeacioncomercial.negocio.constantes.EMensajes;
import co.movistar.planeacioncomercial.negocio.delegado.AppUsuariosDelegado;
import co.movistar.planeacioncomercial.negocio.excepciones.PlaneacionComercialException;
import io.jsonwebtoken.*;

import java.io.UnsupportedEncodingException;
import java.nio.charset.StandardCharsets;
import java.sql.Connection;
import java.util.Date;
import java.util.regex.Pattern;

/**
 * @author Manuel Ernesto Bonilla Muñoz <mebonilla9@gmail.com>
 */
public final class TokenUtil {

  static {
    // espacio para agregar el MacProvider y almacenarlo en el ApplicationContext
  }

  public static String generarToken(AppUsuarios usuario) throws PlaneacionComercialException {
    String tokenGenerado = null;
    try {
      tokenGenerado = Jwts.builder()
              .setSubject(usuario.getIdUsuario().toString())
              .setId(usuario.getCodInterno())
              .setIssuedAt(new Date())
              .setExpiration(DateUtil.obtenerFechaExpiracionToken(new Date()))
              .claim("nombre", usuario.getNombre())
              .claim("correo", usuario.getCorreo())
              .signWith(SignatureAlgorithm.HS256, "secret".getBytes(StandardCharsets.UTF_8))
              .compact();
    } catch (NullPointerException ex) {
      throw new PlaneacionComercialException(EMensajes.ERROR_GENERACION_TOKEN);
    }
    return "Bearer " + tokenGenerado;
  }

  public static AppUsuarios obtenerUsuarioToken(String token, Connection cnn) throws PlaneacionComercialException {
    AppUsuarios usuario = null;
    try {
      String[] tokenObtenido = token.split(Pattern.quote(" "));
      Jws<Claims> tokenConvertido = Jwts.parser()
              .setSigningKey("secret".getBytes(StandardCharsets.UTF_8))
              .parseClaimsJws(tokenObtenido[1]);
      Date fechaExpiracion = tokenConvertido.getBody().getExpiration();
      if (fechaExpiracion.before(new Date())) {
        throw new PlaneacionComercialException(EMensajes.ERROR_EXPIRACION_TOKEN);
      }
      usuario = new AppUsuariosDelegado(cnn, null).consultar(Long.parseLong(tokenConvertido.getBody().getSubject()));
    } catch (ExpiredJwtException | SignatureException | IllegalArgumentException ex) {
      if (ex.getCause() instanceof ExpiredJwtException) {
        throw new PlaneacionComercialException(EMensajes.ERROR_EXPIRACION_TOKEN);
      }
      throw new PlaneacionComercialException(EMensajes.ERROR_AUTENTICACION_TOKEN);
    }
    return usuario;
  }

  public static Date obtenerFechaExpiracionToken(String token) throws PlaneacionComercialException {
    Date fechaExpiracion = null;
    try {
      String[] tokenObtenido = token.split(Pattern.quote(" "));
      Jws<Claims> tokenConvertido = Jwts.parser()
              .setSigningKey("secret".getBytes(StandardCharsets.UTF_8))
              .parseClaimsJws(tokenObtenido[1]);
      fechaExpiracion = tokenConvertido.getBody().getExpiration();
    } catch (ExpiredJwtException | ArrayIndexOutOfBoundsException ex) {
      throw new PlaneacionComercialException(EMensajes.ERROR_EXPIRACION_TOKEN);
    }
    return fechaExpiracion;
  }
}
