/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package co.movistar.planeacioncomercial.negocio.servlets;

import co.movistar.planeacioncomercial.modelo.conexion.ConexionBD;
import co.movistar.planeacioncomercial.modelo.dto.RespuestaDTO;
import co.movistar.planeacioncomercial.modelo.vo.AppUsuarios;
import co.movistar.planeacioncomercial.negocio.constantes.EAcciones;
import co.movistar.planeacioncomercial.negocio.delegado.AppUsuariosDelegado;
import co.movistar.planeacioncomercial.negocio.excepciones.PlaneacionComercialException;
import com.google.gson.Gson;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Connection;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * @author Manuel Ernesto Bonilla Muñoz <mebonilla9@gmail.com>
 */
public abstract class GenericoServlet extends HttpServlet {

  /**
   * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
   * methods.
   *
   * @param request  servlet request
   * @param response servlet response
   * @throws ServletException if a servlet-specific error occurs
   * @throws IOException      if an I/O error occurs
   */
  protected void processRequest(HttpServletRequest request, HttpServletResponse response)
          throws ServletException, IOException {
    response.addHeader("Access-Control-Allow-Origin", "*");
    response.setContentType("application/json");
    try (PrintWriter out = response.getWriter()) {
      Connection cnn = null;
      RespuestaDTO respuesta;
      EAcciones accion = getAccion(request.getServletPath());
      try {
        cnn = ConexionBD.conectar();
        respuesta = procesar(request, accion, cnn);

        ConexionBD.commit(cnn);
      } catch (PlaneacionComercialException ex) {
        respuesta = new RespuestaDTO();
        respuesta.setCodigo(ex.getCodigo());
        respuesta.setMensaje(ex.getMensaje());
      } finally {
        ConexionBD.desconectar(cnn);
      }
      out.print(new Gson().toJson(respuesta));
    }
  }

  @Override
  protected void doPost(HttpServletRequest request, HttpServletResponse response)
          throws ServletException, IOException {
    try {
      processRequest(request, response);
      Connection c = ConexionBD.conectar();
      List<AppUsuarios> listaUsuarios = new AppUsuariosDelegado(c, null).consultar();
      for (int i = 0; i < listaUsuarios.size(); i++) {
        AppUsuarios au = listaUsuarios.get(i);
        System.out.println(au.getNombre());
      }
    } catch (PlaneacionComercialException ex) {
      Logger.getLogger(GenericoServlet.class.getName()).log(Level.SEVERE, null, ex);
    }
  }

  @Override
  protected void doGet(HttpServletRequest request, HttpServletResponse response)
          throws ServletException, IOException {
    processRequest(request, response);
  }

  public abstract RespuestaDTO procesar(HttpServletRequest request, EAcciones accion, Connection cnn) throws PlaneacionComercialException;

  private EAcciones getAccion(String url) {
    return EAcciones.parse(getURLAccion(url));
  }

  private String getURLAccion(String url) {
    String[] partes = url.split("/");
    String accion = "/" + partes[partes.length - 1];
    return accion;
  }

}
