/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package co.movistar.planeacioncomercial.negocio.servlets.context;

import co.movistar.planeacioncomercial.negocio.util.FileLoadUtil;

import javax.servlet.ServletContext;
import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;
import javax.servlet.annotation.WebListener;
import java.io.File;
import java.io.IOException;

/**
 * @author lord_nightmare
 */
@WebListener
public class PlaneacionComercialServletContextListener implements ServletContextListener {

  private static final String UPLOAD_DIR = "archivos";

  private static final String HOME_UPLOAD_DIR = System.getProperty("user.home") + File.separator + "archivos_planecom";

  @Override
  public void contextInitialized(ServletContextEvent sce) {
    ServletContext contexto = sce.getServletContext();
    //String uploadDir = contexto.getInitParameter(UPLOAD_DIR);
    //String homeUploadDir = contexto.getInitParameter(HOME_UPLOAD_DIR);
    contexto.setAttribute("uploadDir", UPLOAD_DIR);
    contexto.setAttribute("homeUploadDir", HOME_UPLOAD_DIR);
    copiarCarpetaPublica(contexto, UPLOAD_DIR, HOME_UPLOAD_DIR);
    System.out.println("Me inicialize al desplegar");
  }

  @Override
  public void contextDestroyed(ServletContextEvent sce) {
    ServletContext contexto = sce.getServletContext();
    contexto.removeAttribute("uploadDir");
    contexto.removeAttribute("homeUploadDir");
    System.out.println("Me destrui al eliminar despliegue");
  }

  private void copiarCarpetaPublica(ServletContext contexto, String uploadDir, String homeUploadDir) {
    try {
      String rutaAbsolutaApp = contexto.getRealPath("");
      String rutaCargaApp = rutaAbsolutaApp + File.separator + uploadDir;
      File archivoCarga = new File(rutaCargaApp);
      File archivoHomeCarga = new File(homeUploadDir);
      if (archivoCarga.exists() || !archivoHomeCarga.exists()) {
        return;
      }
      FileLoadUtil.copiarCarpetaPublica(rutaCargaApp, homeUploadDir);
    } catch (IOException ex) {
      ex.printStackTrace(System.err);
      System.out.println("Error en la migracion de imagenes a carpeta publica, causa: " + ex.getMessage());
    }
  }

}
