/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package co.movistar.planeacioncomercial.negocio.servlets;

import co.movistar.planeacioncomercial.modelo.dto.RespuestaDTO;
import co.movistar.planeacioncomercial.modelo.vo.AppUsuarios;
import co.movistar.planeacioncomercial.negocio.constantes.EAcciones;
import co.movistar.planeacioncomercial.negocio.constantes.EMensajes;
import co.movistar.planeacioncomercial.negocio.constantes.Routing;
import co.movistar.planeacioncomercial.negocio.delegado.AppUsuariosDelegado;
import co.movistar.planeacioncomercial.negocio.excepciones.PlaneacionComercialException;

import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.sql.Connection;

/**
 * @author Lord_Nightmare
 */
@WebServlet(name = "IndexServlet",
        urlPatterns = {
                Routing.Index.LOGIN,
                Routing.Index.LOGOUT
        })
public class IndexServlet extends GenericoServlet {

  @Override
  public RespuestaDTO procesar(HttpServletRequest request, EAcciones accion, Connection cnn) throws PlaneacionComercialException {
    RespuestaDTO respuesta = null;
    switch (accion) {
      case LOGIN:
        iniciarSesion(request, cnn);
        respuesta = new RespuestaDTO(EMensajes.CONSULTO);
        break;
      case LOGOUT:
        eliminarSesion(request);
        respuesta = new RespuestaDTO(EMensajes.CONSULTO);
        break;
    }
    return respuesta;
  }

  private void iniciarSesion(HttpServletRequest request, Connection cnn) throws PlaneacionComercialException {
    String correo = request.getParameter("correo");
    String contrasena = request.getParameter("contrasena");
    if (correo == null && contrasena == null) {
      throw new PlaneacionComercialException(EMensajes.ERROR_FORMULARIO_VACIO);
    }
    AppUsuariosDelegado usuariosDelegado = new AppUsuariosDelegado(cnn, null);
    AppUsuarios usuarioLogueado = usuariosDelegado.autenticarUsuario(correo, contrasena);
    if (usuarioLogueado.getIdUsuario() == null) {
      throw new PlaneacionComercialException(EMensajes.ERROR_CONSULTAR);
    }
    HttpSession sesion = request.getSession();
    sesion.setAttribute("idUsuario", usuarioLogueado.getIdUsuario());
    sesion.setAttribute("nombreUsuario", usuarioLogueado.getNombre());
  }

  private void eliminarSesion(HttpServletRequest request) throws PlaneacionComercialException {
    if (request.getSession().getAttribute("idUsuario") == null) {
      throw new PlaneacionComercialException(EMensajes.ERROR_SESION_USUARIO);
    }

    HttpSession session = request.getSession();
    session.invalidate();
  }

}
