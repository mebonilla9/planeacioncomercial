/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package co.movistar.planeacioncomercial.negocio.servlets;

import co.movistar.planeacioncomercial.negocio.util.FileLoadUtil;
import com.google.gson.Gson;

import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.Part;
import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Pattern;

/**
 * @author lord_nightmare
 */
@MultipartConfig(
        fileSizeThreshold = 1024 * 1024 * 10,
        maxFileSize = 1024 * 1024 * 50,
        maxRequestSize = 1024 * 1024 * 100
)
@WebServlet(name = "CargarArchivosServlet", urlPatterns = {"/cargar"})
public class CargarArchivosServlet extends HttpServlet {

  private static final long serialVersionUID = 205242440643911308L;

  /**
   * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
   * methods.
   *
   * @param request  servlet request
   * @param response servlet response
   * @throws ServletException if a servlet-specific error occurs
   * @throws IOException      if an I/O error occurs
   */
  protected void processRequest(HttpServletRequest request, HttpServletResponse response)
          throws ServletException, IOException {
    response.setContentType("text/html;charset=UTF-8");
    try (PrintWriter out = response.getWriter()) {
      try {
        ServletContext contexto = request.getServletContext();
        String uploadDir = (String) contexto.getAttribute("uploadDir");
        String homeUploadDir = (String) contexto.getAttribute("homeUploadDir");

        String rutaAbsolutaApp = request.getServletContext().getRealPath("");
        String rutaCargaApp = rutaAbsolutaApp + File.separator + uploadDir;

        File carpetaCarga = new File(rutaCargaApp);
        if (!carpetaCarga.exists()) {
          carpetaCarga.mkdir();
        }
        File carpetaHomeCarga = new File(homeUploadDir);
        if (!carpetaHomeCarga.exists()) {
          carpetaHomeCarga.mkdir();
        }

        System.out.println("Carpeta de carga: " + carpetaCarga.getAbsolutePath());
        List<String> listaArchivos = new ArrayList<>();
        String nombreArchivo;
        for (Part parte : request.getParts()) {
          nombreArchivo = getNombreArchivo(parte);
          File archivoExistente = new File(homeUploadDir + File.separator + nombreArchivo);
          //if (!archivoExistente.exists()) {
          parte.write(homeUploadDir + File.separator + nombreArchivo);
          //}
          listaArchivos.add(nombreArchivo);
        }
        FileLoadUtil.copiarCarpetaPublica(rutaCargaApp, homeUploadDir);
        out.print(new Gson().toJson(listaArchivos));
      } catch (IOException | ServletException e) {
        out.print(e.getMessage());
      }
    }
  }

  private String getNombreArchivo(Part parte) {
    String contentDisp = parte.getHeader("content-disposition");
    System.out.println("Content disposition header: " + contentDisp);
    String[] tokens = contentDisp.split(Pattern.quote(";"));
    for (String token : tokens) {
      if (token.trim().startsWith("filename")) {
        String subName = token.substring(token.indexOf("=") + 2, token.length() - 1).toLowerCase();
        return subName.replace(" ", "_");
      }
    }
    return "";
  }

  // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">

  /**
   * Handles the HTTP <code>GET</code> method.
   *
   * @param request  servlet request
   * @param response servlet response
   * @throws ServletException if a servlet-specific error occurs
   * @throws IOException      if an I/O error occurs
   */
  @Override
  protected void doGet(HttpServletRequest request, HttpServletResponse response)
          throws ServletException, IOException {
    processRequest(request, response);
  }

  /**
   * Handles the HTTP <code>POST</code> method.
   *
   * @param request  servlet request
   * @param response servlet response
   * @throws ServletException if a servlet-specific error occurs
   * @throws IOException      if an I/O error occurs
   */
  @Override
  protected void doPost(HttpServletRequest request, HttpServletResponse response)
          throws ServletException, IOException {
    processRequest(request, response);
  }

  /**
   * Returns a short description of the servlet.
   *
   * @return a String containing servlet description
   */
  @Override
  public String getServletInfo() {
    return "Short description";
  }// </editor-fold>

}
