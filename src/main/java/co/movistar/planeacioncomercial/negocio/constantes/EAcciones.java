/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package co.movistar.planeacioncomercial.negocio.constantes;

/**
 * @author Manuel Ernesto Bonilla Muñoz <mebonilla9@gmail.com>
 */
public enum EAcciones {

  NO_EXISTE("La url no existe"),
  INSERTAR("/insertar"),
  CONSULTAR("/consultar"),
  BUSCAR("/buscar"),
  MODIFICAR("/modificar"),
  LOGIN("/login"),
  LOGOUT("/logout"),
  ARCHIVOS_MODULO("/modulo"),
  ARCHIVOS_PERIODO("/periodo");

  private String accion;

  EAcciones(String accion) {
    this.accion = accion;
  }

  public static EAcciones parse(String accion) {
    EAcciones[] acciones = values();
    for (EAcciones accione : acciones) {
      if (accione.getAccion().equalsIgnoreCase(accion)) {
        return accione;
      }
    }
    return EAcciones.NO_EXISTE;
  }

  public String getAccion() {
    return accion;
  }

  public void setAccion(String accion) {
    this.accion = accion;
  }

}
