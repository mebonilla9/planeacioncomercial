/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package co.movistar.planeacioncomercial.negocio.constantes;

/**
 * @author Lord_Nightmare
 */
public class Routing {

  public static class Index {

    public static final String LOGIN = "/index/login/";
    public static final String LOGOUT = "/index/logout/";

  }

  public static class Archivos {

    public static final String CONSULTAR = "/archivos/consultar/";
    public static final String INSERTAR = "/archivos/insertar/";
    public static final String MODIFICAR = "/archivos/modificar/";
    public static final String MODULO = "/archivos/modulo/";
    public static final String PERIODO = "/archivos/periodo/";

  }

}
