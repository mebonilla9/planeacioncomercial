/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package co.movistar.planeacioncomercial.negocio.constantes;

/**
 * @author Manuel Ernesto Bonilla Muñoz <mebonilla9@gmail.com>
 */
public enum EMensajes {

  INSERTO(1, "Se insertó correctamente"),
  MODIFICO(1, "Se modificó correctamente"),
  ELIMINO(1, "Se eliminó correctamente"),
  CONSULTO(1, "Se ha consultado correctamente"),
  REESTABLECER_CONTRASENA(1, "Su nueva contraseña ha sido enviada a su correo electronico"),
  NO_RESULTADOS(0, "No se encontraron  registros"),
  ERROR_INSERTAR(-1, "Error al insertar el registro"),
  ERROR_MODIFICAR(-1, "Error al modificar el registro"),
  ERROR_CONSULTAR(-1, "Error al consultar el registro"),
  ERROR_ELIMINAR(-1, "Error al consultar el registro"),
  ERROR_REGISTRO_EXISTE(-1, "El registro ya existe"),
  ERROR_CONEXION_BD(-2, "No hay conexión con la base de datos"),
  ERROR_AUTENTICACION_TOKEN(-3, "El token utilizado no es válido"),
  ERROR_EXPIRACION_TOKEN(-3, "La fecha de validez del token ha caducado"),
  ERROR_GENERACION_TOKEN(-3, "El token no ha podido ser generado"),
  ERROR_SESION_USUARIO(-4, "La sesión del usuario ha caducado o no existe"),
  ERROR_FORMULARIO_VACIO(-5, "Los datos del formulario son invalidos"),
  ERROR_DISPOSITIVO_NO_VALIDO(-6, "Inicio de sesión en dispositivo no valido, el usuario ha sido bloqueado, para desbloquearlo contacte al administrador"),
  ERROR_USUARIO_BLOQUEADO(-1, "El usuario ha sido bloqueado por iniciar sesión en un dispositivo no valido"),
  ERROR_CIFRADO_NO_ENCONTRADO(-3, "Algoritmo de cifrado no encontrado"),
  ERROR_DIRECCION_GOOGLE_GEOCODING(-1, "Dirección no encontrada por el API de Google"),
  ERROR_DIRECCION_ENVIO_CORREO(-1, "Direccion de correo invalida, imposible enviar correo"),
  ERROR_PLANTILLA_CORREO(-1, "Direccion de correo invalida, imposible enviar correo"),
  ERROR_COBERTURA_CAJAS(-1, "Lo sentimos,la dirección actual no tiene disponibilidad"),
  ERROR_USUARIO_EXISTENTE(-1, "El usuario que intenta registrar ya se encuentra en el sistema");

  private int codigo;
  private String descripcion;

  EMensajes(int codigo, String descripcion) {
    this.codigo = codigo;
    this.descripcion = descripcion;
  }

  public int getCodigo() {
    return codigo;
  }

  public void setCodigo(int codigo) {
    this.codigo = codigo;
  }

  public String getDescripcion() {
    return descripcion;
  }

  public void setDescripcion(String descripcion) {
    this.descripcion = descripcion;
  }

}
