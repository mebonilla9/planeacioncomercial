package co.movistar.planeacioncomercial.negocio.constantes;

public enum ECanales {

  REGISTRO(0, "registro","NA"),
  CENTROS_EXPERIENCIA(6, "centros", "CD"),
  FUERZA_VENTA_DIRECTA(9, "venta_directa", "VD");

  private Long id;
  private String nombre;
  private String modulo;

  ECanales(long id, String nombre,String modulo) {
    this.id = id;
    this.nombre = nombre;
    this.modulo = modulo;
  }

  public Long getId() {
    return id;
  }

  public String getNombre() {
    return nombre;
  }

  public String getModulo(){
    return modulo;
  }
}
