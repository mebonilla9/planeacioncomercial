/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package co.movistar.planeacioncomercial.negocio.delegado;

import co.movistar.planeacioncomercial.modelo.conexion.ConexionBD;
import co.movistar.planeacioncomercial.modelo.dao.AppCostoxAltaDao;
import co.movistar.planeacioncomercial.modelo.dto.AuditoriaDTO;
import co.movistar.planeacioncomercial.modelo.vo.AppCostoxAlta;
import co.movistar.planeacioncomercial.negocio.constantes.EMensajes;
import co.movistar.planeacioncomercial.negocio.excepciones.PlaneacionComercialException;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.List;

/**
 * @author Manuel Ernesto Bonilla Muñoz <mebonilla9@gmail.com>
 */
public class AppCostoxAltaDelegado extends GenericoDelegado<AppCostoxAlta> {

  private final AppCostoxAltaDao appCostoxAltaDao;

  public AppCostoxAltaDelegado(Connection cnn, AuditoriaDTO auditoria) throws PlaneacionComercialException {
    super(cnn, auditoria);
    appCostoxAltaDao = new AppCostoxAltaDao(cnn);
    genericoDAO = appCostoxAltaDao;
  }

  public List<AppCostoxAlta> consultarPeriodos(Long idUsuario) throws PlaneacionComercialException {
    try {
      return appCostoxAltaDao.consultarPeriodos(idUsuario);
    } catch (SQLException e) {
      ConexionBD.rollback(cnn);
      throw new PlaneacionComercialException(EMensajes.ERROR_CONSULTAR);
    }
  }

  public List<AppCostoxAlta> consultarCostoxAltaProducto(Long idUsuario, String periodo) throws PlaneacionComercialException {
    try {
      return appCostoxAltaDao.consultarProductosDisponibles(idUsuario, periodo);
    } catch (SQLException e) {
      ConexionBD.rollback(cnn);
      throw new PlaneacionComercialException(EMensajes.ERROR_CONSULTAR);
    }
  }

  public List<AppCostoxAlta> consultarCostoxAltaCanalUsuario(Long idUsuario, String producto, String periodo, String canal) throws PlaneacionComercialException {
    try {
      return appCostoxAltaDao.consultarCostoxAltaCanalUsuario(idUsuario, producto, periodo, canal);
    } catch (SQLException e) {
      ConexionBD.rollback(cnn);
      throw new PlaneacionComercialException(EMensajes.ERROR_CONSULTAR);
    }
  }

  public List<AppCostoxAlta> consultarCanalesDisponibles(Long idUsuario, String periodo, String producto) throws PlaneacionComercialException {
    try {
      return appCostoxAltaDao.consultarCanalesDisponibles(idUsuario, periodo, producto);
    } catch (SQLException e) {
      ConexionBD.rollback(cnn);
      throw new PlaneacionComercialException(EMensajes.ERROR_CONSULTAR);
    }
  }


}
