/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package co.movistar.planeacioncomercial.negocio.delegado;

import co.movistar.planeacioncomercial.modelo.dao.AppCiudadDao;
import co.movistar.planeacioncomercial.modelo.dto.AuditoriaDTO;
import co.movistar.planeacioncomercial.modelo.vo.AppCiudad;
import co.movistar.planeacioncomercial.negocio.constantes.EMensajes;
import co.movistar.planeacioncomercial.negocio.excepciones.PlaneacionComercialException;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.List;

/**
 * @author Fabian
 */
public class AppCiudadDelegado extends GenericoDelegado<AppCiudad> {

  private final AppCiudadDao appCiudadDao;

  public AppCiudadDelegado(Connection cnn, AuditoriaDTO auditoria) throws PlaneacionComercialException {
    super(cnn, auditoria);
    appCiudadDao = new AppCiudadDao(cnn);
    genericoDAO = appCiudadDao;
  }

  public List<AppCiudad> consultarPorRegional(Long id) throws PlaneacionComercialException {
    try {
      return appCiudadDao.consultarPorRegional(id);
    } catch (SQLException e) {
      throw new PlaneacionComercialException(EMensajes.ERROR_CONSULTAR);
    }
  }

}
