/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package co.movistar.planeacioncomercial.negocio.delegado;

import co.movistar.planeacioncomercial.modelo.dao.AppNotificacionDao;
import co.movistar.planeacioncomercial.modelo.dto.AuditoriaDTO;
import co.movistar.planeacioncomercial.modelo.vo.AppNotificacion;
import co.movistar.planeacioncomercial.negocio.constantes.EMensajes;
import co.movistar.planeacioncomercial.negocio.excepciones.PlaneacionComercialException;

import java.sql.Connection;
import java.sql.SQLException;

/**
 * @author Manuel Ernesto Bonilla Muñoz <mebonilla9@gmail.com>
 */
public class AppNotificacionDelegado extends GenericoDelegado<AppNotificacion> {

  private final AppNotificacionDao appNotificacionDao;

  public AppNotificacionDelegado(Connection cnn, AuditoriaDTO auditoria) throws PlaneacionComercialException {
    super(cnn, auditoria);
    appNotificacionDao = new AppNotificacionDao(cnn);
    genericoDAO = appNotificacionDao;
  }

  public AppNotificacion obtenerUltimaNotificacion() throws PlaneacionComercialException {
    try {
      return appNotificacionDao.obtenerUltimaNotificacion();
    } catch (SQLException e) {
      throw new PlaneacionComercialException(EMensajes.ERROR_CONSULTAR);
    }
  }

  public AppNotificacion obtenerUltimaNotificacion(Long idCargo) throws PlaneacionComercialException {
    try {
      return appNotificacionDao.obtenerUltimaNotificacion(idCargo);
    } catch (SQLException e) {
      throw new PlaneacionComercialException(EMensajes.ERROR_CONSULTAR);
    }
  }


}
