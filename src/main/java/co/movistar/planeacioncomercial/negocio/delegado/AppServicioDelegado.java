/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package co.movistar.planeacioncomercial.negocio.delegado;

import co.movistar.planeacioncomercial.modelo.dao.AppServicioDao;
import co.movistar.planeacioncomercial.modelo.dto.AuditoriaDTO;
import co.movistar.planeacioncomercial.modelo.vo.AppServicio;
import co.movistar.planeacioncomercial.negocio.constantes.EMensajes;
import co.movistar.planeacioncomercial.negocio.excepciones.PlaneacionComercialException;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.List;

/**
 * @author Manuel Ernesto Bonilla Muñoz <mebonilla9@gmail.com>
 */
public class AppServicioDelegado extends GenericoDelegado<AppServicio> {

  private final AppServicioDao appServicioDao;

  public AppServicioDelegado(Connection cnn, AuditoriaDTO auditoriaDTO) throws PlaneacionComercialException {
    super(cnn, auditoriaDTO);
    appServicioDao = new AppServicioDao(cnn);
    genericoDAO = appServicioDao;
  }

  public List<AppServicio> consultarServiciosUsuario(Long idUsuario) throws PlaneacionComercialException {
    try {
      return appServicioDao.consultarPorUsuario(idUsuario);
    } catch (SQLException e) {
      throw new PlaneacionComercialException(EMensajes.ERROR_CONSULTAR);
    }
  }

}
