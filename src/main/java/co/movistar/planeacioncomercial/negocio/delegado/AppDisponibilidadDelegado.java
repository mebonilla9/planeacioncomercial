/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package co.movistar.planeacioncomercial.negocio.delegado;

import co.movistar.planeacioncomercial.modelo.conexion.ConexionBD;
import co.movistar.planeacioncomercial.modelo.dao.AppDisponibilidadDao;
import co.movistar.planeacioncomercial.modelo.dto.AuditoriaDTO;
import co.movistar.planeacioncomercial.modelo.dto.InfoDisponibilidadDTO;
import co.movistar.planeacioncomercial.modelo.vo.AppDisponibilidad;
import co.movistar.planeacioncomercial.negocio.constantes.EMensajes;
import co.movistar.planeacioncomercial.negocio.excepciones.PlaneacionComercialException;
import com.google.maps.model.LatLng;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 * @author Manuel Ernesto Bonilla Muñoz <mebonilla9@gmail.com>
 */
public class AppDisponibilidadDelegado extends GenericoDelegado<AppDisponibilidad> {

  private final AppDisponibilidadDao appDisponibilidadDao;

  public AppDisponibilidadDelegado(Connection cnn, AuditoriaDTO auditoria) throws PlaneacionComercialException {
    super(cnn, auditoria);
    appDisponibilidadDao = new AppDisponibilidadDao(cnn);
    genericoDAO = appDisponibilidadDao;
  }

  public InfoDisponibilidadDTO consultarInformacionDisponibilidad(String... valores) throws PlaneacionComercialException {
    InfoDisponibilidadDTO idDto = new InfoDisponibilidadDTO();
    try {
      List<AppDisponibilidad> data = appDisponibilidadDao.consultarInformacionDisponibilidad(valores);
      // Refactorizar en DTO
      idDto.setCabecera(this.obtenerCabeceraDisponibilidad(data));
      idDto.setCajas(this.obtenerTiposCajas(data));
      idDto.setCapacidades(this.obtenerCapacidades(data));
    } catch (SQLException ex) {
      ex.printStackTrace();
      ConexionBD.rollback(cnn);
      throw new PlaneacionComercialException(EMensajes.ERROR_CONSULTAR);
    }
    return idDto;
  }

  public List<AppDisponibilidad> consultarDepartamentos() throws PlaneacionComercialException {
    try {
      return appDisponibilidadDao.consultarDepartamentos();
    } catch (SQLException ex) {
      ConexionBD.rollback(cnn);
      throw new PlaneacionComercialException(EMensajes.ERROR_CONSULTAR);
    }
  }

  public List<AppDisponibilidad> consultarLocalidades(String departamento) throws PlaneacionComercialException {
    try {
      return appDisponibilidadDao.consultarLocalidades(departamento);
    } catch (SQLException ex) {
      ConexionBD.rollback(cnn);
      throw new PlaneacionComercialException(EMensajes.ERROR_CONSULTAR);
    }
  }

  public List<AppDisponibilidad> consultarDistritos(String departamento, String localidad) throws PlaneacionComercialException {
    try {
      return appDisponibilidadDao.consultarDistritos(departamento, localidad);
    } catch (SQLException ex) {
      ConexionBD.rollback(cnn);
      throw new PlaneacionComercialException(EMensajes.ERROR_CONSULTAR);
    }
  }

  public List<AppDisponibilidad> consultarArmarios(String mdf) throws PlaneacionComercialException {
    try {
      return appDisponibilidadDao.consultarArmarios(mdf);
    } catch (SQLException ex) {
      ConexionBD.rollback(cnn);
      throw new PlaneacionComercialException(EMensajes.ERROR_CONSULTAR);
    }
  }

  public List<AppDisponibilidad> consultarCajas(String mdf, String armario) throws PlaneacionComercialException {
    try {
      return appDisponibilidadDao.consultarCajas(mdf, armario);
    } catch (SQLException ex) {
      ConexionBD.rollback(cnn);
      throw new PlaneacionComercialException(EMensajes.ERROR_CONSULTAR);
    }
  }

  private AppDisponibilidad obtenerCabeceraDisponibilidad(List<AppDisponibilidad> data) {
    AppDisponibilidad cabecera = new AppDisponibilidad();
    for (int i = 0; i < data.size(); i++) {
      if (cabecera.getRegional() == null && !data.get(i).getRegional().equals("")) {
        cabecera.setRegional(data.get(i).getRegional());
      }
      if (cabecera.getDepartamento() == null && !data.get(i).getDepartamento().equals("")) {
        cabecera.setDepartamento(data.get(i).getDepartamento());
      }
      if (cabecera.getLocalidad() == null && !data.get(i).getLocalidad().equals("")) {
        cabecera.setLocalidad(data.get(i).getLocalidad());
      }
      if (cabecera.getDistrito() == null && !data.get(i).getDistrito().equals("")) {
        cabecera.setDistrito(data.get(i).getDistrito());
      }
      if (cabecera.getArmario() == null && !data.get(i).getArmario().equals("")) {
        cabecera.setArmario(data.get(i).getArmario());
      }
      if (cabecera.getCaja() == null && !data.get(i).getCaja().equals("")) {
        cabecera.setCaja(data.get(i).getCaja());
      }
    }
    return cabecera;
  }

  private List<AppDisponibilidad> obtenerTiposCajas(List<AppDisponibilidad> data) {
    List<AppDisponibilidad> infoCajas = new ArrayList<>();
    for (int i = 0; i < data.size(); i++) {
      if (data.get(i).getCategoria().equals("A") || data.get(i).getCategoria().equals("B") || data.get(i).getCategoria().equals("C")) {
        infoCajas.add(data.get(i));
      }
    }
    return infoCajas;
  }

  private List<AppDisponibilidad> obtenerCapacidades(List<AppDisponibilidad> data) {
    List<AppDisponibilidad> infoCapacidades = new ArrayList<>();
    for (int i = 0; i < data.size(); i++) {
      if (!data.get(i).getCategoria().equals("A") && !data.get(i).getCategoria().equals("B") && !data.get(i).getCategoria().equals("C")) {
        infoCapacidades.add(data.get(i));
      }
    }
    return infoCapacidades;
  }

  public InfoDisponibilidadDTO consultarDisponibilidadDireccion(LatLng coordenadas) throws PlaneacionComercialException {
    InfoDisponibilidadDTO idDto = new InfoDisponibilidadDTO();
    try {
      String[] dataDireccion = appDisponibilidadDao.consultarCajaDireccion(coordenadas);
      if (dataDireccion[6].equals("0")) {
        throw new PlaneacionComercialException(EMensajes.ERROR_COBERTURA_CAJAS);
      }
      dataDireccion[0] = "";
      dataDireccion[1] = "";
      dataDireccion[2] = "";
      List<AppDisponibilidad> data = appDisponibilidadDao.consultarInformacionDisponibilidad(dataDireccion);
      // Refactorizar en DTO
      idDto.setCabecera(this.obtenerCabeceraDisponibilidad(data));
      idDto.setCajas(this.obtenerTiposCajas(data));
      idDto.setCapacidades(this.obtenerCapacidades(data));
    } catch (SQLException e) {
      e.printStackTrace();
      ConexionBD.rollback(cnn);
      throw new PlaneacionComercialException(EMensajes.ERROR_CONSULTAR);
    }
    return idDto;
  }

  public List<AppDisponibilidad> consultarLocalidadCoordenadas(String departamento) throws PlaneacionComercialException {
    try {
      return appDisponibilidadDao.consultarLocalidadCoordenadas(departamento);
    } catch (SQLException e) {
      ConexionBD.rollback(cnn);
      throw new PlaneacionComercialException(EMensajes.ERROR_CONSULTAR);
    }
  }

}
