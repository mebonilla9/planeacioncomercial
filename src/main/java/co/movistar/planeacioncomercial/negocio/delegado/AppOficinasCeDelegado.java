/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package co.movistar.planeacioncomercial.negocio.delegado;

import co.movistar.planeacioncomercial.modelo.dao.AppOficinasCeDao;
import co.movistar.planeacioncomercial.modelo.dto.AuditoriaDTO;
import co.movistar.planeacioncomercial.modelo.vo.AppOficinasCe;
import co.movistar.planeacioncomercial.negocio.constantes.EMensajes;
import co.movistar.planeacioncomercial.negocio.excepciones.PlaneacionComercialException;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.List;

/**
 * @author Fabian
 */
public class AppOficinasCeDelegado extends GenericoDelegado<AppOficinasCe> {

  private final AppOficinasCeDao appOficinasCeDao;

  public AppOficinasCeDelegado(Connection cnn, AuditoriaDTO auditoria) throws PlaneacionComercialException {
    super(cnn, auditoria);
    appOficinasCeDao = new AppOficinasCeDao(cnn);
    genericoDAO = appOficinasCeDao;
  }

  public List<AppOficinasCe> consultarPorCoordinador(Double cedula) throws PlaneacionComercialException {
    try {
      return appOficinasCeDao.consultarPorCoordinador(cedula);
    } catch (SQLException e) {
      throw new PlaneacionComercialException(EMensajes.ERROR_CONSULTAR);
    }
  }

  public AppOficinasCe consultarOficinaPendiente() throws PlaneacionComercialException {
    try{
      return appOficinasCeDao.consultarOficinaPendiente();
    } catch (SQLException e){
      throw new PlaneacionComercialException(EMensajes.ERROR_CONSULTAR);
    }
  }


}
