/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package co.movistar.planeacioncomercial.negocio.delegado;

import co.movistar.planeacioncomercial.modelo.conexion.ConexionBD;
import co.movistar.planeacioncomercial.modelo.dao.AppSegmentacionDao;
import co.movistar.planeacioncomercial.modelo.dto.AuditoriaDTO;
import co.movistar.planeacioncomercial.modelo.vo.AppSegmentacion;
import co.movistar.planeacioncomercial.negocio.constantes.EMensajes;
import co.movistar.planeacioncomercial.negocio.excepciones.PlaneacionComercialException;

import java.sql.Connection;
import java.sql.SQLException;

/**
 * @author Manuel Ernesto Bonilla Muñoz <mebonilla9@gmail.com>
 */
public class AppSegmentacionDelegado extends GenericoDelegado<AppSegmentacion> {

  private final AppSegmentacionDao appSegmentacionDao;

  public AppSegmentacionDelegado(Connection cnn, AuditoriaDTO auditoria) throws PlaneacionComercialException {
    super(cnn, auditoria);
    appSegmentacionDao = new AppSegmentacionDao(cnn);
    genericoDAO = appSegmentacionDao;
  }

  public AppSegmentacion consultarInformacionSegmento(String nit) throws PlaneacionComercialException {
    try {
      return appSegmentacionDao.consultar(nit);
    } catch (SQLException e) {
      ConexionBD.rollback(cnn);
      throw new PlaneacionComercialException(EMensajes.ERROR_CONSULTAR);
    }
  }

}
