/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package co.movistar.planeacioncomercial.negocio.delegado;

import co.movistar.planeacioncomercial.modelo.conexion.ConexionBD;
import co.movistar.planeacioncomercial.modelo.dao.AppGestionadoDao;
import co.movistar.planeacioncomercial.modelo.dto.AuditoriaDTO;
import co.movistar.planeacioncomercial.modelo.vo.AppGestionado;
import co.movistar.planeacioncomercial.negocio.constantes.EMensajes;
import co.movistar.planeacioncomercial.negocio.excepciones.PlaneacionComercialException;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.List;

/**
 * @author Manuel Ernesto Bonilla Muñoz <mebonilla9@gmail.com>
 */
public class AppGestionadoDelegado extends GenericoDelegado<AppGestionado> {

  private final AppGestionadoDao appGestionadoDao;

  public AppGestionadoDelegado(Connection cnn, AuditoriaDTO auditoria) throws PlaneacionComercialException {
    super(cnn, auditoria);
    appGestionadoDao = new AppGestionadoDao(cnn);
    genericoDAO = appGestionadoDao;
  }

  public List<AppGestionado> consultarInfoGestionado(Long idUsuario) throws PlaneacionComercialException {
    try {
      return appGestionadoDao.consultarInfoGestionadoCabecera(idUsuario);
    } catch (SQLException e) {
      ConexionBD.rollback(cnn);
      throw new PlaneacionComercialException(EMensajes.ERROR_CONSULTAR);
    }
  }

  public void editarEjecucion(AppGestionado appGestionado) throws PlaneacionComercialException {
    try {
      appGestionadoDao.editarEjecucion(appGestionado);
    } catch (Exception e) {
      e.printStackTrace(System.err);
      System.out.println("-------------------Error: " + e.getMessage() + " ---------------------");
      ConexionBD.rollback(cnn);
      throw new PlaneacionComercialException(EMensajes.ERROR_MODIFICAR);
    }
  }


}
