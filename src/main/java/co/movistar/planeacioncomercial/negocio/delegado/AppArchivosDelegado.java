/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package co.movistar.planeacioncomercial.negocio.delegado;

import co.movistar.planeacioncomercial.modelo.conexion.ConexionBD;
import co.movistar.planeacioncomercial.modelo.dao.AppArchivosDao;
import co.movistar.planeacioncomercial.modelo.dto.AuditoriaDTO;
import co.movistar.planeacioncomercial.modelo.vo.AppArchivos;
import co.movistar.planeacioncomercial.negocio.constantes.EMensajes;
import co.movistar.planeacioncomercial.negocio.excepciones.PlaneacionComercialException;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.List;

/**
 * @author Lord_Nightmare
 */
public class AppArchivosDelegado extends GenericoDelegado<AppArchivos> {

  private final AppArchivosDao appArchivosDao;

  public AppArchivosDelegado(Connection cnn, AuditoriaDTO auditoriaDTO) throws PlaneacionComercialException {
    super(cnn, auditoriaDTO);
    appArchivosDao = new AppArchivosDao(cnn);
    genericoDAO = appArchivosDao;
  }

  public List<String> consultarCategoriasModulo(String modulo) throws PlaneacionComercialException {
    try {
      return new AppArchivosDao(cnn).consultarCategoriasModulo(modulo);
    } catch (SQLException e) {
      throw new PlaneacionComercialException(EMensajes.ERROR_CONSULTAR);
    }
  }

  public List<AppArchivos> consultarArchivosModuloCategoria(String modulo, String categoria) throws PlaneacionComercialException {
    try {
      return new AppArchivosDao(cnn).consultarArchivosModuloCategoria(modulo, categoria);
    } catch (SQLException e) {
      ConexionBD.rollback(cnn);
      throw new PlaneacionComercialException(EMensajes.ERROR_CONSULTAR);
    }
  }

}
