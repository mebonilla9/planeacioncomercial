/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package co.movistar.planeacioncomercial.negocio.delegado;

import co.movistar.planeacioncomercial.modelo.dao.AppAdminCanalDao;
import co.movistar.planeacioncomercial.modelo.dto.AuditoriaDTO;
import co.movistar.planeacioncomercial.modelo.vo.AppAdminCanal;
import co.movistar.planeacioncomercial.negocio.constantes.EMensajes;
import co.movistar.planeacioncomercial.negocio.excepciones.PlaneacionComercialException;

import java.sql.Connection;
import java.sql.SQLException;

/**
 * @author Fabian
 */
public class AppAdminCanalDelegado extends GenericoDelegado<AppAdminCanal> {

  private final AppAdminCanalDao appAdminCanalDao;

  public AppAdminCanalDelegado(Connection cnn, AuditoriaDTO auditoria) throws PlaneacionComercialException {
    super(cnn, auditoria);
    appAdminCanalDao = new AppAdminCanalDao(cnn);
    genericoDAO = appAdminCanalDao;
  }

  public AppAdminCanal consultarPorModulo(String modulo) throws PlaneacionComercialException {
    try {
      return appAdminCanalDao.consultarPorModulo(modulo);
    } catch (SQLException e) {
      throw new PlaneacionComercialException(EMensajes.ERROR_CONSULTAR);
    }
  }

}
