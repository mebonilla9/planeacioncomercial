/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package co.movistar.planeacioncomercial.negocio.delegado;

import co.movistar.planeacioncomercial.modelo.conexion.ConexionBD;
import co.movistar.planeacioncomercial.modelo.dao.AppUsuariosDao;
import co.movistar.planeacioncomercial.modelo.dto.AuditoriaDTO;
import co.movistar.planeacioncomercial.modelo.dto.InfoMetaDTO;
import co.movistar.planeacioncomercial.modelo.dto.InsertarUsuarioDto;
import co.movistar.planeacioncomercial.modelo.vo.*;
import co.movistar.planeacioncomercial.negocio.constantes.ECanales;
import co.movistar.planeacioncomercial.negocio.constantes.EMensajes;
import co.movistar.planeacioncomercial.negocio.excepciones.PlaneacionComercialException;
import co.movistar.planeacioncomercial.negocio.util.CryptoUtil;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * @author Manuel Ernesto Bonilla Muñoz <mebonilla9@gmail.com>
 */
public class AppUsuariosDelegado extends GenericoDelegado<AppUsuarios> {

  private final AppUsuariosDao appUsuarioDao;

  public AppUsuariosDelegado(Connection cnn, AuditoriaDTO auditoria) throws PlaneacionComercialException {
    super(cnn, auditoria);
    appUsuarioDao = new AppUsuariosDao(cnn);
    genericoDAO = appUsuarioDao;
  }

  public AppUsuarios autenticarUsuario(String correo, String contrasena) throws PlaneacionComercialException {
    try {
      return new AppUsuariosDao(cnn).consultarUsuarioAutenticar(correo, contrasena);
    } catch (SQLException e) {
      System.out.println("--------------------------------- Fallo en consulta de usuario ---------------------------------");
      e.printStackTrace(System.err);
      throw new PlaneacionComercialException(EMensajes.ERROR_CONSULTAR);
    }
  }

  public AppUsuarios autenticarUsuario(String correo, String contrasena, String macAddress) throws PlaneacionComercialException {
    try {
      AppUsuarios usuarioAutorizado = appUsuarioDao.consultarUsuarioAutenticar(correo, contrasena);
      if (usuarioAutorizado.getBloqueo()) {
        throw new PlaneacionComercialException(EMensajes.ERROR_USUARIO_BLOQUEADO);
      }
      registrarMacAddress(usuarioAutorizado, macAddress);
      bloquearUsuario(usuarioAutorizado, macAddress);
      return usuarioAutorizado;
    } catch (SQLException e) {
      throw new PlaneacionComercialException(EMensajes.ERROR_CONSULTAR);
    }
  }

  public List<AppUsuarios> consultarSubordinados(Long id) throws PlaneacionComercialException {
    try {
      return appUsuarioDao.consultarSubordinados(id);
    } catch (SQLException e) {
      throw new PlaneacionComercialException(EMensajes.ERROR_CONSULTAR);
    }
  }

  public List<InfoMetaDTO> consultarSubordinadosNov(Long id, ECanales canal) throws PlaneacionComercialException {
    try {
      List<InfoMetaDTO> subordinados = new ArrayList<>();
      List<AppUsuarios> usuarioSubs = appUsuarioDao.consultarSubordinadosCanal(id, canal);
      for (AppUsuarios usuario : usuarioSubs) {
        InfoMetaDTO infoMetaDTO = new InfoMetaDTO();
        infoMetaDTO.setUsuario(usuario);

        switch (canal) {
          case CENTROS_EXPERIENCIA:
            infoMetaDTO.setInfoRol(new AppDatosCeDelegado(cnn, null).consultarPorUsuario(usuario.getIdUsuario()));
            if (infoMetaDTO.getInfoRol() != null) {
              ((AppDatosCe) infoMetaDTO.getInfoRol()).setPunto(new AppOficinasCeDelegado(cnn, null).consultar(((AppDatosCe) infoMetaDTO.getInfoRol()).getPunto().getIdOficina()));
              ((AppDatosCe) infoMetaDTO.getInfoRol()).setRol(new AppRolDelegado(cnn, null).consultar(((AppDatosCe) infoMetaDTO.getInfoRol()).getRol().getIdRol()));
            }
            break;
          case FUERZA_VENTA_DIRECTA:
            infoMetaDTO.setInfoRol(new AppVentasCeDelegado(cnn, null).consultarPorUsuario(((AppVentasCe) infoMetaDTO.getInfoRol()).getOficina().getIdOficina()));
            if (infoMetaDTO.getInfoRol() != null) {
              ((AppVentasCe) infoMetaDTO.getInfoRol()).setOficina(new AppOficinasCeDelegado(cnn, null).consultar(((AppVentasCe) infoMetaDTO.getInfoRol()).getOficina().getIdOficina()));
              ((AppVentasCe) infoMetaDTO.getInfoRol()).setRol(new AppRolDelegado(cnn, null).consultar(((AppVentasCe) infoMetaDTO.getInfoRol()).getRol().getIdRol()));
            }
            break;
        }


        if (infoMetaDTO.getInfoRol() != null) {
          infoMetaDTO.setNovedad(new AppHistoriaNovedadDelegado(cnn, null).consultarUltimaNovedad(infoMetaDTO.getUsuario().getIdUsuario()));

          if (infoMetaDTO.getNovedad() != null) {
            infoMetaDTO.setUltimaNovedad(new AppNovedadDelegado(cnn, null).consultar(infoMetaDTO.getNovedad().getIdNovedad()));
          }
        }

        // Obtener el periodo maximo de metas existentes
        /*AppMetaUsuario maxima = appMetaUsuarioDelegado.consultarFechaMaxima();

        // Obtener el primer dia de la fecha maxima de meta
        LocalDate fechaLocalIni = DateUtil.obtenerFechaLocal(maxima.getPeriodo());//.withDayOfMonth(1);
        // Obtener el ultimo dia de la fecha maxima de meta
        LocalDate fechaLocalFin = DateUtil.obtenerFechaLocal(maxima.getPeriodo()).withDayOfMonth(DateUtil.obtenerFechaLocal(maxima.getPeriodo()).lengthOfMonth());*/

        // Buscar novedad de incapacidad por el intervalo mensual de la meta a evaluar
        //infoMetaDTO.setNovedad(new AppHistoriaNovedadDelegado(cnn, null).consultarUltimaIncapacidad(usuario.getIdUsuario(), DateUtil.obtenerFechaClasica(fechaLocalIni), DateUtil.obtenerFechaClasica(fechaLocalFin)));
        subordinados.add(infoMetaDTO);
      }
      return subordinados;
    } catch (SQLException e) {
      throw new PlaneacionComercialException(EMensajes.ERROR_CONSULTAR);
    }
  }

  public List<AppUsuarios> consultarSubordinadosPuntos(Long id) throws PlaneacionComercialException {
    try {
      return appUsuarioDao.consultarSubordinadosPuntos(id);
    } catch (SQLException e) {
      throw new PlaneacionComercialException(EMensajes.ERROR_CONSULTAR);
    }
  }

  private void registrarMacAddress(AppUsuarios usuarioAutorizado, String macAddress) throws SQLException {
    if (usuarioAutorizado.getMac() == null) {
      usuarioAutorizado.setMac(macAddress);
      appUsuarioDao.editarMacAddress(usuarioAutorizado);
    }
  }

  private void bloquearUsuario(AppUsuarios usuario, String macAddress) throws SQLException, PlaneacionComercialException {
    if (!usuario.getMac().equals(macAddress)) {
      usuario.setBloqueo(true);
      appUsuarioDao.bloquearUsuario(usuario);
      ConexionBD.commit(cnn);
      throw new PlaneacionComercialException(EMensajes.ERROR_DISPOSITIVO_NO_VALIDO);
    }
  }

  public AppUsuarios reestablecerContrasenaCorreo(String correo) throws PlaneacionComercialException {
    try {
      AppUsuarios usuario = appUsuarioDao.consultar(correo);
      if (usuario == null) {
        throw new PlaneacionComercialException(EMensajes.NO_RESULTADOS);
      }
      usuario.setPassword(CryptoUtil.generarContrasenaDefecto());
      genericoDAO.editar(usuario);
      return usuario;
    } catch (SQLException | NullPointerException e) {
      throw new PlaneacionComercialException(EMensajes.ERROR_PLANTILLA_CORREO);
    }
  }

  public InsertarUsuarioDto consultarPorCedula(Double cedula, ECanales canal, Long idJefe) throws PlaneacionComercialException {
    try {
      InsertarUsuarioDto usuarioDto = new InsertarUsuarioDto();
      usuarioDto.setUsuario(appUsuarioDao.consultarPorCedula(cedula, idJefe, canal));
      if (usuarioDto.getUsuario() == null) {
        throw new PlaneacionComercialException(EMensajes.NO_RESULTADOS);
      }
      if (usuarioDto.getUsuario().getCargo().getIdCargo() > 0) {
        usuarioDto.getUsuario().setCargo(new AppCargoDelegado(cnn, null).consultar(usuarioDto.getUsuario().getCargo().getIdCargo()));
      }
      //usuarioDto.setPunto(new AppDatosCeDelegado(cnn, null).consultarPorUsuario(usuarioDto.getUsuario().getIdUsuario()));
      List<AppNovedad> novedades = new AppNovedadDelegado(cnn, null).consultar();
      usuarioDto.setListaNovedades(new ArrayList<>());
      for (AppNovedad novedad : novedades) {
        AppHistoriaNovedad miNovedad = new AppHistoriaNovedadDelegado(cnn, null).consultarUltimaNovedad(usuarioDto.getUsuario().getIdUsuario());
        if (miNovedad != null) {
          usuarioDto.getListaNovedades().add(miNovedad);
        }
      }

      switch (canal) {
        case CENTROS_EXPERIENCIA:
          AppDatosCe dce = new AppDatosCeDelegado(cnn, null).consultarPorUsuario(usuarioDto.getUsuario().getIdUsuario());
          usuarioDto.setDatosAdicionales(dce);
          break;
        case FUERZA_VENTA_DIRECTA:
          AppVentasCe vce = new AppVentasCeDelegado(cnn, null).consultarPorUsuario(usuarioDto.getUsuario().getIdUsuario());
          if (vce != null) {
            vce.setOficina(new AppOficinasCeDelegado(cnn, null).consultar(vce.getOficina().getIdOficina()));
          }
          usuarioDto.setDatosAdicionales(vce);
          break;
      }
      return usuarioDto;
    } catch (SQLException e) {
      throw new PlaneacionComercialException(EMensajes.ERROR_CONSULTAR);
    }
  }

  public void insertarUsuario(InsertarUsuarioDto content, AppUsuarios usuarioActual, ECanales modulo) throws PlaneacionComercialException {
    AppUsuarios anterior = null;
    if (content.getUsuario().getIdUsuario() != null) {
      anterior = this.consultar(content.getUsuario().getIdUsuario());

    } else {
      evaluarExistenciaUsuario(content, modulo);
      content.getUsuario().setPassword(CryptoUtil.generarContrasenaDefecto());
      content.getUsuario().setEstado(new AppEstados(1L));
      content.getUsuario().setIdJefe(usuarioActual.getIdUsuario());
      content.getUsuario().setIdCanal(usuarioActual.getIdCanal());
      this.insertar(content.getUsuario());
    }
    if (!content.getListaNovedades().isEmpty()) {
      this.procesarNovedades(content, anterior, usuarioActual, modulo);
    }
    if (anterior != null) {
      this.editar(content.getUsuario());
    }

    if (!content.getListaNovedades().isEmpty()) {
      evaluarRegistroCanal(content, usuarioActual, modulo);
    }

    if (anterior != null) {
      this.editar(content.getUsuario());
    }

    /*
      Validar punto del usuario
     */


    if (modulo.equals(ECanales.CENTROS_EXPERIENCIA) || modulo.equals(ECanales.REGISTRO)) {
      AppDatosCe datosCe = (AppDatosCe) content.getDatosAdicionales();
      AppOficinasCe pendiente = new AppOficinasCeDelegado(cnn, null).consultarOficinaPendiente();
      content.getUsuario().setIdJefe(
        datosCe.getPunto().getIdOficina().equals(pendiente.getIdOficina()) ?
          0L :
          usuarioActual.getIdUsuario()
      );
      new AppDatosCeDelegado(cnn, null).editar(datosCe);
      content.setDatosAdicionales(datosCe);
    } else if (modulo.equals(ECanales.FUERZA_VENTA_DIRECTA)) {
      AppVentasCe ventasCe = (AppVentasCe) content.getDatosAdicionales();
      AppOficinasCe pendiente = new AppOficinasCeDelegado(cnn, null).consultarOficinaPendiente();
      content.getUsuario().setIdJefe(
        ventasCe.getOficina().getIdOficina().equals(pendiente.getIdOficina()) ?
          0L :
          usuarioActual.getIdUsuario()
      );
      new AppVentasCeDelegado(cnn, null).editar(ventasCe);
      content.setDatosAdicionales(ventasCe);
    } else {
      content.getUsuario().setIdJefe(usuarioActual.getIdUsuario());
    }

    this.editar(content.getUsuario());

    ConexionBD.commit(cnn);
  }

  private void evaluarExistenciaUsuario(InsertarUsuarioDto content, ECanales modulo) throws PlaneacionComercialException {
    try{
      AppUsuarios usuarioRecibido = appUsuarioDao.consultarExistenciaPorCedula(content.getUsuario().getCedula());
      if (usuarioRecibido != null && usuarioRecibido.getIdUsuario() != null)
        throw new PlaneacionComercialException(EMensajes.ERROR_USUARIO_EXISTENTE);
    } catch (SQLException e){
      throw new PlaneacionComercialException(EMensajes.ERROR_CONSULTAR);
    }
  }

  private void evaluarRegistroCanal(InsertarUsuarioDto content, AppUsuarios usuarioActual, ECanales modulo) throws PlaneacionComercialException {
    if (modulo != ECanales.REGISTRO) {
      switch (modulo) {
        case CENTROS_EXPERIENCIA:
          guardarDatosCe(content, usuarioActual);
          break;
        case FUERZA_VENTA_DIRECTA:
          guardarVentasCe(content, usuarioActual);
          break;
        default:
      }
    }
  }

  private void procesarNovedades(InsertarUsuarioDto content, AppUsuarios anterior, AppUsuarios usuarioActual, ECanales modulo) throws PlaneacionComercialException {
    for (AppHistoriaNovedad novedad : content.getListaNovedades()) {
      AppNovedad nov = new AppNovedadDelegado(cnn, null).consultar(novedad.getIdNovedad());
      switch (nov.getTipo()) {
        case "V":
          cargarNovedadValor(novedad, content, anterior, usuarioActual);
          break;
        case "F":
          cargarNovedadVacIncDes(novedad, content, usuarioActual, modulo);
          break;
        default:
          break;
      }
    }
  }

  private void cargarNovedadValor(AppHistoriaNovedad novedad, InsertarUsuarioDto content, AppUsuarios anterior, AppUsuarios usuarioActual) throws PlaneacionComercialException {
    switch (novedad.getIdNovedad().intValue()) {
      case 1:
        cargarNovedadPunto(novedad, content, usuarioActual);
        break;
      case 5:
        cargarNovedadCambioRol(novedad, content, usuarioActual);
        break;
      case 6:
        cargarNovedadCambioJefe(novedad, content, anterior, usuarioActual);
        break;
      default:
        break;
    }
  }

  private void cargarNovedadPunto(AppHistoriaNovedad novedad, InsertarUsuarioDto content, AppUsuarios usuarioActual) throws PlaneacionComercialException {
    if (content.getDatosAdicionales() instanceof AppOficinasCe) {
      return;
    }
    AppDatosCe punto = (AppDatosCe) content.getDatosAdicionales();
    AppDatosCeDelegado datosCeDelegado = new AppDatosCeDelegado(cnn, null);
    AppDatosCe puntoAnterior = datosCeDelegado.consultarPorUsuario(content.getUsuario().getIdUsuario());
    if (puntoAnterior != null) {
      novedad.setValorAnterior(puntoAnterior.getPunto().getIdOficina().toString());
    }
    novedad.setIdUsuariomodifica(usuarioActual.getIdUsuario());
    novedad.setIdUsuarionovedad(content.getUsuario().getIdUsuario());
    novedad.setValorAnterior(punto.getPunto().getIdOficina().toString());
    AppHistoriaNovedadDelegado historiaNovedadDelegado = new AppHistoriaNovedadDelegado(cnn, null);
    if (novedad.getIdHistoria() == null) {
      novedad.setFechaInicio(new Date());
      historiaNovedadDelegado.insertar(novedad);
    } else {
      historiaNovedadDelegado.editar(novedad);
    }
    punto.getPunto().setIdOficina(Long.parseLong(novedad.getValorNuevo()));
    punto.setNovedad(new AppNovedad(novedad.getIdNovedad()));
    punto.setFecha(novedad.getFechaInicio());
    if (puntoAnterior != null) {
      punto.setIdDatosce(puntoAnterior.getIdDatosce());
      datosCeDelegado.editar(punto);
    } else {
      punto.setIdUsuariomodifica(usuarioActual.getIdUsuario());
      punto.setRol(new AppRol(content.getUsuario().getCargo().getIdCargo()));
      datosCeDelegado.insertar(punto);
    }
    content.setDatosAdicionales(punto);
  }

  private void cargarNovedadVacIncDes(AppHistoriaNovedad novedad, InsertarUsuarioDto content, AppUsuarios usuarioActual, ECanales modulo) throws PlaneacionComercialException {
    novedad.setIdUsuarionovedad(content.getUsuario().getIdUsuario());
    novedad.setIdUsuariomodifica(usuarioActual.getIdUsuario());
    if(novedad.getIdNovedad().intValue() == 4){
      content.getUsuario().setIdJefe(0L);
      AppOficinasCe pendiente = new AppOficinasCeDelegado(cnn, null).consultarOficinaPendiente();
      if (modulo.equals(ECanales.CENTROS_EXPERIENCIA)) {
        ((AppDatosCe) content.getDatosAdicionales()).getPunto().setIdOficina(pendiente.getIdOficina());
      }
      if (modulo.equals(ECanales.FUERZA_VENTA_DIRECTA)){
        ((AppVentasCe) content.getDatosAdicionales()).getOficina().setIdOficina(pendiente.getIdOficina());
      }
    }
    new AppHistoriaNovedadDelegado(cnn, null).insertar(novedad);
  }

  private void cargarNovedadCambioRol(AppHistoriaNovedad novedad, InsertarUsuarioDto content, AppUsuarios usuarioActual) throws PlaneacionComercialException {
    novedad.setIdUsuarionovedad(content.getUsuario().getIdUsuario());
    novedad.setIdUsuariomodifica(usuarioActual.getIdUsuario());
    new AppHistoriaNovedadDelegado(cnn, null).insertar(novedad);
    AppDatosCe datos = (AppDatosCe) content.getDatosAdicionales();
    datos.setRol(new AppRol(Long.parseLong(novedad.getValorNuevo())));
    content.setDatosAdicionales(datos);
  }

  private void cargarNovedadCambioJefe(AppHistoriaNovedad novedad, InsertarUsuarioDto content, AppUsuarios anterior, AppUsuarios usuarioActual) throws PlaneacionComercialException {
    novedad.setIdUsuarionovedad(content.getUsuario().getIdUsuario());
    novedad.setValorAnterior(anterior.getIdJefe().toString());
    novedad.setValorNuevo(content.getUsuario().getIdJefe().toString());
    novedad.setIdUsuariomodifica(usuarioActual.getIdUsuario());
    new AppHistoriaNovedadDelegado(cnn, null).insertar(novedad);
  }

  private void guardarDatosCe(InsertarUsuarioDto content, AppUsuarios usuarioActual) throws PlaneacionComercialException {
    AppDatosCe datos;
    if (content.getDatosAdicionales() != null) {
      datos = (AppDatosCe) content.getDatosAdicionales();
    } else {
      datos = new AppDatosCeDelegado(cnn, null).consultarPorUsuario(content.getUsuario().getIdUsuario());
      if (datos == null) {
        datos = new AppDatosCe();
        datos.setIdUsuario(content.getUsuario().getIdUsuario());
      }
    }
    datos.setNovedad(new AppNovedad(content.getListaNovedades().get(0).getIdNovedad()));
    for (AppHistoriaNovedad novedad : content.getListaNovedades()) {
      switch (novedad.getIdNovedad().intValue()) {
        case 1:
          datos.setPunto(new AppOficinasCe(Long.parseLong(novedad.getValorNuevo())));
          if (datos.getRol() == null) {
            datos.setRol(new AppRol(0l));
          }
          break;
        case 5:
          datos.setRol(new AppRol(Long.parseLong(novedad.getValorNuevo())));
          break;
      }
    }
    datos.setFecha(new Date());
    datos.setIdUsuariomodifica(usuarioActual.getIdUsuario());
    if (datos.getIdUsuario() == null) {
      datos.setIdUsuario(content.getUsuario().getIdUsuario());
    }
    if (datos.getIdDatosce() == null) {
      content.setDatosAdicionales(datos);
      new AppDatosCeDelegado(cnn, null).insertar(datos);
      return;
    }
    new AppDatosCeDelegado(cnn, null).editar(datos);
    content.setDatosAdicionales(datos);
  }

  private void guardarVentasCe(InsertarUsuarioDto content, AppUsuarios usuarioActual) throws PlaneacionComercialException {
    AppVentasCe ventas = new AppVentasCeDelegado(cnn, null).consultarPorUsuario(content.getUsuario().getIdUsuario());
    if (ventas == null) {
      ventas = new AppVentasCe();
      ventas.setIdUsuario(content.getUsuario().getIdUsuario());
    }
    ventas.setNovedad(new AppNovedad(content.getListaNovedades().get(0).getIdNovedad()));
    for (AppHistoriaNovedad novedad : content.getListaNovedades()) {
      switch (novedad.getIdNovedad().intValue()) {
        case 6:
          ventas.setIdSupervisor(Long.parseLong(novedad.getValorNuevo()));
          break;
        case 7:
          ventas.setOficina(new AppOficinasCe(Long.parseLong(novedad.getValorNuevo())));
          break;
      }
    }
    ventas.setFecha(new Date());
    ventas.setIdUsuarioModifica(usuarioActual.getIdUsuario());
    new AppVentasCeDelegado(cnn, null).insertar(ventas);
  }
}
