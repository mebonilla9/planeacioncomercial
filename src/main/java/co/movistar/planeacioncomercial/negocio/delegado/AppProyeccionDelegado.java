/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package co.movistar.planeacioncomercial.negocio.delegado;

import co.movistar.planeacioncomercial.modelo.conexion.ConexionBD;
import co.movistar.planeacioncomercial.modelo.dao.AppProyeccionDao;
import co.movistar.planeacioncomercial.modelo.dto.AuditoriaDTO;
import co.movistar.planeacioncomercial.modelo.vo.AppProyeccion;
import co.movistar.planeacioncomercial.negocio.constantes.EMensajes;
import co.movistar.planeacioncomercial.negocio.excepciones.PlaneacionComercialException;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.List;

/**
 * @author lord_nightmare
 */
public class AppProyeccionDelegado extends GenericoDelegado<AppProyeccion> {

  private final AppProyeccionDao appProyeccionDao;

  public AppProyeccionDelegado(Connection cnn, AuditoriaDTO auditoria) throws PlaneacionComercialException {
    super(cnn, auditoria);
    appProyeccionDao = new AppProyeccionDao(cnn);
    genericoDAO = appProyeccionDao;
  }

  public List<AppProyeccion> consultarProyeccionProducto(Long idUsuario) throws PlaneacionComercialException {
    try {
      return appProyeccionDao.consultarProductosDisponibles(idUsuario);
    } catch (SQLException e) {
      ConexionBD.rollback(cnn);
      throw new PlaneacionComercialException(EMensajes.ERROR_CONSULTAR);
    }
  }

  public List<AppProyeccion> consultarProyeccionProductoUsuario(Long idUsuario, String producto) throws PlaneacionComercialException {
    try {
      return appProyeccionDao.consultarProyeccionProductoUsuario(idUsuario, producto);
    } catch (SQLException e) {
      ConexionBD.rollback(cnn);
      throw new PlaneacionComercialException(EMensajes.ERROR_CONSULTAR);
    }
  }
}
