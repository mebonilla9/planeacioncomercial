/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package co.movistar.planeacioncomercial.negocio.delegado;

import co.movistar.planeacioncomercial.modelo.dao.AppHistoriaNovedadDao;
import co.movistar.planeacioncomercial.modelo.dto.AuditoriaDTO;
import co.movistar.planeacioncomercial.modelo.vo.AppHistoriaNovedad;
import co.movistar.planeacioncomercial.negocio.constantes.EMensajes;
import co.movistar.planeacioncomercial.negocio.excepciones.PlaneacionComercialException;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.Date;

/**
 * @author Fabian
 */
public class AppHistoriaNovedadDelegado extends GenericoDelegado<AppHistoriaNovedad> {

  private final AppHistoriaNovedadDao appHistoriaNovedadDao;

  public AppHistoriaNovedadDelegado(Connection cnn, AuditoriaDTO auditoria) throws PlaneacionComercialException {
    super(cnn, auditoria);
    appHistoriaNovedadDao = new AppHistoriaNovedadDao(cnn);
    genericoDAO = appHistoriaNovedadDao;
  }

  public AppHistoriaNovedad consultarUltimaNovedad(Long idUsuario) throws PlaneacionComercialException {
    try {
      return appHistoriaNovedadDao.consultarUltimaNovedad(idUsuario);
    } catch (SQLException e) {
      e.printStackTrace();
      throw new PlaneacionComercialException(EMensajes.ERROR_CONSULTAR);
    }
  }

  public AppHistoriaNovedad consultarUltimaIncapacidad(Long idUsuario, Date fechaIni, Date fechaFin) throws PlaneacionComercialException {
    try {
      return appHistoriaNovedadDao.consultarUltimaIncapacidad(idUsuario, fechaIni, fechaFin);
    } catch (SQLException e) {
      throw new PlaneacionComercialException(EMensajes.ERROR_CONSULTAR);
    }
  }

}
