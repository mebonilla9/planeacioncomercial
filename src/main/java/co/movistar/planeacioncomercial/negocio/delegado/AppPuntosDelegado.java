/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package co.movistar.planeacioncomercial.negocio.delegado;

import co.movistar.planeacioncomercial.modelo.dao.AppPuntosDao;
import co.movistar.planeacioncomercial.modelo.dto.AuditoriaDTO;
import co.movistar.planeacioncomercial.modelo.vo.AppPuntos;
import co.movistar.planeacioncomercial.negocio.constantes.EMensajes;
import co.movistar.planeacioncomercial.negocio.excepciones.PlaneacionComercialException;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.List;

/**
 * @author Manuel Ernesto Bonilla Muñoz <mebonilla9@gmail.com>
 */
public class AppPuntosDelegado extends GenericoDelegado<AppPuntos> {

  private final AppPuntosDao appPuntosDao;

  public AppPuntosDelegado(Connection cnn, AuditoriaDTO auditoria) throws PlaneacionComercialException {
    super(cnn, auditoria);
    appPuntosDao = new AppPuntosDao(cnn);
    genericoDAO = appPuntosDao;
  }

  public List<AppPuntos> consultarPuntosUsuario(Long id) throws PlaneacionComercialException {
    try {
      return appPuntosDao.consultarPuntosUsuario(id);
    } catch (SQLException e) {
      throw new PlaneacionComercialException(EMensajes.ERROR_CONSULTAR);
    }
  }

  public List<AppPuntos> consultarPuntosNovedad(Long id) throws PlaneacionComercialException {
    try {
      return appPuntosDao.consultarPuntosNovedad(id);
    } catch (SQLException e) {
      throw new PlaneacionComercialException(EMensajes.ERROR_CONSULTAR);
    }
  }

  public AppPuntos consultarPuntoCoordinador(Long idUsuario, Long idCoordinador) throws PlaneacionComercialException {
    try {
      return appPuntosDao.consultarPuntoCoordinador(idUsuario, idCoordinador);
    } catch (SQLException e) {
      throw new PlaneacionComercialException(EMensajes.ERROR_CONSULTAR);
    }
  }

  public AppPuntos consultarPuntoPendiente() throws PlaneacionComercialException {
    try{
      return appPuntosDao.consultarPuntoPendiente();
    } catch(SQLException e){
      throw new PlaneacionComercialException(EMensajes.ERROR_CONSULTAR);
    }
  }
}
