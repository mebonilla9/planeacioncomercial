/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package co.movistar.planeacioncomercial.negocio.delegado;

import co.movistar.planeacioncomercial.modelo.conexion.ConexionBD;
import co.movistar.planeacioncomercial.modelo.dao.AppUsuarioGeografiaDao;
import co.movistar.planeacioncomercial.modelo.dto.AuditoriaDTO;
import co.movistar.planeacioncomercial.modelo.vo.AppUsuarioGeografia;
import co.movistar.planeacioncomercial.negocio.constantes.EMensajes;
import co.movistar.planeacioncomercial.negocio.excepciones.PlaneacionComercialException;

import java.sql.Connection;
import java.sql.SQLException;

/**
 * @author Manuel Ernesto Bonilla Muñoz <mebonilla9@gmail.com>
 */
public class AppUsuarioGeografiaDelegado extends GenericoDelegado<AppUsuarioGeografia> {

  private final AppUsuarioGeografiaDao appUsuarioGeografiaDao;

  public AppUsuarioGeografiaDelegado(Connection cnn, AuditoriaDTO auditoria) throws PlaneacionComercialException {
    super(cnn, auditoria);
    appUsuarioGeografiaDao = new AppUsuarioGeografiaDao(cnn);
    genericoDAO = appUsuarioGeografiaDao;
  }

  public AppUsuarioGeografia consultarRegionalUsuario(Long idUsuario) throws PlaneacionComercialException {
    try {
      return appUsuarioGeografiaDao.consultar(idUsuario);
    } catch (SQLException ex) {
      ConexionBD.rollback(cnn);
      throw new PlaneacionComercialException(EMensajes.ERROR_CONSULTAR);
    }

  }

}
