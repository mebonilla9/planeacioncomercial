/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package co.movistar.planeacioncomercial.negocio.delegado;

import co.movistar.planeacioncomercial.modelo.conexion.ConexionBD;
import co.movistar.planeacioncomercial.modelo.dao.AppAltasDao;
import co.movistar.planeacioncomercial.modelo.dto.AuditoriaDTO;
import co.movistar.planeacioncomercial.modelo.dto.GeneracionAltasDTO;
import co.movistar.planeacioncomercial.modelo.vo.AppAltas;
import co.movistar.planeacioncomercial.negocio.constantes.EMensajes;
import co.movistar.planeacioncomercial.negocio.excepciones.PlaneacionComercialException;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.List;

/**
 * @author Manuel Ernesto Bonilla Muñoz <mebonilla9@gmail.com>
 */
public class AppAltasDelegado extends GenericoDelegado<AppAltas> {

  private final AppAltasDao appAltasDao;

  public AppAltasDelegado(Connection cnn, AuditoriaDTO auditoria) throws PlaneacionComercialException {
    super(cnn, auditoria);
    appAltasDao = new AppAltasDao(cnn);
    genericoDAO = appAltasDao;
  }

  public List<GeneracionAltasDTO> consultarInformacionAltasCanal(String producto, Long idUsuario) throws PlaneacionComercialException {
    try {
      return appAltasDao.mostrarInformacionAltas(producto, "CANAL", idUsuario);
    } catch (SQLException ex) {
      ConexionBD.rollback(cnn);
      throw new PlaneacionComercialException(EMensajes.ERROR_CONSULTAR);
    }
  }

  public List<GeneracionAltasDTO> consultarInformacionAltasRegional(String producto, Long idUsuario) throws PlaneacionComercialException {
    try {
      return appAltasDao.mostrarInformacionAltas(producto, "REGIONAL", idUsuario);
    } catch (SQLException ex) {
      ConexionBD.rollback(cnn);
      throw new PlaneacionComercialException(EMensajes.ERROR_CONSULTAR);
    }
  }

  public List<GeneracionAltasDTO> consultarInformacionAltasSegmento(String producto, Long idUsuario) throws PlaneacionComercialException {
    try {
      return appAltasDao.mostrarInformacionAltas(producto, "SEGMENTO", idUsuario);
    } catch (SQLException ex) {
      ConexionBD.rollback(cnn);
      throw new PlaneacionComercialException(EMensajes.ERROR_CONSULTAR);
    }
  }

  public List<AppAltas> consultarProductosPorUsuario(Long idUsuario) throws PlaneacionComercialException {
    try {
      return appAltasDao.consultarProductosPorUsuario(idUsuario);
    } catch (SQLException ex) {
      ConexionBD.rollback(cnn);
      throw new PlaneacionComercialException(EMensajes.ERROR_CONSULTAR);
    }
  }
}
