/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package co.movistar.planeacioncomercial.negocio.delegado;

import co.movistar.planeacioncomercial.modelo.conexion.ConexionBD;
import co.movistar.planeacioncomercial.modelo.dao.AppArticulosDao;
import co.movistar.planeacioncomercial.modelo.dto.AuditoriaDTO;
import co.movistar.planeacioncomercial.modelo.vo.AppArticulos;
import co.movistar.planeacioncomercial.negocio.constantes.EMensajes;
import co.movistar.planeacioncomercial.negocio.excepciones.PlaneacionComercialException;

import java.sql.Connection;
import java.util.List;

/**
 * @author Manuel Ernesto Bonilla Muñoz <mebonilla9@gmail.com>
 */
public class AppArticulosDelegado extends GenericoDelegado<AppArticulos> {

  private final AppArticulosDao appArticulosDao;

  public AppArticulosDelegado(Connection cnn, AuditoriaDTO auditoria) throws PlaneacionComercialException {
    super(cnn, auditoria);
    appArticulosDao = new AppArticulosDao(cnn);
    genericoDAO = appArticulosDao;
  }

  public List<AppArticulos> consultarPorPunto(Long id) throws PlaneacionComercialException {
    try {
      return appArticulosDao.consultarPorPunto(id);
    } catch (Exception e) {
      ConexionBD.rollback(cnn);
      throw new PlaneacionComercialException(EMensajes.CONSULTO);
    }
  }


}
