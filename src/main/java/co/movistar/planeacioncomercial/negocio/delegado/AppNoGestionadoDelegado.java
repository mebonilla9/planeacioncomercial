/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package co.movistar.planeacioncomercial.negocio.delegado;

import co.movistar.planeacioncomercial.modelo.conexion.ConexionBD;
import co.movistar.planeacioncomercial.modelo.dao.AppNoGestionadoDao;
import co.movistar.planeacioncomercial.modelo.dto.AuditoriaDTO;
import co.movistar.planeacioncomercial.modelo.vo.AppNoGestionado;
import co.movistar.planeacioncomercial.negocio.constantes.EMensajes;
import co.movistar.planeacioncomercial.negocio.excepciones.PlaneacionComercialException;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.List;

/**
 * @author Manuel Ernesto Bonilla Muñoz <mebonilla9@gmail.com>
 */
public class AppNoGestionadoDelegado extends GenericoDelegado<AppNoGestionado> {

  private final AppNoGestionadoDao appNoGestionadoDao;

  public AppNoGestionadoDelegado(Connection cnn, AuditoriaDTO auditoria) throws PlaneacionComercialException {
    super(cnn, auditoria);
    appNoGestionadoDao = new AppNoGestionadoDao(cnn);
    genericoDAO = appNoGestionadoDao;
  }

  public List<AppNoGestionado> consultarInfoNoGestionado(Long idUsuario) throws PlaneacionComercialException {
    try {
      return appNoGestionadoDao.consultarInfoNoGestionadoCabecera(idUsuario);
    } catch (SQLException e) {
      ConexionBD.rollback(cnn);
      throw new PlaneacionComercialException(EMensajes.ERROR_CONSULTAR);
    }
  }

  public void editarEjecucion(AppNoGestionado appNoGestionado) throws PlaneacionComercialException {
    try {
      appNoGestionadoDao.editarEjecucion(appNoGestionado);
    } catch (Exception e) {
      e.printStackTrace(System.err);
      System.out.println("-------------------Error: " + e.getMessage() + " ---------------------");
      ConexionBD.rollback(cnn);
      throw new PlaneacionComercialException(EMensajes.ERROR_MODIFICAR);
    }
  }
}
