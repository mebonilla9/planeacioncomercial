/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package co.movistar.planeacioncomercial.negocio.delegado;

import co.movistar.planeacioncomercial.modelo.conexion.ConexionBD;
import co.movistar.planeacioncomercial.modelo.dao.crud.IGenericoDAO;
import co.movistar.planeacioncomercial.modelo.dto.AuditoriaDTO;
import co.movistar.planeacioncomercial.negocio.constantes.EMensajes;
import co.movistar.planeacioncomercial.negocio.excepciones.PlaneacionComercialException;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.List;

/**
 * @author Manuel Ernesto Bonilla Muñoz <mebonilla9@gmail.com>
 */
public abstract class GenericoDelegado<T> {

  protected Connection cnn;
  protected IGenericoDAO genericoDAO;
  protected boolean confirmar = true;

  public GenericoDelegado(Connection cnn, AuditoriaDTO auditoriaDTO) throws PlaneacionComercialException {
    this.cnn = cnn;
  }

  public void insertar(T entidad) throws PlaneacionComercialException {
    try {
      genericoDAO.insertar(entidad);
    } catch (SQLException ex) {
      ex.printStackTrace();
      ConexionBD.rollback(cnn);
      throw new PlaneacionComercialException(EMensajes.ERROR_INSERTAR);
    }
  }

  public void editar(T entidad) throws PlaneacionComercialException {
    try {
      genericoDAO.editar(entidad);

    } catch (SQLException ex) {
      ex.printStackTrace();
      ConexionBD.rollback(cnn);
      throw new PlaneacionComercialException(EMensajes.ERROR_MODIFICAR);
    }
  }

  public List<T> consultar() throws PlaneacionComercialException {
    try {
      return genericoDAO.consultar();
    } catch (SQLException ex) {
      ConexionBD.rollback(cnn);
      throw new PlaneacionComercialException(EMensajes.ERROR_CONSULTAR);
    }
  }

  public T consultar(long id) throws PlaneacionComercialException {
    try {
      return (T) genericoDAO.consultar(id);
    } catch (SQLException ex) {
      ex.printStackTrace();
      ConexionBD.rollback(cnn);
      throw new PlaneacionComercialException(EMensajes.ERROR_CONSULTAR);
    }
  }

}
