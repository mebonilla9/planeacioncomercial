/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package co.movistar.planeacioncomercial.negocio.delegado;

import co.movistar.planeacioncomercial.modelo.conexion.ConexionBD;
import co.movistar.planeacioncomercial.modelo.dao.AppPresupuestoComercialDao;
import co.movistar.planeacioncomercial.modelo.dto.AuditoriaDTO;
import co.movistar.planeacioncomercial.modelo.vo.AppPresupuestoComercial;
import co.movistar.planeacioncomercial.negocio.constantes.EMensajes;
import co.movistar.planeacioncomercial.negocio.excepciones.PlaneacionComercialException;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.List;

/**
 * @author Manuel Ernesto Bonilla Muñoz <mebonilla9@gmail.com>
 */
public class AppPresupuestoComercialDelegado extends GenericoDelegado<AppPresupuestoComercial> {

  private final AppPresupuestoComercialDao appPresupuestoComercialDao;

  public AppPresupuestoComercialDelegado(Connection cnn, AuditoriaDTO auditoria) throws PlaneacionComercialException {
    super(cnn, auditoria);
    appPresupuestoComercialDao = new AppPresupuestoComercialDao(cnn);
    genericoDAO = appPresupuestoComercialDao;
  }

  public List<AppPresupuestoComercial> consultarCategoriasUsuario(Long idUsuario) throws PlaneacionComercialException {
    try {
      return appPresupuestoComercialDao.consultarCategoriaUsuario(idUsuario);
    } catch (SQLException e) {
      ConexionBD.rollback(cnn);
      throw new PlaneacionComercialException(EMensajes.ERROR_CONSULTAR);
    }
  }

  public List<AppPresupuestoComercial> consultarPeriodosUsuario(Long idUsuario) throws PlaneacionComercialException {
    try {
      return appPresupuestoComercialDao.consultarPeriodoUsuario(idUsuario);
    } catch (SQLException e) {
      ConexionBD.rollback(cnn);
      throw new PlaneacionComercialException(EMensajes.ERROR_CONSULTAR);
    }
  }

  public List<AppPresupuestoComercial> consultarMetaUsuario(Long idUsuario, String categoria, String periodo) throws PlaneacionComercialException {
    try {
      return appPresupuestoComercialDao.consultarMetaUsuario(idUsuario, categoria, periodo);
    } catch (SQLException e) {
      ConexionBD.rollback(cnn);
      throw new PlaneacionComercialException(EMensajes.ERROR_CONSULTAR);
    }
  }

}
