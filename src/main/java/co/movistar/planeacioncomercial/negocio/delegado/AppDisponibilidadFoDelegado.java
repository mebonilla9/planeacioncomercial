/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package co.movistar.planeacioncomercial.negocio.delegado;

import co.movistar.planeacioncomercial.modelo.dao.AppDisponibilidadFoDao;
import co.movistar.planeacioncomercial.modelo.dto.AuditoriaDTO;
import co.movistar.planeacioncomercial.modelo.vo.AppDisponibilidadFo;
import co.movistar.planeacioncomercial.negocio.constantes.EMensajes;
import co.movistar.planeacioncomercial.negocio.excepciones.PlaneacionComercialException;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.List;

/**
 * @author Manuel Ernesto Bonilla Muñoz <mebonilla9@gmail.com>
 */
public class AppDisponibilidadFoDelegado extends GenericoDelegado<AppDisponibilidadFo> {

  private final AppDisponibilidadFoDao appDisponibilidadFoDao;

  public AppDisponibilidadFoDelegado(Connection cnn, AuditoriaDTO auditoria) throws PlaneacionComercialException {
    super(cnn, auditoria);
    appDisponibilidadFoDao = new AppDisponibilidadFoDao(cnn);
    genericoDAO = appDisponibilidadFoDao;
  }

  public AppDisponibilidadFo consultarPorDireccion(AppDisponibilidadFo disFibra) throws PlaneacionComercialException {
    try {
      return appDisponibilidadFoDao.consultar(disFibra);
    } catch (SQLException e) {
      e.printStackTrace(System.err);
      throw new PlaneacionComercialException(EMensajes.ERROR_CONSULTAR);
    }
  }

  public AppDisponibilidadFo consultarDireccionNoParametrizada(AppDisponibilidadFo disFibra) throws PlaneacionComercialException {
    try {
      return appDisponibilidadFoDao.consultarNoParametrizada(disFibra);
    } catch (SQLException e) {
      e.printStackTrace(System.err);
      throw new PlaneacionComercialException(EMensajes.ERROR_CONSULTAR);
    }
  }

  public List<AppDisponibilidadFo> consultarDireccionesNoParametrizada(AppDisponibilidadFo disFibra) throws PlaneacionComercialException {
    try {
      return appDisponibilidadFoDao.consultarDireccionesNoParametrizada(disFibra);
    } catch (SQLException e) {
      e.printStackTrace(System.err);
      throw new PlaneacionComercialException(EMensajes.ERROR_CONSULTAR);
    }
  }

  public List<AppDisponibilidadFo> consultarRegionales(Boolean param) throws PlaneacionComercialException {
    try {
      return appDisponibilidadFoDao.consultarRegionales(param);
    } catch (SQLException e) {
      e.printStackTrace(System.err);
      throw new PlaneacionComercialException(EMensajes.ERROR_CONSULTAR);
    }
  }

  public List<AppDisponibilidadFo> consultarDepartamentos(String regional, Boolean param) throws PlaneacionComercialException {
    try {
      return appDisponibilidadFoDao.consultarDepartamentos(regional, param);
    } catch (SQLException e) {
      e.printStackTrace(System.err);
      throw new PlaneacionComercialException(EMensajes.ERROR_CONSULTAR);
    }
  }

  public List<AppDisponibilidadFo> consultarLocalidades(String departamento, Boolean param) throws PlaneacionComercialException {
    try {
      return appDisponibilidadFoDao.consultarLocalidades(departamento, param);
    } catch (SQLException e) {
      e.printStackTrace(System.err);
      throw new PlaneacionComercialException(EMensajes.ERROR_CONSULTAR);
    }
  }

  public List<AppDisponibilidadFo> consultarBarrios(String localidad, Boolean param) throws PlaneacionComercialException {
    try {
      return appDisponibilidadFoDao.consultarBarrios(localidad, param);
    } catch (SQLException e) {
      e.printStackTrace(System.err);
      throw new PlaneacionComercialException(EMensajes.ERROR_CONSULTAR);
    }
  }

  public List<AppDisponibilidadFo> consultarPrincipal(String localidad, String barrio) throws PlaneacionComercialException {
    try {
      return appDisponibilidadFoDao.consultarPrincipal(localidad, barrio);
    } catch (SQLException e) {
      e.printStackTrace(System.err);
      throw new PlaneacionComercialException(EMensajes.ERROR_CONSULTAR);
    }
  }

  public List<AppDisponibilidadFo> consultarReferenciaPrincipal(String localidad, String barrio, String principal) throws PlaneacionComercialException {
    try {
      return appDisponibilidadFoDao.consultarReferenciaPrincipal(localidad, barrio, principal);
    } catch (SQLException e) {
      e.printStackTrace(System.err);
      throw new PlaneacionComercialException(EMensajes.ERROR_CONSULTAR);
    }
  }

  public List<AppDisponibilidadFo> consultarCruce(String localidad, String barrio, String principal, String referenciaPrincipal) throws PlaneacionComercialException {
    try {
      return appDisponibilidadFoDao.consultarCruce(localidad, barrio, principal, referenciaPrincipal);
    } catch (SQLException e) {
      e.printStackTrace(System.err);
      throw new PlaneacionComercialException(EMensajes.ERROR_CONSULTAR);
    }
  }

  public List<AppDisponibilidadFo> consultarReferenciaCruce(String localidad, String barrio, String principal, String referenciaPrincipal, String cruce) throws PlaneacionComercialException {
    try {
      return appDisponibilidadFoDao.consultarReferenciaCruce(localidad, barrio, principal, referenciaPrincipal, cruce);
    } catch (SQLException e) {
      e.printStackTrace(System.err);
      throw new PlaneacionComercialException(EMensajes.ERROR_CONSULTAR);
    }
  }

  public List<AppDisponibilidadFo> consultarPlaca(String localidad, String barrio, String principal, String referenciaPrincipal, String cruce, String referenciaCruce) throws PlaneacionComercialException {
    try {
      return appDisponibilidadFoDao.consultarPlaca(localidad, barrio, principal, referenciaPrincipal, cruce, referenciaCruce);
    } catch (SQLException e) {
      e.printStackTrace(System.err);
      throw new PlaneacionComercialException(EMensajes.ERROR_CONSULTAR);
    }
  }

  public List<AppDisponibilidadFo> consultarComplemento(String localidad, String barrio, String principal, String referenciaPrincipal, String cruce, String referenciaCruce, String placa) throws PlaneacionComercialException {
    try {
      return appDisponibilidadFoDao.consultarComplemento(localidad, barrio, principal, referenciaPrincipal, cruce, referenciaCruce, placa);
    } catch (SQLException e) {
      e.printStackTrace(System.err);
      throw new PlaneacionComercialException(EMensajes.ERROR_CONSULTAR);
    }
  }

}
