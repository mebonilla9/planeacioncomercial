/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package co.movistar.planeacioncomercial.negocio.delegado;

import co.movistar.planeacioncomercial.modelo.conexion.ConexionBD;
import co.movistar.planeacioncomercial.modelo.dao.AppMenuDao;
import co.movistar.planeacioncomercial.modelo.dto.AuditoriaDTO;
import co.movistar.planeacioncomercial.modelo.vo.AppMenu;
import co.movistar.planeacioncomercial.negocio.constantes.EMensajes;
import co.movistar.planeacioncomercial.negocio.excepciones.PlaneacionComercialException;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 * @author Manuel Ernesto Bonilla Muñoz <mebonilla9@gmail.com>
 */
public class AppMenuDelegado extends GenericoDelegado<AppMenu> {

  private final AppMenuDao appMenuDao;

  public AppMenuDelegado(Connection cnn, AuditoriaDTO auditoria) throws PlaneacionComercialException {
    super(cnn, auditoria);
    appMenuDao = new AppMenuDao(cnn);
    genericoDAO = appMenuDao;
  }

  public List<AppMenu> consultarMenuUsuario(Long idUsuario) throws PlaneacionComercialException {
    try {
      List<AppMenu> menuBase = appMenuDao.consultarCategoriaMenuUsuario(idUsuario);
      if (menuBase.isEmpty()) {
        return new ArrayList<>();
      }
      for (int i = 0; i < menuBase.size(); i++) {
        AppMenu categoria = menuBase.get(i);
        categoria.setSubMenu(appMenuDao.consultarMenuUsuario(idUsuario, categoria.getIdMenu()));
      }
      return menuBase;
    } catch (SQLException ex) {
      ConexionBD.rollback(cnn);
      throw new PlaneacionComercialException(EMensajes.ERROR_CONSULTAR);
    }
  }

}
