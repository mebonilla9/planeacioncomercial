/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package co.movistar.planeacioncomercial.negocio.delegado;

import co.movistar.planeacioncomercial.modelo.conexion.ConexionBD;
import co.movistar.planeacioncomercial.modelo.dao.AppDireccionesFoDao;
import co.movistar.planeacioncomercial.modelo.dto.AuditoriaDTO;
import co.movistar.planeacioncomercial.modelo.vo.AppDireccionesFo;
import co.movistar.planeacioncomercial.negocio.constantes.EMensajes;
import co.movistar.planeacioncomercial.negocio.excepciones.PlaneacionComercialException;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 * @author Manuel Ernesto Bonilla Muñoz <mebonilla9@gmail.com>
 */
public class AppDireccionesFoDelegado extends GenericoDelegado<AppDireccionesFo> {

  private final AppDireccionesFoDao appDireccionesFoDao;

  public AppDireccionesFoDelegado(Connection cnn, AuditoriaDTO auditoria) throws PlaneacionComercialException {
    super(cnn, auditoria);
    appDireccionesFoDao = new AppDireccionesFoDao(cnn);
    genericoDAO = appDireccionesFoDao;
  }

  public List<String> consultarMunicipios(String tipo) throws PlaneacionComercialException {
    List<String> listaMunicipios = new ArrayList<>();
    try {
      List<AppDireccionesFo> consultarMunicipios = appDireccionesFoDao.consultarMunicipios(tipo);
      for (AppDireccionesFo municipio : consultarMunicipios) {
        listaMunicipios.add(municipio.getMunicipio());
      }
    } catch (SQLException e) {
      e.printStackTrace();
      ConexionBD.rollback(cnn);
      throw new PlaneacionComercialException(EMensajes.ERROR_CONSULTAR);
    }
    return listaMunicipios;
  }

  public List<String> consultarConjunto(String municipio, String tipo) throws PlaneacionComercialException {
    List<String> listaConjuntos = new ArrayList<>();
    try {
      List<AppDireccionesFo> consultarMunicipios = appDireccionesFoDao.consultarConjunto(municipio, tipo);
      for (AppDireccionesFo conjunto : consultarMunicipios) {
        listaConjuntos.add(conjunto.getConjunto());
      }
    } catch (SQLException e) {
      e.printStackTrace();
      ConexionBD.rollback(cnn);
      throw new PlaneacionComercialException(EMensajes.ERROR_CONSULTAR);
    }
    return listaConjuntos;
  }

  public List<String> consultarComplemento(String conjunto, String tipo) throws PlaneacionComercialException {
    List<String> listaComplementos = new ArrayList<>();
    try {
      List<AppDireccionesFo> consultarComplementos = appDireccionesFoDao.consultarComplemento(conjunto, tipo);
      for (AppDireccionesFo complemento : consultarComplementos) {
        listaComplementos.add(complemento.getComplemento());
      }
    } catch (SQLException e) {
      e.printStackTrace();
      ConexionBD.rollback(cnn);
      throw new PlaneacionComercialException(EMensajes.ERROR_CONSULTAR);
    }
    return listaComplementos;
  }

  public List<String> consultarDireccionConjunto(String conjunto, String tipo) throws PlaneacionComercialException {
    List<String> listaDirecciones = new ArrayList<>();
    try {
      List<AppDireccionesFo> consultarDirecciones = appDireccionesFoDao.consultarDireccionConjunto(conjunto, tipo);
      for (AppDireccionesFo direcciones : consultarDirecciones) {
        listaDirecciones.add(direcciones.getDireccionCompleta());
      }
    } catch (SQLException e) {
      e.printStackTrace();
      ConexionBD.rollback(cnn);
      throw new PlaneacionComercialException(EMensajes.ERROR_CONSULTAR);
    }
    return listaDirecciones;
  }

  public String consultarDireccionComplemento(String complemento, String tipo) throws PlaneacionComercialException {
    String direccion = "";
    try {
      direccion = appDireccionesFoDao.consultarDireccionComplemento(complemento, tipo).getDireccionCompleta();
    } catch (SQLException e) {
      e.printStackTrace();
      ConexionBD.rollback(cnn);
      throw new PlaneacionComercialException(EMensajes.ERROR_CONSULTAR);
    }
    return direccion;
  }

}
