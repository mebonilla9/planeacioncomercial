/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package co.movistar.planeacioncomercial.negocio.delegado;

import co.movistar.planeacioncomercial.modelo.dao.AppRegionalDao;
import co.movistar.planeacioncomercial.modelo.dto.AuditoriaDTO;
import co.movistar.planeacioncomercial.modelo.vo.AppRegional;
import co.movistar.planeacioncomercial.negocio.constantes.EMensajes;
import co.movistar.planeacioncomercial.negocio.excepciones.PlaneacionComercialException;

import java.sql.Connection;
import java.sql.SQLException;

/**
 * @author Manuel Ernesto Bonilla Muñoz <mebonilla9@gmail.com>
 */
public class AppRegionalDelegado extends GenericoDelegado<AppRegional> {

  private final AppRegionalDao appRegionalDao;

  public AppRegionalDelegado(Connection cnn, AuditoriaDTO auditoria) throws PlaneacionComercialException {
    super(cnn, auditoria);
    appRegionalDao = new AppRegionalDao(cnn);
    genericoDAO = appRegionalDao;
  }

  public AppRegional consultarCiudad(Long idCiudad) throws PlaneacionComercialException {
    try {
      return appRegionalDao.consultarCiudad(idCiudad);
    } catch (SQLException e) {
      throw new PlaneacionComercialException(EMensajes.ERROR_CONSULTAR);
    }
  }

}
