/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package co.movistar.planeacioncomercial.negocio.delegado;

import co.movistar.planeacioncomercial.modelo.conexion.ConexionBD;
import co.movistar.planeacioncomercial.modelo.dao.AppCabeceraDao;
import co.movistar.planeacioncomercial.modelo.dto.AuditoriaDTO;
import co.movistar.planeacioncomercial.modelo.vo.AppCabecera;
import co.movistar.planeacioncomercial.negocio.constantes.EMensajes;
import co.movistar.planeacioncomercial.negocio.excepciones.PlaneacionComercialException;

import java.sql.Connection;
import java.sql.SQLException;

/**
 * @author Manuel Ernesto Bonilla Muñoz <mebonilla9@gmail.com>
 */
public class AppCabeceraDelegado extends GenericoDelegado<AppCabecera> {

  private final AppCabeceraDao appCabeceraDao;

  public AppCabeceraDelegado(Connection cnn, AuditoriaDTO auditoria) throws PlaneacionComercialException {
    super(cnn, auditoria);
    appCabeceraDao = new AppCabeceraDao(cnn);
    genericoDAO = appCabeceraDao;
  }

  public AppCabecera consultarInfoCabecera(Long idUsuario) throws PlaneacionComercialException {
    try {
      return appCabeceraDao.consultarCabeceraPorUsuario(idUsuario);
    } catch (SQLException e) {
      ConexionBD.rollback(cnn);
      throw new PlaneacionComercialException(EMensajes.ERROR_CONSULTAR);
    }

  }

  public void ejecutarActualizacionCalculador(Long idUsuario) throws PlaneacionComercialException {
    try {
      appCabeceraDao.ejecutarActualizacionCalculador(idUsuario);
    } catch (SQLException e) {
      e.printStackTrace(System.err);
      System.out.println("-------------------Error: " + e.getMessage() + " ---------------------");
      ConexionBD.rollback(cnn);
      throw new PlaneacionComercialException(EMensajes.ERROR_MODIFICAR);
    }
  }

}
