/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package co.movistar.planeacioncomercial.negocio.delegado;

import co.movistar.planeacioncomercial.modelo.dao.AppBarriosDao;
import co.movistar.planeacioncomercial.modelo.dto.AuditoriaDTO;
import co.movistar.planeacioncomercial.modelo.vo.AppBarrios;
import co.movistar.planeacioncomercial.negocio.excepciones.PlaneacionComercialException;

import java.sql.Connection;

/**
 * @author Manuel Ernesto Bonilla Muñoz <mebonilla9@gmail.com>
 */
public class AppBarriosDelegado extends GenericoDelegado<AppBarrios> {

  private final AppBarriosDao appBarriosDao;

  public AppBarriosDelegado(Connection cnn, AuditoriaDTO auditoria) throws PlaneacionComercialException {
    super(cnn, auditoria);
    appBarriosDao = new AppBarriosDao(cnn);
    genericoDAO = appBarriosDao;
  }

}
