/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package co.movistar.planeacioncomercial.negocio.delegado;

import co.movistar.planeacioncomercial.modelo.conexion.ConexionBD;
import co.movistar.planeacioncomercial.modelo.dao.AppLiquidacionHistDao;
import co.movistar.planeacioncomercial.modelo.dto.AuditoriaDTO;
import co.movistar.planeacioncomercial.modelo.vo.AppLiquidacionHist;
import co.movistar.planeacioncomercial.negocio.constantes.EMensajes;
import co.movistar.planeacioncomercial.negocio.excepciones.PlaneacionComercialException;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.List;

/**
 * @author Manuel Ernesto Bonilla Muñoz <mebonilla9@gmail.com>
 */
public class AppLiquidacionHistDelegado extends GenericoDelegado<AppLiquidacionHist> {

  private final AppLiquidacionHistDao appLiquidacionHistDao;

  public AppLiquidacionHistDelegado(Connection cnn, AuditoriaDTO auditoria) throws PlaneacionComercialException {
    super(cnn, auditoria);
    appLiquidacionHistDao = new AppLiquidacionHistDao(cnn);
    genericoDAO = appLiquidacionHistDao;
  }

  public AppLiquidacionHist obtenerResumenLiquidacion(Long idUsuario, String periodo) throws PlaneacionComercialException {
    try {
      return appLiquidacionHistDao.obtenerResumenLiquidacion(idUsuario, periodo);
    } catch (SQLException e) {
      ConexionBD.rollback(cnn);
      throw new PlaneacionComercialException(EMensajes.ERROR_CONSULTAR);
    }
  }

  public List<AppLiquidacionHist> consultarPeriodos(Long idUsuario) throws PlaneacionComercialException {
    try {
      return appLiquidacionHistDao.consultarPeriodos(idUsuario);
    } catch (SQLException e) {
      ConexionBD.rollback(cnn);
      throw new PlaneacionComercialException(EMensajes.ERROR_CONSULTAR);
    }
  }

  public List<AppLiquidacionHist> consultarCategoria(Long idUsuario) throws PlaneacionComercialException {
    try {
      return appLiquidacionHistDao.consultarCategoria(idUsuario);
    } catch (SQLException e) {
      ConexionBD.rollback(cnn);
      throw new PlaneacionComercialException(EMensajes.ERROR_CONSULTAR);
    }
  }

  public List<AppLiquidacionHist> consultarDetalleLiquidacion(Long idUsuario, String categoria, String periodo) throws PlaneacionComercialException {
    try {
      return appLiquidacionHistDao.consultarDetalleLiquidacion(idUsuario, categoria, periodo);
    } catch (SQLException e) {
      ConexionBD.rollback(cnn);
      throw new PlaneacionComercialException(EMensajes.ERROR_CONSULTAR);
    }
  }
}
