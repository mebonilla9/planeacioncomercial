/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package co.movistar.planeacioncomercial.negocio.delegado;

import co.movistar.planeacioncomercial.modelo.conexion.ConexionBD;
import co.movistar.planeacioncomercial.modelo.dao.AppBajasDao;
import co.movistar.planeacioncomercial.modelo.dto.AuditoriaDTO;
import co.movistar.planeacioncomercial.modelo.vo.AppBajas;
import co.movistar.planeacioncomercial.negocio.constantes.EMensajes;
import co.movistar.planeacioncomercial.negocio.excepciones.PlaneacionComercialException;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.List;

/**
 * @author Manuel Ernesto Bonilla Muñoz <mebonilla9@gmail.com>
 */
public class AppBajasDelegado extends GenericoDelegado<AppBajas> {

  private final AppBajasDao appBajasDao;

  public AppBajasDelegado(Connection cnn, AuditoriaDTO auditoria) throws PlaneacionComercialException {
    super(cnn, auditoria);
    appBajasDao = new AppBajasDao(cnn);
    genericoDAO = appBajasDao;
  }

  public List<AppBajas> consultarProductos() throws PlaneacionComercialException {
    try {
      return appBajasDao.consultarProductos();
    } catch (SQLException ex) {
      ConexionBD.rollback(cnn);
      throw new PlaneacionComercialException(EMensajes.ERROR_CONSULTAR);
    }
  }

  public List<AppBajas> consultarRegionalCanal() throws PlaneacionComercialException {
    try {
      return appBajasDao.consultarRegionalCanal();
    } catch (SQLException ex) {
      ConexionBD.rollback(cnn);
      throw new PlaneacionComercialException(EMensajes.ERROR_CONSULTAR);
    }
  }

  public List<AppBajas> consultar(String producto, String regional) throws PlaneacionComercialException {
    try {
      return appBajasDao.consultar(producto, regional);
    } catch (SQLException ex) {
      ConexionBD.rollback(cnn);
      throw new PlaneacionComercialException(EMensajes.ERROR_CONSULTAR);
    }
  }

}
