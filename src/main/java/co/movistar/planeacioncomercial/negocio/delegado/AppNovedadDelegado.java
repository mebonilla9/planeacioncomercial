/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package co.movistar.planeacioncomercial.negocio.delegado;

import co.movistar.planeacioncomercial.modelo.dao.AppNovedadDao;
import co.movistar.planeacioncomercial.modelo.dto.AuditoriaDTO;
import co.movistar.planeacioncomercial.modelo.vo.AppNovedad;
import co.movistar.planeacioncomercial.negocio.constantes.ECanales;
import co.movistar.planeacioncomercial.negocio.constantes.EMensajes;
import co.movistar.planeacioncomercial.negocio.excepciones.PlaneacionComercialException;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.List;

/**
 * @author Fabian
 */
public class AppNovedadDelegado extends GenericoDelegado<AppNovedad> {

  private final AppNovedadDao appNovedadDao;

  public AppNovedadDelegado(Connection cnn, AuditoriaDTO auditoria) throws PlaneacionComercialException {
    super(cnn, auditoria);
    appNovedadDao = new AppNovedadDao(cnn);
    genericoDAO = appNovedadDao;
  }

  @Deprecated
  public List<AppNovedad> consultarCentroExperiencia() throws PlaneacionComercialException {
    try {
      return appNovedadDao.consultarCentroExperiencia();
    } catch (SQLException ex) {
      throw new PlaneacionComercialException(EMensajes.ERROR_CONSULTAR);
    }
  }

  @Deprecated
  public List<AppNovedad> consultarVentaDirecta() throws PlaneacionComercialException {
    try {
      return appNovedadDao.consultarVentaDirecta();
    } catch (SQLException ex) {
      throw new PlaneacionComercialException(EMensajes.ERROR_CONSULTAR);
    }
  }

  public List<AppNovedad> consultarNovedadesModulo(ECanales canal) throws PlaneacionComercialException{
    try {
      return appNovedadDao.listarNovedadesModulo(canal);
    } catch (SQLException ex){
      throw new PlaneacionComercialException(EMensajes.ERROR_CONSULTAR);
    }
  }

}
