/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package co.movistar.planeacioncomercial.negocio.delegado;

import co.movistar.planeacioncomercial.modelo.dao.AppDatosCeDao;
import co.movistar.planeacioncomercial.modelo.dto.AuditoriaDTO;
import co.movistar.planeacioncomercial.modelo.vo.AppDatosCe;
import co.movistar.planeacioncomercial.negocio.constantes.EMensajes;
import co.movistar.planeacioncomercial.negocio.excepciones.PlaneacionComercialException;

import java.sql.Connection;
import java.sql.SQLException;

/**
 * @author Fabian
 */
public class AppDatosCeDelegado extends GenericoDelegado<AppDatosCe> {

  private final AppDatosCeDao appDatosCeDao;

  public AppDatosCeDelegado(Connection cnn, AuditoriaDTO auditoria) throws PlaneacionComercialException {
    super(cnn, auditoria);
    appDatosCeDao = new AppDatosCeDao(cnn);
    genericoDAO = appDatosCeDao;
  }

  public AppDatosCe consultarPorUsuario(Long idUsuario) throws PlaneacionComercialException {
    try {
      return appDatosCeDao.consultarPorUsuario(idUsuario);
    } catch (SQLException e) {
      e.printStackTrace();
      throw new PlaneacionComercialException(EMensajes.ERROR_CONSULTAR);
    }
  }
}
