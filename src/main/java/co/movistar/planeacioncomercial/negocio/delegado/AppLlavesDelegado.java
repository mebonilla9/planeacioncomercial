/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package co.movistar.planeacioncomercial.negocio.delegado;

import co.movistar.planeacioncomercial.modelo.dao.AppLlavesDao;
import co.movistar.planeacioncomercial.modelo.dto.AuditoriaDTO;
import co.movistar.planeacioncomercial.modelo.vo.AppLlaves;
import co.movistar.planeacioncomercial.negocio.excepciones.PlaneacionComercialException;

import java.sql.Connection;

/**
 * @author Manuel Ernesto Bonilla Muñoz <mebonilla9@gmail.com>
 */
public class AppLlavesDelegado extends GenericoDelegado<AppLlaves> {

  private final AppLlavesDao appLlavesDao;

  public AppLlavesDelegado(Connection cnn, AuditoriaDTO auditoria) throws PlaneacionComercialException {
    super(cnn, auditoria);
    appLlavesDao = new AppLlavesDao(cnn);
    genericoDAO = appLlavesDao;
  }


}
