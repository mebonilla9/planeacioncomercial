/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package co.movistar.planeacioncomercial.negocio.delegado;

import co.movistar.planeacioncomercial.modelo.dao.AppMetaUsuarioDao;
import co.movistar.planeacioncomercial.modelo.dto.AuditoriaDTO;
import co.movistar.planeacioncomercial.modelo.dto.InfoMetaDTO;
import co.movistar.planeacioncomercial.modelo.vo.AppMetaUsuario;
import co.movistar.planeacioncomercial.modelo.vo.AppUsuarios;
import co.movistar.planeacioncomercial.negocio.constantes.EMensajes;
import co.movistar.planeacioncomercial.negocio.excepciones.PlaneacionComercialException;
import co.movistar.planeacioncomercial.negocio.util.DateUtil;

import java.sql.Connection;
import java.sql.SQLException;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

/**
 * @author Fabian
 */
public class AppMetaUsuarioDelegado extends GenericoDelegado<AppMetaUsuario> {

  private final AppMetaUsuarioDao appMetaUsuarioDao;

  public AppMetaUsuarioDelegado(Connection cnn, AuditoriaDTO auditoria) throws PlaneacionComercialException {
    super(cnn, auditoria);
    appMetaUsuarioDao = new AppMetaUsuarioDao(cnn);
    genericoDAO = appMetaUsuarioDao;
  }

  public List<InfoMetaDTO> consultarInfoCoordinador(AppUsuarios usuarioAutorizado) throws PlaneacionComercialException {
    // Lista de metas a retornar por el webservice
    List<InfoMetaDTO> listaMetas = new ArrayList<>();
    try {
      // Lista de usuarios asignados al coordinador
      List<AppUsuarios> listaUsuarios = new AppUsuariosDelegado(cnn, null).consultarSubordinados(usuarioAutorizado.getIdUsuario());
      // iterar la lista de usuarios para obtener su informacion adicional!
      for (AppUsuarios usuario : listaUsuarios) {
        InfoMetaDTO meta = new InfoMetaDTO();
        meta.setUsuario(usuario);
        // consultar el punto en comun que tienen el coordinador y su usuario a cargo
        meta.setPunto(new AppPuntosDelegado(cnn, null).consultarPuntoCoordinador(usuario.getIdUsuario(), usuarioAutorizado.getIdUsuario()));
        // Obtener el rol actual del usuario;
        meta.setInfoRol(new AppDatosCeDelegado(cnn, null).consultarPorUsuario(usuario.getIdUsuario()));
        // Listar las metas del usuario segun el punto que comparte con el coordinador
        if (meta.getPunto() != null) {
          meta.setInfoMetas(appMetaUsuarioDao.consultarPorMetasUsuario(usuario.getIdUsuario(), usuarioAutorizado.getIdUsuario(), meta.getPunto().getIdPunto()));
        }
        // Obtener el periodo maximo de metas existentes
        AppMetaUsuario maxima = appMetaUsuarioDao.consultarFechaMaxima();

        // Obtener el primer dia de la fecha maxima de meta
        LocalDate fechaLocalIni = DateUtil.obtenerFechaLocal(maxima.getPeriodo()).withDayOfMonth(1);
        // Obtener el ultimo dia de la fecha maxima de meta
        LocalDate fechaLocalFin = DateUtil.obtenerFechaLocal(maxima.getPeriodo()).withDayOfMonth(DateUtil.obtenerFechaLocal(maxima.getPeriodo()).lengthOfMonth());

        // Buscar novedad de incapacidad por el intervalo mensual de la meta a evaluar
        meta.setNovedad(new AppHistoriaNovedadDelegado(cnn, null).consultarUltimaIncapacidad(usuario.getIdUsuario(), DateUtil.obtenerFechaClasica(fechaLocalIni), DateUtil.obtenerFechaClasica(fechaLocalFin)));

        listaMetas.add(meta);
      }
    } catch (SQLException e) {
      throw new PlaneacionComercialException(EMensajes.ERROR_CONSULTAR);
    }
    return listaMetas;
  }

  public void cargarMetas(List<InfoMetaDTO> metas) throws PlaneacionComercialException {
    try {
      for (InfoMetaDTO meta : metas) {
        subirMetasUsuario(meta);
      }
      cnn.commit();
    } catch (SQLException e) {
      throw new PlaneacionComercialException(EMensajes.ERROR_MODIFICAR);
    }
  }

  private void subirMetasUsuario(InfoMetaDTO meta) throws SQLException {
    if (meta.getInfoMetas() != null) {
      for (AppMetaUsuario infoMeta : meta.getInfoMetas()) {
        genericoDAO.editar(infoMeta);
      }
    }
  }

  public AppMetaUsuario consultarFechaMaxima() throws PlaneacionComercialException {
    try {
      return appMetaUsuarioDao.consultarFechaMaxima();
    } catch (SQLException e) {
      throw new PlaneacionComercialException(EMensajes.ERROR_CONSULTAR);
    }
  }

}
