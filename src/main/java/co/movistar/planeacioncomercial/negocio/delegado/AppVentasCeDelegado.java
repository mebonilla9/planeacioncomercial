/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package co.movistar.planeacioncomercial.negocio.delegado;

import co.movistar.planeacioncomercial.modelo.dao.AppVentasCeDao;
import co.movistar.planeacioncomercial.modelo.dto.AuditoriaDTO;
import co.movistar.planeacioncomercial.modelo.vo.AppVentasCe;
import co.movistar.planeacioncomercial.negocio.constantes.EMensajes;
import co.movistar.planeacioncomercial.negocio.excepciones.PlaneacionComercialException;

import java.sql.Connection;
import java.sql.SQLException;

/**
 * @author Fabian
 */
public class AppVentasCeDelegado extends GenericoDelegado<AppVentasCe> {

  private final AppVentasCeDao appVentasCeDao;

  public AppVentasCeDelegado(Connection cnn, AuditoriaDTO auditoria) throws PlaneacionComercialException {
    super(cnn, auditoria);
    appVentasCeDao = new AppVentasCeDao(cnn);
    genericoDAO = appVentasCeDao;
  }

  public AppVentasCe consultarPorUsuario(Long idUsuario) throws PlaneacionComercialException {
    try {
      return appVentasCeDao.consultarPorUsuario(idUsuario);
    } catch (SQLException e) {
      e.printStackTrace();
      throw new PlaneacionComercialException(EMensajes.ERROR_CONSULTAR);
    }
  }

}
