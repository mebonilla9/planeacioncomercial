package co.movistar.planeacioncomercial.modelo.vo;

import java.io.Serializable;
import java.util.Date;

/**
 * @author Fabian
 */
public class AppMetaUsuario implements Serializable {

  private Long idMetaUsuario;
  private Long idMeta;
  private Long valorMetaEsp;
  private Long valorMetaAsig;
  private Long idUsuario;
  private Date periodo;
  private Long idCoordinador;
  private String idPunto;

  public AppMetaUsuario() {
  }

  /**
   * @return the idMetaUsuario
   */
  public Long getIdMetaUsuario() {
    return idMetaUsuario;
  }

  /**
   * @param idMetaUsuario the idMetaUsuario to set
   */
  public void setIdMetaUsuario(Long idMetaUsuario) {
    this.idMetaUsuario = idMetaUsuario;
  }

  /**
   * @return the idMeta
   */
  public Long getIdMeta() {
    return idMeta;
  }

  /**
   * @param idMeta the idMeta to set
   */
  public void setIdMeta(Long idMeta) {
    this.idMeta = idMeta;
  }

  /**
   * @return the valorMetaEsp
   */
  public Long getValorMetaEsp() {
    return valorMetaEsp;
  }

  /**
   * @param valorMetaEsp the valorMetaEsp to set
   */
  public void setValorMetaEsp(Long valorMetaEsp) {
    this.valorMetaEsp = valorMetaEsp;
  }

  /**
   * @return the valorMetaAsig
   */
  public Long getValorMetaAsig() {
    return valorMetaAsig;
  }

  /**
   * @param valorMetaAsig the valorMetaAsig to set
   */
  public void setValorMetaAsig(Long valorMetaAsig) {
    this.valorMetaAsig = valorMetaAsig;
  }

  /**
   * @return the idUsuariomodifica
   */
  public Long getIdUsuario() {
    return idUsuario;
  }

  /**
   * @param idUsuario the idUsuariomodifica to set
   */
  public void setIdUsuario(Long idUsuario) {
    this.idUsuario = idUsuario;
  }

  /**
   * @return the periodo
   */
  public Date getPeriodo() {
    return periodo;
  }

  /**
   * @param periodo the periodo to set
   */
  public void setPeriodo(Date periodo) {
    this.periodo = periodo;
  }

  /**
   * @return the idCoordinador
   */
  public Long getIdCoordinador() {
    return idCoordinador;
  }

  /**
   * @param idCoordinador the idCoordinador to set
   */
  public void setIdCoordinador(Long idCoordinador) {
    this.idCoordinador = idCoordinador;
  }

  /**
   * @return the idPunto
   */
  public String getIdPunto() {
    return idPunto;
  }

  /**
   * @param idPunto the idPunto to set
   */
  public void setIdPunto(String idPunto) {
    this.idPunto = idPunto;
  }

}
