package co.movistar.planeacioncomercial.modelo.vo;

import java.io.Serializable;

/**
 * @author Manuel Ernesto Bonilla Muñoz <mebonilla9@gmail.com>
 */
public class AppPresupuestoComercial implements Serializable {

  private Long idPpto;
  private Long idUsuario;
  private Long documento;
  private String nomUsuario;
  private String producto;
  private String regional;
  private String jefatura;
  private String canal;
  private String departamento;
  private String segmento;
  private Long presupuesto;
  private String categoria;
  private String periodo;

  public AppPresupuestoComercial() {
  }

  /**
   * @return the idPpto
   */
  public Long getIdPpto() {
    return idPpto;
  }

  /**
   * @param idPpto the idPpto to set
   */
  public void setIdPpto(Long idPpto) {
    this.idPpto = idPpto;
  }

  /**
   * @return the idUsuario
   */
  public Long getIdUsuario() {
    return idUsuario;
  }

  /**
   * @param idUsuario the idUsuario to set
   */
  public void setIdUsuario(Long idUsuario) {
    this.idUsuario = idUsuario;
  }

  /**
   * @return the documento
   */
  public Long getDocumento() {
    return documento;
  }

  /**
   * @param documento the documento to set
   */
  public void setDocumento(Long documento) {
    this.documento = documento;
  }

  /**
   * @return the nomUsuario
   */
  public String getNomUsuario() {
    return nomUsuario;
  }

  /**
   * @param nomUsuario the nomUsuario to set
   */
  public void setNomUsuario(String nomUsuario) {
    this.nomUsuario = nomUsuario;
  }

  /**
   * @return the producto
   */
  public String getProducto() {
    return producto;
  }

  /**
   * @param producto the producto to set
   */
  public void setProducto(String producto) {
    this.producto = producto;
  }

  /**
   * @return the regional
   */
  public String getRegional() {
    return regional;
  }

  /**
   * @param regional the regional to set
   */
  public void setRegional(String regional) {
    this.regional = regional;
  }

  /**
   * @return the jefatura
   */
  public String getJefatura() {
    return jefatura;
  }

  /**
   * @param jefatura the jefatura to set
   */
  public void setJefatura(String jefatura) {
    this.jefatura = jefatura;
  }

  /**
   * @return the canal
   */
  public String getCanal() {
    return canal;
  }

  /**
   * @param canal the canal to set
   */
  public void setCanal(String canal) {
    this.canal = canal;
  }

  /**
   * @return the departamento
   */
  public String getDepartamento() {
    return departamento;
  }

  /**
   * @param departamento the departamento to set
   */
  public void setDepartamento(String departamento) {
    this.departamento = departamento;
  }

  /**
   * @return the segmento
   */
  public String getSegmento() {
    return segmento;
  }

  /**
   * @param segmento the segmento to set
   */
  public void setSegmento(String segmento) {
    this.segmento = segmento;
  }

  /**
   * @return the presupuesto
   */
  public Long getPresupuesto() {
    return presupuesto;
  }

  /**
   * @param presupuesto the presupuesto to set
   */
  public void setPresupuesto(Long presupuesto) {
    this.presupuesto = presupuesto;
  }

  /**
   * @return the categoria
   */
  public String getCategoria() {
    return categoria;
  }

  /**
   * @param categoria the categoria to set
   */
  public void setCategoria(String categoria) {
    this.categoria = categoria;
  }

  /**
   * @return the periodo
   */
  public String getPeriodo() {
    return periodo;
  }

  /**
   * @param periodo the periodo to set
   */
  public void setPeriodo(String periodo) {
    this.periodo = periodo;
  }


}
