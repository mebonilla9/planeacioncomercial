package co.movistar.planeacioncomercial.modelo.vo;

import java.io.Serializable;

/**
 * @author Manuel Ernesto Bonilla Muñoz <mebonilla9@gmail.com>
 */
public class AppConsultaSegmento implements Serializable {

  private Long IdConsulta;
  private String Nit;
  private Long IdAgente;
  private String Fecha;
  private Long IdUsuario;

  public AppConsultaSegmento() {
  }

  public Long getIdConsulta() {
    return IdConsulta;
  }

  public void setIdConsulta(Long IdConsulta) {
    this.IdConsulta = IdConsulta;
  }

  public String getNit() {
    return Nit;
  }

  public void setNit(String Nit) {
    this.Nit = Nit;
  }

  public Long getIdAgente() {
    return IdAgente;
  }

  public void setIdAgente(Long IdAgente) {
    this.IdAgente = IdAgente;
  }

  public String getFecha() {
    return Fecha;
  }

  public void setFecha(String Fecha) {
    this.Fecha = Fecha;
  }

  public Long getIdUsuario() {
    return IdUsuario;
  }

  public void setIdUsuario(Long IdUsuario) {
    this.IdUsuario = IdUsuario;
  }

}
