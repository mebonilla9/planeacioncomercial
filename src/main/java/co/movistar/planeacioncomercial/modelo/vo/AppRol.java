package co.movistar.planeacioncomercial.modelo.vo;

import java.io.Serializable;

/**
 * @author Manuel Ernesto Bonilla Muñoz <mebonilla9@gmail.com>
 */
public class AppRol implements Serializable {

  private Long idRol;
  private String nombreRol;

  public AppRol() {
  }

  public AppRol(Long idRol) {
    this.idRol = idRol;
  }

  public Long getIdRol() {
    return idRol;
  }

  public void setIdRol(Long idRol) {
    this.idRol = idRol;
  }

  public String getNombreRol() {
    return nombreRol;
  }

  public void setNombreRol(String nombreRol) {
    this.nombreRol = nombreRol;
  }

}
