package co.movistar.planeacioncomercial.modelo.vo;

import java.io.Serializable;
import java.util.LinkedHashMap;
import java.util.Map;

/**
 * @author Manuel Ernesto Bonilla Muñoz <mebonilla9@gmail.com>
 */
public class AppBajas implements Serializable {

  private Long idBaja;
  private String producto;
  private String regional;
  private Long fechaVenta;
  private String promedio;
  private String paisProm;
  private Map<String, String> periodos;

  public AppBajas() {
    periodos = new LinkedHashMap<>();
  }

  /**
   * @return the idBaja
   */
  public Long getIdBaja() {
    return idBaja;
  }

  /**
   * @param idBaja the idBaja to set
   */
  public void setIdBaja(Long idBaja) {
    this.idBaja = idBaja;
  }

  /**
   * @return the producto
   */
  public String getProducto() {
    return producto;
  }

  /**
   * @param producto the producto to set
   */
  public void setProducto(String producto) {
    this.producto = producto;
  }

  /**
   * @return the regional
   */
  public String getRegional() {
    return regional;
  }

  /**
   * @param regional the regional to set
   */
  public void setRegional(String regional) {
    this.regional = regional;
  }

  /**
   * @return the fechaVenta
   */
  public Long getFechaVenta() {
    return fechaVenta;
  }

  /**
   * @param fechaVenta the fechaVenta to set
   */
  public void setFechaVenta(Long fechaVenta) {
    this.fechaVenta = fechaVenta;
  }

  /**
   * @return the promedio
   */
  public String getPromedio() {
    return promedio;
  }

  /**
   * @param promedio the promedio to set
   */
  public void setPromedio(String promedio) {
    this.promedio = promedio;
  }

  /**
   * @return the paisProm
   */
  public String getPaisProm() {
    return paisProm;
  }

  /**
   * @param paisProm the paisProm to set
   */
  public void setPaisProm(String paisProm) {
    this.paisProm = paisProm;
  }

  /**
   * @return the atributos
   */
  public Map<String, String> getPeriodos() {
    return periodos;
  }

  /**
   * @param periodos the atributos to set
   */
  public void setPeriodos(Map<String, String> periodos) {
    this.periodos = periodos;
  }


}
