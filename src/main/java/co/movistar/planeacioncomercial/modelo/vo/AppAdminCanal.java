package co.movistar.planeacioncomercial.modelo.vo;

import java.io.Serializable;
import java.util.Date;

/**
 * @author Fabian
 */
public class AppAdminCanal implements Serializable {

  private Long idAdminCanal;
  private Date fechaInicio;
  private Date fechaFinal;
  private String nombreCanal;

  public AppAdminCanal() {
  }

  /**
   * @return the idAdminCanal
   */
  public Long getIdAdminCanal() {
    return idAdminCanal;
  }

  /**
   * @param idAdminCanal the idAdminCanal to set
   */
  public void setIdAdminCanal(Long idAdminCanal) {
    this.idAdminCanal = idAdminCanal;
  }

  /**
   * @return the fechaInicio
   */
  public Date getFechaInicio() {
    return fechaInicio;
  }

  /**
   * @param fechaInicio the fechaInicio to set
   */
  public void setFechaInicio(Date fechaInicio) {
    this.fechaInicio = fechaInicio;
  }

  /**
   * @return the fechaFinal
   */
  public Date getFechaFinal() {
    return fechaFinal;
  }

  /**
   * @param fechaFinal the fechaFinal to set
   */
  public void setFechaFinal(Date fechaFinal) {
    this.fechaFinal = fechaFinal;
  }

  /**
   * @return the nombreCanal
   */
  public String getNombreCanal() {
    return nombreCanal;
  }

  /**
   * @param nombreCanal the nombreCanal to set
   */
  public void setNombreCanal(String nombreCanal) {
    this.nombreCanal = nombreCanal;
  }


}
