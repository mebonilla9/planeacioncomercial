package co.movistar.planeacioncomercial.modelo.vo;

import java.io.Serializable;

/**
 * @author Manuel Ernesto Bonilla Muñoz <mebonilla9@gmail.com>
 */
public class AppCabecera implements Serializable {

  private Long idCabecera;
  private Long idUsuario;
  private Long cedula;
  private String correo;
  private String nombre;
  private String cargo;
  private String regionalf;
  private String canalf;
  private String esquema;

  public AppCabecera() {
  }

  /**
   * @return the idCabecera
   */
  public Long getIdCabecera() {
    return idCabecera;
  }

  /**
   * @param idCabecera the idCabecera to set
   */
  public void setIdCabecera(Long idCabecera) {
    this.idCabecera = idCabecera;
  }

  /**
   * @return the idUsuario
   */
  public Long getIdUsuario() {
    return idUsuario;
  }

  /**
   * @param idUsuario the idUsuario to set
   */
  public void setIdUsuario(Long idUsuario) {
    this.idUsuario = idUsuario;
  }

  /**
   * @return the cedula
   */
  public Long getCedula() {
    return cedula;
  }

  /**
   * @param cedula the cedula to set
   */
  public void setCedula(Long cedula) {
    this.cedula = cedula;
  }

  /**
   * @return the correo
   */
  public String getCorreo() {
    return correo;
  }

  /**
   * @param correo the correo to set
   */
  public void setCorreo(String correo) {
    this.correo = correo;
  }

  /**
   * @return the nombre
   */
  public String getNombre() {
    return nombre;
  }

  /**
   * @param nombre the nombre to set
   */
  public void setNombre(String nombre) {
    this.nombre = nombre;
  }

  /**
   * @return the cargo
   */
  public String getCargo() {
    return cargo;
  }

  /**
   * @param cargo the cargo to set
   */
  public void setCargo(String cargo) {
    this.cargo = cargo;
  }

  /**
   * @return the regionalf
   */
  public String getRegionalf() {
    return regionalf;
  }

  /**
   * @param regionalf the regionalf to set
   */
  public void setRegionalf(String regionalf) {
    this.regionalf = regionalf;
  }

  /**
   * @return the canalf
   */
  public String getCanalf() {
    return canalf;
  }

  /**
   * @param canalf the canalf to set
   */
  public void setCanalf(String canalf) {
    this.canalf = canalf;
  }

  /**
   * @return the esquema
   */
  public String getEsquema() {
    return esquema;
  }

  /**
   * @param esquema the esquema to set
   */
  public void setEsquema(String esquema) {
    this.esquema = esquema;
  }


}
