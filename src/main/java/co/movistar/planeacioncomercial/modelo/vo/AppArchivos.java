package co.movistar.planeacioncomercial.modelo.vo;

import java.io.Serializable;

/**
 * @author Manuel Ernesto Bonilla Muñoz <mebonilla9@gmail.com>
 */
public class AppArchivos implements Serializable {

  private Long id;
  private String nombreArchivo;
  private Long mes;
  private String ruta;
  private String modulo;
  private Boolean estado;
  private String categoria;

  public AppArchivos() {
  }

  /**
   * @return the id
   */
  public Long getId() {
    return id;
  }

  /**
   * @param id the id to set
   */
  public void setId(Long id) {
    this.id = id;
  }

  /**
   * @return the nombreArchivo
   */
  public String getNombreArchivo() {
    return nombreArchivo;
  }

  /**
   * @param nombreArchivo the nombreArchivo to set
   */
  public void setNombreArchivo(String nombreArchivo) {
    this.nombreArchivo = nombreArchivo;
  }

  /**
   * @return the mes
   */
  public Long getMes() {
    return mes;
  }

  /**
   * @param mes the mes to set
   */
  public void setMes(Long mes) {
    this.mes = mes;
  }

  /**
   * @return the ruta
   */
  public String getRuta() {
    return ruta;
  }

  /**
   * @param ruta the ruta to set
   */
  public void setRuta(String ruta) {
    this.ruta = ruta;
  }

  /**
   * @return the modulo
   */
  public String getModulo() {
    return modulo;
  }

  /**
   * @param modulo the modulo to set
   */
  public void setModulo(String modulo) {
    this.modulo = modulo;
  }

  /**
   * @return the estado
   */
  public Boolean getEstado() {
    return estado;
  }

  /**
   * @param estado the estado to set
   */
  public void setEstado(Boolean estado) {
    this.estado = estado;
  }

  /**
   * @return the categoria
   */
  public String getCategoria() {
    return categoria;
  }

  /**
   * @param categoria the categoria to set
   */
  public void setCategoria(String categoria) {
    this.categoria = categoria;
  }


}
