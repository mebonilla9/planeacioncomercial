package co.movistar.planeacioncomercial.modelo.vo;

import java.io.Serializable;

/**
 * @author Manuel Ernesto Bonilla Muñoz <mebonilla9@gmail.com>
 */
public class AppArticulos implements Serializable {

  private Long idInventario;
  private Long idArticulo;
  private String nombreArticulo;
  private String idPunto;
  private Long cantidad;

  public AppArticulos() {
  }

  /**
   * @return the idInventario
   */
  public Long getIdInventario() {
    return idInventario;
  }

  /**
   * @param idInventario the idInventario to set
   */
  public void setIdInventario(Long idInventario) {
    this.idInventario = idInventario;
  }

  /**
   * @return the idArticulo
   */
  public Long getIdArticulo() {
    return idArticulo;
  }

  /**
   * @param idArticulo the idArticulo to set
   */
  public void setIdArticulo(Long idArticulo) {
    this.idArticulo = idArticulo;
  }

  /**
   * @return the nombreArticulo
   */
  public String getNombreArticulo() {
    return nombreArticulo;
  }

  /**
   * @param nombreArticulo the nombreArticulo to set
   */
  public void setNombreArticulo(String nombreArticulo) {
    this.nombreArticulo = nombreArticulo;
  }

  /**
   * @return the idPunto
   */
  public String getIdPunto() {
    return idPunto;
  }

  /**
   * @param idPunto the idPunto to set
   */
  public void setIdPunto(String idPunto) {
    this.idPunto = idPunto;
  }

  /**
   * @return the cantidad
   */
  public Long getCantidad() {
    return cantidad;
  }

  /**
   * @param cantidad the cantidad to set
   */
  public void setCantidad(Long cantidad) {
    this.cantidad = cantidad;
  }


}
