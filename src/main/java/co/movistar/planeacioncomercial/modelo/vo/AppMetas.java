package co.movistar.planeacioncomercial.modelo.vo;

import java.io.Serializable;

/**
 * @author Fabian
 */
public class AppMetas implements Serializable {

  protected Long idMeta;
  protected String nombre;

  public AppMetas() {
  }

  /**
   * @return the idMeta
   */
  public Long getIdMeta() {
    return idMeta;
  }

  /**
   * @param idMeta the idMeta to set
   */
  public void setIdMeta(Long idMeta) {
    this.idMeta = idMeta;
  }

  /**
   * @return the nombre
   */
  public String getNombre() {
    return nombre;
  }

  /**
   * @param nombre the nombre to set
   */
  public void setNombre(String nombre) {
    this.nombre = nombre;
  }


}
