package co.movistar.planeacioncomercial.modelo.vo;

import java.io.Serializable;
import java.util.Date;

/**
 * @author Manuel Ernesto Bonilla Muñoz <mebonilla9@gmail.com>
 */
public class AppHistoriaNovedad implements Serializable {

  private Long idHistoria;
  private Long idUsuarionovedad;
  private Date fechaInicio;
  private Date fechaFinal;
  private Long idUsuariomodifica;
  private String valorAnterior;
  private String valorNuevo;
  private Long idNovedad;

  public AppHistoriaNovedad() {
  }


  public Long getIdHistoria() {
    return idHistoria;
  }

  public void setIdHistoria(Long idHistoria) {
    this.idHistoria = idHistoria;
  }

  public Long getIdUsuarionovedad() {
    return idUsuarionovedad;
  }

  public void setIdUsuarionovedad(Long idUsuarionovedad) {
    this.idUsuarionovedad = idUsuarionovedad;
  }

  public Date getFechaInicio() {
    return fechaInicio;
  }

  public void setFechaInicio(Date fechaInicio) {
    this.fechaInicio = fechaInicio;
  }

  public Date getFechaFinal() {
    return fechaFinal;
  }

  public void setFechaFinal(Date fechaFinal) {
    this.fechaFinal = fechaFinal;
  }

  public Long getIdUsuariomodifica() {
    return idUsuariomodifica;
  }

  public void setIdUsuariomodifica(Long idUsuariomodifica) {
    this.idUsuariomodifica = idUsuariomodifica;
  }

  public String getValorAnterior() {
    return valorAnterior;
  }

  public void setValorAnterior(String valorAnterior) {
    this.valorAnterior = valorAnterior;
  }

  public String getValorNuevo() {
    return valorNuevo;
  }

  public void setValorNuevo(String valorNuevo) {
    this.valorNuevo = valorNuevo;
  }

  public Long getIdNovedad() {
    return idNovedad;
  }

  public void setIdNovedad(Long idNovedad) {
    this.idNovedad = idNovedad;
  }
}
