package co.movistar.planeacioncomercial.modelo.vo;

import java.io.Serializable;

/**
 * @author Manuel Ernesto Bonilla Muñoz <mebonilla9@gmail.com>
 */
public class AppUsuarios implements Serializable {

  private Long idUsuario;
  private AppEstados estado;
  private Double cedula;
  private String correo;
  private String password;
  private String nombre;
  private String codInterno;
  private Long idJefe;
  private AppCargo cargo;
  private String mac;
  private Boolean bloqueo;
  private Long idCanal;
  private Boolean tutor;
  private Boolean malla;
  private Long contrato;

  public AppUsuarios() {
  }

  public AppUsuarios(Long idUsuario) {
    this.idUsuario = idUsuario;
  }

  /**
   * @return the idUsuario
   */
  public Long getIdUsuario() {
    return idUsuario;
  }

  /**
   * @param idUsuario the idUsuario to set
   */
  public void setIdUsuario(Long idUsuario) {
    this.idUsuario = idUsuario;
  }

  /**
   * @return the estado
   */
  public AppEstados getEstado() {
    return estado;
  }

  /**
   * @param estado the estado to set
   */
  public void setEstado(AppEstados estado) {
    this.estado = estado;
  }

  /**
   * @return the cedula
   */
  public Double getCedula() {
    return cedula;
  }

  /**
   * @param cedula the cedula to set
   */
  public void setCedula(Double cedula) {
    this.cedula = cedula;
  }

  /**
   * @return the correo
   */
  public String getCorreo() {
    return correo;
  }

  /**
   * @param correo the correo to set
   */
  public void setCorreo(String correo) {
    this.correo = correo;
  }

  /**
   * @return the password
   */
  public String getPassword() {
    return password;
  }

  /**
   * @param password the password to set
   */
  public void setPassword(String password) {
    this.password = password;
  }

  /**
   * @return the nombre
   */
  public String getNombre() {
    return nombre;
  }

  /**
   * @param nombre the nombre to set
   */
  public void setNombre(String nombre) {
    this.nombre = nombre;
  }

  /**
   * @return the codInterno
   */
  public String getCodInterno() {
    return codInterno;
  }

  /**
   * @param codInterno the codInterno to set
   */
  public void setCodInterno(String codInterno) {
    this.codInterno = codInterno;
  }

  /**
   * @return the idJefe
   */
  public Long getIdJefe() {
    return idJefe;
  }

  /**
   * @param idJefe the idJefe to set
   */
  public void setIdJefe(Long idJefe) {
    this.idJefe = idJefe;
  }

  /**
   * @return the cargo
   */
  public AppCargo getCargo() {
    return cargo;
  }

  /**
   * @param cargo the cargo to set
   */
  public void setCargo(AppCargo cargo) {
    this.cargo = cargo;
  }

  /**
   * @return the mac
   */
  public String getMac() {
    return mac;
  }

  /**
   * @param mac the mac to set
   */
  public void setMac(String mac) {
    this.mac = mac;
  }

  /**
   * @return the bloqueo
   */
  public Boolean getBloqueo() {
    return bloqueo;
  }

  /**
   * @param bloqueo the bloqueo to set
   */
  public void setBloqueo(Boolean bloqueo) {
    this.bloqueo = bloqueo;
  }

  /**
   * @return the idCanal
   */
  public Long getIdCanal() {
    return idCanal;
  }

  /**
   * @param idCanal the idCanal to set
   */
  public void setIdCanal(Long idCanal) {
    this.idCanal = idCanal;
  }

  /**
   * @return the tutor
   */
  public Boolean getTutor() {
    return tutor;
  }

  /**
   * @param tutor the tutor to set
   */
  public void setTutor(Boolean tutor) {
    this.tutor = tutor;
  }

  /**
   * @return the malla
   */
  public Boolean getMalla() {
    return malla;
  }

  /**
   * @param malla the malla to set
   */
  public void setMalla(Boolean malla) {
    this.malla = malla;
  }

  public Long getContrato() {
    return contrato;
  }

  public void setContrato(Long contrato) {
    this.contrato = contrato;
  }
}
