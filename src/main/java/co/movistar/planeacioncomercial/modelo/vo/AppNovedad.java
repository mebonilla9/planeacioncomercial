package co.movistar.planeacioncomercial.modelo.vo;

import java.io.Serializable;

/**
 * @author Manuel Ernesto Bonilla Muñoz <mebonilla9@gmail.com>
 */
public class AppNovedad implements Serializable {

  private Long idNovedad;
  private String nombreNovedad;
  private String modulos;
  private String tipo;

  public AppNovedad() {
  }

  public AppNovedad(Long idNovedad) {
    this.idNovedad = idNovedad;
  }

  /**
   * @return the idNovedad
   */
  public Long getIdNovedad() {
    return idNovedad;
  }

  /**
   * @param idNovedad the idNovedad to set
   */
  public void setIdNovedad(Long idNovedad) {
    this.idNovedad = idNovedad;
  }

  /**
   * @return the nombreNovedad
   */
  public String getNombreNovedad() {
    return nombreNovedad;
  }

  /**
   * @param nombreNovedad the nombreNovedad to set
   */
  public void setNombreNovedad(String nombreNovedad) {
    this.nombreNovedad = nombreNovedad;
  }


  public String getModulos() {
    return modulos;
  }

  public void setModulos(String modulos) {
    this.modulos = modulos;
  }

  public String getTipo() {
    return tipo;
  }

  public void setTipo(String tipo) {
    this.tipo = tipo;
  }
}
