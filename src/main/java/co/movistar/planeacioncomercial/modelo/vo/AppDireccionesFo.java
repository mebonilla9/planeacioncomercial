package co.movistar.planeacioncomercial.modelo.vo;

import java.io.Serializable;

/**
 * @author Manuel Ernesto Bonilla Muñoz <mebonilla9@gmail.com>
 */
public class AppDireccionesFo implements Serializable {

  private Long idDireccion;
  private String tipo;
  private String municipio;
  private String conjunto;
  private String complemento;
  private String direccionCompleta;

  public AppDireccionesFo() {
  }

  /**
   * @return the idDireccion
   */
  public Long getIdDireccion() {
    return idDireccion;
  }

  /**
   * @param idDireccion the idDireccion to set
   */
  public void setIdDireccion(Long idDireccion) {
    this.idDireccion = idDireccion;
  }

  /**
   * @return the tipo
   */
  public String getTipo() {
    return tipo;
  }

  /**
   * @param tipo the tipo to set
   */
  public void setTipo(String tipo) {
    this.tipo = tipo;
  }

  /**
   * @return the municipio
   */
  public String getMunicipio() {
    return municipio;
  }

  /**
   * @param municipio the municipio to set
   */
  public void setMunicipio(String municipio) {
    this.municipio = municipio;
  }

  /**
   * @return the conjunto
   */
  public String getConjunto() {
    return conjunto;
  }

  /**
   * @param conjunto the conjunto to set
   */
  public void setConjunto(String conjunto) {
    this.conjunto = conjunto;
  }

  /**
   * @return the complemento
   */
  public String getComplemento() {
    return complemento;
  }

  /**
   * @param complemento the complemento to set
   */
  public void setComplemento(String complemento) {
    this.complemento = complemento;
  }

  /**
   * @return the direccionCompleta
   */
  public String getDireccionCompleta() {
    return direccionCompleta;
  }

  /**
   * @param direccionCompleta the direccionCompleta to set
   */
  public void setDireccionCompleta(String direccionCompleta) {
    this.direccionCompleta = direccionCompleta;
  }


}
