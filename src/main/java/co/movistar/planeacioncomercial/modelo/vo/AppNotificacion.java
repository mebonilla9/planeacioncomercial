package co.movistar.planeacioncomercial.modelo.vo;

import java.io.Serializable;

/**
 * @author Manuel Ernesto Bonilla Muñoz <mebonilla9@gmail.com>
 */
public class AppNotificacion implements Serializable {

  private Long idNotificacion;
  private String notificacion;
  private AppCargo cargo;
  private Long activo;
  private Long nivel;

  public AppNotificacion() {
  }

  public Long getIdNotificacion() {
    return idNotificacion;
  }

  public void setIdNotificacion(Long idNotificacion) {
    this.idNotificacion = idNotificacion;
  }

  public String getNotificacion() {
    return notificacion;
  }

  public void setNotificacion(String notificacion) {
    this.notificacion = notificacion;
  }

  public Long getActivo() {
    return activo;
  }

  public void setActivo(Long activo) {
    this.activo = activo;
  }

  /**
   * @return the cargo
   */
  public AppCargo getCargo() {
    return cargo;
  }

  /**
   * @param cargo the cargo to set
   */
  public void setCargo(AppCargo cargo) {
    this.cargo = cargo;
  }

  /**
   * @return the nivel
   */
  public Long getNivel() {
    return nivel;
  }

  /**
   * @param nivel the nivel to set
   */
  public void setNivel(Long nivel) {
    this.nivel = nivel;
  }

}
