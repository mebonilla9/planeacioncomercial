package co.movistar.planeacioncomercial.modelo.vo;

import java.io.Serializable;

/**
 * @author Manuel Ernesto Bonilla Muñoz <mebonilla9@gmail.com>
 */
public class AppBarrios implements Serializable {

  private Long idBarrio;
  private Long divipola;
  private String departamento;
  private String municipio;
  private String nombre;

  public AppBarrios() {
  }

  /**
   * @return the idBarrio
   */
  public Long getIdBarrio() {
    return idBarrio;
  }

  /**
   * @param idBarrio the idBarrio to set
   */
  public void setIdBarrio(Long idBarrio) {
    this.idBarrio = idBarrio;
  }

  /**
   * @return the divipola
   */
  public Long getDivipola() {
    return divipola;
  }

  /**
   * @param divipola the divipola to set
   */
  public void setDivipola(Long divipola) {
    this.divipola = divipola;
  }

  /**
   * @return the departamento
   */
  public String getDepartamento() {
    return departamento;
  }

  /**
   * @param departamento the departamento to set
   */
  public void setDepartamento(String departamento) {
    this.departamento = departamento;
  }

  /**
   * @return the municipio
   */
  public String getMunicipio() {
    return municipio;
  }

  /**
   * @param municipio the municipio to set
   */
  public void setMunicipio(String municipio) {
    this.municipio = municipio;
  }

  /**
   * @return the nombre
   */
  public String getNombre() {
    return nombre;
  }

  /**
   * @param nombre the nombre to set
   */
  public void setNombre(String nombre) {
    this.nombre = nombre;
  }


}
