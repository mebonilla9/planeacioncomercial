package co.movistar.planeacioncomercial.modelo.vo;

import com.fasterxml.jackson.annotation.JsonIgnore;

import java.io.Serializable;
import java.util.Date;

/**
 * @author Manuel Ernesto Bonilla Muñoz <mebonilla9@gmail.com>
 */
public class AppVentasCe implements Serializable {

  @JsonIgnore
  private Long idVentasCe;
  private Long idUsuarioModifica;
  private AppOficinasCe oficina;
  private Long idSupervisor;
  private Date fecha;
  private Long idUsuario;
  private AppRol rol;
  private AppNovedad novedad;

  public AppVentasCe() {
  }

  /**
   * @return the idVentasCe
   */
  public Long getIdVentasCe() {
    return idVentasCe;
  }

  /**
   * @param idVentasCe the idVentasCe to set
   */
  public void setIdVentasCe(Long idVentasCe) {
    this.idVentasCe = idVentasCe;
  }

  /**
   * @return the idUsuarioModifica
   */
  public Long getIdUsuarioModifica() {
    return idUsuarioModifica;
  }

  /**
   * @param idUsuarioModifica the idUsuarioModifica to set
   */
  public void setIdUsuarioModifica(Long idUsuarioModifica) {
    this.idUsuarioModifica = idUsuarioModifica;
  }

  /**
   * @return the oficina
   */
  public AppOficinasCe getOficina() {
    return oficina;
  }

  /**
   * @param oficina the ciudad to set
   */
  public void setOficina(AppOficinasCe oficina) {
    this.oficina = oficina;
  }

  /**
   * @return the idSupervisor
   */
  public Long getIdSupervisor() {
    return idSupervisor;
  }

  /**
   * @param idSupervisor the idSupervisor to set
   */
  public void setIdSupervisor(Long idSupervisor) {
    this.idSupervisor = idSupervisor;
  }

  /**
   * @return the fecha
   */
  public Date getFecha() {
    return fecha;
  }

  /**
   * @param fecha the fecha to set
   */
  public void setFecha(Date fecha) {
    this.fecha = fecha;
  }

  /**
   * @return the idUsuario
   */
  public Long getIdUsuario() {
    return idUsuario;
  }

  /**
   * @param idUsuario the idUsuario to set
   */
  public void setIdUsuario(Long idUsuario) {
    this.idUsuario = idUsuario;
  }

  /**
   * @return the novedad
   */
  public AppNovedad getNovedad() {
    return novedad;
  }

  /**
   * @param novedad the novedad to set
   */
  public void setNovedad(AppNovedad novedad) {
    this.novedad = novedad;
  }

  public AppRol getRol() {
    return rol;
  }

  public void setRol(AppRol rol) {
    this.rol = rol;
  }
}
