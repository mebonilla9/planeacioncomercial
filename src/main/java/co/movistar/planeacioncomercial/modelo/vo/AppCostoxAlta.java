package co.movistar.planeacioncomercial.modelo.vo;

import java.io.Serializable;

/**
 * @author Manuel Ernesto Bonilla Muñoz <mebonilla9@gmail.com>
 */
public class AppCostoxAlta implements Serializable {

  private Long idCosto;
  private Long idUsuario;
  private String canal;
  private String segmento;
  private String producto;
  private String periodo;
  private Double costoPorAlta;
  private Double participacion;
  private Double vecesArpu;

  public AppCostoxAlta() {
  }

  /**
   * @return the idCosto
   */
  public Long getIdCosto() {
    return idCosto;
  }

  /**
   * @param idCosto the idCosto to set
   */
  public void setIdCosto(Long idCosto) {
    this.idCosto = idCosto;
  }

  /**
   * @return the idUsuario
   */
  public Long getIdUsuario() {
    return idUsuario;
  }

  /**
   * @param idUsuario the idUsuario to set
   */
  public void setIdUsuario(Long idUsuario) {
    this.idUsuario = idUsuario;
  }

  /**
   * @return the canal
   */
  public String getCanal() {
    return canal;
  }

  /**
   * @param canal the canal to set
   */
  public void setCanal(String canal) {
    this.canal = canal;
  }

  /**
   * @return the segmento
   */
  public String getSegmento() {
    return segmento;
  }

  /**
   * @param segmento the segmento to set
   */
  public void setSegmento(String segmento) {
    this.segmento = segmento;
  }

  /**
   * @return the producto
   */
  public String getProducto() {
    return producto;
  }

  /**
   * @param producto the producto to set
   */
  public void setProducto(String producto) {
    this.producto = producto;
  }

  /**
   * @return the periodo
   */
  public String getPeriodo() {
    return periodo;
  }

  /**
   * @param periodo the periodo to set
   */
  public void setPeriodo(String periodo) {
    this.periodo = periodo;
  }

  /**
   * @return the costoPorAlta
   */
  public Double getCostoPorAlta() {
    return costoPorAlta;
  }

  /**
   * @param costoPorAlta the costoPorAlta to set
   */
  public void setCostoPorAlta(Double costoPorAlta) {
    this.costoPorAlta = costoPorAlta;
  }

  /**
   * @return the participacion
   */
  public Double getParticipacion() {
    return participacion;
  }

  /**
   * @param participacion the participacion to set
   */
  public void setParticipacion(Double participacion) {
    this.participacion = participacion;
  }

  /**
   * @return the vecesArpu
   */
  public Double getVecesArpu() {
    return vecesArpu;
  }

  /**
   * @param vecesArpu the vecesArpu to set
   */
  public void setVecesArpu(Double vecesArpu) {
    this.vecesArpu = vecesArpu;
  }


}
