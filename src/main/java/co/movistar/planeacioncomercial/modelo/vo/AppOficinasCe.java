package co.movistar.planeacioncomercial.modelo.vo;

import java.io.Serializable;

/**
 * @author Manuel Ernesto Bonilla Muñoz <mebonilla9@gmail.com>
 */
public class AppOficinasCe implements Serializable {

  private Long idOficina;
  private String nombrePunto;
  private String direccion;
  private String ciudad;
  private String departamento;
  private String regional;
  private Long ccCoordinador;

  public AppOficinasCe() {
  }

  public AppOficinasCe(Long idOficina) {
    this.idOficina = idOficina;
  }

  /**
   * @return the idOficina
   */
  public Long getIdOficina() {
    return idOficina;
  }

  /**
   * @param idOficina the idOficina to set
   */
  public void setIdOficina(Long idOficina) {
    this.idOficina = idOficina;
  }

  /**
   * @return the nombrePunto
   */
  public String getNombrePunto() {
    return nombrePunto;
  }

  /**
   * @param nombrePunto the nombrePunto to set
   */
  public void setNombrePunto(String nombrePunto) {
    this.nombrePunto = nombrePunto;
  }

  /**
   * @return the direccion
   */
  public String getDireccion() {
    return direccion;
  }

  /**
   * @param direccion the direccion to set
   */
  public void setDireccion(String direccion) {
    this.direccion = direccion;
  }

  /**
   * @return the ciudad
   */
  public String getCiudad() {
    return ciudad;
  }

  /**
   * @param ciudad the ciudad to set
   */
  public void setCiudad(String ciudad) {
    this.ciudad = ciudad;
  }

  /**
   * @return the departamento
   */
  public String getDepartamento() {
    return departamento;
  }

  /**
   * @param departamento the departamento to set
   */
  public void setDepartamento(String departamento) {
    this.departamento = departamento;
  }

  /**
   * @return the regional
   */
  public String getRegional() {
    return regional;
  }

  /**
   * @param regional the regional to set
   */
  public void setRegional(String regional) {
    this.regional = regional;
  }

  /**
   * @return the CcCoordinador
   */
  public Long getCcCoordinador() {
    return ccCoordinador;
  }

  /**
   * @param CcCoordinador the CcCoordinador to set
   */
  public void setCcCoordinador(Long CcCoordinador) {
    this.ccCoordinador = CcCoordinador;
  }


}
