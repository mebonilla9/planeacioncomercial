package co.movistar.planeacioncomercial.modelo.vo;

import java.io.Serializable;

/**
 * @author Manuel Ernesto Bonilla Muñoz <mebonilla9@gmail.com>
 */
public class AppCruceVial implements Serializable {

  private Long idCruceVial;
  private String tipoVia;
  private String cruceVial;

  public AppCruceVial() {
  }

  /**
   * @return the idCruceVial
   */
  public Long getIdCruceVial() {
    return idCruceVial;
  }

  /**
   * @param idCruceVial the idCruceVial to set
   */
  public void setIdCruceVial(Long idCruceVial) {
    this.idCruceVial = idCruceVial;
  }

  /**
   * @return the tipoVia
   */
  public String getTipoVia() {
    return tipoVia;
  }

  /**
   * @param tipoVia the tipoVia to set
   */
  public void setTipoVia(String tipoVia) {
    this.tipoVia = tipoVia;
  }

  /**
   * @return the cruceVial
   */
  public String getCruceVial() {
    return cruceVial;
  }

  /**
   * @param cruceVial the cruceVial to set
   */
  public void setCruceVial(String cruceVial) {
    this.cruceVial = cruceVial;
  }


}
