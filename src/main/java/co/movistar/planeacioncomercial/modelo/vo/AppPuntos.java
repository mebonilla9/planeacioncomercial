package co.movistar.planeacioncomercial.modelo.vo;

import java.io.Serializable;

/**
 * @author Manuel Ernesto Bonilla Muñoz <mebonilla9@gmail.com>
 */
public class AppPuntos implements Serializable {

  private Long idPunto;
  private String nombrePunto;
  private String direccion;
  private String ciudad;
  private String departamento;
  private String regional;
  private Long idUsuario;
  private Long ccCoordinador;

  public AppPuntos() {
  }

  public AppPuntos(Long idPunto) {
    this.idPunto = idPunto;
  }

  /**
   * @return the idPunto
   */
  public Long getIdPunto() {
    return idPunto;
  }

  /**
   * @param idPunto the idPunto to set
   */
  public void setIdPunto(Long idPunto) {
    this.idPunto = idPunto;
  }

  /**
   * @return the nombrePunto
   */
  public String getNombrePunto() {
    return nombrePunto;
  }

  /**
   * @param nombrePunto the nombrePunto to set
   */
  public void setNombrePunto(String nombrePunto) {
    this.nombrePunto = nombrePunto;
  }

  /**
   * @return the direccion
   */
  public String getDireccion() {
    return direccion;
  }

  /**
   * @param direccion the direccion to set
   */
  public void setDireccion(String direccion) {
    this.direccion = direccion;
  }

  /**
   * @return the ciudad
   */
  public String getCiudad() {
    return ciudad;
  }

  /**
   * @param ciudad the ciudad to set
   */
  public void setCiudad(String ciudad) {
    this.ciudad = ciudad;
  }

  /**
   * @return the departamento
   */
  public String getDepartamento() {
    return departamento;
  }

  /**
   * @param departamento the departamento to set
   */
  public void setDepartamento(String departamento) {
    this.departamento = departamento;
  }

  /**
   * @return the regional
   */
  public String getRegional() {
    return regional;
  }

  /**
   * @param regional the regional to set
   */
  public void setRegional(String regional) {
    this.regional = regional;
  }

  /**
   * @return the idUsuario
   */
  public Long getIdUsuario() {
    return idUsuario;
  }

  /**
   * @param idUsuario the idUsuario to set
   */
  public void setIdUsuario(Long idUsuario) {
    this.idUsuario = idUsuario;
  }

  /**
   * @return the ccCoordinador
   */
  public Long getCcCoordinador() {
    return ccCoordinador;
  }

  /**
   * @param ccCoordinador the idCoordinador to set
   */
  public void setCcCoordinador(Long ccCoordinador) {
    this.ccCoordinador = ccCoordinador;
  }

}
