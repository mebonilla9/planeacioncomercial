package co.movistar.planeacioncomercial.modelo.vo;

import java.io.Serializable;

/**
 * @author Fabian
 */
public class AppCiudad implements Serializable {

  private Long idCiudad;
  private String nombre;
  private Boolean estado;
  private Long idRegional;

  public AppCiudad() {
  }

  public AppCiudad(Long idCiudad) {
    this.idCiudad = idCiudad;
  }

  /**
   * @return the idCiudad
   */
  public Long getIdCiudad() {
    return idCiudad;
  }

  /**
   * @param idCiudad the idCiudad to set
   */
  public void setIdCiudad(Long idCiudad) {
    this.idCiudad = idCiudad;
  }

  /**
   * @return the nombre
   */
  public String getNombre() {
    return nombre;
  }

  /**
   * @param nombre the nombre to set
   */
  public void setNombre(String nombre) {
    this.nombre = nombre;
  }

  /**
   * @return the estado
   */
  public Boolean getEstado() {
    return estado;
  }

  /**
   * @param estado the estado to set
   */
  public void setEstado(Boolean estado) {
    this.estado = estado;
  }

  /**
   * @return the idRegional
   */
  public Long getIdRegional() {
    return idRegional;
  }

  /**
   * @param idRegional the idRegional to set
   */
  public void setIdRegional(Long idRegional) {
    this.idRegional = idRegional;
  }


}
