package co.movistar.planeacioncomercial.modelo.vo;

import java.io.Serializable;
import java.util.Date;

/**
 * @author Manuel Ernesto Bonilla Muñoz <mebonilla9@gmail.com>
 */
public class AppDatosCe implements Serializable {

  private Long idDatosce;
  private Long idUsuariomodifica;
  private AppRol rol;
  private AppOficinasCe punto;
  private AppNovedad novedad;
  private Date fecha;
  private Long idUsuario;

  public AppDatosCe() {
  }

  public AppDatosCe(Long idDatosce) {
    this.idDatosce = idDatosce;
  }

  /**
   * @return the idDatosce
   */
  public Long getIdDatosce() {
    return idDatosce;
  }

  /**
   * @param idDatosce the idDatosce to set
   */
  public void setIdDatosce(Long idDatosce) {
    this.idDatosce = idDatosce;
  }

  /**
   * @return the idUsuariomodifica
   */
  public Long getIdUsuariomodifica() {
    return idUsuariomodifica;
  }

  /**
   * @param idUsuariomodifica the idUsuariomodifica to set
   */
  public void setIdUsuariomodifica(Long idUsuariomodifica) {
    this.idUsuariomodifica = idUsuariomodifica;
  }

  /**
   * @return the punto
   */
  public AppOficinasCe getPunto() {
    return punto;
  }

  /**
   * @param punto the oficina to set
   */
  public void setPunto(AppOficinasCe punto) {
    this.punto = punto;
  }

  /**
   * @return the novedad
   */
  public AppNovedad getNovedad() {
    return novedad;
  }

  /**
   * @param novedad the novedad to set
   */
  public void setNovedad(AppNovedad novedad) {
    this.novedad = novedad;
  }

  /**
   * @return the fecha
   */
  public Date getFecha() {
    return fecha;
  }

  /**
   * @param fecha the fecha to set
   */
  public void setFecha(Date fecha) {
    this.fecha = fecha;
  }

  /**
   * @return the idUsuario
   */
  public Long getIdUsuario() {
    return idUsuario;
  }

  /**
   * @param idUsuario the idUsuario to set
   */
  public void setIdUsuario(Long idUsuario) {
    this.idUsuario = idUsuario;
  }

  /**
   * @return the rol
   */
  public AppRol getRol() {
    return rol;
  }

  /**
   * @param rol the rol to set
   */
  public void setRol(AppRol rol) {
    this.rol = rol;
  }

}
