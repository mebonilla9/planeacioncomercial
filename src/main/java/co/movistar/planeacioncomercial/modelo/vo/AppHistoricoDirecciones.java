package co.movistar.planeacioncomercial.modelo.vo;

import java.io.Serializable;

/**
 * @author Manuel Ernesto Bonilla Muñoz <mebonilla9@gmail.com>
 */
public class AppHistoricoDirecciones implements Serializable {

  private Long idDireccion;
  private Long idUsuario;
  private String direccion;
  private String barrio;
  private String ciudad;
  private String coordenadaX;
  private String coordenadaY;

  public AppHistoricoDirecciones() {
  }

  /**
   * @return the idDireccion
   */
  public Long getIdDireccion() {
    return idDireccion;
  }

  /**
   * @param idDireccion the idDireccion to set
   */
  public void setIdDireccion(Long idDireccion) {
    this.idDireccion = idDireccion;
  }

  /**
   * @return the idUsuario
   */
  public Long getIdUsuario() {
    return idUsuario;
  }

  /**
   * @param idUsuario the idUsuario to set
   */
  public void setIdUsuario(Long idUsuario) {
    this.idUsuario = idUsuario;
  }

  /**
   * @return the direccion
   */
  public String getDireccion() {
    return direccion;
  }

  /**
   * @param direccion the direccion to set
   */
  public void setDireccion(String direccion) {
    this.direccion = direccion;
  }

  /**
   * @return the barrio
   */
  public String getBarrio() {
    return barrio;
  }

  /**
   * @param barrio the barrio to set
   */
  public void setBarrio(String barrio) {
    this.barrio = barrio;
  }

  /**
   * @return the ciudad
   */
  public String getCiudad() {
    return ciudad;
  }

  /**
   * @param ciudad the ciudad to set
   */
  public void setCiudad(String ciudad) {
    this.ciudad = ciudad;
  }

  /**
   * @return the coordenadaX
   */
  public String getCoordenadaX() {
    return coordenadaX;
  }

  /**
   * @param coordenadaX the coordenadaX to set
   */
  public void setCoordenadaX(String coordenadaX) {
    this.coordenadaX = coordenadaX;
  }

  /**
   * @return the coordenadaY
   */
  public String getCoordenadaY() {
    return coordenadaY;
  }

  /**
   * @param coordenadaY the coordenadaY to set
   */
  public void setCoordenadaY(String coordenadaY) {
    this.coordenadaY = coordenadaY;
  }

}
