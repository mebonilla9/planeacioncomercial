/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package co.movistar.planeacioncomercial.modelo.dto;

import co.movistar.planeacioncomercial.modelo.vo.AppDisponibilidad;

import java.io.Serializable;
import java.util.List;

/**
 * @author Lord_Nightmare
 */
public class InfoDisponibilidadDTO implements Serializable {

  private AppDisponibilidad cabecera;
  private List<AppDisponibilidad> cajas;
  private List<AppDisponibilidad> capacidades;

  /**
   * @return the cabecera
   */
  public AppDisponibilidad getCabecera() {
    return cabecera;
  }

  /**
   * @param cabecera the cabecera to set
   */
  public void setCabecera(AppDisponibilidad cabecera) {
    this.cabecera = cabecera;
  }

  /**
   * @return the cajas
   */
  public List<AppDisponibilidad> getCajas() {
    return cajas;
  }

  /**
   * @param cajas the cajas to set
   */
  public void setCajas(List<AppDisponibilidad> cajas) {
    this.cajas = cajas;
  }

  /**
   * @return the capacidades
   */
  public List<AppDisponibilidad> getCapacidades() {
    return capacidades;
  }

  /**
   * @param capacidades the capacidades to set
   */
  public void setCapacidades(List<AppDisponibilidad> capacidades) {
    this.capacidades = capacidades;
  }

}
