/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package co.movistar.planeacioncomercial.modelo.dto;

import co.movistar.planeacioncomercial.modelo.vo.AppDatosCe;
import co.movistar.planeacioncomercial.modelo.vo.AppHistoriaNovedad;
import co.movistar.planeacioncomercial.modelo.vo.AppUsuarios;

import java.io.Serializable;
import java.util.List;

/**
 * @author Lord_Nightmare
 */
public class InsertarUsuarioDto implements Serializable {

  private AppUsuarios usuario;
  //private AppDatosCe punto;
  private List<AppHistoriaNovedad> listaNovedades;
  private Object datosAdicionales;

  /**
   * @return the usuario
   */
  public AppUsuarios getUsuario() {
    return usuario;
  }

  /**
   * @param usuario the usuario to set
   */
  public void setUsuario(AppUsuarios usuario) {
    this.usuario = usuario;
  }

  /**
   * @return the punto
   */
  /*public AppDatosCe getPunto() {
    return punto;
  }*/

  /**
   * @param punto the punto to set
   */
  /*public void setPunto(AppDatosCe punto) {
    this.punto = punto;
  }*/

  /**
   * @return the listaNovedades
   */
  public List<AppHistoriaNovedad> getListaNovedades() {
    return listaNovedades;
  }

  /**
   * @param listaNovedades the listaNovedades to set
   */
  public void setListaNovedades(List<AppHistoriaNovedad> listaNovedades) {
    this.listaNovedades = listaNovedades;
  }

  /**
   * @return the datosAdicionales
   */
  public Object getDatosAdicionales() {
    return datosAdicionales;
  }

  /**
   * @param datosAdicionales the datosAdicionales to set
   */
  public void setDatosAdicionales(Object datosAdicionales) {
    this.datosAdicionales = datosAdicionales;
  }

}
