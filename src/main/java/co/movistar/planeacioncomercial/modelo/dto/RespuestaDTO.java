/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package co.movistar.planeacioncomercial.modelo.dto;

import co.movistar.planeacioncomercial.negocio.constantes.EMensajes;
import co.movistar.planeacioncomercial.negocio.excepciones.PlaneacionComercialException;

/**
 * @author Manuel Ernesto Bonilla Muñoz <mebonilla9@gmail.com>
 */
public class RespuestaDTO {

  private int codigo;
  private String mensaje;
  private Object datos;

  public RespuestaDTO() {

  }

  public RespuestaDTO(EMensajes mensaje) {
    this.codigo = mensaje.getCodigo();
    this.mensaje = mensaje.getDescripcion();
  }

  public RespuestaDTO(PlaneacionComercialException ex){
    this.codigo = ex.getCodigo();
    this.mensaje = ex.getMensaje();
  }

  public int getCodigo() {
    return codigo;
  }

  public void setCodigo(int codigo) {
    this.codigo = codigo;
  }

  public String getMensaje() {
    return mensaje;
  }

  public void setMensaje(String mensaje) {
    this.mensaje = mensaje;
  }

  public Object getDatos() {
    return datos;
  }

  public void setDatos(Object datos) {
    this.datos = datos;
  }

}
