/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package co.movistar.planeacioncomercial.modelo.dto;

import co.movistar.planeacioncomercial.modelo.vo.*;

import java.util.List;

/**
 * @author Lord_Nightmare
 */
public class InfoMetaDTO {

  private AppUsuarios usuario;
  private AppPuntos punto;
  private Object infoRol;
  private List<AppMetaUsuario> infoMetas;
  private AppHistoriaNovedad novedad;
  private AppNovedad ultimaNovedad;

  /**
   * @return the punto
   */
  public AppPuntos getPunto() {
    return punto;
  }

  /**
   * @param punto the punto to set
   */
  public void setPunto(AppPuntos punto) {
    this.punto = punto;
  }

  /**
   * @return the usuario
   */
  public AppUsuarios getUsuario() {
    return usuario;
  }

  /**
   * @param usuario the usuario to set
   */
  public void setUsuario(AppUsuarios usuario) {
    this.usuario = usuario;
  }

  /**
   * @return the infoRol
   */
  public Object getInfoRol() {
    return infoRol;
  }

  /**
   * @param infoRol the infoRol to set
   */
  public void setInfoRol(Object infoRol) {
    this.infoRol = infoRol;
  }

  /**
   * @return the infoMetas
   */
  public List<AppMetaUsuario> getInfoMetas() {
    return infoMetas;
  }

  /**
   * @param infoMetas the infoMetas to set
   */
  public void setInfoMetas(List<AppMetaUsuario> infoMetas) {
    this.infoMetas = infoMetas;
  }

  /**
   * @return the novedad
   */
  public AppHistoriaNovedad getNovedad() {
    return novedad;
  }

  /**
   * @param novedad the novedad to set
   */
  public void setNovedad(AppHistoriaNovedad novedad) {
    this.novedad = novedad;
  }


  public AppNovedad getUltimaNovedad() {
    return ultimaNovedad;
  }

  public void setUltimaNovedad(AppNovedad ultimaNovedad) {
    this.ultimaNovedad = ultimaNovedad;
  }
}
