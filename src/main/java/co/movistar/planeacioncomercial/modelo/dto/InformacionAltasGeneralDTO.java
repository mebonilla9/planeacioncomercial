/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package co.movistar.planeacioncomercial.modelo.dto;

import co.movistar.planeacioncomercial.modelo.vo.AppPorcentajeCorte;

import java.io.Serializable;
import java.util.List;

/**
 * @author Lord_Nightmare
 */
public class InformacionAltasGeneralDTO implements Serializable {

  private AppPorcentajeCorte porcentajeCorte;
  private List<List<GeneracionAltasDTO>> listaAltas;

  /**
   * @return the porcentajeCorte
   */
  public AppPorcentajeCorte getPorcentajeCorte() {
    return porcentajeCorte;
  }

  /**
   * @param porcentajeCorte the porcentajeCorte to set
   */
  public void setPorcentajeCorte(AppPorcentajeCorte porcentajeCorte) {
    this.porcentajeCorte = porcentajeCorte;
  }

  /**
   * @return the listaAltas
   */
  public List<List<GeneracionAltasDTO>> getListaAltas() {
    return listaAltas;
  }

  /**
   * @param listaAltas the listaAltas to set
   */
  public void setListaAltas(List<List<GeneracionAltasDTO>> listaAltas) {
    this.listaAltas = listaAltas;
  }


}
