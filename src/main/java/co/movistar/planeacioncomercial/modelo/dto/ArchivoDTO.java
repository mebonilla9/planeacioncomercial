/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package co.movistar.planeacioncomercial.modelo.dto;

import java.util.List;

/**
 * @author lord_nightmare
 */
public class ArchivoDTO {

  private List<String> uploadedFiles;
  private Integer code;
  private String message;

  public ArchivoDTO(List<String> uploadedFiles, Integer code, String message) {
    this.uploadedFiles = uploadedFiles;
    this.code = code;
    this.message = message;
  }

  /**
   * @return the uploadedFiles
   */
  public List<String> getUploadedFiles() {
    return uploadedFiles;
  }

  /**
   * @param uploadedFiles the uploadedFiles to set
   */
  public void setUploadedFiles(List<String> uploadedFiles) {
    this.uploadedFiles = uploadedFiles;
  }

  /**
   * @return the code
   */
  public Integer getCode() {
    return code;
  }

  /**
   * @param code the code to set
   */
  public void setCode(Integer code) {
    this.code = code;
  }

  /**
   * @return the message
   */
  public String getMessage() {
    return message;
  }

  /**
   * @param message the message to set
   */
  public void setMessage(String message) {
    this.message = message;
  }


}
