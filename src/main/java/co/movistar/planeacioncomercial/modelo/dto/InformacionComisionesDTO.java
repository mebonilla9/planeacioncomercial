/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package co.movistar.planeacioncomercial.modelo.dto;

import co.movistar.planeacioncomercial.modelo.vo.AppCostoxAlta;

import java.util.List;

/**
 * @author lord_nightmare
 */
public class InformacionComisionesDTO {

  private List<List<AppCostoxAlta>> listaComisiones;

  /**
   * @return the listaComisiones
   */
  public List<List<AppCostoxAlta>> getListaComisiones() {
    return listaComisiones;
  }

  /**
   * @param listaComisiones the listaComisiones to set
   */
  public void setListaComisiones(List<List<AppCostoxAlta>> listaComisiones) {
    this.listaComisiones = listaComisiones;
  }

}
