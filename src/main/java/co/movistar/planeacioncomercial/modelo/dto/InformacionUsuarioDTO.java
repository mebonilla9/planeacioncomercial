/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package co.movistar.planeacioncomercial.modelo.dto;

import co.movistar.planeacioncomercial.modelo.vo.AppMenu;
import co.movistar.planeacioncomercial.modelo.vo.AppUsuarios;

import java.util.List;

/**
 * @author lord_nightmare
 */
public class InformacionUsuarioDTO {

  private AppUsuarios usuario;
  private List<AppMenu> menu;

  /**
   * @return the usuario
   */
  public AppUsuarios getUsuario() {
    return usuario;
  }

  /**
   * @param usuario the usuario to set
   */
  public void setUsuario(AppUsuarios usuario) {
    this.usuario = usuario;
  }

  /**
   * @return the menu
   */
  public List<AppMenu> getMenu() {
    return menu;
  }

  /**
   * @param menu the menu to set
   */
  public void setMenu(List<AppMenu> menu) {
    this.menu = menu;
  }

}
