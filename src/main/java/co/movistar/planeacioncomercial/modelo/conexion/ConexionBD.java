/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package co.movistar.planeacioncomercial.modelo.conexion;

import co.movistar.planeacioncomercial.negocio.constantes.EMensajes;
import co.movistar.planeacioncomercial.negocio.excepciones.PlaneacionComercialException;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.sql.DataSource;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * @author Manuel Ernesto Bonilla Muñoz <mebonilla9@gmail.com>
 */
public class ConexionBD {

  public static Connection conectar() throws PlaneacionComercialException {
    try {
      Class.forName("com.microsoft.sqlserver.jdbc.SQLServerDriver");
      Context ctx = new InitialContext();
      DataSource ds = (DataSource) ctx.lookup("java:/jdbc/AppPlanecomDS");
      Connection cnn = ds.getConnection();
      cnn.setAutoCommit(false);
      return cnn;
    } catch (NamingException | SQLException | ClassNotFoundException ex) {
      ex.printStackTrace();
      throw new PlaneacionComercialException(EMensajes.ERROR_CONEXION_BD);
    }
  }

  public static void desconectar(Connection cnn) {
    desconectar(cnn, null);
  }

  public static void desconectar(PreparedStatement ps) {
    desconectar(null, ps);

  }

  public static void desconectar(Connection cnn, PreparedStatement ps) {
    try {
      if (ps != null) {
        ps.close();
      }
      if (cnn != null) {
        cnn.close();
      }
    } catch (SQLException ex) {
      Logger.getLogger(ConexionBD.class.getName()).log(Level.SEVERE, null, ex);
    }
  }

  public static void rollback(Connection cnn) {
    try {
      cnn.rollback();
    } catch (SQLException ex) {

    }
  }

  public static void commit(Connection cnn) {
    try {
      cnn.commit();
    } catch (SQLException ex) {
      Logger.getLogger(ConexionBD.class.getName()).log(Level.SEVERE, null, ex);
    }
  }

}
