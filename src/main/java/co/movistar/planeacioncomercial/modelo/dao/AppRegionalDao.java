package co.movistar.planeacioncomercial.modelo.dao;

import co.movistar.planeacioncomercial.modelo.conexion.ConexionBD;
import co.movistar.planeacioncomercial.modelo.dao.crud.AppRegionalCrud;
import co.movistar.planeacioncomercial.modelo.vo.AppRegional;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * @author Manuel Ernesto Bonilla Muñoz <mebonilla9@gmail.com>
 */
public class AppRegionalDao extends AppRegionalCrud {

  public AppRegionalDao(Connection cnn) {
    super(cnn);
  }

  public AppRegional consultarCiudad(Long idCiudad) throws SQLException {
    PreparedStatement sentencia = null;
    AppRegional obj = null;
    try {
      String sql = "select * from APP_REGIONAL where ID_REGIONAL = (select ID_REGIONAL from APP_CIUDAD where ID_CIUDAD = ?)";
      sentencia = cnn.prepareStatement(sql);
      sentencia.setLong(1, idCiudad);
      ResultSet rs = sentencia.executeQuery();
      if (rs.next()) {
        obj = getAppRegional(rs);
      }
    } finally {
      ConexionBD.desconectar(sentencia);
    }
    return obj;
  }
}
