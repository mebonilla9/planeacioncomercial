package co.movistar.planeacioncomercial.modelo.dao;

import co.movistar.planeacioncomercial.modelo.conexion.ConexionBD;
import co.movistar.planeacioncomercial.modelo.dao.crud.AppOficinasCeCrud;
import co.movistar.planeacioncomercial.modelo.vo.AppOficinasCe;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class AppOficinasCeDao extends AppOficinasCeCrud {

  public AppOficinasCeDao(Connection cnn) {
    super(cnn);
  }

  public List<AppOficinasCe> consultarPorCoordinador(Double cedula) throws SQLException {
    PreparedStatement sentencia = null;
    List<AppOficinasCe> lista = new ArrayList<>();
    try {

      String sql = "select * from APP_OFICINASCE where CC_coordinador=? or lower(Nombre_oficina) like lower('%pendiente%');";
      sentencia = cnn.prepareStatement(sql);
      sentencia.setDouble(1, cedula);
      ResultSet rs = sentencia.executeQuery();
      while (rs.next()) {
        lista.add(getAppOficinasCe(rs));
      }
    } finally {
      ConexionBD.desconectar(sentencia);
    }
    return lista;
  }


  public AppOficinasCe consultarOficinaPendiente() throws SQLException {
    PreparedStatement sentencia = null;
    AppOficinasCe obj = null;
    try {

      String sql = "select * from APP_OFICINASCE where lower(Nombre_oficina) like lower(?)";
      sentencia = cnn.prepareStatement(sql);
      sentencia.setString(1, "%Pendiente%");
      ResultSet rs = sentencia.executeQuery();
      if (rs.next()) {
        obj = getAppOficinasCe(rs);
      }
    } finally {
      ConexionBD.desconectar(sentencia);
    }
    return obj;
  }
}
