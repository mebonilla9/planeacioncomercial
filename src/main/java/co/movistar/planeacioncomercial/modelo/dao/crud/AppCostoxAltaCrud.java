package co.movistar.planeacioncomercial.modelo.dao.crud;

import co.movistar.planeacioncomercial.modelo.conexion.ConexionBD;
import co.movistar.planeacioncomercial.modelo.vo.AppCostoxAlta;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class AppCostoxAltaCrud implements IGenericoDAO<AppCostoxAlta> {

  protected final int ID = 1;
  protected Connection cnn;

  public AppCostoxAltaCrud(Connection cnn) {
    this.cnn = cnn;
  }

  public static AppCostoxAlta getAppCostoxAlta(ResultSet rs) throws SQLException {
    AppCostoxAlta appCostoxAlta = new AppCostoxAlta();
    appCostoxAlta.setIdCosto(rs.getLong("ID_COSTO"));
    appCostoxAlta.setIdUsuario(rs.getLong("ID_USUARIO"));
    appCostoxAlta.setCanal(rs.getString("CANAL"));
    appCostoxAlta.setSegmento(rs.getString("SEGMENTO"));
    appCostoxAlta.setProducto(rs.getString("PRODUCTO"));
    appCostoxAlta.setPeriodo(rs.getString("PERIODO"));
    appCostoxAlta.setCostoPorAlta(rs.getDouble("COSTO_PORALTA"));
    appCostoxAlta.setParticipacion(rs.getDouble("PARTICIPACION"));
    appCostoxAlta.setVecesArpu(rs.getDouble("VECES_ARPU"));

    return appCostoxAlta;
  }

  public static AppCostoxAlta getAppCostoxAlta(ResultSet rs, Map<String, Integer> columnas) throws SQLException {
    AppCostoxAlta appCostoxAlta = new AppCostoxAlta();
    Integer columna = columnas.get("APP_COSTOXALTA_ID_COSTO");
    if (columna != null) {
      appCostoxAlta.setIdCosto(rs.getLong(columna));
    }
    columna = columnas.get("APP_COSTOXALTA_ID_USUARIO");
    if (columna != null) {
      appCostoxAlta.setIdUsuario(rs.getLong(columna));
    }
    columna = columnas.get("APP_COSTOXALTA_CANAL");
    if (columna != null) {
      appCostoxAlta.setCanal(rs.getString(columna));
    }
    columna = columnas.get("APP_COSTOXALTA_SEGMENTO");
    if (columna != null) {
      appCostoxAlta.setSegmento(rs.getString(columna));
    }
    columna = columnas.get("APP_COSTOXALTA_PRODUCTO");
    if (columna != null) {
      appCostoxAlta.setProducto(rs.getString(columna));
    }
    columna = columnas.get("APP_COSTOXALTA_PERIODO");
    if (columna != null) {
      appCostoxAlta.setPeriodo(rs.getString(columna));
    }
    columna = columnas.get("APP_COSTOXALTA_COSTO_PORALTA");
    if (columna != null) {
      appCostoxAlta.setCostoPorAlta(rs.getDouble(columna));
    }
    columna = columnas.get("APP_COSTOXALTA_PARTICIPACION");
    if (columna != null) {
      appCostoxAlta.setParticipacion(rs.getDouble(columna));
    }
    columna = columnas.get("APP_COSTOXALTA_VECES_ARPU");
    if (columna != null) {
      appCostoxAlta.setVecesArpu(rs.getDouble(columna));
    }
    return appCostoxAlta;
  }

  @Override
  public void insertar(AppCostoxAlta appCostoxAlta) throws SQLException {
    PreparedStatement sentencia = null;
    try {
      int i = 1;
      String sql = "insert into APP_COSTOXALTA(ID_USUARIO,CANAL,SEGMENTO,PRODUCTO,PERIODO,COSTO_PORALTA,PARTICIPACION,VECES_ARPU) values (?,?,?,?,?,?,?,?)";
      sentencia = cnn.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);
      sentencia.setObject(i++, appCostoxAlta.getIdUsuario());
      sentencia.setObject(i++, appCostoxAlta.getCanal());
      sentencia.setObject(i++, appCostoxAlta.getSegmento());
      sentencia.setObject(i++, appCostoxAlta.getProducto());
      sentencia.setObject(i++, appCostoxAlta.getPeriodo());
      sentencia.setObject(i++, appCostoxAlta.getCostoPorAlta());
      sentencia.setObject(i++, appCostoxAlta.getParticipacion());
      sentencia.setObject(i++, appCostoxAlta.getVecesArpu());

      sentencia.executeUpdate();
      ResultSet rs = sentencia.getGeneratedKeys();
      if (rs.next()) {
        appCostoxAlta.setIdCosto(rs.getLong(ID));
      }
    } finally {
      ConexionBD.desconectar(sentencia);
    }
  }

  @Override
  public void editar(AppCostoxAlta appCostoxAlta) throws SQLException {
    PreparedStatement sentencia = null;
    try {
      int i = 1;
      String sql = "update APP_COSTOXALTA set ID_USUARIO=?,CANAL=?,SEGMENTO=?,PRODUCTO=?,PERIODO=?,COSTO_PORALTA=?,PARTICIPACION=?,VECES_ARPU=? where ID_COSTO=?";
      sentencia = cnn.prepareStatement(sql);
      sentencia.setObject(i++, appCostoxAlta.getIdUsuario());
      sentencia.setObject(i++, appCostoxAlta.getCanal());
      sentencia.setObject(i++, appCostoxAlta.getSegmento());
      sentencia.setObject(i++, appCostoxAlta.getProducto());
      sentencia.setObject(i++, appCostoxAlta.getPeriodo());
      sentencia.setObject(i++, appCostoxAlta.getCostoPorAlta());
      sentencia.setObject(i++, appCostoxAlta.getParticipacion());
      sentencia.setObject(i++, appCostoxAlta.getVecesArpu());
      sentencia.setObject(i++, appCostoxAlta.getIdCosto());

      sentencia.executeUpdate();
    } finally {
      ConexionBD.desconectar(sentencia);
    }
  }

  @Override
  public List<AppCostoxAlta> consultar() throws SQLException {
    PreparedStatement sentencia = null;
    List<AppCostoxAlta> lista = new ArrayList<>();
    try {

      String sql = "select * from APP_COSTOXALTA";
      sentencia = cnn.prepareStatement(sql);
      ResultSet rs = sentencia.executeQuery();
      while (rs.next()) {
        lista.add(getAppCostoxAlta(rs));
      }
    } finally {
      ConexionBD.desconectar(sentencia);
    }
    return lista;

  }

  @Override
  public AppCostoxAlta consultar(Long id) throws SQLException {
    PreparedStatement sentencia = null;
    AppCostoxAlta obj = null;
    try {

      String sql = "select * from APP_COSTOXALTA where ID_COSTO=?";
      sentencia = cnn.prepareStatement(sql);
      sentencia.setLong(1, id);
      ResultSet rs = sentencia.executeQuery();
      if (rs.next()) {
        obj = getAppCostoxAlta(rs);
      }
    } finally {
      ConexionBD.desconectar(sentencia);
    }
    return obj;
  }

}
