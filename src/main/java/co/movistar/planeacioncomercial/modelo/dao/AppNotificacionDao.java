package co.movistar.planeacioncomercial.modelo.dao;

import co.movistar.planeacioncomercial.modelo.conexion.ConexionBD;
import co.movistar.planeacioncomercial.modelo.dao.crud.AppNotificacionCrud;
import co.movistar.planeacioncomercial.modelo.vo.AppNotificacion;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;


public class AppNotificacionDao extends AppNotificacionCrud {

  public AppNotificacionDao(Connection cnn) {
    super(cnn);
  }

  public AppNotificacion obtenerUltimaNotificacion() throws SQLException {
    PreparedStatement sentencia = null;
    AppNotificacion obj = null;
    try {
      String sql = "select TOP 1 * FROM APP_NOTIFICACION WHERE Nivel = 2 ORDER BY Id_notificacion DESC;";
      sentencia = cnn.prepareStatement(sql);
      ResultSet rs = sentencia.executeQuery();
      if (rs.next()) {
        obj = getAppNotificacion(rs);
      }
    } finally {
      ConexionBD.desconectar(sentencia);
    }
    return obj;
  }

  public AppNotificacion obtenerUltimaNotificacion(Long idCargo) throws SQLException {
    PreparedStatement sentencia = null;
    AppNotificacion obj = null;
    try {
      String sql = "select TOP 1 * FROM APP_NOTIFICACION WHERE id_cargo = ? AND Nivel = 1 ORDER BY Id_notificacion DESC;";
      sentencia = cnn.prepareStatement(sql);
      sentencia.setLong(1, idCargo);
      ResultSet rs = sentencia.executeQuery();
      if (rs.next()) {
        obj = getAppNotificacion(rs);
      }
    } finally {
      ConexionBD.desconectar(sentencia);
    }
    return obj;
  }
}
