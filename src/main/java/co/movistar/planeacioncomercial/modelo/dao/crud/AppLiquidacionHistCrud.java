package co.movistar.planeacioncomercial.modelo.dao.crud;

import co.movistar.planeacioncomercial.modelo.conexion.ConexionBD;
import co.movistar.planeacioncomercial.modelo.vo.AppLiquidacionHist;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class AppLiquidacionHistCrud implements IGenericoDAO<AppLiquidacionHist> {

  protected final int ID = 1;
  protected Connection cnn;

  public AppLiquidacionHistCrud(Connection cnn) {
    this.cnn = cnn;
  }

  public static AppLiquidacionHist getAppLiquidacionHist(ResultSet rs) throws SQLException {
    AppLiquidacionHist appLiquidacionHist = new AppLiquidacionHist();
    appLiquidacionHist.setIdConsLiquidacion(rs.getLong("Id_cons_liquidacion"));
    appLiquidacionHist.setPeriodo(rs.getString("Periodo"));
    appLiquidacionHist.setCedula(rs.getLong("Cedula"));
    appLiquidacionHist.setVariable(rs.getString("Variable"));
    appLiquidacionHist.setMeta(rs.getLong("Meta"));
    appLiquidacionHist.setEjecutado(rs.getDouble("Ejecutado"));
    appLiquidacionHist.setCumplimiento(rs.getDouble("Cumplimiento"));
    appLiquidacionHist.setPesoVariable(rs.getDouble("Peso_Variable"));
    appLiquidacionHist.setPesoCat(rs.getDouble("Peso_Cat"));
    appLiquidacionHist.setPesoGestion(rs.getDouble("Peso_Gestion"));
    appLiquidacionHist.setComIP(rs.getDouble("Com_IP"));
    appLiquidacionHist.setIpEjecucionSinAcel(rs.getDouble("IP_Ejecucion_sin_Acel"));
    appLiquidacionHist.setIpLiquidar(rs.getDouble("IP_Liquidar"));
    appLiquidacionHist.setTotalAcel(rs.getDouble("Total_Acel"));
    appLiquidacionHist.setCurvaAcelerador(rs.getDouble("Curva_Acelerador"));
    appLiquidacionHist.setVariable100(rs.getDouble("Variable_100"));
    appLiquidacionHist.setValorMes(rs.getDouble("Valor_Mes"));
    appLiquidacionHist.setAjusteMesesAnteriores(rs.getDouble("Ajuste_Meses_Anteriores"));
    appLiquidacionHist.setGarantizado(rs.getDouble("Garantizado"));
    appLiquidacionHist.setGarantizadoNvosCanales(rs.getDouble("Garantizado_Nvos_Canales"));
    appLiquidacionHist.setAcelTeUFVDPrepago(rs.getString("Acel_TeU_FVDPrepago"));
    appLiquidacionHist.setAcelPortacionesFVDPrepago(rs.getString("Acel_Portaciones_FVDPrepago"));
    appLiquidacionHist.setValorAPagar(rs.getDouble("Valor_a_Pagar"));
    appLiquidacionHist.setCategoria(rs.getString("Categoria"));
    appLiquidacionHist.setIdUsuario(rs.getLong("Id_Usuario"));

    return appLiquidacionHist;
  }

  public static AppLiquidacionHist getAppLiquidacionHist(ResultSet rs, Map<String, Integer> columnas) throws SQLException {
    AppLiquidacionHist appLiquidacionHist = new AppLiquidacionHist();
    Integer columna = columnas.get("APP_LIQUIDACION_HIST_Id_cons_liquidacion");
    if (columna != null) {
      appLiquidacionHist.setIdConsLiquidacion(rs.getLong(columna));
    }
    columna = columnas.get("APP_LIQUIDACION_HIST_Periodo");
    if (columna != null) {
      appLiquidacionHist.setPeriodo(rs.getString(columna));
    }
    columna = columnas.get("APP_LIQUIDACION_HIST_Cedula");
    if (columna != null) {
      appLiquidacionHist.setCedula(rs.getLong(columna));
    }
    columna = columnas.get("APP_LIQUIDACION_HIST_Variable");
    if (columna != null) {
      appLiquidacionHist.setVariable(rs.getString(columna));
    }
    columna = columnas.get("APP_LIQUIDACION_HIST_Meta");
    if (columna != null) {
      appLiquidacionHist.setMeta(rs.getLong(columna));
    }
    columna = columnas.get("APP_LIQUIDACION_HIST_Ejecutado");
    if (columna != null) {
      appLiquidacionHist.setEjecutado(rs.getDouble(columna));
    }
    columna = columnas.get("APP_LIQUIDACION_HIST_Cumplimiento");
    if (columna != null) {
      appLiquidacionHist.setCumplimiento(rs.getDouble(columna));
    }
    columna = columnas.get("APP_LIQUIDACION_HIST_Peso_Variable");
    if (columna != null) {
      appLiquidacionHist.setPesoVariable(rs.getDouble(columna));
    }
    columna = columnas.get("APP_LIQUIDACION_HIST_Peso_Cat");
    if (columna != null) {
      appLiquidacionHist.setPesoCat(rs.getDouble(columna));
    }
    columna = columnas.get("APP_LIQUIDACION_HIST_Peso_Gestion");
    if (columna != null) {
      appLiquidacionHist.setPesoGestion(rs.getDouble(columna));
    }
    columna = columnas.get("APP_LIQUIDACION_HIST_Com_IP");
    if (columna != null) {
      appLiquidacionHist.setComIP(rs.getDouble(columna));
    }
    columna = columnas.get("APP_LIQUIDACION_HIST_IP_Ejecucion_sin_Acel");
    if (columna != null) {
      appLiquidacionHist.setIpEjecucionSinAcel(rs.getDouble(columna));
    }
    columna = columnas.get("APP_LIQUIDACION_HIST_IP_Liquidar");
    if (columna != null) {
      appLiquidacionHist.setIpLiquidar(rs.getDouble(columna));
    }
    columna = columnas.get("APP_LIQUIDACION_HIST_Total_Acel");
    if (columna != null) {
      appLiquidacionHist.setTotalAcel(rs.getDouble(columna));
    }
    columna = columnas.get("APP_LIQUIDACION_HIST_Curva_Acelerador");
    if (columna != null) {
      appLiquidacionHist.setCurvaAcelerador(rs.getDouble(columna));
    }
    columna = columnas.get("APP_LIQUIDACION_HIST_Variable_100");
    if (columna != null) {
      appLiquidacionHist.setVariable100(rs.getDouble(columna));
    }
    columna = columnas.get("APP_LIQUIDACION_HIST_Valor_Mes");
    if (columna != null) {
      appLiquidacionHist.setValorMes(rs.getDouble(columna));
    }
    columna = columnas.get("APP_LIQUIDACION_HIST_Ajuste_Meses_Anteriores");
    if (columna != null) {
      appLiquidacionHist.setAjusteMesesAnteriores(rs.getDouble(columna));
    }
    columna = columnas.get("APP_LIQUIDACION_HIST_Garantizado");
    if (columna != null) {
      appLiquidacionHist.setGarantizado(rs.getDouble(columna));
    }
    columna = columnas.get("APP_LIQUIDACION_HIST_Garantizado_Nvos_Canales");
    if (columna != null) {
      appLiquidacionHist.setGarantizadoNvosCanales(rs.getDouble(columna));
    }
    columna = columnas.get("APP_LIQUIDACION_HIST_Acel_TeU_FVDPrepago");
    if (columna != null) {
      appLiquidacionHist.setAcelTeUFVDPrepago(rs.getString(columna));
    }
    columna = columnas.get("APP_LIQUIDACION_HIST_Acel_Portaciones_FVDPrepago");
    if (columna != null) {
      appLiquidacionHist.setAcelPortacionesFVDPrepago(rs.getString(columna));
    }
    columna = columnas.get("APP_LIQUIDACION_HIST_Valor_a_Pagar");
    if (columna != null) {
      appLiquidacionHist.setValorAPagar(rs.getDouble(columna));
    }
    columna = columnas.get("APP_LIQUIDACION_HIST_Categoria");
    if (columna != null) {
      appLiquidacionHist.setCategoria(rs.getString(columna));
    }
    columna = columnas.get("APP_LIQUIDACION_HIST_Id_Usuario");
    if (columna != null) {
      appLiquidacionHist.setIdUsuario(rs.getLong(columna));
    }
    return appLiquidacionHist;
  }

  @Override
  public void insertar(AppLiquidacionHist appLiquidacionHist) throws SQLException {
    PreparedStatement sentencia = null;
    try {
      int i = 1;
      String sql = "insert into APP_LIQUIDACION_HIST(Periodo,Cedula,Variable,Meta,Ejecutado,Cumplimiento,Peso_Variable,Peso_Cat,Peso_Gestion,Com_IP,IP_Ejecucion_sin_Acel,IP_Liquidar,Total_Acel,Curva_Acelerador,Variable_100,Valor_Mes,Ajuste_Meses_Anteriores,Garantizado,Garantizado_Nvos_Canales,Acel_TeU_FVDPrepago,Acel_Portaciones_FVDPrepago,Valor_a_Pagar,Categoria,Id_Usuario) values (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
      sentencia = cnn.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);
      sentencia.setObject(i++, appLiquidacionHist.getPeriodo());
      sentencia.setObject(i++, appLiquidacionHist.getCedula());
      sentencia.setObject(i++, appLiquidacionHist.getVariable());
      sentencia.setObject(i++, appLiquidacionHist.getMeta());
      sentencia.setObject(i++, appLiquidacionHist.getEjecutado());
      sentencia.setObject(i++, appLiquidacionHist.getCumplimiento());
      sentencia.setObject(i++, appLiquidacionHist.getPesoVariable());
      sentencia.setObject(i++, appLiquidacionHist.getPesoCat());
      sentencia.setObject(i++, appLiquidacionHist.getPesoGestion());
      sentencia.setObject(i++, appLiquidacionHist.getComIP());
      sentencia.setObject(i++, appLiquidacionHist.getIpEjecucionSinAcel());
      sentencia.setObject(i++, appLiquidacionHist.getIpLiquidar());
      sentencia.setObject(i++, appLiquidacionHist.getTotalAcel());
      sentencia.setObject(i++, appLiquidacionHist.getCurvaAcelerador());
      sentencia.setObject(i++, appLiquidacionHist.getVariable100());
      sentencia.setObject(i++, appLiquidacionHist.getValorMes());
      sentencia.setObject(i++, appLiquidacionHist.getAjusteMesesAnteriores());
      sentencia.setObject(i++, appLiquidacionHist.getGarantizado());
      sentencia.setObject(i++, appLiquidacionHist.getGarantizadoNvosCanales());
      sentencia.setObject(i++, appLiquidacionHist.getAcelTeUFVDPrepago());
      sentencia.setObject(i++, appLiquidacionHist.getAcelPortacionesFVDPrepago());
      sentencia.setObject(i++, appLiquidacionHist.getValorAPagar());
      sentencia.setObject(i++, appLiquidacionHist.getCategoria());
      sentencia.setObject(i++, appLiquidacionHist.getIdUsuario());
      sentencia.executeUpdate();
      ResultSet rs = sentencia.getGeneratedKeys();
      if (rs.next()) {
        appLiquidacionHist.setIdConsLiquidacion(rs.getLong(ID));
      }
    } finally {
      ConexionBD.desconectar(sentencia);
    }
  }

  @Override
  public void editar(AppLiquidacionHist appLiquidacionHist) throws SQLException {
    PreparedStatement sentencia = null;
    try {
      int i = 1;
      String sql = "update APP_LIQUIDACION_HIST set Periodo=?,Cedula=?,Variable=?,Meta=?,Ejecutado=?,Cumplimiento=?,Peso_Variable=?,Peso_Cat=?,Peso_Gestion=?,Com_IP=?,IP_Ejecucion_sin_Acel=?,IP_Liquidar=?,Total_Acel=?,Curva_Acelerador=?,Variable_100=?,Valor_Mes=?,Ajuste_Meses_Anteriores=?,Garantizado=?,Garantizado_Nvos_Canales=?,Acel_TeU_FVDPrepago=?,Acel_Portaciones_FVDPrepago=?,Valor_a_Pagar=?,Categoria=?,Id_Usuario=? where Id_cons_liquidacion=?";
      sentencia = cnn.prepareStatement(sql);
      sentencia.setObject(i++, appLiquidacionHist.getPeriodo());
      sentencia.setObject(i++, appLiquidacionHist.getCedula());
      sentencia.setObject(i++, appLiquidacionHist.getVariable());
      sentencia.setObject(i++, appLiquidacionHist.getMeta());
      sentencia.setObject(i++, appLiquidacionHist.getEjecutado());
      sentencia.setObject(i++, appLiquidacionHist.getCumplimiento());
      sentencia.setObject(i++, appLiquidacionHist.getPesoVariable());
      sentencia.setObject(i++, appLiquidacionHist.getPesoCat());
      sentencia.setObject(i++, appLiquidacionHist.getPesoGestion());
      sentencia.setObject(i++, appLiquidacionHist.getComIP());
      sentencia.setObject(i++, appLiquidacionHist.getIpEjecucionSinAcel());
      sentencia.setObject(i++, appLiquidacionHist.getIpLiquidar());
      sentencia.setObject(i++, appLiquidacionHist.getTotalAcel());
      sentencia.setObject(i++, appLiquidacionHist.getCurvaAcelerador());
      sentencia.setObject(i++, appLiquidacionHist.getVariable100());
      sentencia.setObject(i++, appLiquidacionHist.getValorMes());
      sentencia.setObject(i++, appLiquidacionHist.getAjusteMesesAnteriores());
      sentencia.setObject(i++, appLiquidacionHist.getGarantizado());
      sentencia.setObject(i++, appLiquidacionHist.getGarantizadoNvosCanales());
      sentencia.setObject(i++, appLiquidacionHist.getAcelTeUFVDPrepago());
      sentencia.setObject(i++, appLiquidacionHist.getAcelPortacionesFVDPrepago());
      sentencia.setObject(i++, appLiquidacionHist.getValorAPagar());
      sentencia.setObject(i++, appLiquidacionHist.getCategoria());
      sentencia.setObject(i++, appLiquidacionHist.getIdUsuario());
      sentencia.setObject(i++, appLiquidacionHist.getIdConsLiquidacion());
      sentencia.executeUpdate();
    } finally {
      ConexionBD.desconectar(sentencia);
    }
  }

  @Override
  public List<AppLiquidacionHist> consultar() throws SQLException {
    PreparedStatement sentencia = null;
    List<AppLiquidacionHist> lista = new ArrayList<>();
    try {

      String sql = "select * from APP_LIQUIDACION_HIST";
      sentencia = cnn.prepareStatement(sql);
      ResultSet rs = sentencia.executeQuery();
      while (rs.next()) {
        lista.add(getAppLiquidacionHist(rs));
      }
    } finally {
      ConexionBD.desconectar(sentencia);
    }
    return lista;

  }

  @Override
  public AppLiquidacionHist consultar(Long id) throws SQLException {
    PreparedStatement sentencia = null;
    AppLiquidacionHist obj = null;
    try {

      String sql = "select * from APP_LIQUIDACION_HIST where Id_cons_liquidacion=?";
      sentencia = cnn.prepareStatement(sql);
      sentencia.setLong(1, id);
      ResultSet rs = sentencia.executeQuery();
      if (rs.next()) {
        obj = getAppLiquidacionHist(rs);
      }
    } finally {
      ConexionBD.desconectar(sentencia);
    }
    return obj;
  }

}
