package co.movistar.planeacioncomercial.modelo.dao.crud;

import co.movistar.planeacioncomercial.modelo.conexion.ConexionBD;
import co.movistar.planeacioncomercial.modelo.vo.AppPresupuestoComercial;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class AppPresupuestoComercialCrud implements IGenericoDAO<AppPresupuestoComercial> {

  protected final int ID = 1;
  protected Connection cnn;

  public AppPresupuestoComercialCrud(Connection cnn) {
    this.cnn = cnn;
  }

  public static AppPresupuestoComercial getappPresupuestoComercial(ResultSet rs) throws SQLException {
    AppPresupuestoComercial appPresupuestoComercial = new AppPresupuestoComercial();
    appPresupuestoComercial.setIdPpto(rs.getLong("ID_PPTO"));
    appPresupuestoComercial.setIdUsuario(rs.getLong("ID_USUARIO"));
    appPresupuestoComercial.setDocumento(rs.getLong("DOCUMENTO"));
    appPresupuestoComercial.setNomUsuario(rs.getString("NOM_USUARIO"));
    appPresupuestoComercial.setProducto(rs.getString("PRODUCTO"));
    appPresupuestoComercial.setRegional(rs.getString("REGIONAL"));
    appPresupuestoComercial.setJefatura(rs.getString("JEFATURA"));
    appPresupuestoComercial.setCanal(rs.getString("CANAL"));
    appPresupuestoComercial.setDepartamento(rs.getString("DEPARTAMENTO"));
    appPresupuestoComercial.setSegmento(rs.getString("SEGMENTO"));
    appPresupuestoComercial.setPresupuesto(rs.getLong("PRESUPUESTO"));
    appPresupuestoComercial.setCategoria(rs.getString("CATEGORIA"));
    appPresupuestoComercial.setCategoria(rs.getString("PERIODO"));

    return appPresupuestoComercial;
  }

  public static AppPresupuestoComercial getappPresupuestoComercial(ResultSet rs, Map<String, Integer> columnas) throws SQLException {
    AppPresupuestoComercial appPresupuestoComercial = new AppPresupuestoComercial();
    Integer columna = columnas.get("APP_PRESUPUESTO_COMERCIAL_ID_PPTO");
    if (columna != null) {
      appPresupuestoComercial.setIdPpto(rs.getLong(columna));
    }
    columna = columnas.get("APP_PRESUPUESTO_COMERCIAL_ID_USUARIO");
    if (columna != null) {
      appPresupuestoComercial.setIdUsuario(rs.getLong(columna));
    }
    columna = columnas.get("APP_PRESUPUESTO_COMERCIAL_DOCUMENTO");
    if (columna != null) {
      appPresupuestoComercial.setDocumento(rs.getLong(columna));
    }
    columna = columnas.get("APP_PRESUPUESTO_COMERCIAL_NOM_USUARIO");
    if (columna != null) {
      appPresupuestoComercial.setNomUsuario(rs.getString(columna));
    }
    columna = columnas.get("APP_PRESUPUESTO_COMERCIAL_PRODUCTO");
    if (columna != null) {
      appPresupuestoComercial.setProducto(rs.getString(columna));
    }
    columna = columnas.get("APP_PRESUPUESTO_COMERCIAL_REGIONAL");
    if (columna != null) {
      appPresupuestoComercial.setRegional(rs.getString(columna));
    }
    columna = columnas.get("APP_PRESUPUESTO_COMERCIAL_JEFATURA");
    if (columna != null) {
      appPresupuestoComercial.setJefatura(rs.getString(columna));
    }
    columna = columnas.get("APP_PRESUPUESTO_COMERCIAL_CANAL");
    if (columna != null) {
      appPresupuestoComercial.setCanal(rs.getString(columna));
    }
    columna = columnas.get("APP_PRESUPUESTO_COMERCIAL_DEPARTAMENTO");
    if (columna != null) {
      appPresupuestoComercial.setDepartamento(rs.getString(columna));
    }
    columna = columnas.get("APP_PRESUPUESTO_COMERCIAL_SEGMENTO");
    if (columna != null) {
      appPresupuestoComercial.setSegmento(rs.getString(columna));
    }
    columna = columnas.get("APP_PRESUPUESTO_COMERCIAL_PRESUPUESTO");
    if (columna != null) {
      appPresupuestoComercial.setPresupuesto(rs.getLong(columna));
    }
    columna = columnas.get("APP_PRESUPUESTO_COMERCIAL_CATEGORIA");
    if (columna != null) {
      appPresupuestoComercial.setCategoria(rs.getString(columna));
    }
    columna = columnas.get("APP_PRESUPUESTO_COMERCIAL_PERIODO");
    if (columna != null) {
      appPresupuestoComercial.setPeriodo(rs.getString(columna));
    }
    return appPresupuestoComercial;
  }

  @Override
  public void insertar(AppPresupuestoComercial appPresupuestoComercial) throws SQLException {
    PreparedStatement sentencia = null;
    try {
      int i = 1;
      String sql = "insert into APP_PRESUPUESTO_COMERCIAL(ID_USUARIO,DOCUMENTO,NOM_USUARIO,PRODUCTO,REGIONAL,JEFATURA,CANAL,DEPARTAMENTO,SEGMENTO,PRESUPUESTO,CATEGORIA,PERIODO) values (?,?,?,?,?,?,?,?,?,?,?,?)";
      sentencia = cnn.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);
      sentencia.setObject(i++, appPresupuestoComercial.getIdUsuario());
      sentencia.setObject(i++, appPresupuestoComercial.getDocumento());
      sentencia.setObject(i++, appPresupuestoComercial.getNomUsuario());
      sentencia.setObject(i++, appPresupuestoComercial.getProducto());
      sentencia.setObject(i++, appPresupuestoComercial.getRegional());
      sentencia.setObject(i++, appPresupuestoComercial.getJefatura());
      sentencia.setObject(i++, appPresupuestoComercial.getCanal());
      sentencia.setObject(i++, appPresupuestoComercial.getDepartamento());
      sentencia.setObject(i++, appPresupuestoComercial.getSegmento());
      sentencia.setObject(i++, appPresupuestoComercial.getPresupuesto());
      sentencia.setObject(i++, appPresupuestoComercial.getCategoria());
      sentencia.setObject(i++, appPresupuestoComercial.getPeriodo());

      sentencia.executeUpdate();
      ResultSet rs = sentencia.getGeneratedKeys();
      if (rs.next()) {
        appPresupuestoComercial.setIdPpto(rs.getLong(ID));
      }
    } finally {
      ConexionBD.desconectar(sentencia);
    }
  }

  @Override
  public void editar(AppPresupuestoComercial appPresupuestoComercial) throws SQLException {
    PreparedStatement sentencia = null;
    try {
      int i = 1;
      String sql = "update APP_PRESUPUESTO_COMERCIAL set ID_USUARIO=?,DOCUMENTO=?,NOM_USUARIO=?,PRODUCTO=?,REGIONAL=?,JEFATURA=?,CANAL=?,DEPARTAMENTO=?,SEGMENTO=?,PRESUPUESTO=?,CATEGORIA=?,PERIODO=? where ID_PPTO=?";
      sentencia = cnn.prepareStatement(sql);
      sentencia.setObject(i++, appPresupuestoComercial.getIdUsuario());
      sentencia.setObject(i++, appPresupuestoComercial.getDocumento());
      sentencia.setObject(i++, appPresupuestoComercial.getNomUsuario());
      sentencia.setObject(i++, appPresupuestoComercial.getProducto());
      sentencia.setObject(i++, appPresupuestoComercial.getRegional());
      sentencia.setObject(i++, appPresupuestoComercial.getJefatura());
      sentencia.setObject(i++, appPresupuestoComercial.getCanal());
      sentencia.setObject(i++, appPresupuestoComercial.getDepartamento());
      sentencia.setObject(i++, appPresupuestoComercial.getSegmento());
      sentencia.setObject(i++, appPresupuestoComercial.getPresupuesto());
      sentencia.setObject(i++, appPresupuestoComercial.getCategoria());
      sentencia.setObject(i++, appPresupuestoComercial.getPeriodo());
      sentencia.setObject(i++, appPresupuestoComercial.getIdPpto());

      sentencia.executeUpdate();
    } finally {
      ConexionBD.desconectar(sentencia);
    }
  }

  @Override
  public List<AppPresupuestoComercial> consultar() throws SQLException {
    PreparedStatement sentencia = null;
    List<AppPresupuestoComercial> lista = new ArrayList<>();
    try {

      String sql = "select * from APP_PRESUPUESTO_COMERCIAL";
      sentencia = cnn.prepareStatement(sql);
      ResultSet rs = sentencia.executeQuery();
      while (rs.next()) {
        lista.add(getappPresupuestoComercial(rs));
      }
    } finally {
      ConexionBD.desconectar(sentencia);
    }
    return lista;

  }

  @Override
  public AppPresupuestoComercial consultar(Long id) throws SQLException {
    PreparedStatement sentencia = null;
    AppPresupuestoComercial obj = null;
    try {

      String sql = "select * from APP_PRESUPUESTO_COMERCIAL where ID_PPTO=?";
      sentencia = cnn.prepareStatement(sql);
      sentencia.setLong(1, id);
      ResultSet rs = sentencia.executeQuery();
      if (rs.next()) {
        obj = getappPresupuestoComercial(rs);
      }
    } finally {
      ConexionBD.desconectar(sentencia);
    }
    return obj;
  }

}
