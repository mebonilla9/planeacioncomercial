package co.movistar.planeacioncomercial.modelo.dao.crud;

import co.movistar.planeacioncomercial.modelo.conexion.ConexionBD;
import co.movistar.planeacioncomercial.modelo.vo.AppEstados;
import co.movistar.planeacioncomercial.modelo.vo.AppRegional;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * @author Manuel Ernesto Bonilla Muñoz <mebonilla9@gmail.com>
 */
public class AppRegionalCrud implements IGenericoDAO<AppRegional> {

  protected final int ID = 1;
  protected Connection cnn;

  public AppRegionalCrud(Connection cnn) {
    this.cnn = cnn;
  }

  public static AppRegional getAppRegional(ResultSet rs) throws SQLException {
    AppRegional appRegional = new AppRegional();
    appRegional.setIdRegional(rs.getLong("ID_REGIONAL"));
    appRegional.setNombreRegional(rs.getString("NOMBRE_REGIONAL"));
    appRegional.setEstado(new AppEstados(rs.getLong("ID_ESTADO")));
    return appRegional;
  }

  public static AppRegional getAppRegional(ResultSet rs, Map<String, Integer> columnas) throws SQLException {
    AppRegional appRegional = new AppRegional();
    Integer columna = columnas.get("APP_REGIONAL_ID_REGIONAL");
    if (columna != null) {
      appRegional.setIdRegional(rs.getLong(columna));
    }
    columna = columnas.get("APP_REGIONAL_NOMBRE_REGIONAL");
    if (columna != null) {
      appRegional.setNombreRegional(rs.getString(columna));
    }
    columna = columnas.get("APP_REGIONAL_ID_ESTADO");
    if (columna != null) {
      appRegional.setEstado(new AppEstados(rs.getLong(columna)));
    }
    return appRegional;
  }

  @Override
  public void insertar(AppRegional appRegional) throws SQLException {
    PreparedStatement sentencia = null;
    try {
      int i = 1;
      String sql = "insert into APP_REGIONAL(NOMBRE_REGIONAL,ID_ESTADO) values (?,?)";
      sentencia = cnn.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);
      sentencia.setObject(i++, appRegional.getNombreRegional());
      sentencia.setObject(i++, appRegional.getEstado().getIdEstado());

      sentencia.executeUpdate();
      ResultSet rs = sentencia.getGeneratedKeys();
      if (rs.next()) {
        appRegional.setIdRegional(rs.getLong(ID));
      }
    } finally {
      ConexionBD.desconectar(sentencia);
    }
  }

  @Override
  public void editar(AppRegional appRegional) throws SQLException {
    PreparedStatement sentencia = null;
    try {
      int i = 1;
      String sql = "update APP_REGIONAL set NOMBRE_REGIONAL=?,ID_ESTADO=? where ID_REGIONAL=?";
      sentencia = cnn.prepareStatement(sql);
      sentencia.setObject(i++, appRegional.getNombreRegional());
      sentencia.setObject(i++, appRegional.getEstado().getIdEstado());
      sentencia.setObject(i++, appRegional.getIdRegional());

      sentencia.executeUpdate();
    } finally {
      ConexionBD.desconectar(sentencia);
    }
  }

  @Override
  public List<AppRegional> consultar() throws SQLException {
    PreparedStatement sentencia = null;
    List<AppRegional> lista = new ArrayList<>();
    try {

      String sql = "select * from APP_REGIONAL";
      sentencia = cnn.prepareStatement(sql);
      ResultSet rs = sentencia.executeQuery();
      while (rs.next()) {
        lista.add(getAppRegional(rs));
      }
    } finally {
      ConexionBD.desconectar(sentencia);
    }
    return lista;

  }

  @Override
  public AppRegional consultar(Long id) throws SQLException {
    PreparedStatement sentencia = null;
    AppRegional obj = null;
    try {
      String sql = "select * from APP_REGIONAL where ID_REGIONAL=?";
      sentencia = cnn.prepareStatement(sql);
      sentencia.setLong(1, id);
      ResultSet rs = sentencia.executeQuery();
      if (rs.next()) {
        obj = getAppRegional(rs);
      }
    } finally {
      ConexionBD.desconectar(sentencia);
    }
    return obj;
  }

}
