package co.movistar.planeacioncomercial.modelo.dao.crud;

import co.movistar.planeacioncomercial.modelo.conexion.ConexionBD;
import co.movistar.planeacioncomercial.modelo.vo.*;
import co.movistar.planeacioncomercial.negocio.util.DateUtil;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class AppDatosCeCrud implements IGenericoDAO<AppDatosCe> {

  protected final int ID = 1;
  protected Connection cnn;

  public AppDatosCeCrud(Connection cnn) {
    this.cnn = cnn;
  }

  public static AppDatosCe getAppDatosCe(ResultSet rs) throws SQLException {
    AppDatosCe appDatosCe = new AppDatosCe();
    appDatosCe.setIdDatosce(rs.getLong("Id_Datosce"));
    appDatosCe.setIdUsuariomodifica(rs.getLong("Id_Usuariomodifica"));
    appDatosCe.setRol(new AppRol(rs.getLong("Id_Rol")));
    appDatosCe.setPunto(new AppOficinasCe(rs.getLong("Id_Oficina")));
    appDatosCe.setNovedad(new AppNovedad(rs.getLong("Id_Novedad")));
    appDatosCe.setFecha(rs.getDate("Fecha"));
    appDatosCe.setIdUsuario(rs.getLong("Id_Usuario"));

    return appDatosCe;
  }

  public static AppDatosCe getAppDatosCe(ResultSet rs, Map<String, Integer> columnas) throws SQLException {
    AppDatosCe appDatosCe = new AppDatosCe();
    Integer columna = columnas.get("APP_DATOS_CE_Id_Datosce");
    if (columna != null) {
      appDatosCe.setIdDatosce(rs.getLong(columna));
    }
    columna = columnas.get("APP_DATOS_CE_Id_Usuariomodifica");
    if (columna != null) {
      appDatosCe.setIdUsuariomodifica(rs.getLong(columna));
    }
    columna = columnas.get("APP_DATOS_CE_Id_Rol");
    if (columna != null) {
      appDatosCe.setRol(new AppRol(rs.getLong(columna)));
    }
    columna = columnas.get("APP_DATOS_CE_Id_Oficina");
    if (columna != null) {
      appDatosCe.setPunto(new AppOficinasCe(rs.getLong(columna)));
    }
    columna = columnas.get("APP_DATOS_CE_Id_Novedad");
    if (columna != null) {
      appDatosCe.setNovedad(new AppNovedad(rs.getLong(columna)));
    }
    columna = columnas.get("APP_DATOS_CE_Fecha");
    if (columna != null) {
      appDatosCe.setFecha(rs.getDate(columna));
    }
    columna = columnas.get("APP_DATOS_CE_Id_Usuario");
    if (columna != null) {
      appDatosCe.setIdUsuario(rs.getLong(columna));
    }
    return appDatosCe;
  }

  @Override
  public void insertar(AppDatosCe appDatosCe) throws SQLException {
    PreparedStatement sentencia = null;
    try {
      int i = 1;
      String sql = "insert into APP_DATOS_CE(Id_Usuariomodifica,Id_Rol,Id_Oficina,Id_Novedad,Fecha,Id_Usuario) values (?,?,?,?,?,?)";
      sentencia = cnn.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);
      sentencia.setObject(i++, appDatosCe.getIdUsuariomodifica());
      sentencia.setObject(i++, appDatosCe.getRol().getIdRol());
      sentencia.setObject(i++, appDatosCe.getPunto().getIdOficina());
      sentencia.setObject(i++, appDatosCe.getNovedad().getIdNovedad());
      sentencia.setObject(i++, DateUtil.balancearFecha(appDatosCe.getFecha()));
      sentencia.setObject(i++, appDatosCe.getIdUsuario());

      sentencia.executeUpdate();
      ResultSet rs = sentencia.getGeneratedKeys();
      if (rs.next()) {
        appDatosCe.setIdDatosce(rs.getLong(ID));
      }
    } finally {
      ConexionBD.desconectar(sentencia);
    }
  }

  @Override
  public void editar(AppDatosCe appDatosCe) throws SQLException {
    PreparedStatement sentencia = null;
    try {
      int i = 1;
      String sql = "update APP_DATOS_CE set Id_Usuariomodifica=?,Id_Rol=?,Id_Oficina=?,Id_Novedad=?,Fecha=?,Id_Usuario=? where Id_Datosce=?";
      sentencia = cnn.prepareStatement(sql);
      sentencia.setObject(i++, appDatosCe.getIdUsuariomodifica());
      sentencia.setObject(i++, appDatosCe.getRol().getIdRol());
      sentencia.setObject(i++, appDatosCe.getPunto().getIdOficina());
      sentencia.setObject(i++, appDatosCe.getNovedad().getIdNovedad());
      sentencia.setObject(i++, DateUtil.balancearFecha(appDatosCe.getFecha()));
      sentencia.setObject(i++, appDatosCe.getIdUsuario());
      sentencia.setObject(i++, appDatosCe.getIdDatosce());

      sentencia.executeUpdate();
    } finally {
      ConexionBD.desconectar(sentencia);
    }
  }

  @Override
  public List<AppDatosCe> consultar() throws SQLException {
    PreparedStatement sentencia = null;
    List<AppDatosCe> lista = new ArrayList<>();
    try {

      String sql = "select * from APP_DATOS_CE";
      sentencia = cnn.prepareStatement(sql);
      ResultSet rs = sentencia.executeQuery();
      while (rs.next()) {
        lista.add(getAppDatosCe(rs));
      }
    } finally {
      ConexionBD.desconectar(sentencia);
    }
    return lista;

  }

  @Override
  public AppDatosCe consultar(Long id) throws SQLException {
    PreparedStatement sentencia = null;
    AppDatosCe obj = null;
    try {

      String sql = "select * from APP_DATOS_CE where Id_Datosce=?";
      sentencia = cnn.prepareStatement(sql);
      sentencia.setLong(1, id);
      ResultSet rs = sentencia.executeQuery();
      if (rs.next()) {
        obj = getAppDatosCe(rs);
      }
    } finally {
      ConexionBD.desconectar(sentencia);
    }
    return obj;
  }

}
