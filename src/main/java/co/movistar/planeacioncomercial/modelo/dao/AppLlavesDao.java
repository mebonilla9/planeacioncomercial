package co.movistar.planeacioncomercial.modelo.dao;

import co.movistar.planeacioncomercial.modelo.dao.crud.AppLlavesCrud;

import java.sql.Connection;

public class AppLlavesDao extends AppLlavesCrud {

  public AppLlavesDao(Connection cnn) {
    super(cnn);
  }
}
