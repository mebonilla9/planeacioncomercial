package co.movistar.planeacioncomercial.modelo.dao.crud;

import co.movistar.planeacioncomercial.modelo.conexion.ConexionBD;
import co.movistar.planeacioncomercial.modelo.vo.AppGestionado;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class AppGestionadoCrud implements IGenericoDAO<AppGestionado> {

  protected final int ID = 1;
  protected Connection cnn;

  public AppGestionadoCrud(Connection cnn) {
    this.cnn = cnn;
  }

  public static AppGestionado getAppGestionado(ResultSet rs) throws SQLException {
    AppGestionado appGestionado = new AppGestionado();
    appGestionado.setIdGestionado(rs.getLong("ID_GESTIONADO"));
    appGestionado.setIdUsuario(rs.getLong("ID_USUARIO"));
    appGestionado.setCedula(rs.getLong("CEDULA"));
    appGestionado.setCorreo(rs.getString("CORREO"));
    appGestionado.setnVariable(rs.getString("N_VARIABLE"));
    appGestionado.setVariable(rs.getString("VARIABLE"));
    appGestionado.setSegmento(rs.getString("SEGMENTO"));
    appGestionado.setMeta(rs.getDouble("META"));
    appGestionado.setEjecucion(rs.getLong("EJECUCION") == 0 ? null : rs.getLong("EJECUCION"));
    appGestionado.setCumplimiento(rs.getDouble("CUMPLIMIENTO") == 0 ? null : rs.getDouble("CUMPLIMIENTO"));
    appGestionado.setPesoEsquema(rs.getDouble("PESO_ESQUEMA") == 0 ? null : rs.getDouble("PESO_ESQUEMA"));
    appGestionado.setIpVariable(rs.getDouble("IP_VARIABLE") == 0 ? null : rs.getDouble("IP_VARIABLE"));

    return appGestionado;
  }

  public static AppGestionado getAppGestionado(ResultSet rs, Map<String, Integer> columnas) throws SQLException {
    AppGestionado appGestionado = new AppGestionado();
    Integer columna = columnas.get("APP_GESTIONADO_ID_GESTIONADO");
    if (columna != null) {
      appGestionado.setIdGestionado(rs.getLong(columna));
    }
    columna = columnas.get("APP_GESTIONADO_ID_USUARIO");
    if (columna != null) {
      appGestionado.setIdUsuario(rs.getLong(columna));
    }
    columna = columnas.get("APP_GESTIONADO_CEDULA");
    if (columna != null) {
      appGestionado.setCedula(rs.getLong(columna));
    }
    columna = columnas.get("APP_GESTIONADO_CORREO");
    if (columna != null) {
      appGestionado.setCorreo(rs.getString(columna));
    }
    columna = columnas.get("APP_GESTIONADO_N_VARIABLE");
    if (columna != null) {
      appGestionado.setnVariable(rs.getString(columna));
    }
    columna = columnas.get("APP_GESTIONADO_VARIABLE");
    if (columna != null) {
      appGestionado.setVariable(rs.getString(columna));
    }
    columna = columnas.get("APP_GESTIONADO_SEGMENTO");
    if (columna != null) {
      appGestionado.setSegmento(rs.getString(columna));
    }
    columna = columnas.get("APP_GESTIONADO_META");
    if (columna != null) {
      appGestionado.setMeta(rs.getDouble(columna));
    }
    columna = columnas.get("APP_GESTIONADO_EJECUCION");
    if (columna != null) {
      appGestionado.setEjecucion(rs.getLong(columna));
    }
    columna = columnas.get("APP_GESTIONADO_CUMPLIMIENTO");
    if (columna != null) {
      appGestionado.setCumplimiento(rs.getDouble(columna));
    }
    columna = columnas.get("APP_GESTIONADO_PESO_ESQUEMA");
    if (columna != null) {
      appGestionado.setPesoEsquema(rs.getDouble(columna));
    }
    columna = columnas.get("APP_GESTIONADO_IP_VARIABLE");
    if (columna != null) {
      appGestionado.setIpVariable(rs.getDouble(columna));
    }
    return appGestionado;
  }

  @Override
  public void insertar(AppGestionado appGestionado) throws SQLException {
    PreparedStatement sentencia = null;
    try {
      int i = 1;
      String sql = "insert into APP_GESTIONADO(ID_USUARIO,CEDULA,CORREO,N_VARIABLE,VARIABLE,SEGMENTO,META,EJECUCION,CUMPLIMIENTO,PESO_ESQUEMA,IP_VARIABLE) values (?,?,?,?,?,?,?,?,?,?,?)";
      sentencia = cnn.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);
      sentencia.setObject(i++, appGestionado.getIdUsuario());
      sentencia.setObject(i++, appGestionado.getCedula());
      sentencia.setObject(i++, appGestionado.getCorreo());
      sentencia.setObject(i++, appGestionado.getnVariable());
      sentencia.setObject(i++, appGestionado.getVariable());
      sentencia.setObject(i++, appGestionado.getSegmento());
      sentencia.setObject(i++, appGestionado.getMeta());
      sentencia.setObject(i++, appGestionado.getEjecucion());
      sentencia.setObject(i++, appGestionado.getCumplimiento());
      sentencia.setObject(i++, appGestionado.getPesoEsquema());
      sentencia.setObject(i++, appGestionado.getIpVariable());

      sentencia.executeUpdate();
      ResultSet rs = sentencia.getGeneratedKeys();
      if (rs.next()) {
        appGestionado.setIdGestionado(rs.getLong(ID));
      }
    } finally {
      ConexionBD.desconectar(sentencia);
    }
  }

  @Override
  public void editar(AppGestionado appGestionado) throws SQLException {
    PreparedStatement sentencia = null;
    try {
      int i = 1;
      String sql = "update APP_GESTIONADO set ID_USUARIO=?,CEDULA=?,CORREO=?,N_VARIABLE=?,VARIABLE=?,SEGMENTO=?,META=?,EJECUCION=?,CUMPLIMIENTO=?,PESO_ESQUEMA=?,IP_VARIABLE=? where ID_GESTIONADO=?";
      sentencia = cnn.prepareStatement(sql);
      sentencia.setObject(i++, appGestionado.getIdUsuario());
      sentencia.setObject(i++, appGestionado.getCedula());
      sentencia.setObject(i++, appGestionado.getCorreo());
      sentencia.setObject(i++, appGestionado.getnVariable());
      sentencia.setObject(i++, appGestionado.getVariable());
      sentencia.setObject(i++, appGestionado.getSegmento());
      sentencia.setObject(i++, appGestionado.getMeta());
      sentencia.setObject(i++, appGestionado.getEjecucion());
      sentencia.setObject(i++, appGestionado.getCumplimiento());
      sentencia.setObject(i++, appGestionado.getPesoEsquema());
      sentencia.setObject(i++, appGestionado.getIpVariable());
      sentencia.setObject(i++, appGestionado.getIdGestionado());

      sentencia.executeUpdate();
    } finally {
      ConexionBD.desconectar(sentencia);
    }
  }

  @Override
  public List<AppGestionado> consultar() throws SQLException {
    PreparedStatement sentencia = null;
    List<AppGestionado> lista = new ArrayList<>();
    try {

      String sql = "select * from APP_GESTIONADO";
      sentencia = cnn.prepareStatement(sql);
      ResultSet rs = sentencia.executeQuery();
      while (rs.next()) {
        lista.add(getAppGestionado(rs));
      }
    } finally {
      ConexionBD.desconectar(sentencia);
    }
    return lista;

  }

  @Override
  public AppGestionado consultar(Long id) throws SQLException {
    PreparedStatement sentencia = null;
    AppGestionado obj = null;
    try {

      String sql = "select * from APP_GESTIONADO where ID_GESTIONADO=?";
      sentencia = cnn.prepareStatement(sql);
      sentencia.setLong(1, id);
      ResultSet rs = sentencia.executeQuery();
      if (rs.next()) {
        obj = getAppGestionado(rs);
      }
    } finally {
      ConexionBD.desconectar(sentencia);
    }
    return obj;
  }

}
