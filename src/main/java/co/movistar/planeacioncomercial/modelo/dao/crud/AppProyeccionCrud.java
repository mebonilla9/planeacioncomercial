/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package co.movistar.planeacioncomercial.modelo.dao.crud;

import co.movistar.planeacioncomercial.modelo.conexion.ConexionBD;
import co.movistar.planeacioncomercial.modelo.vo.AppProyeccion;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * @author lord_nightmare
 */
public class AppProyeccionCrud implements IGenericoDAO<AppProyeccion> {

  protected final int ID = 1;
  protected Connection cnn;

  public AppProyeccionCrud(Connection cnn) {
    this.cnn = cnn;
  }

  public static AppProyeccion getAppProyeccion(ResultSet rs) throws SQLException {
    AppProyeccion appProyeccion = new AppProyeccion();
    appProyeccion.setIdUsuario(rs.getLong("ID_USUARIO"));
    appProyeccion.setProducto(rs.getString("Producto"));
    appProyeccion.setSegmento(rs.getString("Segmento"));
    appProyeccion.setAltas(rs.getLong("Altas"));
    appProyeccion.setPresupuesto(rs.getLong("Presupuesto"));
    appProyeccion.setAlcorte(rs.getLong("Alcorte"));
    return appProyeccion;
  }

  public static AppProyeccion getAppProyeccion(ResultSet rs, Map<String, Integer> columnas) throws SQLException {
    AppProyeccion appProyeccion = new AppProyeccion();
    Integer columna = columnas.get("APP_PROYECCION_ID_USUARIO");
    if (columna != null) {
      appProyeccion.setIdUsuario(rs.getLong(columna));
    }
    columna = columnas.get("APP_PROYECCION_Producto");
    if (columna != null) {
      appProyeccion.setProducto(rs.getString(columna));
    }
    columna = columnas.get("APP_PROYECCION_Segmento");
    if (columna != null) {
      appProyeccion.setSegmento(rs.getString(columna));
    }
    columna = columnas.get("APP_PROYECCION_Altas");
    if (columna != null) {
      appProyeccion.setAltas(rs.getLong(columna));
    }
    columna = columnas.get("APP_PROYECCION_Presupuesto");
    if (columna != null) {
      appProyeccion.setPresupuesto(rs.getLong(columna));
    }
    columna = columnas.get("APP_PROYECCION_Alcorte");
    if (columna != null) {
      appProyeccion.setAlcorte(rs.getLong(columna));
    }
    return appProyeccion;
  }

  @Override
  public void insertar(AppProyeccion appProyeccion) throws SQLException {
    PreparedStatement sentencia = null;
    try {
      int i = 1;
      String sql = "insert into APP_PROYECCION(Producto,Segmento,Altas,Presupuesto,Alcorte) values (?,?,?,?,?)";
      sentencia = cnn.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);
      sentencia.setObject(i++, appProyeccion.getProducto());
      sentencia.setObject(i++, appProyeccion.getSegmento());
      sentencia.setObject(i++, appProyeccion.getAltas());
      sentencia.setObject(i++, appProyeccion.getPresupuesto());
      sentencia.setObject(i++, appProyeccion.getAlcorte());

      sentencia.executeUpdate();
      ResultSet rs = sentencia.getGeneratedKeys();
      if (rs.next()) {
        appProyeccion.setIdUsuario(rs.getLong(ID));
      }
    } finally {
      ConexionBD.desconectar(sentencia);
    }
  }

  @Override
  public void editar(AppProyeccion appProyeccion) throws SQLException {
    PreparedStatement sentencia = null;
    try {
      int i = 1;
      String sql = "update APP_PROYECCION set Producto=?, Segmento=?, Altas=?, Presupuesto=?, Alcorte=? where ID_USUARIO=?";
      sentencia = cnn.prepareStatement(sql);
      sentencia.setObject(i++, appProyeccion.getProducto());
      sentencia.setObject(i++, appProyeccion.getSegmento());
      sentencia.setObject(i++, appProyeccion.getAltas());
      sentencia.setObject(i++, appProyeccion.getPresupuesto());
      sentencia.setObject(i++, appProyeccion.getAlcorte());
      sentencia.setObject(i++, appProyeccion.getIdUsuario());

      sentencia.executeUpdate();
    } finally {
      ConexionBD.desconectar(sentencia);
    }
  }

  @Override
  public List<AppProyeccion> consultar() throws SQLException {
    PreparedStatement sentencia = null;
    List<AppProyeccion> lista = new ArrayList<>();
    try {
      String sql = "select * from APP_PROYECCION";
      sentencia = cnn.prepareStatement(sql);
      ResultSet rs = sentencia.executeQuery();
      while (rs.next()) {
        lista.add(getAppProyeccion(rs));
      }
    } finally {
      ConexionBD.desconectar(sentencia);
    }
    return lista;
  }

  @Override
  public AppProyeccion consultar(Long id) throws SQLException {
    PreparedStatement sentencia = null;
    AppProyeccion obj = null;
    try {
      String sql = "select * from APP_PROYECCION where ID_USUARIO=?";
      sentencia = cnn.prepareStatement(sql);
      sentencia.setLong(1, id);
      ResultSet rs = sentencia.executeQuery();
      if (rs.next()) {
        obj = getAppProyeccion(rs);
      }
    } finally {
      ConexionBD.desconectar(sentencia);
    }
    return obj;
  }

}
