package co.movistar.planeacioncomercial.modelo.dao.crud;

import co.movistar.planeacioncomercial.modelo.conexion.ConexionBD;
import co.movistar.planeacioncomercial.modelo.vo.AppCargo;
import co.movistar.planeacioncomercial.modelo.vo.AppEstados;
import co.movistar.planeacioncomercial.modelo.vo.AppUsuarios;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * @author Manuel Ernesto Bonilla Muñoz <mebonilla9@gmail.com>
 */
public class AppUsuariosCrud implements IGenericoDAO<AppUsuarios> {

  protected final int ID = 1;
  protected Connection cnn;

  public AppUsuariosCrud(Connection cnn) {
    this.cnn = cnn;
  }

  public static AppUsuarios getAppUsuarios(ResultSet rs) throws SQLException {
    AppUsuarios appUsuarios = new AppUsuarios();
    appUsuarios.setIdUsuario(rs.getLong("ID_USUARIO"));
    appUsuarios.setEstado(new AppEstados(rs.getLong("ID_ESTADO")));
    appUsuarios.setCedula(rs.getDouble("CEDULA"));
    appUsuarios.setCorreo(rs.getString("CORREO"));
    //appUsuarios.setPassword(rs.getString("PASSWORD"));
    appUsuarios.setNombre(rs.getString("NOMBRE"));
    appUsuarios.setCodInterno(rs.getString("COD_INTERNO"));
    appUsuarios.setIdJefe(rs.getLong("ID_JEFE"));
    appUsuarios.setCargo(new AppCargo(rs.getLong("ID_CARGO")));
    appUsuarios.setMac(rs.getString("MAC"));
    appUsuarios.setBloqueo(rs.getBoolean("BLOQUEO"));
    appUsuarios.setIdCanal(rs.getLong("ID_CANAL"));
    appUsuarios.setTutor(rs.getBoolean("TUTOR"));
    appUsuarios.setMalla(rs.getBoolean("MALLA"));
    appUsuarios.setContrato(rs.getLong("CONTRATO"));
    return appUsuarios;
  }

  public static AppUsuarios getAppUsuarios(ResultSet rs, Map<String, Integer> columnas) throws SQLException {
    AppUsuarios appUsuarios = new AppUsuarios();
    Integer columna = columnas.get("APP_USUARIOS_ID_USUARIO");
    if (columna != null) {
      appUsuarios.setIdUsuario(rs.getLong(columna));
    }
    columna = columnas.get("APP_USUARIOS_ID_ESTADO");
    if (columna != null) {
      appUsuarios.setEstado(new AppEstados(rs.getLong(columna)));
    }
    columna = columnas.get("APP_USUARIOS_CEDULA");
    if (columna != null) {
      appUsuarios.setCedula(rs.getDouble(columna));
    }
    columna = columnas.get("APP_USUARIOS_CORREO");
    if (columna != null) {
      appUsuarios.setCorreo(rs.getString(columna));
    }
        /*columna = columnas.get("APP_USUARIOS_PASSWORD");
        if (columna != null) {
            appUsuarios.setPassword(rs.getString(columna));
        }*/
    columna = columnas.get("APP_USUARIOS_NOMBRE");
    if (columna != null) {
      appUsuarios.setNombre(rs.getString(columna));
    }
    columna = columnas.get("APP_USUARIOS_COD_INTERNO");
    if (columna != null) {
      appUsuarios.setCodInterno(rs.getString(columna));
    }
    columna = columnas.get("APP_USUARIOS_ID_JEFE");
    if (columna != null) {
      appUsuarios.setIdJefe(rs.getLong(columna));
    }
    columna = columnas.get("APP_USUARIOS_ID_CARGO");
    if (columna != null) {
      appUsuarios.setCargo(new AppCargo(rs.getLong(columna)));
    }
    columna = columnas.get("APP_USUARIOS_MAC");
    if (columna != null) {
      appUsuarios.setMac(rs.getString(columna));
    }
    columna = columnas.get("APP_USUARIOS_BLOQUEO");
    if (columna != null) {
      appUsuarios.setBloqueo(rs.getBoolean("columna"));
    }
    columna = columnas.get("APP_USUARIOS_ID_CANAL");
    if (columna != null) {
      appUsuarios.setIdCanal(rs.getLong("columna"));
    }
    columna = columnas.get("APP_USUARIOS_TUTOR");
    if (columna != null) {
      appUsuarios.setTutor(rs.getBoolean("columna"));
    }
    columna = columnas.get("APP_USUARIOS_MALLA");
    if (columna != null) {
      appUsuarios.setMalla(rs.getBoolean("columna"));
    }
    columna = columnas.get("APP_USUARIOS_CONTRATO");
    if (columna != null) {
      appUsuarios.setContrato(rs.getLong("columna"));
    }
    return appUsuarios;
  }

  @Override
  public void insertar(AppUsuarios appUsuarios) throws SQLException {
    PreparedStatement sentencia = null;
    try {
      int i = 1;
      String sql = "insert into APP_USUARIOS(ID_ESTADO,CEDULA,CORREO,PASSWORD,NOMBRE,COD_INTERNO,ID_JEFE,ID_CARGO,MAC,BLOQUEO,ID_CANAL,TUTOR,MALLA,CONTRATO) values (?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
      sentencia = cnn.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);
      sentencia.setObject(i++, appUsuarios.getEstado().getIdEstado());
      sentencia.setObject(i++, appUsuarios.getCedula());
      sentencia.setObject(i++, appUsuarios.getCorreo());
      sentencia.setObject(i++, appUsuarios.getPassword());
      sentencia.setObject(i++, appUsuarios.getNombre());
      sentencia.setObject(i++, appUsuarios.getCodInterno());
      sentencia.setObject(i++, appUsuarios.getIdJefe());
      sentencia.setObject(i++, appUsuarios.getCargo().getIdCargo());
      sentencia.setObject(i++, appUsuarios.getMac());
      sentencia.setObject(i++, appUsuarios.getBloqueo());
      sentencia.setObject(i++, appUsuarios.getIdCanal());
      sentencia.setObject(i++, appUsuarios.getTutor());
      sentencia.setObject(i++, appUsuarios.getMalla());
      sentencia.setObject(i++, appUsuarios.getContrato());

      sentencia.executeUpdate();
      ResultSet rs = sentencia.getGeneratedKeys();
      if (rs.next()) {
        appUsuarios.setIdUsuario(rs.getLong(ID));
      }
    } finally {
      ConexionBD.desconectar(sentencia);
    }
  }

  @Override
  public void editar(AppUsuarios appUsuarios) throws SQLException {
    PreparedStatement sentencia = null;
    try {
      int i = 1;
      String sql = "update APP_USUARIOS set ID_ESTADO=?,CEDULA=?,CORREO=?,PASSWORD=?,NOMBRE=?,COD_INTERNO=?,ID_JEFE=?,ID_CARGO=?,MAC=?,BLOQUEO=?,ID_CANAL=?,TUTOR=?,MALLA=?,CONTRATO=? where ID_USUARIO=?";
      sentencia = cnn.prepareStatement(sql);
      sentencia.setObject(i++, appUsuarios.getEstado().getIdEstado());
      sentencia.setObject(i++, appUsuarios.getCedula());
      sentencia.setObject(i++, appUsuarios.getCorreo());
      sentencia.setObject(i++, appUsuarios.getPassword());
      sentencia.setObject(i++, appUsuarios.getNombre());
      sentencia.setObject(i++, appUsuarios.getCodInterno());
      sentencia.setObject(i++, appUsuarios.getIdJefe());
      sentencia.setObject(i++, appUsuarios.getCargo().getIdCargo());
      sentencia.setObject(i++, appUsuarios.getMac());
      sentencia.setObject(i++, appUsuarios.getBloqueo());
      sentencia.setObject(i++, appUsuarios.getIdCanal());
      sentencia.setObject(i++, appUsuarios.getTutor());
      sentencia.setObject(i++, appUsuarios.getMalla());
      sentencia.setObject(i++, appUsuarios.getContrato());
      sentencia.setObject(i++, appUsuarios.getIdUsuario());

      sentencia.executeUpdate();
    } finally {
      ConexionBD.desconectar(sentencia);
    }
  }

  @Override
  public List<AppUsuarios> consultar() throws SQLException {
    PreparedStatement sentencia = null;
    List<AppUsuarios> lista = new ArrayList<>();
    try {

      String sql = "select * from APP_USUARIOS";
      sentencia = cnn.prepareStatement(sql);
      ResultSet rs = sentencia.executeQuery();
      while (rs.next()) {
        lista.add(getAppUsuarios(rs));
      }
    } finally {
      ConexionBD.desconectar(sentencia);
    }
    return lista;

  }

  @Override
  public AppUsuarios consultar(Long id) throws SQLException {
    PreparedStatement sentencia = null;
    AppUsuarios obj = null;
    try {

      String sql = "select * from APP_USUARIOS where ID_USUARIO=?";
      sentencia = cnn.prepareStatement(sql);
      sentencia.setLong(1, id);
      ResultSet rs = sentencia.executeQuery();
      if (rs.next()) {
        obj = getAppUsuarios(rs);
      }
    } finally {
      ConexionBD.desconectar(sentencia);
    }
    return obj;
  }

}
