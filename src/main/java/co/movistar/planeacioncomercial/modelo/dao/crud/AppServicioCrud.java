/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package co.movistar.planeacioncomercial.modelo.dao.crud;

import co.movistar.planeacioncomercial.modelo.conexion.ConexionBD;
import co.movistar.planeacioncomercial.modelo.vo.AppServicio;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * @author lord_nightmare
 */
public class AppServicioCrud implements IGenericoDAO<AppServicio> {

  protected final int ID = 1;
  protected Connection cnn;

  public AppServicioCrud(Connection cnn) {
    this.cnn = cnn;
  }

  public static AppServicio getAppServicio(ResultSet rs) throws SQLException {
    AppServicio appServicio = new AppServicio();
    appServicio.setIdServicio(rs.getLong("Id_servicio"));
    appServicio.setCanal(rs.getString("Canal"));
    appServicio.setRegional(rs.getString("Regional"));
    appServicio.setNivelServicio(rs.getDouble("Nivel_Servicio"));
    appServicio.setVisitas(rs.getLong("Visitas"));
    appServicio.setTma(rs.getString("Tma"));
    appServicio.setIdUsuario(rs.getLong("id_usuario"));
    return appServicio;
  }

  public static AppServicio getAppServicio(ResultSet rs, Map<String, Integer> columnas) throws SQLException {
    AppServicio appServicio = new AppServicio();
    Integer columna = columnas.get("APP_REGIONAL_Id_servicio");
    if (columna != null) {
      appServicio.setIdServicio(rs.getLong(columna));
    }
    columna = columnas.get("APP_REGIONAL_Canal");
    if (columna != null) {
      appServicio.setCanal(rs.getString(columna));
    }
    columna = columnas.get("APP_REGIONAL_Regional");
    if (columna != null) {
      appServicio.setRegional(rs.getString(columna));
    }
    columna = columnas.get("APP_REGIONAL_Nivel_Servicio");
    if (columna != null) {
      appServicio.setNivelServicio(rs.getDouble(columna));
    }
    columna = columnas.get("APP_REGIONAL_Visitas");
    if (columna != null) {
      appServicio.setVisitas(rs.getLong(columna));
    }
    columna = columnas.get("APP_REGIONAL_Tma");
    if (columna != null) {
      appServicio.setTma(rs.getString(columna));
    }
    columna = columnas.get("APP_REGIONAL_id_usuario");
    if (columna != null) {
      appServicio.setIdUsuario(rs.getLong(columna));
    }
    return appServicio;
  }

  @Override
  public void insertar(AppServicio appServicio) throws SQLException {
    PreparedStatement sentencia = null;
    try {
      int i = 1;
      String sql = "insert into APP_SERVICIO(Canal,Regional,Nivel_Servicio,Visitas,Tma,id_usuario) values (?,?,?,?,?,?)";
      sentencia = cnn.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);
      sentencia.setObject(i++, appServicio.getCanal());
      sentencia.setObject(i++, appServicio.getRegional());
      sentencia.setObject(i++, appServicio.getNivelServicio());
      sentencia.setObject(i++, appServicio.getVisitas());
      sentencia.setObject(i++, appServicio.getTma());
      sentencia.setObject(i++, appServicio.getIdUsuario());

      sentencia.executeUpdate();
      ResultSet rs = sentencia.getGeneratedKeys();
      if (rs.next()) {
        appServicio.setIdServicio(rs.getLong(ID));
      }
    } finally {
      ConexionBD.desconectar(sentencia);
    }
  }

  @Override
  public void editar(AppServicio appServicio) throws SQLException {
    PreparedStatement sentencia = null;
    try {
      int i = 1;
      String sql = "update APP_SERVICIO set Canal=?, Regional=?, Nivel_Servicio=?, Visitas=?, Tma=?, id_usuario=? where Id_servicio=?";
      sentencia = cnn.prepareStatement(sql);
      sentencia.setObject(i++, appServicio.getCanal());
      sentencia.setObject(i++, appServicio.getRegional());
      sentencia.setObject(i++, appServicio.getNivelServicio());
      sentencia.setObject(i++, appServicio.getVisitas());
      sentencia.setObject(i++, appServicio.getTma());
      sentencia.setObject(i++, appServicio.getIdUsuario());
      sentencia.setObject(i++, appServicio.getIdServicio());

      sentencia.executeUpdate();
    } finally {
      ConexionBD.desconectar(sentencia);
    }
  }

  @Override
  public List<AppServicio> consultar() throws SQLException {
    PreparedStatement sentencia = null;
    List<AppServicio> lista = new ArrayList<>();
    try {
      String sql = "select * from dbo.APP_SERVICIO";
      sentencia = cnn.prepareStatement(sql);
      ResultSet rs = sentencia.executeQuery();
      while (rs.next()) {
        lista.add(getAppServicio(rs));
      }
    } finally {
      ConexionBD.desconectar(sentencia);
    }
    return lista;
  }

  @Override
  public AppServicio consultar(Long id) throws SQLException {
    PreparedStatement sentencia = null;
    AppServicio obj = null;
    try {
      String sql = "select * from APP_SERVICIO where Id_servicio=?";
      sentencia = cnn.prepareStatement(sql);
      sentencia.setLong(1, id);
      ResultSet rs = sentencia.executeQuery();
      if (rs.next()) {
        obj = getAppServicio(rs);
      }
    } finally {
      ConexionBD.desconectar(sentencia);
    }
    return obj;
  }

}
