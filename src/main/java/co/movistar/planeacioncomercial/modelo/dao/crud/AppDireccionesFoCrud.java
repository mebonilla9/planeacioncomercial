package co.movistar.planeacioncomercial.modelo.dao.crud;

import co.movistar.planeacioncomercial.modelo.conexion.ConexionBD;
import co.movistar.planeacioncomercial.modelo.vo.AppDireccionesFo;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class AppDireccionesFoCrud implements IGenericoDAO<AppDireccionesFo> {

  protected final int ID = 1;
  protected Connection cnn;

  public AppDireccionesFoCrud(Connection cnn) {
    this.cnn = cnn;
  }

  public static AppDireccionesFo getAppDireccionesFo(ResultSet rs) throws SQLException {
    AppDireccionesFo appDireccionesFo = new AppDireccionesFo();
    appDireccionesFo.setIdDireccion(rs.getLong("ID_DIRECCION"));
    appDireccionesFo.setTipo(rs.getString("TIPO"));
    appDireccionesFo.setMunicipio(rs.getString("MUNICIPIO"));
    appDireccionesFo.setConjunto(rs.getString("CONJUNTO"));
    appDireccionesFo.setComplemento(rs.getString("COMPLEMENTO"));
    appDireccionesFo.setDireccionCompleta(rs.getString("DIRECCION_COMPLETA"));

    return appDireccionesFo;
  }

  public static AppDireccionesFo getAppDireccionesFo(ResultSet rs, Map<String, Integer> columnas) throws SQLException {
    AppDireccionesFo appDireccionesFo = new AppDireccionesFo();
    Integer columna = columnas.get("APP_DIRECCIONESFO_ID_DIRECCION");
    if (columna != null) {
      appDireccionesFo.setIdDireccion(rs.getLong(columna));
    }
    columna = columnas.get("APP_DIRECCIONESFO_TIPO");
    if (columna != null) {
      appDireccionesFo.setTipo(rs.getString(columna));
    }
    columna = columnas.get("APP_DIRECCIONESFO_MUNICIPIO");
    if (columna != null) {
      appDireccionesFo.setMunicipio(rs.getString(columna));
    }
    columna = columnas.get("APP_DIRECCIONESFO_CONJUNTO");
    if (columna != null) {
      appDireccionesFo.setConjunto(rs.getString(columna));
    }
    columna = columnas.get("APP_DIRECCIONESFO_COMPLEMENTO");
    if (columna != null) {
      appDireccionesFo.setComplemento(rs.getString(columna));
    }
    columna = columnas.get("APP_DIRECCIONESFO_DIRECCION_COMPLETA");
    if (columna != null) {
      appDireccionesFo.setDireccionCompleta(rs.getString(columna));
    }
    return appDireccionesFo;
  }

  @Override
  public void insertar(AppDireccionesFo appDireccionesFo) throws SQLException {
    PreparedStatement sentencia = null;
    try {
      int i = 1;
      String sql = "insert into APP_DIRECCIONESFO(TIPO,MUNICIPIO,CONJUNTO,COMPLEMENTO,DIRECCION_COMPLETA) values (?,?,?,?,?)";
      sentencia = cnn.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);
      sentencia.setObject(i++, appDireccionesFo.getTipo());
      sentencia.setObject(i++, appDireccionesFo.getMunicipio());
      sentencia.setObject(i++, appDireccionesFo.getConjunto());
      sentencia.setObject(i++, appDireccionesFo.getComplemento());
      sentencia.setObject(i++, appDireccionesFo.getDireccionCompleta());

      sentencia.executeUpdate();
      ResultSet rs = sentencia.getGeneratedKeys();
      if (rs.next()) {
        appDireccionesFo.setIdDireccion(rs.getLong(ID));
      }
    } finally {
      ConexionBD.desconectar(sentencia);
    }
  }

  @Override
  public void editar(AppDireccionesFo appDireccionesFo) throws SQLException {
    PreparedStatement sentencia = null;
    try {
      int i = 1;
      String sql = "update APP_DIRECCIONESFO set TIPO=?,MUNICIPIO=?,CONJUNTO=?,COMPLEMENTO=?,DIRECCION_COMPLETA=? where ID_DIRECCION=?";
      sentencia = cnn.prepareStatement(sql);
      sentencia.setObject(i++, appDireccionesFo.getTipo());
      sentencia.setObject(i++, appDireccionesFo.getMunicipio());
      sentencia.setObject(i++, appDireccionesFo.getConjunto());
      sentencia.setObject(i++, appDireccionesFo.getComplemento());
      sentencia.setObject(i++, appDireccionesFo.getDireccionCompleta());
      sentencia.setObject(i++, appDireccionesFo.getIdDireccion());

      sentencia.executeUpdate();
    } finally {
      ConexionBD.desconectar(sentencia);
    }
  }

  @Override
  public List<AppDireccionesFo> consultar() throws SQLException {
    PreparedStatement sentencia = null;
    List<AppDireccionesFo> lista = new ArrayList<>();
    try {

      String sql = "select * from APP_DIRECCIONESFO";
      sentencia = cnn.prepareStatement(sql);
      ResultSet rs = sentencia.executeQuery();
      while (rs.next()) {
        lista.add(getAppDireccionesFo(rs));
      }
    } finally {
      ConexionBD.desconectar(sentencia);
    }
    return lista;

  }

  @Override
  public AppDireccionesFo consultar(Long id) throws SQLException {
    PreparedStatement sentencia = null;
    AppDireccionesFo obj = null;
    try {

      String sql = "select * from APP_DIRECCIONESFO where ID_DIRECCION=?";
      sentencia = cnn.prepareStatement(sql);
      sentencia.setLong(1, id);
      ResultSet rs = sentencia.executeQuery();
      if (rs.next()) {
        obj = getAppDireccionesFo(rs);
      }
    } finally {
      ConexionBD.desconectar(sentencia);
    }
    return obj;
  }

}
