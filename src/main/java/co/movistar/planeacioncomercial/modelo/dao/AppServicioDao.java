/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package co.movistar.planeacioncomercial.modelo.dao;

import co.movistar.planeacioncomercial.modelo.conexion.ConexionBD;
import co.movistar.planeacioncomercial.modelo.dao.crud.AppServicioCrud;
import co.movistar.planeacioncomercial.modelo.vo.AppServicio;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 * @author Manuel Ernesto Bonilla Muñoz <mebonilla9@gmail.com>
 */
public class AppServicioDao extends AppServicioCrud {

  public AppServicioDao(Connection cnn) {
    super(cnn);
  }

  public List<AppServicio> consultarPorUsuario(Long idUsuario) throws SQLException {
    PreparedStatement sentencia = null;
    List<AppServicio> lista = new ArrayList<>();
    try {
      String sql = "select * from APP_SERVICIO where id_usuario=?";
      sentencia = cnn.prepareStatement(sql);
      sentencia.setLong(1, idUsuario);
      ResultSet rs = sentencia.executeQuery();
      while (rs.next()) {
        lista.add(getAppServicio(rs));
      }
    } finally {
      ConexionBD.desconectar(sentencia);
    }
    return lista;
  }

}
