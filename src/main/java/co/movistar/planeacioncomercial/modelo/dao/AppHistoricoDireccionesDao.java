package co.movistar.planeacioncomercial.modelo.dao;

import co.movistar.planeacioncomercial.modelo.dao.crud.AppHistoricoDireccionesCrud;

import java.sql.Connection;

public class AppHistoricoDireccionesDao extends AppHistoricoDireccionesCrud {

  public AppHistoricoDireccionesDao(Connection cnn) {
    super(cnn);
  }
}
