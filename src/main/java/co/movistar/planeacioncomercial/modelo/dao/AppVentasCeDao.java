package co.movistar.planeacioncomercial.modelo.dao;

import co.movistar.planeacioncomercial.modelo.conexion.ConexionBD;
import co.movistar.planeacioncomercial.modelo.dao.crud.AppVentasCeCrud;
import co.movistar.planeacioncomercial.modelo.vo.AppVentasCe;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

public class AppVentasCeDao extends AppVentasCeCrud {

  public AppVentasCeDao(Connection cnn) {
    super(cnn);
  }

  public AppVentasCe consultarPorUsuario(Long idUsuario) throws SQLException {
    PreparedStatement sentencia = null;
    AppVentasCe obj = null;
    try {

      String sql = "select TOP 1 * from APP_VENTAS_CE where Id_Usuario=?";
      sentencia = cnn.prepareStatement(sql);
      sentencia.setLong(1, idUsuario);
      ResultSet rs = sentencia.executeQuery();
      if (rs.next()) {
        obj = getAppVentasCe(rs);
      }
    } finally {
      ConexionBD.desconectar(sentencia);
    }
    return obj;
  }
}
