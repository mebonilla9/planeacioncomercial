package co.movistar.planeacioncomercial.modelo.dao;

import co.movistar.planeacioncomercial.modelo.dao.crud.AppMetasCrud;

import java.sql.Connection;

public class AppMetasDao extends AppMetasCrud {

  public AppMetasDao(Connection cnn) {
    super(cnn);
  }
}
