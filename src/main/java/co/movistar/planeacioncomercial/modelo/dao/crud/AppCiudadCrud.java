package co.movistar.planeacioncomercial.modelo.dao.crud;

import co.movistar.planeacioncomercial.modelo.conexion.ConexionBD;
import co.movistar.planeacioncomercial.modelo.vo.AppCiudad;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class AppCiudadCrud implements IGenericoDAO<AppCiudad> {

  protected final int ID = 1;
  protected Connection cnn;

  public AppCiudadCrud(Connection cnn) {
    this.cnn = cnn;
  }

  public static AppCiudad getAppCiudad(ResultSet rs) throws SQLException {
    AppCiudad appCiudad = new AppCiudad();
    appCiudad.setIdCiudad(rs.getLong("ID_CIUDAD"));
    appCiudad.setNombre(rs.getString("NOMBRE"));
    appCiudad.setEstado(rs.getBoolean("ESTADO"));
    appCiudad.setIdRegional(rs.getLong("ID_REGIONAL"));

    return appCiudad;
  }

  public static AppCiudad getAppCiudad(ResultSet rs, Map<String, Integer> columnas) throws SQLException {
    AppCiudad appCiudad = new AppCiudad();
    Integer columna = columnas.get("APP_CIUDAD_ID_CIUDAD");
    if (columna != null) {
      appCiudad.setIdCiudad(rs.getLong(columna));
    }
    columna = columnas.get("APP_CIUDAD_NOMBRE");
    if (columna != null) {
      appCiudad.setNombre(rs.getString(columna));
    }
    columna = columnas.get("APP_CIUDAD_ESTADO");
    if (columna != null) {
      appCiudad.setEstado(rs.getBoolean(columna));
    }
    columna = columnas.get("APP_CIUDAD_ID_REGIONAL");
    if (columna != null) {
      appCiudad.setIdRegional(rs.getLong(columna));
    }
    return appCiudad;
  }

  @Override
  public void insertar(AppCiudad appCiudad) throws SQLException {
    PreparedStatement sentencia = null;
    try {
      int i = 1;
      String sql = "insert into APP_CIUDAD(NOMBRE,ESTADO,ID_REGIONAL) values (?,?,?)";
      sentencia = cnn.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);
      sentencia.setObject(i++, appCiudad.getNombre());
      sentencia.setObject(i++, appCiudad.getEstado());
      sentencia.setObject(i++, appCiudad.getIdRegional());

      sentencia.executeUpdate();
      ResultSet rs = sentencia.getGeneratedKeys();
      if (rs.next()) {
        appCiudad.setIdCiudad(rs.getLong(ID));
      }
    } finally {
      ConexionBD.desconectar(sentencia);
    }
  }

  @Override
  public void editar(AppCiudad appCiudad) throws SQLException {
    PreparedStatement sentencia = null;
    try {
      int i = 1;
      String sql = "update APP_CIUDAD set NOMBRE=?,ESTADO=?,ID_REGIONAL=? where ID_CIUDAD=?";
      sentencia = cnn.prepareStatement(sql);
      sentencia.setObject(i++, appCiudad.getNombre());
      sentencia.setObject(i++, appCiudad.getEstado());
      sentencia.setObject(i++, appCiudad.getIdRegional());
      sentencia.setObject(i++, appCiudad.getIdCiudad());

      sentencia.executeUpdate();
    } finally {
      ConexionBD.desconectar(sentencia);
    }
  }

  @Override
  public List<AppCiudad> consultar() throws SQLException {
    PreparedStatement sentencia = null;
    List<AppCiudad> lista = new ArrayList<>();
    try {

      String sql = "select * from APP_CIUDAD";
      sentencia = cnn.prepareStatement(sql);
      ResultSet rs = sentencia.executeQuery();
      while (rs.next()) {
        lista.add(getAppCiudad(rs));
      }
    } finally {
      ConexionBD.desconectar(sentencia);
    }
    return lista;

  }

  @Override
  public AppCiudad consultar(Long id) throws SQLException {
    PreparedStatement sentencia = null;
    AppCiudad obj = null;
    try {

      String sql = "select * from APP_CIUDAD where ID_CIUDAD=?";
      sentencia = cnn.prepareStatement(sql);
      sentencia.setLong(1, id);
      ResultSet rs = sentencia.executeQuery();
      if (rs.next()) {
        obj = getAppCiudad(rs);
      }
    } finally {
      ConexionBD.desconectar(sentencia);
    }
    return obj;
  }

}
