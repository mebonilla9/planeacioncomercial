package co.movistar.planeacioncomercial.modelo.dao;

import co.movistar.planeacioncomercial.modelo.conexion.ConexionBD;
import co.movistar.planeacioncomercial.modelo.dao.crud.AppGestionadoCrud;
import co.movistar.planeacioncomercial.modelo.vo.AppGestionado;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class AppGestionadoDao extends AppGestionadoCrud {

  public AppGestionadoDao(Connection cnn) {
    super(cnn);
  }

  public List<AppGestionado> consultarInfoGestionadoCabecera(Long idUsuario) throws SQLException {
    PreparedStatement sentencia = null;
    List<AppGestionado> lista = new ArrayList<>();
    try {
      String sql = "select * from APP_GESTIONADO where ID_USUARIO = ? order by ORDEN asc";
      sentencia = cnn.prepareStatement(sql);
      sentencia.setLong(1, idUsuario);
      ResultSet rs = sentencia.executeQuery();
      while (rs.next()) {
        lista.add(getAppGestionado(rs));
      }
    } finally {
      ConexionBD.desconectar(sentencia);
    }
    return lista;
  }

  public void editarEjecucion(AppGestionado appGestionado) throws SQLException {
    PreparedStatement sentencia = null;
    try {
      int i = 1;
      String sql = "update APP_GESTIONADO set EJECUCION=? where ID_GESTIONADO=?";
      sentencia = cnn.prepareStatement(sql);
      sentencia.setObject(i++, appGestionado.getEjecucion());
      sentencia.setObject(i++, appGestionado.getIdGestionado());
      sentencia.executeUpdate();
    } finally {
      ConexionBD.desconectar(sentencia);
    }
  }
}
