package co.movistar.planeacioncomercial.modelo.dao;

import co.movistar.planeacioncomercial.modelo.conexion.ConexionBD;
import co.movistar.planeacioncomercial.modelo.dao.crud.AppSegmentacionCrud;
import co.movistar.planeacioncomercial.modelo.vo.AppSegmentacion;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

public class AppSegmentacionDao extends AppSegmentacionCrud {

  public AppSegmentacionDao(Connection cnn) {
    super(cnn);
  }

  public AppSegmentacion consultar(String nit) throws SQLException {
    PreparedStatement sentencia = null;
    AppSegmentacion obj = null;
    try {

      String sql = "select * from APP_SEGMENTACION where nit=?";
      sentencia = cnn.prepareStatement(sql);
      sentencia.setString(1, nit);
      ResultSet rs = sentencia.executeQuery();
      if (rs.next()) {
        obj = getAppSegmentacion(rs);
      }
    } finally {
      ConexionBD.desconectar(sentencia);
    }
    return obj;
  }
}
