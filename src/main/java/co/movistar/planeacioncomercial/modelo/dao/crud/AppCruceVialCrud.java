package co.movistar.planeacioncomercial.modelo.dao.crud;

import co.movistar.planeacioncomercial.modelo.conexion.ConexionBD;
import co.movistar.planeacioncomercial.modelo.vo.AppCruceVial;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class AppCruceVialCrud implements IGenericoDAO<AppCruceVial> {

  protected final int ID = 1;
  protected Connection cnn;

  public AppCruceVialCrud(Connection cnn) {
    this.cnn = cnn;
  }

  public static AppCruceVial getAppCruceVial(ResultSet rs) throws SQLException {
    AppCruceVial appCruceVial = new AppCruceVial();
    appCruceVial.setIdCruceVial(rs.getLong("ID_CRUCE_VIAL"));
    appCruceVial.setTipoVia(rs.getString("TIPO_VIA"));
    appCruceVial.setCruceVial(rs.getString("CRUCE_VIAL"));

    return appCruceVial;
  }

  public static AppCruceVial getappCruceVial(ResultSet rs, Map<String, Integer> columnas) throws SQLException {
    AppCruceVial appCruceVial = new AppCruceVial();
    Integer columna = columnas.get("APP_CRUCE_VIAL_ID_CRUCE_VIAL");
    if (columna != null) {
      appCruceVial.setIdCruceVial(rs.getLong(columna));
    }
    columna = columnas.get("APP_CRUCE_VIAL_TIPO_VIA");
    if (columna != null) {
      appCruceVial.setTipoVia(rs.getString(columna));
    }
    columna = columnas.get("APP_CRUCE_VIAL_CRUCE_VIAL");
    if (columna != null) {
      appCruceVial.setCruceVial(rs.getString(columna));
    }
    return appCruceVial;
  }

  @Override
  public void insertar(AppCruceVial appCruceVial) throws SQLException {
    PreparedStatement sentencia = null;
    try {
      int i = 1;
      String sql = "insert into APP_CRUCE_VIAL(TIPO_VIA,CRUCE_VIAL) values (?,?)";
      sentencia = cnn.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);
      sentencia.setObject(i++, appCruceVial.getTipoVia());
      sentencia.setObject(i++, appCruceVial.getCruceVial());

      sentencia.executeUpdate();
      ResultSet rs = sentencia.getGeneratedKeys();
      if (rs.next()) {
        appCruceVial.setIdCruceVial(rs.getLong(ID));
      }
    } finally {
      ConexionBD.desconectar(sentencia);
    }
  }

  @Override
  public void editar(AppCruceVial appCruceVial) throws SQLException {
    PreparedStatement sentencia = null;
    try {
      int i = 1;
      String sql = "update APP_CRUCE_VIAL set TIPO_VIA=?,CRUCE_VIAL=? where ID_CRUCE_VIAL=?";
      sentencia = cnn.prepareStatement(sql);
      sentencia.setObject(i++, appCruceVial.getTipoVia());
      sentencia.setObject(i++, appCruceVial.getCruceVial());
      sentencia.setObject(i++, appCruceVial.getIdCruceVial());

      sentencia.executeUpdate();
    } finally {
      ConexionBD.desconectar(sentencia);
    }
  }

  @Override
  public List<AppCruceVial> consultar() throws SQLException {
    PreparedStatement sentencia = null;
    List<AppCruceVial> lista = new ArrayList<>();
    try {

      String sql = "select * from APP_CRUCE_VIAL";
      sentencia = cnn.prepareStatement(sql);
      ResultSet rs = sentencia.executeQuery();
      while (rs.next()) {
        lista.add(getAppCruceVial(rs));
      }
    } finally {
      ConexionBD.desconectar(sentencia);
    }
    return lista;

  }

  @Override
  public AppCruceVial consultar(Long id) throws SQLException {
    PreparedStatement sentencia = null;
    AppCruceVial obj = null;
    try {

      String sql = "select * from APP_CRUCE_VIAL where ID_CRUCE_VIAL=?";
      sentencia = cnn.prepareStatement(sql);
      sentencia.setLong(1, id);
      ResultSet rs = sentencia.executeQuery();
      if (rs.next()) {
        obj = getAppCruceVial(rs);
      }
    } finally {
      ConexionBD.desconectar(sentencia);
    }
    return obj;
  }

}
