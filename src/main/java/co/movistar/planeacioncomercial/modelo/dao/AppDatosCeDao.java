package co.movistar.planeacioncomercial.modelo.dao;

import co.movistar.planeacioncomercial.modelo.conexion.ConexionBD;
import co.movistar.planeacioncomercial.modelo.dao.crud.AppDatosCeCrud;
import co.movistar.planeacioncomercial.modelo.vo.AppDatosCe;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

public class AppDatosCeDao extends AppDatosCeCrud {

  public AppDatosCeDao(Connection cnn) {
    super(cnn);
  }

  public AppDatosCe consultarPorUsuario(Long idUsuario) throws SQLException {
    PreparedStatement sentencia = null;
    AppDatosCe obj = null;
    try {
      String sql = "select TOP 1 * from APP_DATOS_CE where Id_Usuario = ?";
      sentencia = cnn.prepareStatement(sql);
      sentencia.setLong(1, idUsuario);
      ResultSet rs = sentencia.executeQuery();
      if (rs.next()) {
        obj = getAppDatosCe(rs);
      }
    } finally {
      ConexionBD.desconectar(sentencia);
    }
    return obj;
  }
}
