package co.movistar.planeacioncomercial.modelo.dao.crud;

import co.movistar.planeacioncomercial.modelo.conexion.ConexionBD;
import co.movistar.planeacioncomercial.modelo.vo.AppHistoricoDirecciones;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class AppHistoricoDireccionesCrud implements IGenericoDAO<AppHistoricoDirecciones> {

  protected final int ID = 1;
  protected Connection cnn;

  public AppHistoricoDireccionesCrud(Connection cnn) {
    this.cnn = cnn;
  }

  public static AppHistoricoDirecciones getAppHistoricoDirecciones(ResultSet rs) throws SQLException {
    AppHistoricoDirecciones appHistoricoDirecciones = new AppHistoricoDirecciones();
    appHistoricoDirecciones.setIdDireccion(rs.getLong("ID_DIRECCION"));
    appHistoricoDirecciones.setIdUsuario(rs.getLong("ID_USUARIO"));
    appHistoricoDirecciones.setDireccion(rs.getString("DIRECCION"));
    appHistoricoDirecciones.setBarrio(rs.getString("BARRIO"));
    appHistoricoDirecciones.setCiudad(rs.getString("CIUDAD"));
    appHistoricoDirecciones.setCoordenadaX(rs.getString("COORDENADA_X"));
    appHistoricoDirecciones.setCoordenadaY(rs.getString("COORDENADA_Y"));

    return appHistoricoDirecciones;
  }

  public static AppHistoricoDirecciones getAppHistoricoDirecciones(ResultSet rs, Map<String, Integer> columnas) throws SQLException {
    AppHistoricoDirecciones appHistoricoDirecciones = new AppHistoricoDirecciones();
    Integer columna = columnas.get("APP_HISTORICO_DIRECCIONES_ID_DIRECCION");
    if (columna != null) {
      appHistoricoDirecciones.setIdDireccion(rs.getLong(columna));
    }
    columna = columnas.get("APP_HISTORICO_DIRECCIONES_ID_USUARIO");
    if (columna != null) {
      appHistoricoDirecciones.setIdUsuario(rs.getLong(columna));
    }
    columna = columnas.get("APP_HISTORICO_DIRECCIONES_DIRECCION");
    if (columna != null) {
      appHistoricoDirecciones.setDireccion(rs.getString(columna));
    }
    columna = columnas.get("APP_HISTORICO_DIRECCIONES_BARRIO");
    if (columna != null) {
      appHistoricoDirecciones.setBarrio(rs.getString(columna));
    }
    columna = columnas.get("APP_HISTORICO_DIRECCIONES_CIUDAD");
    if (columna != null) {
      appHistoricoDirecciones.setCiudad(rs.getString(columna));
    }
    columna = columnas.get("APP_HISTORICO_DIRECCIONES_COORDENADA_X");
    if (columna != null) {
      appHistoricoDirecciones.setCoordenadaX(rs.getString(columna));
    }
    columna = columnas.get("APP_HISTORICO_DIRECCIONES_COORDENADA_Y");
    if (columna != null) {
      appHistoricoDirecciones.setCoordenadaY(rs.getString(columna));
    }
    return appHistoricoDirecciones;
  }

  @Override
  public void insertar(AppHistoricoDirecciones appHistoricoDirecciones) throws SQLException {
    PreparedStatement sentencia = null;
    try {
      int i = 1;
      String sql = "insert into APP_HISTORICO_DIRECCIONES(ID_USUARIO,DIRECCION,BARRIO,CIUDAD,COORDENADA_X,COORDENADA_Y) values (?,?,?,?,?,?)";
      sentencia = cnn.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);
      sentencia.setObject(i++, appHistoricoDirecciones.getIdUsuario());
      sentencia.setObject(i++, appHistoricoDirecciones.getDireccion());
      sentencia.setObject(i++, appHistoricoDirecciones.getBarrio());
      sentencia.setObject(i++, appHistoricoDirecciones.getCiudad());
      sentencia.setObject(i++, appHistoricoDirecciones.getCoordenadaX());
      sentencia.setObject(i++, appHistoricoDirecciones.getCoordenadaY());

      sentencia.executeUpdate();
      ResultSet rs = sentencia.getGeneratedKeys();
      if (rs.next()) {
        appHistoricoDirecciones.setIdDireccion(rs.getLong(ID));
      }
    } finally {
      ConexionBD.desconectar(sentencia);
    }
  }

  @Override
  public void editar(AppHistoricoDirecciones appHistoricoDirecciones) throws SQLException {
    PreparedStatement sentencia = null;
    try {
      int i = 1;
      String sql = "update APP_HISTORICO_DIRECCIONES set ID_USUARIO=?,DIRECCION=?,BARRIO=?,CIUDAD=?,COORDENADA_X=?,COORDENADA_Y=? where ID_DIRECCION=?";
      sentencia = cnn.prepareStatement(sql);
      sentencia.setObject(i++, appHistoricoDirecciones.getIdUsuario());
      sentencia.setObject(i++, appHistoricoDirecciones.getDireccion());
      sentencia.setObject(i++, appHistoricoDirecciones.getBarrio());
      sentencia.setObject(i++, appHistoricoDirecciones.getCiudad());
      sentencia.setObject(i++, appHistoricoDirecciones.getCoordenadaX());
      sentencia.setObject(i++, appHistoricoDirecciones.getCoordenadaY());
      sentencia.setObject(i++, appHistoricoDirecciones.getIdDireccion());

      sentencia.executeUpdate();
    } finally {
      ConexionBD.desconectar(sentencia);
    }
  }

  @Override
  public List<AppHistoricoDirecciones> consultar() throws SQLException {
    PreparedStatement sentencia = null;
    List<AppHistoricoDirecciones> lista = new ArrayList<>();
    try {

      String sql = "select * from APP_HISTORICO_DIRECCIONES";
      sentencia = cnn.prepareStatement(sql);
      ResultSet rs = sentencia.executeQuery();
      while (rs.next()) {
        lista.add(getAppHistoricoDirecciones(rs));
      }
    } finally {
      ConexionBD.desconectar(sentencia);
    }
    return lista;

  }

  @Override
  public AppHistoricoDirecciones consultar(Long id) throws SQLException {
    PreparedStatement sentencia = null;
    AppHistoricoDirecciones obj = null;
    try {

      String sql = "select * from APP_HISTORICO_DIRECCIONES where ID_DIRECCION=?";
      sentencia = cnn.prepareStatement(sql);
      sentencia.setLong(1, id);
      ResultSet rs = sentencia.executeQuery();
      if (rs.next()) {
        obj = getAppHistoricoDirecciones(rs);
      }
    } finally {
      ConexionBD.desconectar(sentencia);
    }
    return obj;
  }

}
