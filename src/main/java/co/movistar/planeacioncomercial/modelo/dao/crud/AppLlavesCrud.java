package co.movistar.planeacioncomercial.modelo.dao.crud;

import co.movistar.planeacioncomercial.modelo.conexion.ConexionBD;
import co.movistar.planeacioncomercial.modelo.vo.AppLlaves;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class AppLlavesCrud implements IGenericoDAO<AppLlaves> {

  protected final int ID = 1;
  protected Connection cnn;

  public AppLlavesCrud(Connection cnn) {
    this.cnn = cnn;
  }

  public static AppLlaves getAppLlaves(ResultSet rs) throws SQLException {
    AppLlaves appLlaves = new AppLlaves();
    appLlaves.setIdLlave(rs.getLong("Id_llave"));
    appLlaves.setLlave(rs.getString("Llave"));
    return appLlaves;
  }

  public static AppLlaves getAppLlaves(ResultSet rs, Map<String, Integer> columnas) throws SQLException {
    AppLlaves appLlaves = new AppLlaves();
    Integer columna = columnas.get("APP_LLAVES_Id_llave");
    if (columna != null) {
      appLlaves.setIdLlave(rs.getLong(columna));
    }
    columna = columnas.get("APP_LLAVES_Llave");
    if (columna != null) {
      appLlaves.setLlave(rs.getString(columna));
    }
    return appLlaves;
  }

  @Override
  public void insertar(AppLlaves appLlaves) throws SQLException {
    System.out.println(appLlaves.getLlave().length());
    PreparedStatement sentencia = null;
    try {
      int i = 1;
      String sql = "insert into APP_LLAVES(Llave) values (?)";
      sentencia = cnn.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);
      sentencia.setObject(i++, appLlaves.getLlave());
      sentencia.executeUpdate();
      ResultSet rs = sentencia.getGeneratedKeys();
      if (rs.next()) {
        appLlaves.setIdLlave(rs.getLong(ID));
      }
    } finally {
      ConexionBD.desconectar(sentencia);
    }
  }

  @Override
  public void editar(AppLlaves appLlaves) throws SQLException {
    PreparedStatement sentencia = null;
    try {
      int i = 1;
      String sql = "update APP_LLAVES set Llave=? where Id_llave=?";
      sentencia = cnn.prepareStatement(sql);
      sentencia.setObject(i++, appLlaves.getLlave());
      sentencia.setObject(i++, appLlaves.getIdLlave());
      sentencia.executeUpdate();
    } finally {
      ConexionBD.desconectar(sentencia);
    }
  }

  @Override
  public List<AppLlaves> consultar() throws SQLException {
    PreparedStatement sentencia = null;
    List<AppLlaves> lista = new ArrayList<>();
    try {

      String sql = "select * from APP_LLAVES";
      sentencia = cnn.prepareStatement(sql);
      ResultSet rs = sentencia.executeQuery();
      while (rs.next()) {
        lista.add(getAppLlaves(rs));
      }
    } finally {
      ConexionBD.desconectar(sentencia);
    }
    return lista;

  }

  @Override
  public AppLlaves consultar(Long id) throws SQLException {
    PreparedStatement sentencia = null;
    AppLlaves obj = null;
    try {
      String sql = "select * from APP_LLAVES where Id_llave=?";
      sentencia = cnn.prepareStatement(sql);
      sentencia.setLong(1, id);
      ResultSet rs = sentencia.executeQuery();
      if (rs.next()) {
        obj = getAppLlaves(rs);
      }
    } finally {
      ConexionBD.desconectar(sentencia);
    }
    return obj;
  }

}
