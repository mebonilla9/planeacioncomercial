package co.movistar.planeacioncomercial.modelo.dao;

import co.movistar.planeacioncomercial.modelo.conexion.ConexionBD;
import co.movistar.planeacioncomercial.modelo.dao.crud.AppBajasCrud;
import co.movistar.planeacioncomercial.modelo.vo.AppBajas;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class AppBajasDao extends AppBajasCrud {

  public AppBajasDao(Connection cnn) {
    super(cnn);
  }

  public List<AppBajas> consultar(String producto, String regional) throws SQLException {
    PreparedStatement sentencia = null;
    List<AppBajas> lista = new ArrayList<>();
    try {
      String sql = "select * from APP_BAJAS WHERE PRODUCTO = ? AND REGIONAL = ?";
      sentencia = cnn.prepareStatement(sql);
      sentencia.setString(1, producto);
      sentencia.setString(2, regional);
      ResultSet rs = sentencia.executeQuery();
      while (rs.next()) {
        lista.add(getappBajas(rs));
      }
    } finally {
      ConexionBD.desconectar(sentencia);
    }
    return lista;

  }

  public List<AppBajas> consultarProductos() throws SQLException {
    PreparedStatement sentencia = null;
    List<AppBajas> lista = new ArrayList<>();
    try {
      String sql = "SELECT DISTINCT PRODUCTO FROM APP_BAJAS WHERE REGIONAL IS NOT NULL ORDER BY PRODUCTO ASC";
      sentencia = cnn.prepareStatement(sql);
      ResultSet rs = sentencia.executeQuery();
      Map<String, Integer> columnas = new HashMap<>();
      columnas.put("APP_BAJAS_PRODUCTO", 1);
      while (rs.next()) {
        lista.add(getappBajas(rs, columnas));
      }
    } finally {
      ConexionBD.desconectar(sentencia);
    }
    return lista;
  }

  public List<AppBajas> consultarRegionalCanal() throws SQLException {
    PreparedStatement sentencia = null;
    List<AppBajas> lista = new ArrayList<>();
    try {
      String sql = "select DISTINCT REGIONAL from APP_BAJAS ORDER BY REGIONAL ASC";
      sentencia = cnn.prepareStatement(sql);
      ResultSet rs = sentencia.executeQuery();
      Map<String, Integer> columnas = new HashMap<>();
      columnas.put("APP_BAJAS_REGIONAL", 1);
      while (rs.next()) {
        lista.add(getappBajas(rs, columnas));
      }
    } finally {
      ConexionBD.desconectar(sentencia);
    }
    return lista;

  }
}
