package co.movistar.planeacioncomercial.modelo.dao;

import co.movistar.planeacioncomercial.modelo.conexion.ConexionBD;
import co.movistar.planeacioncomercial.modelo.dao.crud.AppDisponibilidadFoCrud;
import co.movistar.planeacioncomercial.modelo.vo.AppDisponibilidadFo;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class AppDisponibilidadFoDao extends AppDisponibilidadFoCrud {

  public AppDisponibilidadFoDao(Connection cnn) {
    super(cnn);
  }

  public AppDisponibilidadFo consultar(AppDisponibilidadFo disFibra) throws SQLException {
    PreparedStatement sentencia = null;
    AppDisponibilidadFo obj = null;
    try {
      int i = 1;
      String sql = "select * from APP_DISPONIBILIDAD_FO where REGIONAL=? AND DEPARTAMENTO=? AND LOCALIDAD=? AND BARRIO=? AND PRINCIPAL=? AND REFERENCIA_PRINCIPAL=? AND CRUCE=? AND REFERENCIA_CRUCE=? AND PLACA=? AND COMPLEMENTO=? AND ID_PARAMETRIZADA=?";
      sentencia = cnn.prepareStatement(sql);
      sentencia.setString(i++, disFibra.getRegional());
      sentencia.setString(i++, disFibra.getDepartamento());
      sentencia.setString(i++, disFibra.getLocalidad());
      sentencia.setString(i++, disFibra.getBarrio());
      sentencia.setString(i++, disFibra.getPrincipal());
      sentencia.setString(i++, disFibra.getReferenciaPrincipal());
      sentencia.setString(i++, disFibra.getCruce());
      sentencia.setString(i++, disFibra.getReferenciaCruce());
      sentencia.setString(i++, disFibra.getPlaca());
      sentencia.setString(i++, disFibra.getComplemento() == null ? "" : disFibra.getComplemento());
      sentencia.setBoolean(i++, true);
      ResultSet rs = sentencia.executeQuery();
      if (rs.next()) {
        obj = getAppDisponibilidadFo(rs);
      }
    } finally {
      ConexionBD.desconectar(sentencia);
    }
    return obj;
  }

  public AppDisponibilidadFo consultarNoParametrizada(AppDisponibilidadFo disFibra) throws SQLException {
    PreparedStatement sentencia = null;
    AppDisponibilidadFo obj = null;
    try {
      int i = 1;
      String sql = "select * from APP_DISPONIBILIDAD_FO where REGIONAL=? AND DEPARTAMENTO=? AND LOCALIDAD=? AND BARRIO=? AND DIRECCION=? AND ID_PARAMETRIZADA=?";
      sentencia = cnn.prepareStatement(sql);
      sentencia.setString(i++, disFibra.getRegional());
      sentencia.setString(i++, disFibra.getDepartamento());
      sentencia.setString(i++, disFibra.getLocalidad());
      sentencia.setString(i++, disFibra.getBarrio());
      sentencia.setString(i++, disFibra.getDireccion());
      sentencia.setBoolean(i++, false);
      ResultSet rs = sentencia.executeQuery();
      if (rs.next()) {
        obj = getAppDisponibilidadFo(rs);
      }
    } finally {
      ConexionBD.desconectar(sentencia);
    }
    return obj;
  }

  public List<AppDisponibilidadFo> consultarDireccionesNoParametrizada(AppDisponibilidadFo disFibra) throws SQLException {
    PreparedStatement sentencia = null;
    List<AppDisponibilidadFo> lista = new ArrayList<>();
    try {
      int i = 1;
      String sql = "select DIRECCION from APP_DISPONIBILIDAD_FO where REGIONAL=? AND DEPARTAMENTO=? AND LOCALIDAD=? AND BARRIO=? AND ID_PARAMETRIZADA=?";
      sentencia = cnn.prepareStatement(sql);
      sentencia.setString(i++, disFibra.getRegional());
      sentencia.setString(i++, disFibra.getDepartamento());
      sentencia.setString(i++, disFibra.getLocalidad());
      sentencia.setString(i++, disFibra.getBarrio());
      sentencia.setBoolean(i++, false);
      ResultSet rs = sentencia.executeQuery();
      Map<String, Integer> columnas = new HashMap<>();
      columnas.put("APP_DISPONIBILIDAD_FO_DIRECCION", 1);
      while (rs.next()) {
        lista.add(getAppDisponibilidadFo(rs, columnas));
      }
    } finally {
      ConexionBD.desconectar(sentencia);
    }
    return lista;
  }

  public List<AppDisponibilidadFo> consultarRegionales(Boolean param) throws SQLException {
    PreparedStatement sentencia = null;
    List<AppDisponibilidadFo> lista = new ArrayList<>();
    try {

      String sql = "select distinct REGIONAL from APP_DISPONIBILIDAD_FO WHERE ID_PARAMETRIZADA=? ORDER BY REGIONAL ASC";
      sentencia = cnn.prepareStatement(sql);
      sentencia.setBoolean(1, param);
      ResultSet rs = sentencia.executeQuery();
      Map<String, Integer> columnas = new HashMap<>();
      columnas.put("APP_DISPONIBILIDAD_FO_REGIONAL", 1);
      while (rs.next()) {
        lista.add(getAppDisponibilidadFo(rs, columnas));
      }
    } finally {
      ConexionBD.desconectar(sentencia);
    }
    return lista;
  }

  public List<AppDisponibilidadFo> consultarDepartamentos(String regional, Boolean param) throws SQLException {
    PreparedStatement sentencia = null;
    List<AppDisponibilidadFo> lista = new ArrayList<>();
    try {
      String sql = "select distinct DEPARTAMENTO from APP_DISPONIBILIDAD_FO where REGIONAL = ? AND ID_PARAMETRIZADA=? ORDER BY DEPARTAMENTO ASC";
      sentencia = cnn.prepareStatement(sql);
      sentencia.setString(1, regional);
      sentencia.setBoolean(2, param);
      ResultSet rs = sentencia.executeQuery();
      Map<String, Integer> columnas = new HashMap<>();
      columnas.put("APP_DISPONIBILIDAD_FO_DEPARTAMENTO", 1);
      while (rs.next()) {
        lista.add(getAppDisponibilidadFo(rs, columnas));
      }
    } finally {
      ConexionBD.desconectar(sentencia);
    }
    return lista;
  }

  public List<AppDisponibilidadFo> consultarLocalidades(String departamento, Boolean param) throws SQLException {
    PreparedStatement sentencia = null;
    List<AppDisponibilidadFo> lista = new ArrayList<>();
    try {
      String sql = "select distinct LOCALIDAD from APP_DISPONIBILIDAD_FO where DEPARTAMENTO = ? AND ID_PARAMETRIZADA=? ORDER BY LOCALIDAD ASC";
      sentencia = cnn.prepareStatement(sql);
      sentencia.setString(1, departamento);
      sentencia.setBoolean(2, param);
      ResultSet rs = sentencia.executeQuery();
      Map<String, Integer> columnas = new HashMap<>();
      columnas.put("APP_DISPONIBILIDAD_FO_LOCALIDAD", 1);
      while (rs.next()) {
        lista.add(getAppDisponibilidadFo(rs, columnas));
      }
    } finally {
      ConexionBD.desconectar(sentencia);
    }
    return lista;
  }

  public List<AppDisponibilidadFo> consultarBarrios(String localidad, Boolean param) throws SQLException {
    PreparedStatement sentencia = null;
    List<AppDisponibilidadFo> lista = new ArrayList<>();
    try {
      String sql = "select distinct BARRIO from APP_DISPONIBILIDAD_FO where LOCALIDAD = ? AND ID_PARAMETRIZADA=? ORDER BY BARRIO ASC";
      sentencia = cnn.prepareStatement(sql);
      sentencia.setString(1, localidad);
      sentencia.setBoolean(2, param);
      ResultSet rs = sentencia.executeQuery();
      Map<String, Integer> columnas = new HashMap<>();
      columnas.put("APP_DISPONIBILIDAD_FO_BARRIO", 1);
      while (rs.next()) {
        lista.add(getAppDisponibilidadFo(rs, columnas));
      }
    } finally {
      ConexionBD.desconectar(sentencia);
    }
    return lista;
  }

  public List<AppDisponibilidadFo> consultarPrincipal(String localidad, String barrio) throws SQLException {
    PreparedStatement sentencia = null;
    List<AppDisponibilidadFo> lista = new ArrayList<>();
    try {
      String sql = "select distinct PRINCIPAL from APP_DISPONIBILIDAD_FO where LOCALIDAD = ? AND BARRIO = ? ORDER BY PRINCIPAL ASC";
      sentencia = cnn.prepareStatement(sql);
      sentencia.setString(1, localidad);
      sentencia.setString(2, barrio);
      ResultSet rs = sentencia.executeQuery();
      Map<String, Integer> columnas = new HashMap<>();
      columnas.put("APP_DISPONIBILIDAD_FO_PRINCIPAL", 1);
      while (rs.next()) {
        lista.add(getAppDisponibilidadFo(rs, columnas));
      }
    } finally {
      ConexionBD.desconectar(sentencia);
    }
    return lista;
  }

  public List<AppDisponibilidadFo> consultarReferenciaPrincipal(String localidad, String barrio, String principal) throws SQLException {
    PreparedStatement sentencia = null;
    List<AppDisponibilidadFo> lista = new ArrayList<>();
    try {
      String sql = "select distinct REFERENCIA_PRINCIPAL from APP_DISPONIBILIDAD_FO where LOCALIDAD = ? AND BARRIO = ? AND PRINCIPAL = ? ORDER BY REFERENCIA_PRINCIPAL ASC";
      sentencia = cnn.prepareStatement(sql);
      sentencia.setString(1, localidad);
      sentencia.setString(2, barrio);
      sentencia.setString(3, principal);
      ResultSet rs = sentencia.executeQuery();
      Map<String, Integer> columnas = new HashMap<>();
      columnas.put("APP_DISPONIBILIDAD_FO_REFERENCIA_PRINCIPAL", 1);
      while (rs.next()) {
        lista.add(getAppDisponibilidadFo(rs, columnas));
      }
    } finally {
      ConexionBD.desconectar(sentencia);
    }
    return lista;
  }

  public List<AppDisponibilidadFo> consultarCruce(String localidad, String barrio, String principal, String referenciaPrincipal) throws SQLException {
    PreparedStatement sentencia = null;
    List<AppDisponibilidadFo> lista = new ArrayList<>();
    try {
      String sql = "select distinct CRUCE from APP_DISPONIBILIDAD_FO where LOCALIDAD = ? AND BARRIO = ? AND PRINCIPAL = ? AND REFERENCIA_PRINCIPAL = ? ORDER BY CRUCE ASC";
      sentencia = cnn.prepareStatement(sql);
      sentencia.setString(1, localidad);
      sentencia.setString(2, barrio);
      sentencia.setString(3, principal);
      sentencia.setString(4, referenciaPrincipal);
      ResultSet rs = sentencia.executeQuery();
      Map<String, Integer> columnas = new HashMap<>();
      columnas.put("APP_DISPONIBILIDAD_FO_CRUCE", 1);
      while (rs.next()) {
        lista.add(getAppDisponibilidadFo(rs, columnas));
      }
    } finally {
      ConexionBD.desconectar(sentencia);
    }
    return lista;
  }

  public List<AppDisponibilidadFo> consultarReferenciaCruce(String localidad, String barrio, String principal, String referenciaPrincipal, String cruce) throws SQLException {
    PreparedStatement sentencia = null;
    List<AppDisponibilidadFo> lista = new ArrayList<>();
    try {
      String sql = "select distinct REFERENCIA_CRUCE from APP_DISPONIBILIDAD_FO where LOCALIDAD = ? AND BARRIO = ? AND PRINCIPAL = ? AND REFERENCIA_PRINCIPAL = ? AND CRUCE =? ORDER BY REFERENCIA_CRUCE ASC";
      sentencia = cnn.prepareStatement(sql);
      sentencia.setString(1, localidad);
      sentencia.setString(2, barrio);
      sentencia.setString(3, principal);
      sentencia.setString(4, referenciaPrincipal);
      sentencia.setString(5, cruce);
      ResultSet rs = sentencia.executeQuery();
      Map<String, Integer> columnas = new HashMap<>();
      columnas.put("APP_DISPONIBILIDAD_FO_REFERENCIA_CRUCE", 1);
      while (rs.next()) {
        lista.add(getAppDisponibilidadFo(rs, columnas));
      }
    } finally {
      ConexionBD.desconectar(sentencia);
    }
    return lista;
  }

  public List<AppDisponibilidadFo> consultarPlaca(String localidad, String barrio, String principal, String referenciaPrincipal, String cruce, String referenciaCruce) throws SQLException {
    PreparedStatement sentencia = null;
    List<AppDisponibilidadFo> lista = new ArrayList<>();
    try {
      String sql = "select distinct PLACA from APP_DISPONIBILIDAD_FO where LOCALIDAD = ? AND BARRIO = ? AND PRINCIPAL = ? AND REFERENCIA_PRINCIPAL = ? AND CRUCE =? AND REFERENCIA_CRUCE=? ORDER BY PLACA ASC";
      sentencia = cnn.prepareStatement(sql);
      sentencia.setString(1, localidad);
      sentencia.setString(2, barrio);
      sentencia.setString(3, principal);
      sentencia.setString(4, referenciaPrincipal);
      sentencia.setString(5, cruce);
      sentencia.setString(6, referenciaCruce);
      ResultSet rs = sentencia.executeQuery();
      Map<String, Integer> columnas = new HashMap<>();
      columnas.put("APP_DISPONIBILIDAD_FO_PLACA", 1);
      while (rs.next()) {
        lista.add(getAppDisponibilidadFo(rs, columnas));
      }
    } finally {
      ConexionBD.desconectar(sentencia);
    }
    return lista;
  }

  public List<AppDisponibilidadFo> consultarComplemento(String localidad, String barrio, String principal, String referenciaPrincipal, String cruce, String referenciaCruce, String placa) throws SQLException {
    PreparedStatement sentencia = null;
    List<AppDisponibilidadFo> lista = new ArrayList<>();
    try {
      String sql = "select distinct COMPLEMENTO from APP_DISPONIBILIDAD_FO where LOCALIDAD = ? AND BARRIO = ? AND PRINCIPAL = ? AND REFERENCIA_PRINCIPAL = ? AND CRUCE =? AND REFERENCIA_CRUCE=? AND PLACA=? ORDER BY COMPLEMENTO ASC";
      sentencia = cnn.prepareStatement(sql);
      sentencia.setString(1, localidad);
      sentencia.setString(2, barrio);
      sentencia.setString(3, principal);
      sentencia.setString(4, referenciaPrincipal);
      sentencia.setString(5, cruce);
      sentencia.setString(6, referenciaCruce);
      sentencia.setString(7, placa);
      ResultSet rs = sentencia.executeQuery();
      Map<String, Integer> columnas = new HashMap<>();
      columnas.put("APP_DISPONIBILIDAD_FO_COMPLEMENTO", 1);
      while (rs.next()) {
        lista.add(getAppDisponibilidadFo(rs, columnas));
      }
    } finally {
      ConexionBD.desconectar(sentencia);
    }
    return lista;
  }
}
