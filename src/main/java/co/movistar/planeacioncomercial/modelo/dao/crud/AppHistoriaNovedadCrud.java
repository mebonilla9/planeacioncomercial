package co.movistar.planeacioncomercial.modelo.dao.crud;

import co.movistar.planeacioncomercial.modelo.conexion.ConexionBD;
import co.movistar.planeacioncomercial.modelo.vo.AppHistoriaNovedad;
import co.movistar.planeacioncomercial.negocio.util.DateUtil;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class AppHistoriaNovedadCrud implements IGenericoDAO<AppHistoriaNovedad> {

  protected final int ID = 1;
  protected Connection cnn;

  public AppHistoriaNovedadCrud(Connection cnn) {
    this.cnn = cnn;
  }

  public static AppHistoriaNovedad getAppHistoriaNovedadCrud(ResultSet rs) throws SQLException {
    AppHistoriaNovedad appHistoriaNovedad = new AppHistoriaNovedad();
    appHistoriaNovedad.setIdHistoria(rs.getLong("Id_historia"));
    appHistoriaNovedad.setIdUsuarionovedad(rs.getLong("Id_usuarionovedad"));
    appHistoriaNovedad.setFechaInicio(rs.getTimestamp("Fecha_inicio"));
    appHistoriaNovedad.setFechaFinal(rs.getTimestamp("Fecha_final"));
    appHistoriaNovedad.setIdUsuariomodifica(rs.getLong("Id_usuariomodifica"));
    appHistoriaNovedad.setValorAnterior(rs.getString("Valor_anterior"));
    appHistoriaNovedad.setValorNuevo(rs.getString("Valor_nuevo"));
    appHistoriaNovedad.setIdNovedad(rs.getLong("id_novedad"));

    return appHistoriaNovedad;
  }

  public static AppHistoriaNovedad getAppHistoriaNovedadCrud(ResultSet rs, Map<String, Integer> columnas) throws SQLException {
    AppHistoriaNovedad appHistoriaNovedad = new AppHistoriaNovedad();
    Integer columna = columnas.get("APP_HISTORIANOVEDAD_Id_historia");
    if (columna != null) {
      appHistoriaNovedad.setIdHistoria(rs.getLong(columna));
    }
    columna = columnas.get("APP_HISTORIANOVEDAD_Id_usuarionovedad");
    if (columna != null) {
      appHistoriaNovedad.setIdUsuarionovedad(rs.getLong(columna));
    }
    columna = columnas.get("APP_HISTORIANOVEDAD_Fecha_inicio");
    if (columna != null) {
      appHistoriaNovedad.setFechaInicio(rs.getTimestamp(columna));
    }
    columna = columnas.get("APP_HISTORIANOVEDAD_Fecha_final");
    if (columna != null) {
      appHistoriaNovedad.setFechaFinal(rs.getTimestamp(columna));
    }
    columna = columnas.get("APP_HISTORIANOVEDAD_Id_usuariomodifica");
    if (columna != null) {
      appHistoriaNovedad.setIdUsuariomodifica(rs.getLong(columna));
    }
    columna = columnas.get("APP_HISTORIANOVEDAD_Valor_anterior");
    if (columna != null) {
      appHistoriaNovedad.setValorAnterior(rs.getString(columna));
    }
    columna = columnas.get("APP_HISTORIANOVEDAD_Valor_nuevo");
    if (columna != null) {
      appHistoriaNovedad.setValorNuevo(rs.getString(columna));
    }
    columna = columnas.get("APP_HISTORIANOVEDAD_id_novedad");
    if (columna != null) {
      appHistoriaNovedad.setIdNovedad(rs.getLong(columna));
    }
    return appHistoriaNovedad;
  }

  @Override
  public void insertar(AppHistoriaNovedad appHistoriaNovedad) throws SQLException {
    PreparedStatement sentencia = null;
    try {
      int i = 1;
      String sql = "insert into APP_HISTORIANOVEDAD(Id_usuarionovedad,Fecha_inicio,Fecha_final,Id_usuariomodifica,Valor_anterior,Valor_nuevo,id_novedad) values (?,?,?,?,?,?,?)";
      sentencia = cnn.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);
      sentencia.setObject(i++, appHistoriaNovedad.getIdUsuarionovedad());
      sentencia.setObject(i++, appHistoriaNovedad.getFechaInicio() != null ? DateUtil.balancearFecha(appHistoriaNovedad.getFechaInicio()) : appHistoriaNovedad.getFechaInicio());
      sentencia.setObject(i++, appHistoriaNovedad.getFechaFinal() != null ? DateUtil.balancearFecha(appHistoriaNovedad.getFechaFinal()) : appHistoriaNovedad.getFechaFinal());
      sentencia.setObject(i++, appHistoriaNovedad.getIdUsuariomodifica());
      sentencia.setObject(i++, appHistoriaNovedad.getValorAnterior());
      sentencia.setObject(i++, appHistoriaNovedad.getValorNuevo());
      sentencia.setObject(i++, appHistoriaNovedad.getIdNovedad());

      sentencia.executeUpdate();
      ResultSet rs = sentencia.getGeneratedKeys();
      if (rs.next()) {
        appHistoriaNovedad.setIdHistoria(rs.getLong(ID));
      }
    } finally {
      ConexionBD.desconectar(sentencia);
    }
  }

  @Override
  public void editar(AppHistoriaNovedad appHistoriaNovedad) throws SQLException {
    PreparedStatement sentencia = null;
    try {
      int i = 1;
      String sql = "update APP_HISTORIANOVEDAD set Id_usuarionovedad=?,Fecha_inicio=?,Fecha_final=?,Id_usuariomodifica=?,Valor_anterior=?,Valor_nuevo=?,Id_novedad=? where Id_historia=?";
      sentencia = cnn.prepareStatement(sql);
      sentencia.setObject(i++, appHistoriaNovedad.getIdUsuarionovedad());
      sentencia.setObject(i++, appHistoriaNovedad.getFechaInicio() != null ? DateUtil.balancearFecha(appHistoriaNovedad.getFechaInicio()) : appHistoriaNovedad.getFechaInicio());
      sentencia.setObject(i++, appHistoriaNovedad.getFechaFinal() != null ? DateUtil.balancearFecha(appHistoriaNovedad.getFechaFinal()) : appHistoriaNovedad.getFechaFinal());
      sentencia.setObject(i++, appHistoriaNovedad.getIdUsuariomodifica());
      sentencia.setObject(i++, appHistoriaNovedad.getValorAnterior());
      sentencia.setObject(i++, appHistoriaNovedad.getValorNuevo());
      sentencia.setObject(i++, appHistoriaNovedad.getIdNovedad());
      sentencia.setObject(i++, appHistoriaNovedad.getIdHistoria());

      sentencia.executeUpdate();
    } finally {
      ConexionBD.desconectar(sentencia);
    }
  }

  @Override
  public List<AppHistoriaNovedad> consultar() throws SQLException {
    PreparedStatement sentencia = null;
    List<AppHistoriaNovedad> lista = new ArrayList<>();
    try {

      String sql = "select * from APP_HISTORIANOVEDAD";
      sentencia = cnn.prepareStatement(sql);
      ResultSet rs = sentencia.executeQuery();
      while (rs.next()) {
        lista.add(getAppHistoriaNovedadCrud(rs));
      }
    } finally {
      ConexionBD.desconectar(sentencia);
    }
    return lista;

  }

  @Override
  public AppHistoriaNovedad consultar(Long id) throws SQLException {
    PreparedStatement sentencia = null;
    AppHistoriaNovedad obj = null;
    try {

      String sql = "select * from APP_HISTORIANOVEDAD where Id_novedad=?";
      sentencia = cnn.prepareStatement(sql);
      sentencia.setLong(1, id);
      ResultSet rs = sentencia.executeQuery();
      if (rs.next()) {
        obj = getAppHistoriaNovedadCrud(rs);
      }
    } finally {
      ConexionBD.desconectar(sentencia);
    }
    return obj;
  }

}
