package co.movistar.planeacioncomercial.modelo.dao;

import co.movistar.planeacioncomercial.modelo.dao.crud.AppBarriosCrud;

import java.sql.Connection;

public class AppBarriosDao extends AppBarriosCrud {

  public AppBarriosDao(Connection cnn) {
    super(cnn);
  }
}
