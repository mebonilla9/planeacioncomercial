package co.movistar.planeacioncomercial.modelo.dao.crud;

import co.movistar.planeacioncomercial.modelo.conexion.ConexionBD;
import co.movistar.planeacioncomercial.modelo.vo.AppPorcentajeCorte;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * @author Manuel Ernesto Bonilla Muñoz <mebonilla9@gmail.com>
 */
public class AppPorcentajeCorteCrud implements IGenericoDAO<AppPorcentajeCorte> {

  protected final int ID = 1;
  protected Connection cnn;

  public AppPorcentajeCorteCrud(Connection cnn) {
    this.cnn = cnn;
  }

  public static AppPorcentajeCorte getAPPPORCENTAJECORTE(ResultSet rs) throws SQLException {
    AppPorcentajeCorte appPorcentajeCorte = new AppPorcentajeCorte();
    appPorcentajeCorte.setIdPorcentajeCorte(rs.getLong("ID_PORCENTAJE_CORTE"));
    appPorcentajeCorte.setDeberiamosIrAl(rs.getString("DEBERIAMOS_IR_AL"));
    appPorcentajeCorte.setAltasAlDia(rs.getString("ALTAS_AL_DIA"));
    return appPorcentajeCorte;
  }

  public static AppPorcentajeCorte getAPPPORCENTAJECORTE(ResultSet rs, Map<String, Integer> columnas) throws SQLException {
    AppPorcentajeCorte appPorcentajeCorte = new AppPorcentajeCorte();
    Integer columna = columnas.get("APP_PORCENTAJE_CORTE_ID_PORCENTAJE_CORTE");
    if (columna != null) {
      appPorcentajeCorte.setIdPorcentajeCorte(rs.getLong(columna));
    }
    columna = columnas.get("APP_PORCENTAJE_CORTE_DEBERIAMOS_IR_AL");
    if (columna != null) {
      appPorcentajeCorte.setDeberiamosIrAl(rs.getString(columna));
    }
    columna = columnas.get("APP_PORCENTAJE_CORTE_ALTAS_AL_DIA");
    if (columna != null) {
      appPorcentajeCorte.setAltasAlDia(rs.getString(columna));
    }
    return appPorcentajeCorte;
  }

  @Override
  public void insertar(AppPorcentajeCorte appPorcentajeCorte) throws SQLException {
    PreparedStatement sentencia = null;
    try {
      int i = 1;
      String sql = "insert into APP_PORCENTAJE_CORTE(DEBERIAMOS_IR_AL,ALTAS_AL_DIA) values (?,?)";
      sentencia = cnn.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);
      sentencia.setObject(i++, appPorcentajeCorte.getDeberiamosIrAl());
      sentencia.setObject(i++, appPorcentajeCorte.getAltasAlDia());

      sentencia.executeUpdate();
      ResultSet rs = sentencia.getGeneratedKeys();
      if (rs.next()) {
        appPorcentajeCorte.setIdPorcentajeCorte(rs.getLong(ID));
      }
    } finally {
      ConexionBD.desconectar(sentencia);
    }
  }

  @Override
  public void editar(AppPorcentajeCorte appPorcentajeCorte) throws SQLException {
    PreparedStatement sentencia = null;
    try {
      int i = 1;
      String sql = "update APP_PORCENTAJE_CORTE set DEBERIAMOS_IR_AL=?, ALTAS_AL_DIA = ? where ID_PORCENTAJE_CORTE=?";
      sentencia = cnn.prepareStatement(sql);
      sentencia.setObject(i++, appPorcentajeCorte.getDeberiamosIrAl());
      sentencia.setObject(i++, appPorcentajeCorte.getIdPorcentajeCorte());
      sentencia.setObject(i++, appPorcentajeCorte.getAltasAlDia());

      sentencia.executeUpdate();
    } finally {
      ConexionBD.desconectar(sentencia);
    }
  }

  @Override
  public List<AppPorcentajeCorte> consultar() throws SQLException {
    PreparedStatement sentencia = null;
    List<AppPorcentajeCorte> lista = new ArrayList<>();
    try {

      String sql = "select * from APP_PORCENTAJE_CORTE";
      sentencia = cnn.prepareStatement(sql);
      ResultSet rs = sentencia.executeQuery();
      while (rs.next()) {
        lista.add(getAPPPORCENTAJECORTE(rs));
      }
    } finally {
      ConexionBD.desconectar(sentencia);
    }
    return lista;

  }

  @Override
  public AppPorcentajeCorte consultar(Long id) throws SQLException {
    PreparedStatement sentencia = null;
    AppPorcentajeCorte obj = null;
    try {

      String sql = "select * from APP_PORCENTAJE_CORTE where ID_PORCENTAJE_CORTE=?";
      sentencia = cnn.prepareStatement(sql);
      sentencia.setLong(1, id);
      ResultSet rs = sentencia.executeQuery();
      if (rs.next()) {
        obj = getAPPPORCENTAJECORTE(rs);
      }
    } finally {
      ConexionBD.desconectar(sentencia);
    }
    return obj;
  }

}
