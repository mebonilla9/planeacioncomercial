package co.movistar.planeacioncomercial.modelo.dao.crud;

import co.movistar.planeacioncomercial.modelo.conexion.ConexionBD;
import co.movistar.planeacioncomercial.modelo.vo.AppDisponibilidad;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * @author Manuel Ernesto Bonilla Muñoz <mebonilla9@gmail.com>
 */
public class AppDisponibilidadCrud implements IGenericoDAO<AppDisponibilidad> {

  protected final int ID = 1;
  protected Connection cnn;

  public AppDisponibilidadCrud(Connection cnn) {
    this.cnn = cnn;
  }

  public static AppDisponibilidad getappDisponibilidad(ResultSet rs) throws SQLException {
    AppDisponibilidad appDisponibilidad = new AppDisponibilidad();
    appDisponibilidad.setLlave(rs.getString("llave"));
    appDisponibilidad.setDivipola(rs.getString("Divipola"));
    appDisponibilidad.setMDF(rs.getString("MDF"));
    appDisponibilidad.setArmario(rs.getString("Armario"));
    appDisponibilidad.setPlantainternalibre(rs.getLong("Plantainternalibre"));
    appDisponibilidad.setZonaBa(rs.getString("ZonaBa"));
    appDisponibilidad.setPuertosbalibre(rs.getLong("Puertosbalibre"));
    appDisponibilidad.setPrimariatotal(rs.getLong("Primariatotal"));
    appDisponibilidad.setPrimarialibre(rs.getLong("Primarialibre"));
    appDisponibilidad.setSecundariatotal(rs.getLong("Secundariatotal"));
    appDisponibilidad.setSecundarialibre(rs.getLong("Secundarialibre"));
    appDisponibilidad.setTipored(rs.getString("Tipored"));
    appDisponibilidad.setCaja(rs.getString("Caja"));
    appDisponibilidad.setTipocaja(rs.getString("Tipocaja"));
    appDisponibilidad.setDireccioncaja(rs.getString("Direccioncaja"));
    appDisponibilidad.setPareslibrescaja(rs.getLong("Pareslibrescaja"));
    appDisponibilidad.setSistema(rs.getString("Sistema"));
    appDisponibilidad.setCategoria(rs.getString("Categoria"));
    appDisponibilidad.setOfertar(rs.getString("Ofertar"));
    appDisponibilidad.setVelocidad(rs.getString("Velocidad"));
    appDisponibilidad.setNombrepop(rs.getString("Nombrepop"));
    appDisponibilidad.setTipoequipo(rs.getString("Tipoequipo"));
    appDisponibilidad.setDepartamento(rs.getString("Departamento"));
    appDisponibilidad.setNombreMunicipio(rs.getString("NombreMunicipio"));
    appDisponibilidad.setLocalidad(rs.getString("Localidad"));
    appDisponibilidad.setDistrito(rs.getString("Distrito"));
    appDisponibilidad.setRegional(rs.getString("Regional"));
    appDisponibilidad.setqOferta(rs.getString("Q_Oferta"));
    appDisponibilidad.setX(rs.getDouble("X"));
    appDisponibilidad.setY(rs.getDouble("Y"));
    appDisponibilidad.setxO(rs.getDouble("Xo"));
    appDisponibilidad.setyO(rs.getDouble("Yo"));
    return appDisponibilidad;
  }

  public static AppDisponibilidad getappDisponibilidad(ResultSet rs, Map<String, Integer> columnas) throws SQLException {
    AppDisponibilidad appDisponibilidad = new AppDisponibilidad();
    Integer columna = columnas.get("APP_DISPONIBILIDAD_llave");
    if (columna != null) {
      appDisponibilidad.setLlave(rs.getString(columna));
    }
    columna = columnas.get("APP_DISPONIBILIDAD_Divipola");
    if (columna != null) {
      appDisponibilidad.setDivipola(rs.getString(columna));
    }
    columna = columnas.get("APP_DISPONIBILIDAD_MDF");
    if (columna != null) {
      appDisponibilidad.setMDF(rs.getString(columna));
    }
    columna = columnas.get("APP_DISPONIBILIDAD_Armario");
    if (columna != null) {
      appDisponibilidad.setArmario(rs.getString(columna));
    }
    columna = columnas.get("APP_DISPONIBILIDAD_Plantainternalibre");
    if (columna != null) {
      appDisponibilidad.setPlantainternalibre(rs.getLong(columna));
    }
    columna = columnas.get("APP_DISPONIBILIDAD_ZonaBa");
    if (columna != null) {
      appDisponibilidad.setZonaBa(rs.getString(columna));
    }
    columna = columnas.get("APP_DISPONIBILIDAD_Puertosbalibre");
    if (columna != null) {
      appDisponibilidad.setPuertosbalibre(rs.getLong(columna));
    }
    columna = columnas.get("APP_DISPONIBILIDAD_Primariatotal");
    if (columna != null) {
      appDisponibilidad.setPrimariatotal(rs.getLong(columna));
    }
    columna = columnas.get("APP_DISPONIBILIDAD_Primarialibre");
    if (columna != null) {
      appDisponibilidad.setPrimarialibre(rs.getLong(columna));
    }
    columna = columnas.get("APP_DISPONIBILIDAD_Secundariatotal");
    if (columna != null) {
      appDisponibilidad.setSecundariatotal(rs.getLong(columna));
    }
    columna = columnas.get("APP_DISPONIBILIDAD_Secundarialibre");
    if (columna != null) {
      appDisponibilidad.setSecundarialibre(rs.getLong(columna));
    }
    columna = columnas.get("APP_DISPONIBILIDAD_Tipored");
    if (columna != null) {
      appDisponibilidad.setTipored(rs.getString(columna));
    }
    columna = columnas.get("APP_DISPONIBILIDAD_Caja");
    if (columna != null) {
      appDisponibilidad.setCaja(rs.getString(columna));
    }
    columna = columnas.get("APP_DISPONIBILIDAD_Tipocaja");
    if (columna != null) {
      appDisponibilidad.setTipocaja(rs.getString(columna));
    }
    columna = columnas.get("APP_DISPONIBILIDAD_Direccioncaja");
    if (columna != null) {
      appDisponibilidad.setDireccioncaja(rs.getString(columna));
    }
    columna = columnas.get("APP_DISPONIBILIDAD_Pareslibrescaja");
    if (columna != null) {
      appDisponibilidad.setPareslibrescaja(rs.getLong(columna));
    }
    columna = columnas.get("APP_DISPONIBILIDAD_Sistema");
    if (columna != null) {
      appDisponibilidad.setSistema(rs.getString(columna));
    }
    columna = columnas.get("APP_DISPONIBILIDAD_Categoria");
    if (columna != null) {
      appDisponibilidad.setCategoria(rs.getString(columna));
    }
    columna = columnas.get("APP_DISPONIBILIDAD_Ofertar");
    if (columna != null) {
      appDisponibilidad.setOfertar(rs.getString(columna));
    }
    columna = columnas.get("APP_DISPONIBILIDAD_Velocidad");
    if (columna != null) {
      appDisponibilidad.setVelocidad(rs.getString(columna));
    }
    columna = columnas.get("APP_DISPONIBILIDAD_Nombrepop");
    if (columna != null) {
      appDisponibilidad.setNombrepop(rs.getString(columna));
    }
    columna = columnas.get("APP_DISPONIBILIDAD_Tipoequipo");
    if (columna != null) {
      appDisponibilidad.setTipoequipo(rs.getString(columna));
    }
    columna = columnas.get("APP_DISPONIBILIDAD_Departamento");
    if (columna != null) {
      appDisponibilidad.setDepartamento(rs.getString(columna));
    }
    columna = columnas.get("APP_DISPONIBILIDAD_NombreMunicipio");
    if (columna != null) {
      appDisponibilidad.setNombreMunicipio(rs.getString(columna));
    }
    columna = columnas.get("APP_DISPONIBILIDAD_Localidad");
    if (columna != null) {
      appDisponibilidad.setLocalidad(rs.getString(columna));
    }
    columna = columnas.get("APP_DISPONIBILIDAD_Distrito");
    if (columna != null) {
      appDisponibilidad.setDistrito(rs.getString(columna));
    }
    columna = columnas.get("APP_DISPONIBILIDAD_Regional");
    if (columna != null) {
      appDisponibilidad.setRegional(rs.getString(columna));
    }
    columna = columnas.get("APP_DISPONIBILIDAD_Q_Oferta");
    if (columna != null) {
      appDisponibilidad.setqOferta(rs.getString(columna));
    }
    columna = columnas.get("APP_DISPONIBILIDAD_X");
    if (columna != null) {
      appDisponibilidad.setX(rs.getDouble(columna));
    }
    columna = columnas.get("APP_DISPONIBILIDAD_Y");
    if (columna != null) {
      appDisponibilidad.setY(rs.getDouble(columna));
    }
    columna = columnas.get("APP_DISPONIBILIDAD_Xo");
    if (columna != null) {
      appDisponibilidad.setxO(rs.getDouble(columna));
    }
    columna = columnas.get("APP_DISPONIBILIDAD_Yo");
    if (columna != null) {
      appDisponibilidad.setyO(rs.getDouble(columna));
    }
    return appDisponibilidad;
  }

  @Override
  public void insertar(AppDisponibilidad appDisponibilidad) throws SQLException {
    PreparedStatement sentencia = null;
    try {
      int i = 1;
      String sql = "insert into APP_DISPONIBILIDAD(Divipola,MDF,Armario,Plantainternalibre,ZonaBa,Puertosbalibre,Primariatotal,Primarialibre,Secundariatotal,Secundarialibre,Tipored,Caja,Tipocaja,Direccioncaja,Pareslibrescaja,Sistema,Categoria,Ofertar,Velocidad,Nombrepop,Tipoequipo,Departamento,NombreMunicipio,Localidad,Distrito,Regional,Q_Oferta,X,Y,Xo,Yo) values (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
      sentencia = cnn.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);
      sentencia.setObject(i++, appDisponibilidad.getDivipola());
      sentencia.setObject(i++, appDisponibilidad.getMDF());
      sentencia.setObject(i++, appDisponibilidad.getArmario());
      sentencia.setObject(i++, appDisponibilidad.getPlantainternalibre());
      sentencia.setObject(i++, appDisponibilidad.getZonaBa());
      sentencia.setObject(i++, appDisponibilidad.getPuertosbalibre());
      sentencia.setObject(i++, appDisponibilidad.getPrimariatotal());
      sentencia.setObject(i++, appDisponibilidad.getPrimarialibre());
      sentencia.setObject(i++, appDisponibilidad.getSecundariatotal());
      sentencia.setObject(i++, appDisponibilidad.getSecundarialibre());
      sentencia.setObject(i++, appDisponibilidad.getTipored());
      sentencia.setObject(i++, appDisponibilidad.getCaja());
      sentencia.setObject(i++, appDisponibilidad.getTipocaja());
      sentencia.setObject(i++, appDisponibilidad.getDireccioncaja());
      sentencia.setObject(i++, appDisponibilidad.getPareslibrescaja());
      sentencia.setObject(i++, appDisponibilidad.getSistema());
      sentencia.setObject(i++, appDisponibilidad.getCategoria());
      sentencia.setObject(i++, appDisponibilidad.getOfertar());
      sentencia.setObject(i++, appDisponibilidad.getVelocidad());
      sentencia.setObject(i++, appDisponibilidad.getNombrepop());
      sentencia.setObject(i++, appDisponibilidad.getTipoequipo());
      sentencia.setObject(i++, appDisponibilidad.getDepartamento());
      sentencia.setObject(i++, appDisponibilidad.getNombreMunicipio());
      sentencia.setObject(i++, appDisponibilidad.getLocalidad());
      sentencia.setObject(i++, appDisponibilidad.getDistrito());
      sentencia.setObject(i++, appDisponibilidad.getRegional());
      sentencia.setObject(i++, appDisponibilidad.getqOferta());
      sentencia.setObject(i++, appDisponibilidad.getX());
      sentencia.setObject(i++, appDisponibilidad.getY());
      sentencia.setObject(i++, appDisponibilidad.getxO());
      sentencia.setObject(i++, appDisponibilidad.getyO());
      sentencia.executeUpdate();
      ResultSet rs = sentencia.getGeneratedKeys();
      if (rs.next()) {
        appDisponibilidad.setLlave(rs.getString(ID));
      }
    } finally {
      ConexionBD.desconectar(sentencia);
    }
  }

  @Override
  public void editar(AppDisponibilidad appDisponibilidad) throws SQLException {
    PreparedStatement sentencia = null;
    try {
      int i = 1;
      String sql = "update APP_DISPONIBILIDAD set Divipola=?,MDF=?,Armario=?,Plantainternalibre=?,ZonaBa=?,Puertosbalibre=?,Primariatotal=?,Primarialibre=?,Secundariatotal=?,Secundarialibre=?,Tipored=?,Caja=?,Tipocaja=?,Direccioncaja=?,Pareslibrescaja=?,Sistema=?,Categoria=?,Ofertar=?,Velocidad=?,Nombrepop=?,Tipoequipo=?,Departamento=?,NombreMunicipio=?,Localidad=?,Distrito=?,Regional=?,Q_Oferta=?,X=?,Y=?,Xo=?,Yo=? where llave=?";
      sentencia = cnn.prepareStatement(sql);
      sentencia.setObject(i++, appDisponibilidad.getDivipola());
      sentencia.setObject(i++, appDisponibilidad.getMDF());
      sentencia.setObject(i++, appDisponibilidad.getArmario());
      sentencia.setObject(i++, appDisponibilidad.getPlantainternalibre());
      sentencia.setObject(i++, appDisponibilidad.getZonaBa());
      sentencia.setObject(i++, appDisponibilidad.getPuertosbalibre());
      sentencia.setObject(i++, appDisponibilidad.getPrimariatotal());
      sentencia.setObject(i++, appDisponibilidad.getPrimarialibre());
      sentencia.setObject(i++, appDisponibilidad.getSecundariatotal());
      sentencia.setObject(i++, appDisponibilidad.getSecundarialibre());
      sentencia.setObject(i++, appDisponibilidad.getTipored());
      sentencia.setObject(i++, appDisponibilidad.getCaja());
      sentencia.setObject(i++, appDisponibilidad.getTipocaja());
      sentencia.setObject(i++, appDisponibilidad.getDireccioncaja());
      sentencia.setObject(i++, appDisponibilidad.getPareslibrescaja());
      sentencia.setObject(i++, appDisponibilidad.getSistema());
      sentencia.setObject(i++, appDisponibilidad.getCategoria());
      sentencia.setObject(i++, appDisponibilidad.getOfertar());
      sentencia.setObject(i++, appDisponibilidad.getVelocidad());
      sentencia.setObject(i++, appDisponibilidad.getNombrepop());
      sentencia.setObject(i++, appDisponibilidad.getTipoequipo());
      sentencia.setObject(i++, appDisponibilidad.getDepartamento());
      sentencia.setObject(i++, appDisponibilidad.getNombreMunicipio());
      sentencia.setObject(i++, appDisponibilidad.getLocalidad());
      sentencia.setObject(i++, appDisponibilidad.getDistrito());
      sentencia.setObject(i++, appDisponibilidad.getRegional());
      sentencia.setObject(i++, appDisponibilidad.getLlave());
      sentencia.setObject(i++, appDisponibilidad.getqOferta());
      sentencia.setObject(i++, appDisponibilidad.getX());
      sentencia.setObject(i++, appDisponibilidad.getY());
      sentencia.setObject(i++, appDisponibilidad.getxO());
      sentencia.setObject(i++, appDisponibilidad.getyO());
      sentencia.executeUpdate();
    } finally {
      ConexionBD.desconectar(sentencia);
    }
  }

  @Override
  public List<AppDisponibilidad> consultar() throws SQLException {
    PreparedStatement sentencia = null;
    List<AppDisponibilidad> lista = new ArrayList<>();
    try {

      String sql = "select * from APP_DISPONIBILIDAD";
      sentencia = cnn.prepareStatement(sql);
      ResultSet rs = sentencia.executeQuery();
      while (rs.next()) {
        lista.add(getappDisponibilidad(rs));
      }
    } finally {
      ConexionBD.desconectar(sentencia);
    }
    return lista;

  }

  @Override
  public AppDisponibilidad consultar(Long id) throws SQLException {
    PreparedStatement sentencia = null;
    AppDisponibilidad obj = null;
    try {

      String sql = "select * from APP_DISPONIBILIDAD where llave=?";
      sentencia = cnn.prepareStatement(sql);
      sentencia.setLong(1, id);
      ResultSet rs = sentencia.executeQuery();
      if (rs.next()) {
        obj = getappDisponibilidad(rs);
      }
    } finally {
      ConexionBD.desconectar(sentencia);
    }
    return obj;
  }

}
