package co.movistar.planeacioncomercial.modelo.dao.crud;

import co.movistar.planeacioncomercial.modelo.conexion.ConexionBD;
import co.movistar.planeacioncomercial.modelo.vo.AppAdminCanal;
import co.movistar.planeacioncomercial.negocio.util.DateUtil;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class AppAdminCanalCrud implements IGenericoDAO<AppAdminCanal> {

  protected final int ID = 1;
  protected Connection cnn;

  public AppAdminCanalCrud(Connection cnn) {
    this.cnn = cnn;
  }

  public static AppAdminCanal getAppAdminCanal(ResultSet rs) throws SQLException {
    AppAdminCanal appAdminCanal = new AppAdminCanal();
    appAdminCanal.setIdAdminCanal(rs.getLong("Id_admin_canal"));
    appAdminCanal.setFechaInicio(rs.getTimestamp("Fecha_inicio"));
    appAdminCanal.setFechaFinal(rs.getTimestamp("Fecha_final"));
    appAdminCanal.setNombreCanal(rs.getString("Nombre_canal"));

    return appAdminCanal;
  }

  public static AppAdminCanal getAppAdminCanal(ResultSet rs, Map<String, Integer> columnas) throws SQLException {
    AppAdminCanal appAdminCanal = new AppAdminCanal();
    Integer columna = columnas.get("APP_ADMIN_CANAL_Id_admin_canal");
    if (columna != null) {
      appAdminCanal.setIdAdminCanal(rs.getLong(columna));
    }
    columna = columnas.get("APP_ADMIN_CANAL_Fecha_inicio");
    if (columna != null) {
      appAdminCanal.setFechaInicio(rs.getTimestamp(columna));
    }
    columna = columnas.get("APP_ADMIN_CANAL_Fecha_final");
    if (columna != null) {
      appAdminCanal.setFechaFinal(rs.getTimestamp(columna));
    }
    columna = columnas.get("APP_ADMIN_CANAL_Nombre_canal");
    if (columna != null) {
      appAdminCanal.setNombreCanal(rs.getString(columna));
    }
    return appAdminCanal;
  }

  @Override
  public void insertar(AppAdminCanal appAdminCanal) throws SQLException {
    PreparedStatement sentencia = null;
    try {
      int i = 1;
      String sql = "insert into APP_ADMIN_CANAL(Fecha_inicio,Fecha_final,Nombre_canal) values (?,?,?)";
      sentencia = cnn.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);
      sentencia.setObject(i++, DateUtil.parseTimestamp(appAdminCanal.getFechaInicio()));
      sentencia.setObject(i++, DateUtil.parseTimestamp(appAdminCanal.getFechaFinal()));
      sentencia.setObject(i++, appAdminCanal.getNombreCanal());

      sentencia.executeUpdate();
      ResultSet rs = sentencia.getGeneratedKeys();
      if (rs.next()) {
        appAdminCanal.setIdAdminCanal(rs.getLong(ID));
      }
    } finally {
      ConexionBD.desconectar(sentencia);
    }
  }

  @Override
  public void editar(AppAdminCanal appAdminCanal) throws SQLException {
    PreparedStatement sentencia = null;
    try {
      int i = 1;
      String sql = "update APP_ADMIN_CANAL set Fecha_inicio=?,Fecha_final=?,Nombre_canal=? where Id_admin_canal=?";
      sentencia = cnn.prepareStatement(sql);
      sentencia.setObject(i++, DateUtil.parseTimestamp(appAdminCanal.getFechaInicio()));
      sentencia.setObject(i++, DateUtil.parseTimestamp(appAdminCanal.getFechaFinal()));
      sentencia.setObject(i++, appAdminCanal.getNombreCanal());
      sentencia.setObject(i++, appAdminCanal.getIdAdminCanal());

      sentencia.executeUpdate();
    } finally {
      ConexionBD.desconectar(sentencia);
    }
  }

  @Override
  public List<AppAdminCanal> consultar() throws SQLException {
    PreparedStatement sentencia = null;
    List<AppAdminCanal> lista = new ArrayList<>();
    try {

      String sql = "select * from APP_ADMIN_CANAL";
      sentencia = cnn.prepareStatement(sql);
      ResultSet rs = sentencia.executeQuery();
      while (rs.next()) {
        lista.add(getAppAdminCanal(rs));
      }
    } finally {
      ConexionBD.desconectar(sentencia);
    }
    return lista;

  }

  @Override
  public AppAdminCanal consultar(Long id) throws SQLException {
    PreparedStatement sentencia = null;
    AppAdminCanal obj = null;
    try {

      String sql = "select * from APP_ADMIN_CANAL where Id_admin_canal=?";
      sentencia = cnn.prepareStatement(sql);
      sentencia.setLong(1, id);
      ResultSet rs = sentencia.executeQuery();
      if (rs.next()) {
        obj = getAppAdminCanal(rs);
      }
    } finally {
      ConexionBD.desconectar(sentencia);
    }
    return obj;
  }

}
