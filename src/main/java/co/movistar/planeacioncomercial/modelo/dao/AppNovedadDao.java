package co.movistar.planeacioncomercial.modelo.dao;

import co.movistar.planeacioncomercial.modelo.conexion.ConexionBD;
import co.movistar.planeacioncomercial.modelo.dao.crud.AppNovedadCrud;
import co.movistar.planeacioncomercial.modelo.vo.AppNovedad;
import co.movistar.planeacioncomercial.negocio.constantes.ECanales;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class AppNovedadDao extends AppNovedadCrud {

  public AppNovedadDao(Connection cnn) {
    super(cnn);
  }

  @Deprecated
  public List<AppNovedad> consultarVentaDirecta() throws SQLException {
    PreparedStatement sentencia = null;
    List<AppNovedad> lista = new ArrayList<>();
    try {

      String sql = "select * from APP_NOVEDAD where modulos like '%VD%'";
      sentencia = cnn.prepareStatement(sql);
      ResultSet rs = sentencia.executeQuery();
      while (rs.next()) {
        lista.add(getAppNovedad(rs));
      }
    } finally {
      ConexionBD.desconectar(sentencia);
    }
    return lista;
  }

  @Deprecated
  public List<AppNovedad> consultarCentroExperiencia() throws SQLException {
    PreparedStatement sentencia = null;
    List<AppNovedad> lista = new ArrayList<>();
    try {
      String sql = "select * from APP_NOVEDAD where modulos like '%CD%'";
      sentencia = cnn.prepareStatement(sql);
      ResultSet rs = sentencia.executeQuery();
      while (rs.next()) {
        lista.add(getAppNovedad(rs));
      }
    } finally {
      ConexionBD.desconectar(sentencia);
    }
    return lista;
  }

  public List<AppNovedad> listarNovedadesModulo(ECanales canal) throws SQLException{
    PreparedStatement sentencia = null;
    List<AppNovedad> lista = new ArrayList<>();
    try {
      String sql = "select * from APP_NOVEDAD where modulos like ?";
      sentencia = cnn.prepareStatement(sql);
      sentencia.setString(1,"%"+canal.getModulo()+"%");
      ResultSet rs = sentencia.executeQuery();
      while (rs.next()) {
        lista.add(getAppNovedad(rs));
      }
    } finally {
      ConexionBD.desconectar(sentencia);
    }
    return lista;
  }
}
