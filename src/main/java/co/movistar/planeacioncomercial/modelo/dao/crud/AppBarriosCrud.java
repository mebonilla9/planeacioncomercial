package co.movistar.planeacioncomercial.modelo.dao.crud;

import co.movistar.planeacioncomercial.modelo.conexion.ConexionBD;
import co.movistar.planeacioncomercial.modelo.vo.AppBarrios;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class AppBarriosCrud implements IGenericoDAO<AppBarrios> {

  protected final int ID = 1;
  protected Connection cnn;

  public AppBarriosCrud(Connection cnn) {
    this.cnn = cnn;
  }

  public static AppBarrios getAppBarrios(ResultSet rs) throws SQLException {
    AppBarrios appBarrios = new AppBarrios();
    appBarrios.setIdBarrio(rs.getLong("ID_BARRIO"));
    appBarrios.setDivipola(rs.getLong("DIVIPOLA"));
    appBarrios.setDepartamento(rs.getString("DEPARTAMENTO"));
    appBarrios.setMunicipio(rs.getString("MUNICIPIO"));
    appBarrios.setNombre(rs.getString("NOMBRE"));

    return appBarrios;
  }

  public static AppBarrios getAppBarrios(ResultSet rs, Map<String, Integer> columnas) throws SQLException {
    AppBarrios appBarrios = new AppBarrios();
    Integer columna = columnas.get("APP_BARRIOS_ID_BARRIO");
    if (columna != null) {
      appBarrios.setIdBarrio(rs.getLong(columna));
    }
    columna = columnas.get("APP_BARRIOS_DIVIPOLA");
    if (columna != null) {
      appBarrios.setDivipola(rs.getLong(columna));
    }
    columna = columnas.get("APP_BARRIOS_DEPARTAMENTO");
    if (columna != null) {
      appBarrios.setDepartamento(rs.getString(columna));
    }
    columna = columnas.get("APP_BARRIOS_MUNICIPIO");
    if (columna != null) {
      appBarrios.setMunicipio(rs.getString(columna));
    }
    columna = columnas.get("APP_BARRIOS_NOMBRE");
    if (columna != null) {
      appBarrios.setNombre(rs.getString(columna));
    }
    return appBarrios;
  }

  @Override
  public void insertar(AppBarrios appBarrios) throws SQLException {
    PreparedStatement sentencia = null;
    try {
      int i = 1;
      String sql = "insert into APP_BARRIOS(DIVIPOLA,DEPARTAMENTO,MUNICIPIO,NOMBRE) values (?,?,?,?)";
      sentencia = cnn.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);
      sentencia.setObject(i++, appBarrios.getDivipola());
      sentencia.setObject(i++, appBarrios.getDepartamento());
      sentencia.setObject(i++, appBarrios.getMunicipio());
      sentencia.setObject(i++, appBarrios.getNombre());

      sentencia.executeUpdate();
      ResultSet rs = sentencia.getGeneratedKeys();
      if (rs.next()) {
        appBarrios.setIdBarrio(rs.getLong(ID));
      }
    } finally {
      ConexionBD.desconectar(sentencia);
    }
  }

  @Override
  public void editar(AppBarrios appBarrios) throws SQLException {
    PreparedStatement sentencia = null;
    try {
      int i = 1;
      String sql = "update APP_BARRIOS set DIVIPOLA=?,DEPARTAMENTO=?,MUNICIPIO=?,NOMBRE=? where ID_BARRIO=?";
      sentencia = cnn.prepareStatement(sql);
      sentencia.setObject(i++, appBarrios.getDivipola());
      sentencia.setObject(i++, appBarrios.getDepartamento());
      sentencia.setObject(i++, appBarrios.getMunicipio());
      sentencia.setObject(i++, appBarrios.getNombre());
      sentencia.setObject(i++, appBarrios.getIdBarrio());

      sentencia.executeUpdate();
    } finally {
      ConexionBD.desconectar(sentencia);
    }
  }

  @Override
  public List<AppBarrios> consultar() throws SQLException {
    PreparedStatement sentencia = null;
    List<AppBarrios> lista = new ArrayList<>();
    try {

      String sql = "select * from APP_BARRIOS";
      sentencia = cnn.prepareStatement(sql);
      ResultSet rs = sentencia.executeQuery();
      while (rs.next()) {
        lista.add(getAppBarrios(rs));
      }
    } finally {
      ConexionBD.desconectar(sentencia);
    }
    return lista;

  }

  @Override
  public AppBarrios consultar(Long id) throws SQLException {
    PreparedStatement sentencia = null;
    AppBarrios obj = null;
    try {

      String sql = "select * from APP_BARRIOS where ID_BARRIO=?";
      sentencia = cnn.prepareStatement(sql);
      sentencia.setLong(1, id);
      ResultSet rs = sentencia.executeQuery();
      if (rs.next()) {
        obj = getAppBarrios(rs);
      }
    } finally {
      ConexionBD.desconectar(sentencia);
    }
    return obj;
  }

}
