package co.movistar.planeacioncomercial.modelo.dao.crud;

import co.movistar.planeacioncomercial.modelo.conexion.ConexionBD;
import co.movistar.planeacioncomercial.modelo.vo.AppEstados;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * @author Manuel Ernesto Bonilla Muñoz <mebonilla9@gmail.com>
 */
public class AppEstadosCrud implements IGenericoDAO<AppEstados> {

  protected final int ID = 1;
  protected Connection cnn;

  public AppEstadosCrud(Connection cnn) {
    this.cnn = cnn;
  }

  public static AppEstados getAppEstados(ResultSet rs) throws SQLException {
    AppEstados appEstados = new AppEstados();
    appEstados.setIdEstado(rs.getLong("ID_ESTADO"));
    appEstados.setNombreEstado(rs.getString("NOMBRE_ESTADO"));

    return appEstados;
  }

  public static AppEstados getAppEstados(ResultSet rs, Map<String, Integer> columnas) throws SQLException {
    AppEstados appEstados = new AppEstados();
    Integer columna = columnas.get("APP_ESTADOS_ID_ESTADO");
    if (columna != null) {
      appEstados.setIdEstado(rs.getLong(columna));
    }
    columna = columnas.get("APP_ESTADOS_NOMBRE_ESTADO");
    if (columna != null) {
      appEstados.setNombreEstado(rs.getString(columna));
    }
    return appEstados;
  }

  @Override
  public void insertar(AppEstados appEstados) throws SQLException {
    PreparedStatement sentencia = null;
    try {
      int i = 1;
      String sql = "insert into APP_ESTADOS(NOMBRE_ESTADO) values (?)";
      sentencia = cnn.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);
      sentencia.setObject(i++, appEstados.getNombreEstado());

      sentencia.executeUpdate();
      ResultSet rs = sentencia.getGeneratedKeys();
      if (rs.next()) {
        appEstados.setIdEstado(rs.getLong(ID));
      }
    } finally {
      ConexionBD.desconectar(sentencia);
    }
  }

  @Override
  public void editar(AppEstados appEstados) throws SQLException {
    PreparedStatement sentencia = null;
    try {
      int i = 1;
      String sql = "update APP_ESTADOS set NOMBRE_ESTADO=? where ID_ESTADO=?";
      sentencia = cnn.prepareStatement(sql);
      sentencia.setObject(i++, appEstados.getNombreEstado());
      sentencia.setObject(i++, appEstados.getIdEstado());

      sentencia.executeUpdate();
    } finally {
      ConexionBD.desconectar(sentencia);
    }
  }

  @Override
  public List<AppEstados> consultar() throws SQLException {
    PreparedStatement sentencia = null;
    List<AppEstados> lista = new ArrayList<>();
    try {

      String sql = "select * from APP_ESTADOS";
      sentencia = cnn.prepareStatement(sql);
      ResultSet rs = sentencia.executeQuery();
      while (rs.next()) {
        lista.add(getAppEstados(rs));
      }
    } finally {
      ConexionBD.desconectar(sentencia);
    }
    return lista;

  }

  @Override
  public AppEstados consultar(Long id) throws SQLException {
    PreparedStatement sentencia = null;
    AppEstados obj = null;
    try {

      String sql = "select * from APP_ESTADOS where ID_ESTADO=?";
      sentencia = cnn.prepareStatement(sql);
      sentencia.setLong(1, id);
      ResultSet rs = sentencia.executeQuery();
      if (rs.next()) {
        obj = getAppEstados(rs);
      }
    } finally {
      ConexionBD.desconectar(sentencia);
    }
    return obj;
  }

}
