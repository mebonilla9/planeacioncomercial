package co.movistar.planeacioncomercial.modelo.dao;

import co.movistar.planeacioncomercial.modelo.conexion.ConexionBD;
import co.movistar.planeacioncomercial.modelo.dao.crud.AppCabeceraCrud;
import co.movistar.planeacioncomercial.modelo.vo.AppCabecera;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

public class AppCabeceraDao extends AppCabeceraCrud {

  public AppCabeceraDao(Connection cnn) {
    super(cnn);
  }

  public AppCabecera consultarCabeceraPorUsuario(Long idUsuario) throws SQLException {
    PreparedStatement sentencia = null;
    AppCabecera obj = null;
    try {
      String sql = "select * from APP_CABECERA where ID_USUARIO=?";
      sentencia = cnn.prepareStatement(sql);
      sentencia.setLong(1, idUsuario);
      ResultSet rs = sentencia.executeQuery();
      if (rs.next()) {
        obj = getAppCabecera(rs);
      }
    } finally {
      ConexionBD.desconectar(sentencia);
    }
    return obj;
  }

  public void ejecutarActualizacionCalculador(Long idUsuario) throws SQLException {
    PreparedStatement sentencia = null;
    try {
      String sql = "exec VARIABLE ?";
      sentencia = cnn.prepareStatement(sql);
      sentencia.setLong(1, idUsuario);
      sentencia.executeUpdate();
    } finally {
      ConexionBD.desconectar(sentencia);
    }
  }
}
