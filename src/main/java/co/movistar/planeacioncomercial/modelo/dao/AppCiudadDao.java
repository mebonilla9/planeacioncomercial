package co.movistar.planeacioncomercial.modelo.dao;

import co.movistar.planeacioncomercial.modelo.conexion.ConexionBD;
import co.movistar.planeacioncomercial.modelo.dao.crud.AppCiudadCrud;
import co.movistar.planeacioncomercial.modelo.vo.AppCiudad;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class AppCiudadDao extends AppCiudadCrud {

  public AppCiudadDao(Connection cnn) {
    super(cnn);
  }

  public List<AppCiudad> consultarPorRegional(Long id) throws SQLException {
    PreparedStatement sentencia = null;
    List<AppCiudad> lista = new ArrayList<>();
    try {
      String sql = "select * from APP_CIUDAD where ID_REGIONAL = ?";
      sentencia = cnn.prepareStatement(sql);
      sentencia.setLong(1, id);
      ResultSet rs = sentencia.executeQuery();
      while (rs.next()) {
        lista.add(getAppCiudad(rs));
      }
    } finally {
      ConexionBD.desconectar(sentencia);
    }
    return lista;

  }
}
