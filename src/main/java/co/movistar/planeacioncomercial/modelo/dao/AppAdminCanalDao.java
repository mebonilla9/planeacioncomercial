package co.movistar.planeacioncomercial.modelo.dao;

import co.movistar.planeacioncomercial.modelo.conexion.ConexionBD;
import co.movistar.planeacioncomercial.modelo.dao.crud.AppAdminCanalCrud;
import co.movistar.planeacioncomercial.modelo.vo.AppAdminCanal;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

public class AppAdminCanalDao extends AppAdminCanalCrud {

  public AppAdminCanalDao(Connection cnn) {
    super(cnn);
  }

  public AppAdminCanal consultarPorModulo(String modulo) throws SQLException {
    PreparedStatement sentencia = null;
    AppAdminCanal obj = null;
    try {

      String sql = "select * from APP_ADMIN_CANAL where Nombre_canal=?";
      sentencia = cnn.prepareStatement(sql);
      sentencia.setString(1, modulo);
      ResultSet rs = sentencia.executeQuery();
      if (rs.next()) {
        obj = getAppAdminCanal(rs);
      }
    } finally {
      ConexionBD.desconectar(sentencia);
    }
    return obj;
  }
}
