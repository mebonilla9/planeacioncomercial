package co.movistar.planeacioncomercial.modelo.dao.crud;

import co.movistar.planeacioncomercial.modelo.conexion.ConexionBD;
import co.movistar.planeacioncomercial.modelo.vo.*;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * @author Manuel Ernesto Bonilla Muñoz <mebonilla9@gmail.com>
 */
public class AppUsuarioGeograficaCrud implements IGenericoDAO<AppUsuarioGeografia> {

  protected final int ID = 1;
  protected Connection cnn;

  public AppUsuarioGeograficaCrud(Connection cnn) {
    this.cnn = cnn;
  }

  public static AppUsuarioGeografia getAppUsuarioGeografia(ResultSet rs) throws SQLException {
    AppUsuarioGeografia appUsuarioGeografia = new AppUsuarioGeografia();
    appUsuarioGeografia.setIdUsuarioGeografia(rs.getLong("ID_USUARIO_GEOGRAFIA"));
    appUsuarioGeografia.setCanal(new AppCanales(rs.getLong("ID_CANAL")));
    appUsuarioGeografia.setJefatura(new AppJefatura(rs.getLong("ID_JEFATURA")));
    appUsuarioGeografia.setRegional(new AppRegional(rs.getLong("ID_REGIONAL")));
    appUsuarioGeografia.setUsuario(new AppUsuarios(rs.getLong("ID_USUARIO")));
    return appUsuarioGeografia;
  }

  public static AppUsuarioGeografia getAppUsuarioGeografia(ResultSet rs, Map<String, Integer> columnas) throws SQLException {
    AppUsuarioGeografia appUsuarioGeografia = new AppUsuarioGeografia();
    Integer columna = columnas.get("APP_USUARIO_GEOGRAFIA_ID_USUARIO_GEOGRAFIA");
    if (columna != null) {
      appUsuarioGeografia.setIdUsuarioGeografia(rs.getLong(columna));
    }
    columna = columnas.get("APP_USUARIO_GEOGRAFIA_ID_CANAL");
    if (columna != null) {
      appUsuarioGeografia.setCanal(new AppCanales(rs.getLong(columna)));
    }
    columna = columnas.get("APP_USUARIO_GEOGRAFIA_ID_JEFATURA");
    if (columna != null) {
      appUsuarioGeografia.setJefatura(new AppJefatura(rs.getLong(columna)));
    }
    columna = columnas.get("APP_USUARIO_GEOGRAFIA_ID_REGIONAL");
    if (columna != null) {
      appUsuarioGeografia.setRegional(new AppRegional(rs.getLong(columna)));
    }
    columna = columnas.get("APP_USUARIO_GEOGRAFIA_ID_USUARIO");
    if (columna != null) {
      appUsuarioGeografia.setUsuario(new AppUsuarios(rs.getLong(columna)));
    }
    return appUsuarioGeografia;
  }

  @Override
  public void insertar(AppUsuarioGeografia appUsuarioGeografia) throws SQLException {
    PreparedStatement sentencia = null;
    try {
      int i = 1;
      String sql = "insert into APP_USUARIO_GEOGRAFIA(ID_CANAL,ID_JEFATURA,ID_REGIONAL,ID_USUARIO) values (?,?,?,?)";
      sentencia = cnn.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);
      sentencia.setObject(i++, appUsuarioGeografia.getCanal().getIdCanal());
      sentencia.setObject(i++, appUsuarioGeografia.getJefatura().getIdJefatura());
      sentencia.setObject(i++, appUsuarioGeografia.getRegional().getIdRegional());
      sentencia.setObject(i++, appUsuarioGeografia.getUsuario().getIdUsuario());

      sentencia.executeUpdate();
      ResultSet rs = sentencia.getGeneratedKeys();
      if (rs.next()) {
        appUsuarioGeografia.setIdUsuarioGeografia(rs.getLong(ID));
      }
    } finally {
      ConexionBD.desconectar(sentencia);
    }
  }

  @Override
  public void editar(AppUsuarioGeografia appUsuarioGeografia) throws SQLException {
    PreparedStatement sentencia = null;
    try {
      int i = 1;
      String sql = "update APP_USUARIO_GEOGRAFIA set ID_CANAL=?,ID_JEFATURA=?,ID_REGIONAL=?,ID_USUARIO=? where ID_USUARIO_GEOGRAFIA=?";
      sentencia = cnn.prepareStatement(sql);
      sentencia.setObject(i++, appUsuarioGeografia.getCanal().getIdCanal());
      sentencia.setObject(i++, appUsuarioGeografia.getJefatura().getIdJefatura());
      sentencia.setObject(i++, appUsuarioGeografia.getRegional().getIdRegional());
      sentencia.setObject(i++, appUsuarioGeografia.getUsuario().getIdUsuario());
      sentencia.setObject(i++, appUsuarioGeografia.getIdUsuarioGeografia());

      sentencia.executeUpdate();
    } finally {
      ConexionBD.desconectar(sentencia);
    }
  }

  @Override
  public List<AppUsuarioGeografia> consultar() throws SQLException {
    PreparedStatement sentencia = null;
    List<AppUsuarioGeografia> lista = new ArrayList<>();
    try {

      String sql = "select * from APP_USUARIO_GEOGRAFIA";
      sentencia = cnn.prepareStatement(sql);
      ResultSet rs = sentencia.executeQuery();
      while (rs.next()) {
        lista.add(getAppUsuarioGeografia(rs));
      }
    } finally {
      ConexionBD.desconectar(sentencia);
    }
    return lista;

  }

  @Override
  public AppUsuarioGeografia consultar(Long id) throws SQLException {
    PreparedStatement sentencia = null;
    AppUsuarioGeografia obj = null;
    try {
      String sql = "select * from APP_USUARIO_GEOGRAFIA where ID_USUARIO_GEOGRAFIA=?";
      sentencia = cnn.prepareStatement(sql);
      sentencia.setLong(1, id);
      ResultSet rs = sentencia.executeQuery();
      if (rs.next()) {
        obj = getAppUsuarioGeografia(rs);
      }
    } finally {
      ConexionBD.desconectar(sentencia);
    }
    return obj;
  }

}
