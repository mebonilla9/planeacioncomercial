package co.movistar.planeacioncomercial.modelo.dao;

import co.movistar.planeacioncomercial.modelo.conexion.ConexionBD;
import co.movistar.planeacioncomercial.modelo.dao.crud.AppNoGestionadoCrud;
import co.movistar.planeacioncomercial.modelo.vo.AppNoGestionado;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class AppNoGestionadoDao extends AppNoGestionadoCrud {

  public AppNoGestionadoDao(Connection cnn) {
    super(cnn);
  }

  public List<AppNoGestionado> consultarInfoNoGestionadoCabecera(Long idUsuario) throws SQLException {
    PreparedStatement sentencia = null;
    List<AppNoGestionado> lista = new ArrayList<>();
    try {
      String sql = "select * from APP_NO_GESTIONADO where ID_USUARIO = ?";
      sentencia = cnn.prepareStatement(sql);
      sentencia.setLong(1, idUsuario);
      ResultSet rs = sentencia.executeQuery();
      while (rs.next()) {
        lista.add(getAppNoGestionado(rs));
      }
    } finally {
      ConexionBD.desconectar(sentencia);
    }
    return lista;
  }

  public void editarEjecucion(AppNoGestionado appNoGestionado) throws SQLException {
    PreparedStatement sentencia = null;
    try {
      int i = 1;
      String sql = "update APP_NO_GESTIONADO set EJECUCION=? where ID_NO_GESTIONADO=?";
      sentencia = cnn.prepareStatement(sql);
      sentencia.setObject(i++, appNoGestionado.getEjecucion());
      sentencia.setObject(i++, appNoGestionado.getIdNoGestionado());
      sentencia.executeUpdate();
    } finally {
      ConexionBD.desconectar(sentencia);
    }
  }
}
