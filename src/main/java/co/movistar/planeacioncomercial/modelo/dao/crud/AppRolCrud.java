package co.movistar.planeacioncomercial.modelo.dao.crud;

import co.movistar.planeacioncomercial.modelo.conexion.ConexionBD;
import co.movistar.planeacioncomercial.modelo.vo.AppRol;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class AppRolCrud implements IGenericoDAO<AppRol> {

  protected final int ID = 1;
  protected Connection cnn;

  public AppRolCrud(Connection cnn) {
    this.cnn = cnn;
  }

  public static AppRol getAppRol(ResultSet rs) throws SQLException {
    AppRol appRol = new AppRol();
    appRol.setIdRol(rs.getLong("Id_rol"));
    appRol.setNombreRol(rs.getString("Nombre_rol"));

    return appRol;
  }

  public static AppRol getAppRol(ResultSet rs, Map<String, Integer> columnas) throws SQLException {
    AppRol appRol = new AppRol();
    Integer columna = columnas.get("APP_ROL_Id_rol");
    if (columna != null) {
      appRol.setIdRol(rs.getLong(columna));
    }
    columna = columnas.get("APP_ROL_Nombre_rol");
    if (columna != null) {
      appRol.setNombreRol(rs.getString(columna));
    }
    return appRol;
  }

  @Override
  public void insertar(AppRol appRol) throws SQLException {
    PreparedStatement sentencia = null;
    try {
      int i = 1;
      String sql = "insert into APP_ROL(Nombre_rol) values (?)";
      sentencia = cnn.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);
      sentencia.setObject(i++, appRol.getNombreRol());

      sentencia.executeUpdate();
      ResultSet rs = sentencia.getGeneratedKeys();
      if (rs.next()) {
        appRol.setIdRol(rs.getLong(ID));
      }
    } finally {
      ConexionBD.desconectar(sentencia);
    }
  }

  @Override
  public void editar(AppRol appRol) throws SQLException {
    PreparedStatement sentencia = null;
    try {
      int i = 1;
      String sql = "update APP_ROL set Nombre_rol=? where Id_rol=?";
      sentencia = cnn.prepareStatement(sql);
      sentencia.setObject(i++, appRol.getNombreRol());
      sentencia.setObject(i++, appRol.getIdRol());

      sentencia.executeUpdate();
    } finally {
      ConexionBD.desconectar(sentencia);
    }
  }

  @Override
  public List<AppRol> consultar() throws SQLException {
    PreparedStatement sentencia = null;
    List<AppRol> lista = new ArrayList<>();
    try {

      String sql = "select * from APP_ROL";
      sentencia = cnn.prepareStatement(sql);
      ResultSet rs = sentencia.executeQuery();
      while (rs.next()) {
        lista.add(getAppRol(rs));
      }
    } finally {
      ConexionBD.desconectar(sentencia);
    }
    return lista;

  }

  @Override
  public AppRol consultar(Long id) throws SQLException {
    PreparedStatement sentencia = null;
    AppRol obj = null;
    try {

      String sql = "select * from APP_ROL where Id_rol=?";
      sentencia = cnn.prepareStatement(sql);
      sentencia.setLong(1, id);
      ResultSet rs = sentencia.executeQuery();
      if (rs.next()) {
        obj = getAppRol(rs);
      }
    } finally {
      ConexionBD.desconectar(sentencia);
    }
    return obj;
  }

}
