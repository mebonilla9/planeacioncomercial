package co.movistar.planeacioncomercial.modelo.dao.crud;

import co.movistar.planeacioncomercial.modelo.conexion.ConexionBD;
import co.movistar.planeacioncomercial.modelo.vo.AppArticulos;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class AppArticulosCrud implements IGenericoDAO<AppArticulos> {

  protected final int ID = 1;
  protected Connection cnn;

  public AppArticulosCrud(Connection cnn) {
    this.cnn = cnn;
  }

  public static AppArticulos getAppArticulos(ResultSet rs) throws SQLException {
    AppArticulos appArticulos = new AppArticulos();
    appArticulos.setIdInventario(rs.getLong("Id_inventario"));
    appArticulos.setIdArticulo(rs.getLong("Id_articulo"));
    appArticulos.setNombreArticulo(rs.getString("Nombre_Articulo"));
    appArticulos.setIdPunto(rs.getString("Id_punto"));
    appArticulos.setCantidad(rs.getLong("Cantidad"));

    return appArticulos;
  }

  public static AppArticulos getAppArticulos(ResultSet rs, Map<String, Integer> columnas) throws SQLException {
    AppArticulos appArticulos = new AppArticulos();
    Integer columna = columnas.get("APP_ARTICULOS_Id_inventario");
    if (columna != null) {
      appArticulos.setIdInventario(rs.getLong(columna));
    }
    columna = columnas.get("APP_ARTICULOS_Id_articulo");
    if (columna != null) {
      appArticulos.setIdArticulo(rs.getLong(columna));
    }
    columna = columnas.get("APP_ARTICULOS_Nombre_Articulo");
    if (columna != null) {
      appArticulos.setNombreArticulo(rs.getString(columna));
    }
    columna = columnas.get("APP_ARTICULOS_Id_punto");
    if (columna != null) {
      appArticulos.setIdPunto(rs.getString(columna));
    }
    columna = columnas.get("APP_ARTICULOS_Cantidad");
    if (columna != null) {
      appArticulos.setCantidad(rs.getLong(columna));
    }
    return appArticulos;
  }

  @Override
  public void insertar(AppArticulos appArticulos) throws SQLException {
    PreparedStatement sentencia = null;
    try {
      int i = 1;
      String sql = "insert into APP_ARTICULOS(Id_articulo,Nombre_Articulo,Id_punto,Cantidad) values (?,?,?,?)";
      sentencia = cnn.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);
      sentencia.setObject(i++, appArticulos.getIdArticulo());
      sentencia.setObject(i++, appArticulos.getNombreArticulo());
      sentencia.setObject(i++, appArticulos.getIdPunto());
      sentencia.setObject(i++, appArticulos.getCantidad());

      sentencia.executeUpdate();
      ResultSet rs = sentencia.getGeneratedKeys();
      if (rs.next()) {
        appArticulos.setIdInventario(rs.getLong(ID));
      }
    } finally {
      ConexionBD.desconectar(sentencia);
    }
  }

  @Override
  public void editar(AppArticulos appArticulos) throws SQLException {
    PreparedStatement sentencia = null;
    try {
      int i = 1;
      String sql = "update APP_ARTICULOS set Id_articulo=?,Nombre_Articulo=?,Id_punto=?,Cantidad=? where Id_inventario=?";
      sentencia = cnn.prepareStatement(sql);
      sentencia.setObject(i++, appArticulos.getIdArticulo());
      sentencia.setObject(i++, appArticulos.getNombreArticulo());
      sentencia.setObject(i++, appArticulos.getIdPunto());
      sentencia.setObject(i++, appArticulos.getCantidad());
      sentencia.setObject(i++, appArticulos.getIdInventario());

      sentencia.executeUpdate();
    } finally {
      ConexionBD.desconectar(sentencia);
    }
  }

  @Override
  public List<AppArticulos> consultar() throws SQLException {
    PreparedStatement sentencia = null;
    List<AppArticulos> lista = new ArrayList<>();
    try {

      String sql = "select * from APP_ARTICULOS";
      sentencia = cnn.prepareStatement(sql);
      ResultSet rs = sentencia.executeQuery();
      while (rs.next()) {
        lista.add(getAppArticulos(rs));
      }
    } finally {
      ConexionBD.desconectar(sentencia);
    }
    return lista;

  }

  @Override
  public AppArticulos consultar(Long id) throws SQLException {
    PreparedStatement sentencia = null;
    AppArticulos obj = null;
    try {

      String sql = "select * from APP_ARTICULOS where Id_inventario=?";
      sentencia = cnn.prepareStatement(sql);
      sentencia.setLong(1, id);
      ResultSet rs = sentencia.executeQuery();
      if (rs.next()) {
        obj = getAppArticulos(rs);
      }
    } finally {
      ConexionBD.desconectar(sentencia);
    }
    return obj;
  }

}
