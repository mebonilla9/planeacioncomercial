package co.movistar.planeacioncomercial.modelo.dao.crud;

import co.movistar.planeacioncomercial.modelo.conexion.ConexionBD;
import co.movistar.planeacioncomercial.modelo.vo.AppNoGestionado;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class AppNoGestionadoCrud implements IGenericoDAO<AppNoGestionado> {

  protected final int ID = 1;
  protected Connection cnn;

  public AppNoGestionadoCrud(Connection cnn) {
    this.cnn = cnn;
  }

  public static AppNoGestionado getAppNoGestionado(ResultSet rs) throws SQLException {
    AppNoGestionado appNoGestionado = new AppNoGestionado();
    appNoGestionado.setIdNoGestionado(rs.getLong("ID_NO_GESTIONADO"));
    appNoGestionado.setIdUsuario(rs.getLong("ID_USUARIO"));
    appNoGestionado.setCedula(rs.getLong("CEDULA"));
    appNoGestionado.setCorreo(rs.getString("CORREO"));
    appNoGestionado.setVariable(rs.getString("VARIABLE"));
    appNoGestionado.setMeta(rs.getDouble("META"));
    appNoGestionado.setEjecucion(rs.getLong("EJECUCION") == 0 ? null : rs.getLong("EJECUCION"));
    appNoGestionado.setCumplimiento(rs.getDouble("CUMPLIMIENTO") == 0 ? null : rs.getDouble("CUMPLIMIENTO"));
    appNoGestionado.setPesoEsquema(rs.getDouble("PESO_ESQUEMA") == 0 ? null : rs.getDouble("PESO_ESQUEMA"));
    appNoGestionado.setIpVariable(rs.getDouble("IP_VARIABLE") == 0 ? null : rs.getDouble("IP_VARIABLE"));

    return appNoGestionado;
  }

  public static AppNoGestionado getAppNoGestionado(ResultSet rs, Map<String, Integer> columnas) throws SQLException {
    AppNoGestionado appNoGestionado = new AppNoGestionado();
    Integer columna = columnas.get("APP_NO_GESTIONADO_ID_NO_GESTIONADO");
    if (columna != null) {
      appNoGestionado.setIdNoGestionado(rs.getLong(columna));
    }
    columna = columnas.get("APP_NO_GESTIONADO_ID_USUARIO");
    if (columna != null) {
      appNoGestionado.setIdUsuario(rs.getLong(columna));
    }
    columna = columnas.get("APP_NO_GESTIONADO_CEDULA");
    if (columna != null) {
      appNoGestionado.setCedula(rs.getLong(columna));
    }
    columna = columnas.get("APP_NO_GESTIONADO_CORREO");
    if (columna != null) {
      appNoGestionado.setCorreo(rs.getString(columna));
    }
    columna = columnas.get("APP_NO_GESTIONADO_VARIABLE");
    if (columna != null) {
      appNoGestionado.setVariable(rs.getString(columna));
    }
    columna = columnas.get("APP_NO_GESTIONADO_META");
    if (columna != null) {
      appNoGestionado.setMeta(rs.getDouble(columna));
    }
    columna = columnas.get("APP_NO_GESTIONADO_EJECUCION");
    if (columna != null) {
      appNoGestionado.setEjecucion(rs.getLong(columna));
    }
    columna = columnas.get("APP_NO_GESTIONADO_CUMPLIMIENTO");
    if (columna != null) {
      appNoGestionado.setCumplimiento(rs.getDouble(columna));
    }
    columna = columnas.get("APP_NO_GESTIONADO_PESO_ESQUEMA");
    if (columna != null) {
      appNoGestionado.setPesoEsquema(rs.getDouble(columna));
    }
    columna = columnas.get("APP_NO_GESTIONADO_IP_VARIABLE");
    if (columna != null) {
      appNoGestionado.setIpVariable(rs.getDouble(columna));
    }
    return appNoGestionado;
  }

  @Override
  public void insertar(AppNoGestionado appNoGestionado) throws SQLException {
    PreparedStatement sentencia = null;
    try {
      int i = 1;
      String sql = "insert into APP_NO_GESTIONADO(ID_USUARIO,CEDULA,CORREO,VARIABLE,META,EJECUCION,CUMPLIMIENTO,PESO_ESQUEMA,IP_VARIABLE) values (?,?,?,?,?,?,?,?,?)";
      sentencia = cnn.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);
      sentencia.setObject(i++, appNoGestionado.getIdUsuario());
      sentencia.setObject(i++, appNoGestionado.getCedula());
      sentencia.setObject(i++, appNoGestionado.getCorreo());
      sentencia.setObject(i++, appNoGestionado.getVariable());
      sentencia.setObject(i++, appNoGestionado.getMeta());
      sentencia.setObject(i++, appNoGestionado.getEjecucion());
      sentencia.setObject(i++, appNoGestionado.getCumplimiento());
      sentencia.setObject(i++, appNoGestionado.getPesoEsquema());
      sentencia.setObject(i++, appNoGestionado.getIpVariable());

      sentencia.executeUpdate();
      ResultSet rs = sentencia.getGeneratedKeys();
      if (rs.next()) {
        appNoGestionado.setIdNoGestionado(rs.getLong(ID));
      }
    } finally {
      ConexionBD.desconectar(sentencia);
    }
  }

  @Override
  public void editar(AppNoGestionado appNoGestionado) throws SQLException {
    PreparedStatement sentencia = null;
    try {
      int i = 1;
      String sql = "update APP_NO_GESTIONADO set ID_USUARIO=?,CEDULA=?,CORREO=?,VARIABLE=?,META=?,EJECUCION=?,CUMPLIMIENTO=?,PESO_ESQUEMA=?,IP_VARIABLE=? where ID_NO_GESTIONADO=?";
      sentencia = cnn.prepareStatement(sql);
      sentencia.setObject(i++, appNoGestionado.getIdUsuario());
      sentencia.setObject(i++, appNoGestionado.getCedula());
      sentencia.setObject(i++, appNoGestionado.getCorreo());
      sentencia.setObject(i++, appNoGestionado.getVariable());
      sentencia.setObject(i++, appNoGestionado.getMeta());
      sentencia.setObject(i++, appNoGestionado.getEjecucion());
      sentencia.setObject(i++, appNoGestionado.getCumplimiento());
      sentencia.setObject(i++, appNoGestionado.getPesoEsquema());
      sentencia.setObject(i++, appNoGestionado.getIpVariable());
      sentencia.setObject(i++, appNoGestionado.getIdNoGestionado());

      sentencia.executeUpdate();
    } finally {
      ConexionBD.desconectar(sentencia);
    }
  }

  @Override
  public List<AppNoGestionado> consultar() throws SQLException {
    PreparedStatement sentencia = null;
    List<AppNoGestionado> lista = new ArrayList<>();
    try {

      String sql = "select * from APP_NO_GESTIONADO";
      sentencia = cnn.prepareStatement(sql);
      ResultSet rs = sentencia.executeQuery();
      while (rs.next()) {
        lista.add(getAppNoGestionado(rs));
      }
    } finally {
      ConexionBD.desconectar(sentencia);
    }
    return lista;

  }

  @Override
  public AppNoGestionado consultar(Long id) throws SQLException {
    PreparedStatement sentencia = null;
    AppNoGestionado obj = null;
    try {

      String sql = "select * from APP_NO_GESTIONADO where ID_NO_GESTIONADO=?";
      sentencia = cnn.prepareStatement(sql);
      sentencia.setLong(1, id);
      ResultSet rs = sentencia.executeQuery();
      if (rs.next()) {
        obj = getAppNoGestionado(rs);
      }
    } finally {
      ConexionBD.desconectar(sentencia);
    }
    return obj;
  }

}
