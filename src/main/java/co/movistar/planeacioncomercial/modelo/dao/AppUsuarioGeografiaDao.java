package co.movistar.planeacioncomercial.modelo.dao;

import co.movistar.planeacioncomercial.modelo.conexion.ConexionBD;
import co.movistar.planeacioncomercial.modelo.dao.crud.AppUsuarioGeograficaCrud;
import co.movistar.planeacioncomercial.modelo.vo.AppUsuarioGeografia;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * @author Manuel Ernesto Bonilla Muñoz <mebonilla9@gmail.com>
 */
public class AppUsuarioGeografiaDao extends AppUsuarioGeograficaCrud {

  public AppUsuarioGeografiaDao(Connection cnn) {
    super(cnn);
  }

  @Override
  public AppUsuarioGeografia consultar(Long idUsuario) throws SQLException {
    PreparedStatement sentencia = null;
    AppUsuarioGeografia obj = null;
    try {
      String sql = "select * from APP_USUARIO_GEOGRAFIA where ID_USUARIO = ?";
      sentencia = cnn.prepareStatement(sql);
      sentencia.setLong(1, idUsuario);
      ResultSet rs = sentencia.executeQuery();
      if (rs.next()) {
        obj = getAppUsuarioGeografia(rs);
      }
    } finally {
      ConexionBD.desconectar(sentencia);
    }
    return obj;
  }
}
