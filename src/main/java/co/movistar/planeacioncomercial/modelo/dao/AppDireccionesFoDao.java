package co.movistar.planeacioncomercial.modelo.dao;

import co.movistar.planeacioncomercial.modelo.conexion.ConexionBD;
import co.movistar.planeacioncomercial.modelo.dao.crud.AppDireccionesFoCrud;
import co.movistar.planeacioncomercial.modelo.vo.AppDireccionesFo;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class AppDireccionesFoDao extends AppDireccionesFoCrud {

  public AppDireccionesFoDao(Connection cnn) {
    super(cnn);
  }

  public List<AppDireccionesFo> consultarMunicipios(String tipo) throws SQLException {
    PreparedStatement sentencia = null;
    List<AppDireccionesFo> lista = new ArrayList<>();
    try {
      String sql = "select DISTINCT MUNICIPIO from APP_DIRECCIONESFO WHERE TIPO = ?";
      sentencia = cnn.prepareStatement(sql);
      sentencia.setString(1, tipo);
      Map<String, Integer> columnas = new HashMap<>();
      columnas.put("APP_DIRECCIONESFO_MUNICIPIO", 1);
      ResultSet rs = sentencia.executeQuery();
      while (rs.next()) {
        lista.add(getAppDireccionesFo(rs, columnas));
      }
    } finally {
      ConexionBD.desconectar(sentencia);
    }
    return lista;
  }

  public List<AppDireccionesFo> consultarConjunto(String municipio, String tipo) throws SQLException {
    PreparedStatement sentencia = null;
    List<AppDireccionesFo> lista = new ArrayList<>();
    try {
      String sql = "select DISTINCT CONJUNTO from APP_DIRECCIONESFO WHERE MUNICIPIO =? AND TIPO = ?";
      sentencia = cnn.prepareStatement(sql);
      sentencia.setString(1, municipio);
      sentencia.setString(2, tipo);
      Map<String, Integer> columnas = new HashMap<>();
      columnas.put("APP_DIRECCIONESFO_CONJUNTO", 1);
      ResultSet rs = sentencia.executeQuery();
      while (rs.next()) {
        lista.add(getAppDireccionesFo(rs, columnas));
      }
    } finally {
      ConexionBD.desconectar(sentencia);
    }
    return lista;
  }

  public List<AppDireccionesFo> consultarComplemento(String conjunto, String tipo) throws SQLException {
    PreparedStatement sentencia = null;
    List<AppDireccionesFo> lista = new ArrayList<>();
    try {
      String sql = "select DISTINCT COMPLEMENTO from APP_DIRECCIONESFO WHERE CONJUNTO = ? AND TIPO = ?";
      sentencia = cnn.prepareStatement(sql);
      sentencia.setString(1, conjunto);
      sentencia.setString(2, tipo);
      Map<String, Integer> columnas = new HashMap<>();
      columnas.put("APP_DIRECCIONESFO_COMPLEMENTO", 1);
      ResultSet rs = sentencia.executeQuery();
      while (rs.next()) {
        lista.add(getAppDireccionesFo(rs, columnas));
      }
    } finally {
      ConexionBD.desconectar(sentencia);
    }
    return lista;
  }

  public List<AppDireccionesFo> consultarDireccionConjunto(String conjunto, String tipo) throws SQLException {
    PreparedStatement sentencia = null;
    List<AppDireccionesFo> lista = new ArrayList<>();
    try {
      String sql = "select DISTINCT DIRECCION_COMPLETA from APP_DIRECCIONESFO WHERE CONJUNTO = ? AND TIPO = ?";
      sentencia = cnn.prepareStatement(sql);
      sentencia.setString(1, conjunto);
      sentencia.setString(2, tipo);
      Map<String, Integer> columnas = new HashMap<>();
      columnas.put("APP_DIRECCIONESFO_DIRECCION_COMPLETA", 1);
      ResultSet rs = sentencia.executeQuery();
      while (rs.next()) {
        lista.add(getAppDireccionesFo(rs, columnas));
      }
    } finally {
      ConexionBD.desconectar(sentencia);
    }
    return lista;
  }

  public AppDireccionesFo consultarDireccionComplemento(String complemento, String tipo) throws SQLException {
    PreparedStatement sentencia = null;
    AppDireccionesFo direccion = new AppDireccionesFo();
    try {
      String sql = "select DISTINCT TOP(1) DIRECCION_COMPLETA from APP_DIRECCIONESFO WHERE COMPLEMENTO = ? AND TIPO = ?";
      sentencia = cnn.prepareStatement(sql);
      sentencia.setString(1, complemento);
      sentencia.setString(2, tipo);
      Map<String, Integer> columnas = new HashMap<>();
      columnas.put("APP_DIRECCIONESFO_DIRECCION_COMPLETA", 1);
      ResultSet rs = sentencia.executeQuery();
      while (rs.next()) {
        direccion = getAppDireccionesFo(rs, columnas);
      }
    } finally {
      ConexionBD.desconectar(sentencia);
    }
    return direccion;
  }
}
