/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package co.movistar.planeacioncomercial.modelo.dao;

import co.movistar.planeacioncomercial.modelo.conexion.ConexionBD;
import co.movistar.planeacioncomercial.modelo.dao.crud.AppProyeccionCrud;
import co.movistar.planeacioncomercial.modelo.vo.AppProyeccion;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author lord_nightmare
 */
public class AppProyeccionDao extends AppProyeccionCrud {

  public AppProyeccionDao(Connection cnn) {
    super(cnn);
  }

  public List<AppProyeccion> consultarProductosDisponibles(Long idUsuario) throws SQLException {
    PreparedStatement sentencia = null;
    List<AppProyeccion> lista = new ArrayList<>();
    try {
      String sql = "select DISTINCT Producto from APP_PROYECCION where ID_USUARIO=?";
      sentencia = cnn.prepareStatement(sql);
      sentencia.setLong(1, idUsuario);
      ResultSet rs = sentencia.executeQuery();
      Map<String, Integer> columnas = new HashMap<>();
      columnas.put("APP_PROYECCION_Producto", 1);
      while (rs.next()) {
        lista.add(getAppProyeccion(rs, columnas));
      }
    } finally {
      ConexionBD.desconectar(sentencia);
    }
    return lista;
  }

  public List<AppProyeccion> consultarProyeccionProductoUsuario(Long idUsuario, String producto) throws SQLException {
    PreparedStatement sentencia = null;
    List<AppProyeccion> lista = new ArrayList<>();
    try {
      String sql = "select * from APP_PROYECCION WHERE ID_USUARIO = ? AND Producto = ? order by Segmento asc";
      sentencia = cnn.prepareStatement(sql);
      sentencia.setLong(1, idUsuario);
      sentencia.setString(2, producto);
      ResultSet rs = sentencia.executeQuery();
      while (rs.next()) {
        lista.add(getAppProyeccion(rs));
      }
    } finally {
      ConexionBD.desconectar(sentencia);
    }
    return lista;

  }

}
