package co.movistar.planeacioncomercial.modelo.dao;

import co.movistar.planeacioncomercial.modelo.conexion.ConexionBD;
import co.movistar.planeacioncomercial.modelo.dao.crud.AppUsuariosCrud;
import co.movistar.planeacioncomercial.modelo.vo.AppUsuarios;
import co.movistar.planeacioncomercial.negocio.constantes.ECanales;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 * @author Manuel Ernesto Bonilla Muñoz <mebonilla9@gmail.com>
 */
public class AppUsuariosDao extends AppUsuariosCrud {

  public AppUsuariosDao(Connection cnn) {
    super(cnn);
  }

  public AppUsuarios consultarUsuarioAutenticar(String correo, String contrasena) throws SQLException {
    PreparedStatement sentencia = null;
    AppUsuarios obj = null;
    try {
      String sql = "select * from APP_USUARIOS where CORREO = ? AND PASSWORD = ? AND ID_ESTADO = 1";
      sentencia = cnn.prepareStatement(sql);
      sentencia.setString(1, correo);
      sentencia.setString(2, contrasena);
      ResultSet rs = sentencia.executeQuery();
      if (rs.next()) {
        obj = getAppUsuarios(rs);
      }
    } finally {
      ConexionBD.desconectar(sentencia);
    }
    return obj;
  }

  public void editarMacAddress(AppUsuarios appUsuarios) throws SQLException {
    PreparedStatement sentencia = null;
    try {
      int i = 1;
      String sql = "update APP_USUARIOS set MAC=? where ID_USUARIO=?";
      sentencia = cnn.prepareStatement(sql);
      sentencia.setObject(i++, appUsuarios.getMac());
      sentencia.setObject(i++, appUsuarios.getIdUsuario());

      sentencia.executeUpdate();
    } finally {
      ConexionBD.desconectar(sentencia);
    }
  }

  public void bloquearUsuario(AppUsuarios appUsuarios) throws SQLException {
    PreparedStatement sentencia = null;
    try {
      int i = 1;
      String sql = "update APP_USUARIOS set BLOQUEO=? where ID_USUARIO=?";
      sentencia = cnn.prepareStatement(sql);
      sentencia.setObject(i++, appUsuarios.getBloqueo());
      sentencia.setObject(i++, appUsuarios.getIdUsuario());

      sentencia.executeUpdate();
    } finally {
      ConexionBD.desconectar(sentencia);
    }
  }

  public List<AppUsuarios> consultarSubordinados(Long id) throws SQLException {
    PreparedStatement sentencia = null;
    List<AppUsuarios> listaUsuarios = new ArrayList<>();
    try {
      String sql = "SELECT DISTINCT * from APP_USUARIOS where ID_JEFE = ?;";
      sentencia = cnn.prepareStatement(sql);
      sentencia.setLong(1, id);
      ResultSet rs = sentencia.executeQuery();
      while (rs.next()) {
        listaUsuarios.add(getAppUsuarios(rs));
      }
    } finally {
      ConexionBD.desconectar(sentencia);
    }
    return listaUsuarios;
  }

  public List<AppUsuarios> consultarSubordinadosCanal(Long id, ECanales canal) throws SQLException {
    PreparedStatement sentencia = null;
    List<AppUsuarios> listaUsuarios = new ArrayList<>();
    try {
      String sql = "SELECT DISTINCT * from APP_USUARIOS where ID_JEFE = ? AND ID_CANAL = ?";
      sentencia = cnn.prepareStatement(sql);
      sentencia.setLong(1, id);
      sentencia.setLong(2, canal.getId());
      ResultSet rs = sentencia.executeQuery();
      while (rs.next()) {
        listaUsuarios.add(getAppUsuarios(rs));
      }
    } finally {
      ConexionBD.desconectar(sentencia);
    }
    return listaUsuarios;
  }

  public List<AppUsuarios> consultarSubordinadosPuntos(Long id) throws SQLException {
    PreparedStatement sentencia = null;
    List<AppUsuarios> listaUsuarios = new ArrayList<>();
    try {
      String sql = "SELECT DISTINCT apu.* FROM APP_USUARIOS apu INNER JOIN APP_PUNTOS app ON apu.ID_USUARIO = app.Id_usuario WHERE apu.ID_JEFE = ? AND app.Id_usuario IS NOT NULL;";
      sentencia = cnn.prepareStatement(sql);
      sentencia.setLong(1, id);
      ResultSet rs = sentencia.executeQuery();
      while (rs.next()) {
        listaUsuarios.add(getAppUsuarios(rs));
      }
    } finally {
      ConexionBD.desconectar(sentencia);
    }
    return listaUsuarios;
  }

  public AppUsuarios consultar(String correo) throws SQLException {
    PreparedStatement sentencia = null;
    AppUsuarios obj = null;
    try {

      String sql = "select * from APP_USUARIOS where CORREO LIKE ?";
      sentencia = cnn.prepareStatement(sql);
      sentencia.setString(1, correo);
      ResultSet rs = sentencia.executeQuery();
      if (rs.next()) {
        obj = getAppUsuarios(rs);
      }
    } finally {
      ConexionBD.desconectar(sentencia);
    }
    return obj;
  }

  public AppUsuarios consultarPorCedula(Double cedula, Long idJefe, ECanales canal) throws SQLException {
    PreparedStatement sentencia = null;
    AppUsuarios obj = null;
    try {
      String sql = "select * from APP_USUARIOS where CEDULA = ? and ID_CANAL = ? and (ID_JEFE = ? or ID_JEFE IS NULL or ID_JEFE = 0)";
      sentencia = cnn.prepareStatement(sql);
      sentencia.setObject(1, cedula);
      sentencia.setObject(2, canal.getId());
      sentencia.setObject(3, idJefe);
      ResultSet rs = sentencia.executeQuery();
      if (rs.next()) {
        obj = getAppUsuarios(rs);
      }
    } finally {
      ConexionBD.desconectar(sentencia);
    }
    return obj;
  }

  public AppUsuarios consultarExistenciaPorCedula(Double cedula) throws SQLException {
    PreparedStatement sentencia = null;
    AppUsuarios obj = null;
    try {
      String sql = "select * from APP_USUARIOS where CEDULA = ?";
      sentencia = cnn.prepareStatement(sql);
      sentencia.setObject(1, cedula);
      ResultSet rs = sentencia.executeQuery();
      if (rs.next()) {
        obj = getAppUsuarios(rs);
      }
    } finally {
      ConexionBD.desconectar(sentencia);
    }
    return obj;
  }
}
