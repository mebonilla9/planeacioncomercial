package co.movistar.planeacioncomercial.modelo.dao.crud;

import co.movistar.planeacioncomercial.modelo.conexion.ConexionBD;
import co.movistar.planeacioncomercial.modelo.vo.AppOficinasCe;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class AppOficinasCeCrud implements IGenericoDAO<AppOficinasCe> {

  protected final int ID = 1;
  protected Connection cnn;

  public AppOficinasCeCrud(Connection cnn) {
    this.cnn = cnn;
  }

  public static AppOficinasCe getAppOficinasCe(ResultSet rs) throws SQLException {
    AppOficinasCe appOficinasCe = new AppOficinasCe();
    appOficinasCe.setIdOficina(rs.getLong("Id_oficina"));
    appOficinasCe.setNombrePunto(rs.getString("Nombre_oficina"));
    appOficinasCe.setDireccion(rs.getString("Direccion"));
    appOficinasCe.setCiudad(rs.getString("Ciudad"));
    appOficinasCe.setDepartamento(rs.getString("Departamento"));
    appOficinasCe.setRegional(rs.getString("Regional"));
    appOficinasCe.setCcCoordinador(rs.getLong("CC_coordinador"));

    return appOficinasCe;
  }

  public static AppOficinasCe getAppOficinasCe(ResultSet rs, Map<String, Integer> columnas) throws SQLException {
    AppOficinasCe appOficinasCe = new AppOficinasCe();
    Integer columna = columnas.get("APP_OFICINASCE_Id_oficina");
    if (columna != null) {
      appOficinasCe.setIdOficina(rs.getLong(columna));
    }
    columna = columnas.get("APP_OFICINASCE_Nombre_oficina");
    if (columna != null) {
      appOficinasCe.setNombrePunto(rs.getString(columna));
    }
    columna = columnas.get("APP_OFICINASCE_Direccion");
    if (columna != null) {
      appOficinasCe.setDireccion(rs.getString(columna));
    }
    columna = columnas.get("APP_OFICINASCE_Ciudad");
    if (columna != null) {
      appOficinasCe.setCiudad(rs.getString(columna));
    }
    columna = columnas.get("APP_OFICINASCE_Departamento");
    if (columna != null) {
      appOficinasCe.setDepartamento(rs.getString(columna));
    }
    columna = columnas.get("APP_OFICINASCE_Regional");
    if (columna != null) {
      appOficinasCe.setRegional(rs.getString(columna));
    }
    columna = columnas.get("APP_OFICINASCE_CC_coordinador");
    if (columna != null) {
      appOficinasCe.setCcCoordinador(rs.getLong(columna));
    }
    return appOficinasCe;
  }

  @Override
  public void insertar(AppOficinasCe appOficinasCe) throws SQLException {
    PreparedStatement sentencia = null;
    try {
      int i = 1;
      String sql = "insert into APP_OFICINASCE(Nombre_oficina,Direccion,Ciudad,Departamento,Regional,CC_coordinador) values (?,?,?,?,?,?)";
      sentencia = cnn.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);
      sentencia.setObject(i++, appOficinasCe.getNombrePunto());
      sentencia.setObject(i++, appOficinasCe.getDireccion());
      sentencia.setObject(i++, appOficinasCe.getCiudad());
      sentencia.setObject(i++, appOficinasCe.getDepartamento());
      sentencia.setObject(i++, appOficinasCe.getRegional());
      sentencia.setObject(i++, appOficinasCe.getCcCoordinador());

      sentencia.executeUpdate();
      ResultSet rs = sentencia.getGeneratedKeys();
      if (rs.next()) {
        appOficinasCe.setIdOficina(rs.getLong(ID));
      }
    } finally {
      ConexionBD.desconectar(sentencia);
    }
  }

  @Override
  public void editar(AppOficinasCe appOficinasCe) throws SQLException {
    PreparedStatement sentencia = null;
    try {
      int i = 1;
      String sql = "update APP_OFICINASCE set Nombre_oficina=?,Direccion=?,Ciudad=?,Departamento=?,Regional=?,CC_coordinador=? where Id_oficina=?";
      sentencia = cnn.prepareStatement(sql);
      sentencia.setObject(i++, appOficinasCe.getNombrePunto());
      sentencia.setObject(i++, appOficinasCe.getDireccion());
      sentencia.setObject(i++, appOficinasCe.getCiudad());
      sentencia.setObject(i++, appOficinasCe.getDepartamento());
      sentencia.setObject(i++, appOficinasCe.getRegional());
      sentencia.setObject(i++, appOficinasCe.getCcCoordinador());
      sentencia.setObject(i++, appOficinasCe.getIdOficina());

      sentencia.executeUpdate();
    } finally {
      ConexionBD.desconectar(sentencia);
    }
  }

  @Override
  public List<AppOficinasCe> consultar() throws SQLException {
    PreparedStatement sentencia = null;
    List<AppOficinasCe> lista = new ArrayList<>();
    try {

      String sql = "select * from APP_OFICINASCE";
      sentencia = cnn.prepareStatement(sql);
      ResultSet rs = sentencia.executeQuery();
      while (rs.next()) {
        lista.add(getAppOficinasCe(rs));
      }
    } finally {
      ConexionBD.desconectar(sentencia);
    }
    return lista;

  }

  @Override
  public AppOficinasCe consultar(Long id) throws SQLException {
    PreparedStatement sentencia = null;
    AppOficinasCe obj = null;
    try {

      String sql = "select * from APP_OFICINASCE where Id_oficina=?";
      sentencia = cnn.prepareStatement(sql);
      sentencia.setLong(1, id);
      ResultSet rs = sentencia.executeQuery();
      if (rs.next()) {
        obj = getAppOficinasCe(rs);
      }
    } finally {
      ConexionBD.desconectar(sentencia);
    }
    return obj;
  }

}
