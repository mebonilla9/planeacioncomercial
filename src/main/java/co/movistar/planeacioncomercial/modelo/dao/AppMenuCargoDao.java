package co.movistar.planeacioncomercial.modelo.dao;

import co.movistar.planeacioncomercial.modelo.conexion.ConexionBD;
import co.movistar.planeacioncomercial.modelo.dao.crud.AppMenuCargoCrud;
import co.movistar.planeacioncomercial.modelo.vo.AppMenuCargo;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 * @author Manuel Ernesto Bonilla Muñoz <mebonilla9@gmail.com>
 */
public class AppMenuCargoDao extends AppMenuCargoCrud {

  public AppMenuCargoDao(Connection cnn) {
    super(cnn);
  }

  public List<AppMenuCargo> consultarMenuCargoPorUsuario(Long idCargo) throws SQLException {
    PreparedStatement sentencia = null;
    List<AppMenuCargo> lista = new ArrayList<>();
    try {
      String sql = "select * from APP_MENU_CARGO where ID_CARGO=?";
      sentencia = cnn.prepareStatement(sql);
      sentencia.setLong(1, idCargo);
      ResultSet rs = sentencia.executeQuery();
      while (rs.next()) {
        lista.add(getAppMenuCargo(rs));
      }
    } finally {
      ConexionBD.desconectar(sentencia);
    }
    return lista;
  }
}
