package co.movistar.planeacioncomercial.modelo.dao;

import co.movistar.planeacioncomercial.modelo.conexion.ConexionBD;
import co.movistar.planeacioncomercial.modelo.dao.crud.AppAltasCrud;
import co.movistar.planeacioncomercial.modelo.dto.GeneracionAltasDTO;
import co.movistar.planeacioncomercial.modelo.vo.AppAltas;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author Manuel Ernesto Bonilla Muñoz <mebonilla9@gmail.com>
 */
public class AppAltasDao extends AppAltasCrud {

  public AppAltasDao(Connection cnn) {
    super(cnn);
  }

  public List<GeneracionAltasDTO> mostrarInformacionAltas(String producto, String campoPrincipal, Long idUsuario) throws SQLException {
    PreparedStatement sentencia = null;
    List<GeneracionAltasDTO> lista = new ArrayList<>();
    try {
      StringBuilder sql = new StringBuilder();
      sql.append("SELECT ")
              .append(campoPrincipal)
              .append(" , SUM(ALTAS)ALTAS,"
                      + " SUM(PROYECCION)PROYECCION,"
                      + " SUM(PRESUPUESTO)META,"
                      + " NULLIF ( SUM(ALTAS),0)/NULLIF (SUM(PRESUPUESTO),0) CUMPLIMIENTO"
                      + " FROM APP_ALTAS WHERE PRODUCTO = ?");

      sql.append(" AND ID_USUARIO = ?");
      sql.append(" GROUP BY ")
              .append(campoPrincipal);
      sql.append(" ORDER BY ")
              .append(campoPrincipal)
              .append(" ASC ");
      sentencia = cnn.prepareStatement(sql.toString());
      sentencia.setString(1, producto);
      sentencia.setLong(2, idUsuario);
      ResultSet rs = sentencia.executeQuery();
      Map<String, Integer> columnas = new HashMap<>();
      columnas.put("APP_ALTAS_" + campoPrincipal, 1);
      columnas.put("APP_ALTAS_ALTAS", 2);
      columnas.put("APP_ALTAS_PROYECCION", 3);
      while (rs.next()) {
        GeneracionAltasDTO altasDTO = new GeneracionAltasDTO();
        altasDTO.setAppAltas(getAppAltas(rs, columnas));
        altasDTO.setMeta(rs.getLong("META"));
        altasDTO.setCumplimiento(rs.getDouble("CUMPLIMIENTO"));
        lista.add(altasDTO);
      }
    } finally {
      ConexionBD.desconectar(sentencia);
    }
    return lista;
  }

  public List<AppAltas> consultarProductosPorUsuario(Long id) throws SQLException {
    PreparedStatement sentencia = null;
    List<AppAltas> lista = new ArrayList<>();
    try {
      String sql = "SELECT DISTINCT APP_ALTAS.PRODUCTO "
              + "FROM APP_ALTAS INNER JOIN APP_USUARIOS "
              + "ON APP_ALTAS.ID_USUARIO = APP_USUARIOS.ID_USUARIO "
              + "WHERE APP_USUARIOS.ID_USUARIO = ?";
      sentencia = cnn.prepareStatement(sql);
      sentencia.setLong(1, id);
      ResultSet rs = sentencia.executeQuery();
      Map<String, Integer> columnas = new HashMap<>();
      columnas.put("APP_ALTAS_PRODUCTO", 1);
      while (rs.next()) {
        lista.add(getAppAltas(rs, columnas));
      }
    } finally {
      ConexionBD.desconectar(sentencia);
    }
    return lista;
  }
}
