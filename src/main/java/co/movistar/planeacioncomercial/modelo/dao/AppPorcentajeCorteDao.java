package co.movistar.planeacioncomercial.modelo.dao;

import co.movistar.planeacioncomercial.modelo.dao.crud.AppPorcentajeCorteCrud;

import java.sql.Connection;

/**
 * @author Manuel Ernesto Bonilla Muñoz <mebonilla9@gmail.com>
 */
public class AppPorcentajeCorteDao extends AppPorcentajeCorteCrud {

  public AppPorcentajeCorteDao(Connection cnn) {
    super(cnn);
  }
}
