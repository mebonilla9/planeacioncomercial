package co.movistar.planeacioncomercial.modelo.dao.crud;

import co.movistar.planeacioncomercial.modelo.conexion.ConexionBD;
import co.movistar.planeacioncomercial.modelo.vo.AppCabecera;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class AppCabeceraCrud implements IGenericoDAO<AppCabecera> {

  protected final int ID = 1;
  protected Connection cnn;

  public AppCabeceraCrud(Connection cnn) {
    this.cnn = cnn;
  }

  public static AppCabecera getAppCabecera(ResultSet rs) throws SQLException {
    AppCabecera appCabecera = new AppCabecera();
    appCabecera.setIdCabecera(rs.getLong("ID_CABECERA"));
    appCabecera.setIdUsuario(rs.getLong("ID_USUARIO"));
    appCabecera.setCedula(rs.getLong("CEDULA"));
    appCabecera.setCorreo(rs.getString("CORREO"));
    appCabecera.setNombre(rs.getString("NOMBRE"));
    appCabecera.setCargo(rs.getString("CARGO"));
    appCabecera.setRegionalf(rs.getString("REGIONAL_F"));
    appCabecera.setCanalf(rs.getString("CANAL_F"));
    appCabecera.setEsquema(rs.getString("ESQUEMA"));

    return appCabecera;
  }

  public static AppCabecera getAppCabecera(ResultSet rs, Map<String, Integer> columnas) throws SQLException {
    AppCabecera appCabecera = new AppCabecera();
    Integer columna = columnas.get("APP_CABECERA_ID_CABECERA");
    if (columna != null) {
      appCabecera.setIdCabecera(rs.getLong(columna));
    }
    columna = columnas.get("APP_CABECERA_ID_USUARIO");
    if (columna != null) {
      appCabecera.setIdUsuario(rs.getLong(columna));
    }
    columna = columnas.get("APP_CABECERA_CEDULA");
    if (columna != null) {
      appCabecera.setCedula(rs.getLong(columna));
    }
    columna = columnas.get("APP_CABECERA_CORREO");
    if (columna != null) {
      appCabecera.setCorreo(rs.getString(columna));
    }
    columna = columnas.get("APP_CABECERA_NOMBRE");
    if (columna != null) {
      appCabecera.setNombre(rs.getString(columna));
    }
    columna = columnas.get("APP_CABECERA_CARGO");
    if (columna != null) {
      appCabecera.setCargo(rs.getString(columna));
    }
    columna = columnas.get("APP_CABECERA_REGIONAL_F");
    if (columna != null) {
      appCabecera.setRegionalf(rs.getString(columna));
    }
    columna = columnas.get("APP_CABECERA_CANAL_F");
    if (columna != null) {
      appCabecera.setCanalf(rs.getString(columna));
    }
    columna = columnas.get("APP_CABECERA_ESQUEMA");
    if (columna != null) {
      appCabecera.setEsquema(rs.getString(columna));
    }
    return appCabecera;
  }

  @Override
  public void insertar(AppCabecera appCabecera) throws SQLException {
    PreparedStatement sentencia = null;
    try {
      int i = 1;
      String sql = "insert into APP_CABECERA(ID_USUARIO,CEDULA,CORREO,NOMBRE,CARGO,REGIONAL_F,CANAL_F,ESQUEMA) values (?,?,?,?,?,?,?,?)";
      sentencia = cnn.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);
      sentencia.setObject(i++, appCabecera.getIdUsuario());
      sentencia.setObject(i++, appCabecera.getCedula());
      sentencia.setObject(i++, appCabecera.getCorreo());
      sentencia.setObject(i++, appCabecera.getNombre());
      sentencia.setObject(i++, appCabecera.getCargo());
      sentencia.setObject(i++, appCabecera.getRegionalf());
      sentencia.setObject(i++, appCabecera.getCanalf());
      sentencia.setObject(i++, appCabecera.getEsquema());

      sentencia.executeUpdate();
      ResultSet rs = sentencia.getGeneratedKeys();
      if (rs.next()) {
        appCabecera.setIdCabecera(rs.getLong(ID));
      }
    } finally {
      ConexionBD.desconectar(sentencia);
    }
  }

  @Override
  public void editar(AppCabecera appCabecera) throws SQLException {
    PreparedStatement sentencia = null;
    try {
      int i = 1;
      String sql = "update APP_CABECERA set ID_USUARIO=?,CEDULA=?,CORREO=?,NOMBRE=?,CARGO=?,REGIONAL_F=?,CANAL_F=?,ESQUEMA=? where ID_CABECERA=?";
      sentencia = cnn.prepareStatement(sql);
      sentencia.setObject(i++, appCabecera.getIdUsuario());
      sentencia.setObject(i++, appCabecera.getCedula());
      sentencia.setObject(i++, appCabecera.getCorreo());
      sentencia.setObject(i++, appCabecera.getNombre());
      sentencia.setObject(i++, appCabecera.getCargo());
      sentencia.setObject(i++, appCabecera.getRegionalf());
      sentencia.setObject(i++, appCabecera.getCanalf());
      sentencia.setObject(i++, appCabecera.getEsquema());
      sentencia.setObject(i++, appCabecera.getIdCabecera());

      sentencia.executeUpdate();
    } finally {
      ConexionBD.desconectar(sentencia);
    }
  }

  @Override
  public List<AppCabecera> consultar() throws SQLException {
    PreparedStatement sentencia = null;
    List<AppCabecera> lista = new ArrayList<>();
    try {

      String sql = "select * from APP_CABECERA";
      sentencia = cnn.prepareStatement(sql);
      ResultSet rs = sentencia.executeQuery();
      while (rs.next()) {
        lista.add(getAppCabecera(rs));
      }
    } finally {
      ConexionBD.desconectar(sentencia);
    }
    return lista;

  }

  @Override
  public AppCabecera consultar(Long id) throws SQLException {
    PreparedStatement sentencia = null;
    AppCabecera obj = null;
    try {

      String sql = "select * from APP_CABECERA where ID_CABECERA=?";
      sentencia = cnn.prepareStatement(sql);
      sentencia.setLong(1, id);
      ResultSet rs = sentencia.executeQuery();
      if (rs.next()) {
        obj = getAppCabecera(rs);
      }
    } finally {
      ConexionBD.desconectar(sentencia);
    }
    return obj;
  }

}
