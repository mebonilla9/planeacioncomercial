package co.movistar.planeacioncomercial.modelo.dao.crud;

import co.movistar.planeacioncomercial.modelo.conexion.ConexionBD;
import co.movistar.planeacioncomercial.modelo.vo.AppAltas;
import co.movistar.planeacioncomercial.modelo.vo.AppUsuarios;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * @author Manuel Ernesto Bonilla Muñoz <mebonilla9@gmail.com>
 */
public class AppAltasCrud implements IGenericoDAO<AppAltas> {

  protected final int ID = 1;
  protected Connection cnn;

  public AppAltasCrud(Connection cnn) {
    this.cnn = cnn;
  }

  public static AppAltas getAppAltas(ResultSet rs) throws SQLException {
    AppAltas appAltas = new AppAltas();
    appAltas.setIdAltas(rs.getLong("ID_ALTAS"));
    appAltas.setUsuario(new AppUsuarios(rs.getLong("ID_USUARIO")));
    appAltas.setNomUsuario(rs.getString("NOM_USUARIO"));
    appAltas.setProducto(rs.getString("PRODUCTO"));
    appAltas.setRegional(rs.getString("REGIONAL"));
    appAltas.setJefatura(rs.getString("JEFATURA"));
    appAltas.setCanal(rs.getString("CANAL"));
    appAltas.setDepartamento(rs.getString("DEPARTAMENTO"));
    appAltas.setSegmento(rs.getString("SEGMENTO"));
    appAltas.setPresupuesto(rs.getLong("PRESUPUESTO"));
    appAltas.setAltas(rs.getLong("ALTAS"));
    appAltas.setProyeccion(rs.getDouble("PROYECCION"));

    return appAltas;
  }

  public static AppAltas getAppAltas(ResultSet rs, Map<String, Integer> columnas) throws SQLException {
    AppAltas appAltas = new AppAltas();
    Integer columna = columnas.get("APP_ALTAS_ID_ALTAS");
    if (columna != null) {
      appAltas.setIdAltas(rs.getLong(columna));
    }
    columna = columnas.get("APP_ALTAS_ID_USUARIO");
    if (columna != null) {
      appAltas.setUsuario(new AppUsuarios(rs.getLong(columna)));
    }
    columna = columnas.get("APP_ALTAS_NOM_USUARIO");
    if (columna != null) {
      appAltas.setNomUsuario(rs.getString(columna));
    }
    columna = columnas.get("APP_ALTAS_PRODUCTO");
    if (columna != null) {
      appAltas.setProducto(rs.getString(columna));
    }
    columna = columnas.get("APP_ALTAS_REGIONAL");
    if (columna != null) {
      appAltas.setRegional(rs.getString(columna));
    }
    columna = columnas.get("APP_ALTAS_JEFATURA");
    if (columna != null) {
      appAltas.setJefatura(rs.getString(columna));
    }
    columna = columnas.get("APP_ALTAS_CANAL");
    if (columna != null) {
      appAltas.setCanal(rs.getString(columna));
    }
    columna = columnas.get("APP_ALTAS_DEPARTAMENTO");
    if (columna != null) {
      appAltas.setDepartamento(rs.getString(columna));
    }
    columna = columnas.get("APP_ALTAS_SEGMENTO");
    if (columna != null) {
      appAltas.setSegmento(rs.getString(columna));
    }
    columna = columnas.get("APP_ALTAS_PRESUPUESTO");
    if (columna != null) {
      appAltas.setPresupuesto(rs.getLong(columna));
    }
    columna = columnas.get("APP_ALTAS_ALTAS");
    if (columna != null) {
      appAltas.setAltas(rs.getLong(columna));
    }
    columna = columnas.get("APP_ALTAS_PROYECCION");
    if (columna != null) {
      appAltas.setProyeccion(rs.getDouble(columna));
    }
    return appAltas;
  }

  @Override
  public void insertar(AppAltas appAltas) throws SQLException {
    PreparedStatement sentencia = null;
    try {
      int i = 1;
      String sql = "insert into APP_ALTAS(ID_USUARIO,NOM_USUARIO,PRODUCTO,REGIONAL,JEFATURA,CANAL,DEPARTAMENTO,SEGMENTO,PRESUPUESTO,ALTAS,PROYECCION) values (?,?,?,?,?,?,?,?,?,?,?)";
      sentencia = cnn.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);
      sentencia.setObject(i++, appAltas.getUsuario().getIdUsuario());
      sentencia.setObject(i++, appAltas.getNomUsuario());
      sentencia.setObject(i++, appAltas.getProducto());
      sentencia.setObject(i++, appAltas.getRegional());
      sentencia.setObject(i++, appAltas.getJefatura());
      sentencia.setObject(i++, appAltas.getCanal());
      sentencia.setObject(i++, appAltas.getDepartamento());
      sentencia.setObject(i++, appAltas.getSegmento());
      sentencia.setObject(i++, appAltas.getPresupuesto());
      sentencia.setObject(i++, appAltas.getAltas());
      sentencia.setObject(i++, appAltas.getProyeccion());

      sentencia.executeUpdate();
      ResultSet rs = sentencia.getGeneratedKeys();
      if (rs.next()) {
        appAltas.setIdAltas(rs.getLong(ID));
      }
    } finally {
      ConexionBD.desconectar(sentencia);
    }
  }

  @Override
  public void editar(AppAltas appAltas) throws SQLException {
    PreparedStatement sentencia = null;
    try {
      int i = 1;
      String sql = "update APP_ALTAS set ID_USUARIO=?,NOM_USUARIO=?,PRODUCTO=?,REGIONAL=?,JEFATURA=?,CANAL=?,DEPARTAMENTO=?,SEGMENTO=?,PRESUPUESTO=?,ALTAS=?,PROYECCION=? where ID_ALTAS=?";
      sentencia = cnn.prepareStatement(sql);
      sentencia.setObject(i++, appAltas.getUsuario().getIdUsuario());
      sentencia.setObject(i++, appAltas.getNomUsuario());
      sentencia.setObject(i++, appAltas.getProducto());
      sentencia.setObject(i++, appAltas.getRegional());
      sentencia.setObject(i++, appAltas.getJefatura());
      sentencia.setObject(i++, appAltas.getCanal());
      sentencia.setObject(i++, appAltas.getDepartamento());
      sentencia.setObject(i++, appAltas.getSegmento());
      sentencia.setObject(i++, appAltas.getPresupuesto());
      sentencia.setObject(i++, appAltas.getAltas());
      sentencia.setObject(i++, appAltas.getProyeccion());
      sentencia.setObject(i++, appAltas.getIdAltas());

      sentencia.executeUpdate();
    } finally {
      ConexionBD.desconectar(sentencia);
    }
  }

  @Override
  public List<AppAltas> consultar() throws SQLException {
    PreparedStatement sentencia = null;
    List<AppAltas> lista = new ArrayList<>();
    try {
      String sql = "select * from APP_ALTAS";
      sentencia = cnn.prepareStatement(sql);
      ResultSet rs = sentencia.executeQuery();
      while (rs.next()) {
        lista.add(getAppAltas(rs));
      }
    } finally {
      ConexionBD.desconectar(sentencia);
    }
    return lista;
  }

  @Override
  public AppAltas consultar(Long id) throws SQLException {
    PreparedStatement sentencia = null;
    AppAltas obj = null;
    try {

      String sql = "select * from APP_ALTAS where ID_ALTAS=?";
      sentencia = cnn.prepareStatement(sql);
      sentencia.setLong(1, id);
      ResultSet rs = sentencia.executeQuery();
      if (rs.next()) {
        obj = getAppAltas(rs);
      }
    } finally {
      ConexionBD.desconectar(sentencia);
    }
    return obj;
  }

}
