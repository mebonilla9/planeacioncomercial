package co.movistar.planeacioncomercial.modelo.dao.crud;

import co.movistar.planeacioncomercial.modelo.conexion.ConexionBD;
import co.movistar.planeacioncomercial.modelo.vo.AppPuntos;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class AppPuntosCrud implements IGenericoDAO<AppPuntos> {

  protected final int ID = 1;
  protected Connection cnn;

  public AppPuntosCrud(Connection cnn) {
    this.cnn = cnn;
  }

  public static AppPuntos getAppPuntos(ResultSet rs) throws SQLException {
    AppPuntos appPuntos = new AppPuntos();
    appPuntos.setIdPunto(rs.getLong("Id_punto"));
    appPuntos.setNombrePunto(rs.getString("Nombre_punto"));
    appPuntos.setDireccion(rs.getString("Direccion"));
    appPuntos.setCiudad(rs.getString("Ciudad"));
    appPuntos.setDepartamento(rs.getString("Departamento"));
    appPuntos.setRegional(rs.getString("Regional"));
    appPuntos.setIdUsuario(rs.getLong("Id_usuario"));
    appPuntos.setCcCoordinador(rs.getLong("CC_coordinador"));

    return appPuntos;
  }

  public static AppPuntos getAppPuntos(ResultSet rs, Map<String, Integer> columnas) throws SQLException {
    AppPuntos appPuntos = new AppPuntos();
    Integer columna = columnas.get("APP_PUNTOS_Id_punto");
    if (columna != null) {
      appPuntos.setIdPunto(rs.getLong(columna));
    }
    columna = columnas.get("APP_PUNTOS_Nombre_punto");
    if (columna != null) {
      appPuntos.setNombrePunto(rs.getString(columna));
    }
    columna = columnas.get("APP_PUNTOS_Direccion");
    if (columna != null) {
      appPuntos.setDireccion(rs.getString(columna));
    }
    columna = columnas.get("APP_PUNTOS_Ciudad");
    if (columna != null) {
      appPuntos.setCiudad(rs.getString(columna));
    }
    columna = columnas.get("APP_PUNTOS_Departamento");
    if (columna != null) {
      appPuntos.setDepartamento(rs.getString(columna));
    }
    columna = columnas.get("APP_PUNTOS_Regional");
    if (columna != null) {
      appPuntos.setRegional(rs.getString(columna));
    }
    columna = columnas.get("APP_PUNTOS_Id_usuario");
    if (columna != null) {
      appPuntos.setIdUsuario(rs.getLong(columna));
    }
    columna = columnas.get("APP_PUNTOS_CC_coordinador");
    if (columna != null) {
      appPuntos.setCcCoordinador(rs.getLong(columna));
    }
    return appPuntos;
  }

  @Override
  public void insertar(AppPuntos appPuntos) throws SQLException {
    PreparedStatement sentencia = null;
    try {
      int i = 1;
      String sql = "insert into APP_PUNTOS(Nombre_punto,Direccion,Ciudad,Departamento,Regional,Id_usuario,CC_coordinador) values (?,?,?,?,?,?,?)";
      sentencia = cnn.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);
      sentencia.setObject(i++, appPuntos.getNombrePunto());
      sentencia.setObject(i++, appPuntos.getDireccion());
      sentencia.setObject(i++, appPuntos.getCiudad());
      sentencia.setObject(i++, appPuntos.getDepartamento());
      sentencia.setObject(i++, appPuntos.getRegional());
      sentencia.setObject(i++, appPuntos.getIdUsuario());
      sentencia.setObject(i++, appPuntos.getCcCoordinador());

      sentencia.executeUpdate();
      ResultSet rs = sentencia.getGeneratedKeys();
      if (rs.next()) {
        appPuntos.setIdPunto(rs.getLong(ID));
      }
    } finally {
      ConexionBD.desconectar(sentencia);
    }
  }

  @Override
  public void editar(AppPuntos appPuntos) throws SQLException {
    PreparedStatement sentencia = null;
    try {
      int i = 1;
      String sql = "update APP_PUNTOS set Nombre_punto=?,Direccion=?,Ciudad=?,Departamento=?,Regional=?,Id_usuario=?,CC_coordinador=? where Id_punto=?";
      sentencia = cnn.prepareStatement(sql);
      sentencia.setObject(i++, appPuntos.getNombrePunto());
      sentencia.setObject(i++, appPuntos.getDireccion());
      sentencia.setObject(i++, appPuntos.getCiudad());
      sentencia.setObject(i++, appPuntos.getDepartamento());
      sentencia.setObject(i++, appPuntos.getRegional());
      sentencia.setObject(i++, appPuntos.getIdUsuario());
      sentencia.setObject(i++, appPuntos.getCcCoordinador());
      sentencia.setObject(i++, appPuntos.getIdPunto());

      sentencia.executeUpdate();
    } finally {
      ConexionBD.desconectar(sentencia);
    }
  }

  @Override
  public List<AppPuntos> consultar() throws SQLException {
    PreparedStatement sentencia = null;
    List<AppPuntos> lista = new ArrayList<>();
    try {

      String sql = "select * from APP_PUNTOS";
      sentencia = cnn.prepareStatement(sql);
      ResultSet rs = sentencia.executeQuery();
      while (rs.next()) {
        lista.add(getAppPuntos(rs));
      }
    } finally {
      ConexionBD.desconectar(sentencia);
    }
    return lista;

  }

  @Override
  public AppPuntos consultar(Long id) throws SQLException {
    PreparedStatement sentencia = null;
    AppPuntos obj = null;
    try {

      String sql = "select * from APP_PUNTOS where Id_punto=?";
      sentencia = cnn.prepareStatement(sql);
      sentencia.setLong(1, id);
      ResultSet rs = sentencia.executeQuery();
      if (rs.next()) {
        obj = getAppPuntos(rs);
      }
    } finally {
      ConexionBD.desconectar(sentencia);
    }
    return obj;
  }

}
