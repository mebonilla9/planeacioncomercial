package co.movistar.planeacioncomercial.modelo.dao.crud;

import co.movistar.planeacioncomercial.modelo.conexion.ConexionBD;
import co.movistar.planeacioncomercial.modelo.vo.AppConsultaSegmento;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class AppConsultaSegmentoCrud implements IGenericoDAO<AppConsultaSegmento> {

  protected final int ID = 1;
  protected Connection cnn;

  public AppConsultaSegmentoCrud(Connection cnn) {
    this.cnn = cnn;
  }

  public static AppConsultaSegmento getAppConsultaSegmento(ResultSet rs) throws SQLException {
    AppConsultaSegmento appConsultaSegmento = new AppConsultaSegmento();
    appConsultaSegmento.setIdConsulta(rs.getLong("AppConsultaSegmento"));
    appConsultaSegmento.setNit(rs.getString("Nit"));
    appConsultaSegmento.setIdAgente(rs.getLong("Id_agente"));
    appConsultaSegmento.setFecha(rs.getString("Fecha"));
    appConsultaSegmento.setIdUsuario(rs.getLong("Id_usuario"));

    return appConsultaSegmento;
  }

  public static AppConsultaSegmento getAppConsultaSegmento(ResultSet rs, Map<String, Integer> columnas) throws SQLException {
    AppConsultaSegmento appConsultaSegmento = new AppConsultaSegmento();
    Integer columna = columnas.get("APP_CONSULTA_SEGMENTO_Id_consulta");
    if (columna != null) {
      appConsultaSegmento.setIdConsulta(rs.getLong(columna));
    }
    columna = columnas.get("APP_CONSULTA_SEGMENTO_Nit");
    if (columna != null) {
      appConsultaSegmento.setNit(rs.getString(columna));
    }
    columna = columnas.get("APP_CONSULTA_SEGMENTO_Id_agente");
    if (columna != null) {
      appConsultaSegmento.setIdAgente(rs.getLong(columna));
    }
    columna = columnas.get("APP_CONSULTA_SEGMENTO_Fecha");
    if (columna != null) {
      appConsultaSegmento.setFecha(rs.getString(columna));
    }
    columna = columnas.get("APP_CONSULTA_SEGMENTO_Id_usuario");
    if (columna != null) {
      appConsultaSegmento.setIdUsuario(rs.getLong(columna));
    }
    return appConsultaSegmento;
  }

  @Override
  public void insertar(AppConsultaSegmento appConsultaSegmento) throws SQLException {
    PreparedStatement sentencia = null;
    try {
      int i = 1;
      String sql = "insert into APP_CONSULTA_SEGMENTO(Nit,Id_agente,Fecha,Id_usuario) values (?,?,?,?)";
      sentencia = cnn.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);
      sentencia.setObject(i++, appConsultaSegmento.getNit());
      sentencia.setObject(i++, appConsultaSegmento.getIdAgente());
      sentencia.setObject(i++, appConsultaSegmento.getFecha());
      sentencia.setObject(i++, appConsultaSegmento.getIdUsuario());

      sentencia.executeUpdate();
      ResultSet rs = sentencia.getGeneratedKeys();
      if (rs.next()) {
        appConsultaSegmento.setIdConsulta(rs.getLong(ID));
      }
    } finally {
      ConexionBD.desconectar(sentencia);
    }
  }

  @Override
  public void editar(AppConsultaSegmento appConsultaSegmento) throws SQLException {
    PreparedStatement sentencia = null;
    try {
      int i = 1;
      String sql = "update APP_CONSULTA_SEGMENTO set Nit=?,Id_agente=?,Fecha=?,Id_usuario=? where Id_consulta=?";
      sentencia = cnn.prepareStatement(sql);
      sentencia.setObject(i++, appConsultaSegmento.getNit());
      sentencia.setObject(i++, appConsultaSegmento.getIdAgente());
      sentencia.setObject(i++, appConsultaSegmento.getFecha());
      sentencia.setObject(i++, appConsultaSegmento.getIdUsuario());
      sentencia.setObject(i++, appConsultaSegmento.getIdConsulta());

      sentencia.executeUpdate();
    } finally {
      ConexionBD.desconectar(sentencia);
    }
  }

  @Override
  public List<AppConsultaSegmento> consultar() throws SQLException {
    PreparedStatement sentencia = null;
    List<AppConsultaSegmento> lista = new ArrayList<>();
    try {

      String sql = "select * from APP_CONSULTA_SEGMENTO";
      sentencia = cnn.prepareStatement(sql);
      ResultSet rs = sentencia.executeQuery();
      while (rs.next()) {
        lista.add(getAppConsultaSegmento(rs));
      }
    } finally {
      ConexionBD.desconectar(sentencia);
    }
    return lista;

  }

  @Override
  public AppConsultaSegmento consultar(Long id) throws SQLException {
    PreparedStatement sentencia = null;
    AppConsultaSegmento obj = null;
    try {

      String sql = "select * from APP_CONSULTA_SEGMENTO where Id_consulta=?";
      sentencia = cnn.prepareStatement(sql);
      sentencia.setLong(1, id);
      ResultSet rs = sentencia.executeQuery();
      if (rs.next()) {
        obj = getAppConsultaSegmento(rs);
      }
    } finally {
      ConexionBD.desconectar(sentencia);
    }
    return obj;
  }

}
