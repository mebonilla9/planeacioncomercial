package co.movistar.planeacioncomercial.modelo.dao.crud;

import co.movistar.planeacioncomercial.modelo.conexion.ConexionBD;
import co.movistar.planeacioncomercial.modelo.vo.AppEstados;
import co.movistar.planeacioncomercial.modelo.vo.AppJefatura;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * @author Manuel Ernesto Bonilla Muñoz <mebonilla9@gmail.com>
 */
public class AppJefaturaCrud implements IGenericoDAO<AppJefatura> {

  protected final int ID = 1;
  protected Connection cnn;

  public AppJefaturaCrud(Connection cnn) {
    this.cnn = cnn;
  }

  public static AppJefatura getAppJefatura(ResultSet rs) throws SQLException {
    AppJefatura appJefatura = new AppJefatura();
    appJefatura.setIdJefatura(rs.getLong("ID_JEFATURA"));
    appJefatura.setNombreJefatura(rs.getString("NOMBRE_JEFATURA"));
    appJefatura.setEstado(new AppEstados(rs.getLong("ID_ESTADO")));

    return appJefatura;
  }

  public static AppJefatura getAppJefatura(ResultSet rs, Map<String, Integer> columnas) throws SQLException {
    AppJefatura appJefatura = new AppJefatura();
    Integer columna = columnas.get("APP_JEFATURA_ID_JEFATURA");
    if (columna != null) {
      appJefatura.setIdJefatura(rs.getLong(columna));
    }
    columna = columnas.get("APP_JEFATURA_NOMBRE_JEFATURA");
    if (columna != null) {
      appJefatura.setNombreJefatura(rs.getString(columna));
    }
    columna = columnas.get("APP_JEFATURA_ID_ESTADO");
    if (columna != null) {
      appJefatura.setEstado(new AppEstados(rs.getLong(columna)));
    }
    return appJefatura;
  }

  @Override
  public void insertar(AppJefatura appJefatura) throws SQLException {
    PreparedStatement sentencia = null;
    try {
      int i = 1;
      String sql = "insert into APP_JEFATURA(NOMBRE_JEFATURA,ID_ESTADO) values (?,?)";
      sentencia = cnn.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);
      sentencia.setObject(i++, appJefatura.getNombreJefatura());
      sentencia.setObject(i++, appJefatura.getEstado().getIdEstado());

      sentencia.executeUpdate();
      ResultSet rs = sentencia.getGeneratedKeys();
      if (rs.next()) {
        appJefatura.setIdJefatura(rs.getLong(ID));
      }
    } finally {
      ConexionBD.desconectar(sentencia);
    }
  }

  @Override
  public void editar(AppJefatura appJefatura) throws SQLException {
    PreparedStatement sentencia = null;
    try {
      int i = 1;
      String sql = "update APP_JEFATURA set NOMBRE_JEFATURA=?,ID_ESTADO=? where ID_JEFATURA=?";
      sentencia = cnn.prepareStatement(sql);
      sentencia.setObject(i++, appJefatura.getNombreJefatura());
      sentencia.setObject(i++, appJefatura.getEstado().getIdEstado());
      sentencia.setObject(i++, appJefatura.getIdJefatura());

      sentencia.executeUpdate();
    } finally {
      ConexionBD.desconectar(sentencia);
    }
  }

  @Override
  public List<AppJefatura> consultar() throws SQLException {
    PreparedStatement sentencia = null;
    List<AppJefatura> lista = new ArrayList<>();
    try {

      String sql = "select * from APP_JEFATURA";
      sentencia = cnn.prepareStatement(sql);
      ResultSet rs = sentencia.executeQuery();
      while (rs.next()) {
        lista.add(getAppJefatura(rs));
      }
    } finally {
      ConexionBD.desconectar(sentencia);
    }
    return lista;

  }

  @Override
  public AppJefatura consultar(Long id) throws SQLException {
    PreparedStatement sentencia = null;
    AppJefatura obj = null;
    try {

      String sql = "select * from APP_JEFATURA where ID_JEFATURA=?";
      sentencia = cnn.prepareStatement(sql);
      sentencia.setLong(1, id);
      ResultSet rs = sentencia.executeQuery();
      if (rs.next()) {
        obj = getAppJefatura(rs);
      }
    } finally {
      ConexionBD.desconectar(sentencia);
    }
    return obj;
  }

}
