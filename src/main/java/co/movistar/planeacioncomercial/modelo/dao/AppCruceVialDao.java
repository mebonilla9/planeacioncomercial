package co.movistar.planeacioncomercial.modelo.dao;

import co.movistar.planeacioncomercial.modelo.dao.crud.AppCruceVialCrud;

import java.sql.Connection;

public class AppCruceVialDao extends AppCruceVialCrud {

  public AppCruceVialDao(Connection cnn) {
    super(cnn);
  }
}
