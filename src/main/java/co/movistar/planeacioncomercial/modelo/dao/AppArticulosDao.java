package co.movistar.planeacioncomercial.modelo.dao;

import co.movistar.planeacioncomercial.modelo.conexion.ConexionBD;
import co.movistar.planeacioncomercial.modelo.dao.crud.AppArticulosCrud;
import co.movistar.planeacioncomercial.modelo.vo.AppArticulos;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class AppArticulosDao extends AppArticulosCrud {

  public AppArticulosDao(Connection cnn) {
    super(cnn);
  }

  public List<AppArticulos> consultarPorPunto(Long id) throws SQLException {
    PreparedStatement sentencia = null;
    List<AppArticulos> lista = new ArrayList<>();
    try {

      String sql = "select * from APP_ARTICULOS where Id_punto=?";
      sentencia = cnn.prepareStatement(sql);
      sentencia.setLong(1, id);
      ResultSet rs = sentencia.executeQuery();
      while (rs.next()) {
        lista.add(getAppArticulos(rs));
      }
    } finally {
      ConexionBD.desconectar(sentencia);
    }
    return lista;
  }
}
