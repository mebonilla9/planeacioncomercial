package co.movistar.planeacioncomercial.modelo.dao.crud;

import co.movistar.planeacioncomercial.modelo.conexion.ConexionBD;
import co.movistar.planeacioncomercial.modelo.vo.*;
import co.movistar.planeacioncomercial.negocio.util.DateUtil;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class AppVentasCeCrud implements IGenericoDAO<AppVentasCe> {

  protected final int ID = 1;
  protected Connection cnn;

  public AppVentasCeCrud(Connection cnn) {
    this.cnn = cnn;
  }

  public static AppVentasCe getAppVentasCe(ResultSet rs) throws SQLException {
    AppVentasCe appVentasCe = new AppVentasCe();
    appVentasCe.setIdVentasCe(rs.getLong("Id_Ventasce"));
    appVentasCe.setIdUsuarioModifica(rs.getLong("Id_Usuariomodifica"));
    appVentasCe.setOficina(new AppOficinasCe(rs.getLong("Id_Oficina")));
    appVentasCe.setIdSupervisor(rs.getLong("Id_Supervisor"));
    appVentasCe.setFecha(rs.getTimestamp("Fecha"));
    appVentasCe.setIdUsuario(rs.getLong("Id_Usuario"));
    appVentasCe.setNovedad(new AppNovedad(rs.getLong("Id_Novedad")));
    appVentasCe.setRol(new AppRol(rs.getLong("Id_Rol")));
    return appVentasCe;
  }

  public static AppVentasCe getAppVentasCe(ResultSet rs, Map<String, Integer> columnas) throws SQLException {
    AppVentasCe appVentasCe = new AppVentasCe();
    Integer columna = columnas.get("APP_VENTAS_CE_Id_Ventasce");
    if (columna != null) {
      appVentasCe.setIdVentasCe(rs.getLong(columna));
    }
    columna = columnas.get("APP_VENTAS_CE_Id_Usuariomodifica");
    if (columna != null) {
      appVentasCe.setIdUsuarioModifica(rs.getLong(columna));
    }
    columna = columnas.get("APP_VENTAS_CE_Id_Oficina");
    if (columna != null) {
      appVentasCe.setOficina(new AppOficinasCe(rs.getLong(columna)));
    }
    columna = columnas.get("APP_VENTAS_CE_Id_Supervisor");
    if (columna != null) {
      appVentasCe.setIdSupervisor(rs.getLong(columna));
    }
    columna = columnas.get("APP_VENTAS_CE_Fecha");
    if (columna != null) {
      appVentasCe.setFecha(rs.getTimestamp(columna));
    }
    columna = columnas.get("APP_VENTAS_CE_Id_Usuario");
    if (columna != null) {
      appVentasCe.setIdUsuario(rs.getLong(columna));
    }
    columna = columnas.get("APP_VENTAS_CE_Id_Novedad");
    if (columna != null) {
      appVentasCe.setRol(new AppRol(rs.getLong(columna)));
    }
    return appVentasCe;
  }

  @Override
  public void insertar(AppVentasCe appVentasCe) throws SQLException {
    PreparedStatement sentencia = null;
    try {
      int i = 1;
      String sql = "insert into APP_VENTAS_CE(Id_Usuariomodifica,Id_Oficina,Id_Supervisor,Fecha,Id_Usuario,Id_Novedad) values (?,?,?,?,?,?)";
      sentencia = cnn.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);
      sentencia.setObject(i++, appVentasCe.getIdUsuarioModifica());
      sentencia.setObject(i++, appVentasCe.getOficina().getIdOficina());
      sentencia.setObject(i++, appVentasCe.getIdSupervisor());
      sentencia.setObject(i++, DateUtil.parseTimestamp(appVentasCe.getFecha()));
      sentencia.setObject(i++, appVentasCe.getIdUsuario());
      sentencia.setObject(i++, appVentasCe.getNovedad().getIdNovedad());

      sentencia.executeUpdate();
      ResultSet rs = sentencia.getGeneratedKeys();
      if (rs.next()) {
        appVentasCe.setIdVentasCe(rs.getLong(ID));
      }
    } finally {
      ConexionBD.desconectar(sentencia);
    }
  }

  @Override
  public void editar(AppVentasCe appVentasCe) throws SQLException {
    PreparedStatement sentencia = null;
    try {
      int i = 1;
      String sql = "update APP_VENTAS_CE set Id_Usuariomodifica=?,Id_Oficina=?,Id_Supervisor=?,Fecha=?,Id_Usuario=?,IdNovedad=? where Id_Ventasce=?";
      sentencia = cnn.prepareStatement(sql);
      sentencia.setObject(i++, appVentasCe.getIdUsuarioModifica());
      sentencia.setObject(i++, appVentasCe.getOficina().getIdOficina());
      sentencia.setObject(i++, appVentasCe.getIdSupervisor());
      sentencia.setObject(i++, DateUtil.parseTimestamp(appVentasCe.getFecha()));
      sentencia.setObject(i++, appVentasCe.getIdUsuario());
      sentencia.setObject(i++, appVentasCe.getNovedad().getIdNovedad());
      sentencia.setObject(i++, appVentasCe.getIdVentasCe());

      sentencia.executeUpdate();
    } finally {
      ConexionBD.desconectar(sentencia);
    }
  }

  @Override
  public List<AppVentasCe> consultar() throws SQLException {
    PreparedStatement sentencia = null;
    List<AppVentasCe> lista = new ArrayList<>();
    try {

      String sql = "select * from APP_VENTAS_CE";
      sentencia = cnn.prepareStatement(sql);
      ResultSet rs = sentencia.executeQuery();
      while (rs.next()) {
        lista.add(getAppVentasCe(rs));
      }
    } finally {
      ConexionBD.desconectar(sentencia);
    }
    return lista;

  }

  @Override
  public AppVentasCe consultar(Long id) throws SQLException {
    PreparedStatement sentencia = null;
    AppVentasCe obj = null;
    try {

      String sql = "select * from APP_VENTAS_CE where Id_Ventasce=?";
      sentencia = cnn.prepareStatement(sql);
      sentencia.setLong(1, id);
      ResultSet rs = sentencia.executeQuery();
      if (rs.next()) {
        obj = getAppVentasCe(rs);
      }
    } finally {
      ConexionBD.desconectar(sentencia);
    }
    return obj;
  }

}
