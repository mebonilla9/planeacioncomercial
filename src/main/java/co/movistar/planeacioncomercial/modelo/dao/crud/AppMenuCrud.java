package co.movistar.planeacioncomercial.modelo.dao.crud;

import co.movistar.planeacioncomercial.modelo.conexion.ConexionBD;
import co.movistar.planeacioncomercial.modelo.vo.AppMenu;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * @author Manuel Ernesto Bonilla Muñoz <mebonilla9@gmail.com>
 */
public class AppMenuCrud implements IGenericoDAO<AppMenu> {

  protected final int ID = 1;
  protected Connection cnn;

  public AppMenuCrud(Connection cnn) {
    this.cnn = cnn;
  }

  public static AppMenu getAppMenu(ResultSet rs) throws SQLException {
    AppMenu appMenu = new AppMenu();
    appMenu.setIdMenu(rs.getLong("ID_MENU"));
    appMenu.setNombreMenu(rs.getString("NOMBRE_MENU"));
    appMenu.setEstado(rs.getString("ESTADO"));

    return appMenu;
  }

  public static AppMenu getAppMenu(ResultSet rs, Map<String, Integer> columnas) throws SQLException {
    AppMenu appMenu = new AppMenu();
    Integer columna = columnas.get("APP_MENU_ID_MENU");
    if (columna != null) {
      appMenu.setIdMenu(rs.getLong(columna));
    }
    columna = columnas.get("APP_MENU_NOMBRE_MENU");
    if (columna != null) {
      appMenu.setNombreMenu(rs.getString(columna));
    }

    columna = columnas.get("APP_MENU_ESTADO");
    if (columna != null) {
      appMenu.setEstado(rs.getString(columna));
    }
    columna = columnas.get("APP_MENU_MENU_PADRE");
    if (columna != null) {
      appMenu.setMenuPadre(rs.getLong(columna));
    }
    return appMenu;
  }

  @Override
  public void insertar(AppMenu appMenu) throws SQLException {
    PreparedStatement sentencia = null;
    try {
      int i = 1;
      String sql = "insert into APP_MENU(NOMBRE_MENU,ESTADO) values (?,?)";
      sentencia = cnn.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);
      sentencia.setObject(i++, appMenu.getNombreMenu());
      sentencia.setObject(i++, appMenu.getEstado());

      sentencia.executeUpdate();
      ResultSet rs = sentencia.getGeneratedKeys();
      if (rs.next()) {
        appMenu.setIdMenu(rs.getLong(ID));
      }
    } finally {
      ConexionBD.desconectar(sentencia);
    }
  }

  @Override
  public void editar(AppMenu appMenu) throws SQLException {
    PreparedStatement sentencia = null;
    try {
      int i = 1;
      String sql = "update APP_MENU set NOMBRE_MENU=?,ESTADO=? where ID_MENU=?";
      sentencia = cnn.prepareStatement(sql);
      sentencia.setObject(i++, appMenu.getNombreMenu());
      sentencia.setObject(i++, appMenu.getEstado());
      sentencia.setObject(i++, appMenu.getIdMenu());

      sentencia.executeUpdate();
    } finally {
      ConexionBD.desconectar(sentencia);
    }
  }

  @Override
  public List<AppMenu> consultar() throws SQLException {
    PreparedStatement sentencia = null;
    List<AppMenu> lista = new ArrayList<>();
    try {

      String sql = "select * from APP_MENU";
      sentencia = cnn.prepareStatement(sql);
      ResultSet rs = sentencia.executeQuery();
      while (rs.next()) {
        lista.add(getAppMenu(rs));
      }
    } finally {
      ConexionBD.desconectar(sentencia);
    }
    return lista;

  }

  @Override
  public AppMenu consultar(Long id) throws SQLException {
    PreparedStatement sentencia = null;
    AppMenu obj = null;
    try {

      String sql = "select * from APP_MENU where ID_MENU=?";
      sentencia = cnn.prepareStatement(sql);
      sentencia.setLong(1, id);
      ResultSet rs = sentencia.executeQuery();
      if (rs.next()) {
        obj = getAppMenu(rs);
      }
    } finally {
      ConexionBD.desconectar(sentencia);
    }
    return obj;
  }

}
