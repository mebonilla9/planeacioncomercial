package co.movistar.planeacioncomercial.modelo.dao;

import co.movistar.planeacioncomercial.modelo.conexion.ConexionBD;
import co.movistar.planeacioncomercial.modelo.dao.crud.AppLiquidacionHistCrud;
import co.movistar.planeacioncomercial.modelo.vo.AppLiquidacionHist;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class AppLiquidacionHistDao extends AppLiquidacionHistCrud {

  public AppLiquidacionHistDao(Connection cnn) {
    super(cnn);
  }

  public AppLiquidacionHist obtenerResumenLiquidacion(Long idUsuario, String periodo) throws SQLException {
    PreparedStatement sentencia = null;
    AppLiquidacionHist obj = new AppLiquidacionHist();
    try {
      int i = 1;
      String sql = "SELECT "
              + "  cedula, "
              + "  SUM(IP_Ejecucion_sin_Acel) / COUNT(IP_Ejecucion_sin_Acel)             IP_Ejecucion_sin_Acel, "
              + "  SUM(Total_Acel) / COUNT(Total_Acel)                                   Total_Acel, "
              + "  SUM(Variable_100) / COUNT(Variable_100)                               Variable_100, "
              + "  CASE WHEN Garantizado_Nvos_Canales IS NULL "
              + "    THEN 0 "
              + "  ELSE "
              + "    SUM(Garantizado_Nvos_Canales) / COUNT(Garantizado_Nvos_Canales) END Garantizado_Nvos_Canales, "
              + "  SUM(Valor_Mes) / COUNT(Valor_Mes)                                     Valor_Mes, "
              + "  SUM(Valor_a_Pagar) / COUNT(Valor_a_Pagar)                             Valor_a_Pagar, "
              + "  SUM(IP_Liquidar) / COUNT(IP_Liquidar)                                 IP_Liquidar, "
              + "  SUM(Curva_Acelerador) / COUNT(Curva_Acelerador)                       Curva_Acelerador, "
              + "  CASE WHEN Garantizado IS NULL "
              + "    THEN 0 "
              + "  ELSE "
              + "    SUM(Garantizado) / COUNT(Garantizado) END                           Garantizado, "
              + "  CASE WHEN Ajuste_Meses_Anteriores IS NULL "
              + "    THEN 0 "
              + "  ELSE "
              + "    SUM(Ajuste_Meses_Anteriores) / COUNT(Ajuste_Meses_Anteriores) END   Ajuste_Meses_Anteriores "
              + "FROM APP_LIQUIDACION_HIST "
              + "  WHERE Periodo LIKE ? AND Id_Usuario = ? "
              + "GROUP BY "
              + "  cedula, IP_Ejecucion_sin_Acel, Total_Acel, Variable_100, Garantizado_Nvos_Canales, "
              + "  Valor_Mes, Valor_a_Pagar, IP_Liquidar, Curva_Acelerador, Garantizado, Ajuste_Meses_Anteriores;";
      sentencia = cnn.prepareStatement(sql);
      sentencia.setString(1, periodo);
      sentencia.setLong(2, idUsuario);
      ResultSet rs = sentencia.executeQuery();
      Map<String, Integer> columnas = new HashMap<>();
      columnas.put("APP_LIQUIDACION_HIST_Cedula", i++);
      columnas.put("APP_LIQUIDACION_HIST_IP_Ejecucion_sin_Acel", i++);
      columnas.put("APP_LIQUIDACION_HIST_Total_Acel", i++);
      columnas.put("APP_LIQUIDACION_HIST_Variable_100", i++);
      columnas.put("APP_LIQUIDACION_HIST_Garantizado_Nvos_Canales", i++);
      columnas.put("APP_LIQUIDACION_HIST_Valor_Mes", i++);
      columnas.put("APP_LIQUIDACION_HIST_Valor_a_Pagar", i++);
      columnas.put("APP_LIQUIDACION_HIST_IP_Liquidar", i++);
      columnas.put("APP_LIQUIDACION_HIST_Curva_Acelerador", i++);
      columnas.put("APP_LIQUIDACION_HIST_Garantizado", i++);
      columnas.put("APP_LIQUIDACION_HIST_Ajuste_Meses_Anteriores", i++);
      if (rs.next()) {
        obj = getAppLiquidacionHist(rs, columnas);
      }
    } finally {
      ConexionBD.desconectar(sentencia);
    }
    return obj;
  }

  public List<AppLiquidacionHist> consultarPeriodos(Long idUsuario) throws SQLException {
    PreparedStatement sentencia = null;
    List<AppLiquidacionHist> lista = new ArrayList<>();
    try {
      String sql = "SELECT DISTINCT Periodo FROM APP_LIQUIDACION_HIST WHERE Id_Usuario = ?";
      sentencia = cnn.prepareStatement(sql);
      sentencia.setLong(1, idUsuario);
      ResultSet rs = sentencia.executeQuery();
      Map<String, Integer> columnas = new HashMap<>();
      columnas.put("APP_LIQUIDACION_HIST_Periodo", 1);
      while (rs.next()) {
        lista.add(getAppLiquidacionHist(rs, columnas));
      }
    } finally {
      ConexionBD.desconectar(sentencia);
    }
    return lista;
  }

  public List<AppLiquidacionHist> consultarCategoria(Long idUsuario) throws SQLException {
    PreparedStatement sentencia = null;
    List<AppLiquidacionHist> lista = new ArrayList<>();
    try {
      String sql = "SELECT DISTINCT Categoria FROM APP_LIQUIDACION_HIST WHERE Id_Usuario = ?";
      sentencia = cnn.prepareStatement(sql);
      sentencia.setLong(1, idUsuario);
      ResultSet rs = sentencia.executeQuery();
      Map<String, Integer> columnas = new HashMap<>();
      columnas.put("APP_LIQUIDACION_HIST_Categoria", 1);
      while (rs.next()) {
        lista.add(getAppLiquidacionHist(rs, columnas));
      }
    } finally {
      ConexionBD.desconectar(sentencia);
    }
    return lista;
  }

  public List<AppLiquidacionHist> consultarDetalleLiquidacion(Long idUsuario, String categoria, String periodo) throws SQLException {
    PreparedStatement sentencia = null;
    List<AppLiquidacionHist> lista = new ArrayList<>();
    try {
      int i = 1;
      int j = 1;
      String sql = "SELECT "
              + "Cedula, Categoria, Variable, Meta, Ejecutado, Cumplimiento, Peso_Variable, Peso_Cat, Com_IP "
              + "FROM APP_LIQUIDACION_HIST "
              + "WHERE Id_Usuario = ? AND Categoria = ? AND Periodo = ?;";
      sentencia = cnn.prepareStatement(sql);
      sentencia.setLong(i++, idUsuario);
      sentencia.setString(i++, categoria);
      sentencia.setString(i++, periodo);
      ResultSet rs = sentencia.executeQuery();
      Map<String, Integer> columnas = new HashMap<>();
      columnas.put("APP_LIQUIDACION_HIST_Cedula", j++);
      columnas.put("APP_LIQUIDACION_HIST_Categoria", j++);
      columnas.put("APP_LIQUIDACION_HIST_Variable", j++);
      columnas.put("APP_LIQUIDACION_HIST_Meta", j++);
      columnas.put("APP_LIQUIDACION_HIST_Ejecutado", j++);
      columnas.put("APP_LIQUIDACION_HIST_Cumplimiento", j++);
      columnas.put("APP_LIQUIDACION_HIST_Peso_Variable", j++);
      columnas.put("APP_LIQUIDACION_HIST_Peso_Cat", j++);
      columnas.put("APP_LIQUIDACION_HIST_Com_IP", j++);
      while (rs.next()) {
        lista.add(getAppLiquidacionHist(rs, columnas));
      }
    } finally {
      ConexionBD.desconectar(sentencia);
    }
    return lista;
  }

}
