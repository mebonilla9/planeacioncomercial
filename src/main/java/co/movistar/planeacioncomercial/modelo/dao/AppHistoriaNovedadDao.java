package co.movistar.planeacioncomercial.modelo.dao;

import co.movistar.planeacioncomercial.modelo.conexion.ConexionBD;
import co.movistar.planeacioncomercial.modelo.dao.crud.AppHistoriaNovedadCrud;
import co.movistar.planeacioncomercial.modelo.vo.AppHistoriaNovedad;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Date;

public class AppHistoriaNovedadDao extends AppHistoriaNovedadCrud {

  public AppHistoriaNovedadDao(Connection cnn) {
    super(cnn);
  }

  public AppHistoriaNovedad consultarUltimaNovedad(Long idUsuario) throws SQLException {
    PreparedStatement sentencia = null;
    AppHistoriaNovedad obj = null;
    try {

      String sql = "select TOP 1 * from APP_HISTORIANOVEDAD where Id_usuarionovedad=? order by Id_historia desc";
      sentencia = cnn.prepareStatement(sql);
      sentencia.setLong(1, idUsuario);
      ResultSet rs = sentencia.executeQuery();
      if (rs.next()) {
        obj = getAppHistoriaNovedadCrud(rs);
      }
    } finally {
      ConexionBD.desconectar(sentencia);
    }
    return obj;
  }

  public AppHistoriaNovedad consultarUltimaIncapacidad(Long idUsuario, Date fechaIni, Date fechaFin) throws SQLException {
    PreparedStatement sentencia = null;
    AppHistoriaNovedad obj = null;
    int i = 1;
    try {

      String sql = "select TOP 1 * from APP_HISTORIANOVEDAD where Id_novedad=3 and Id_usuarionovedad=? and Fecha_final between ? and ? and Fecha_inicio between ? and ?";
      sentencia = cnn.prepareStatement(sql);
      sentencia.setLong(i++, idUsuario);
      sentencia.setObject(i++, fechaIni);
      sentencia.setObject(i++, fechaFin);
      sentencia.setObject(i++, fechaIni);
      sentencia.setObject(i++, fechaFin);
      ResultSet rs = sentencia.executeQuery();
      if (rs.next()) {
        obj = getAppHistoriaNovedadCrud(rs);
      }
    } finally {
      ConexionBD.desconectar(sentencia);
    }
    return obj;
  }
}
