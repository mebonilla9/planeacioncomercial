/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package co.movistar.planeacioncomercial.modelo.dao.crud;

import java.sql.SQLException;
import java.util.List;

/**
 * @author Manuel Ernesto Bonilla Muñoz <mebonilla9@gmail.com>
 */
public interface IGenericoDAO<T> {

  void insertar(T entidad) throws SQLException;

  void editar(T entidad) throws SQLException;

  List<T> consultar() throws SQLException;

  T consultar(Long id) throws SQLException;
}
