package co.movistar.planeacioncomercial.modelo.dao;

import co.movistar.planeacioncomercial.modelo.conexion.ConexionBD;
import co.movistar.planeacioncomercial.modelo.dao.crud.AppMetaUsuarioCrud;
import co.movistar.planeacioncomercial.modelo.vo.AppMetaUsuario;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class AppMetaUsuarioDao extends AppMetaUsuarioCrud {

  public AppMetaUsuarioDao(Connection cnn) {
    super(cnn);
  }

  public List<AppMetaUsuario> consultarPorMetasUsuario(Long idUsuario, Long idCoordinador, Long idPunto) throws SQLException {
    PreparedStatement sentencia = null;
    List<AppMetaUsuario> lista = new ArrayList<>();
    try {
      String sql = "select * from APP_META_USUARIO where Id_Usuario=? and Id_Coordinador=? and Id_Punto=? and Periodo=(SELECT DISTINCT TOP 1  Periodo from APP_META_USUARIO ORDER BY Periodo DESC)";
      sentencia = cnn.prepareStatement(sql);
      sentencia.setLong(1, idUsuario);
      sentencia.setLong(2, idCoordinador);
      sentencia.setLong(3, idPunto);
      ResultSet rs = sentencia.executeQuery();
      while (rs.next()) {
        lista.add(getAppMetaUsuario(rs));
      }
    } finally {
      ConexionBD.desconectar(sentencia);
    }
    return lista;
  }

  public AppMetaUsuario consultarFechaMaxima() throws SQLException {
    PreparedStatement sentencia = null;
    AppMetaUsuario obj = null;
    try {
      String sql = "SELECT DISTINCT TOP 1 Periodo from APP_META_USUARIO ORDER BY Periodo DESC";
      sentencia = cnn.prepareStatement(sql);
      ResultSet rs = sentencia.executeQuery();
      Map<String, Integer> columnas = new HashMap<>();
      columnas.put("APP_META_USUARIO_Periodo", 1);
      if (rs.next()) {
        obj = getAppMetaUsuario(rs, columnas);
      }
    } finally {
      ConexionBD.desconectar(sentencia);
    }
    return obj;
  }
  //


}
