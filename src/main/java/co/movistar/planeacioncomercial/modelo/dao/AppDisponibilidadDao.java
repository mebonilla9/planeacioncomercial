package co.movistar.planeacioncomercial.modelo.dao;

import co.movistar.planeacioncomercial.modelo.conexion.ConexionBD;
import co.movistar.planeacioncomercial.modelo.dao.crud.AppDisponibilidadCrud;
import co.movistar.planeacioncomercial.modelo.vo.AppDisponibilidad;
import com.google.maps.model.LatLng;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author Manuel Ernesto Bonilla Muñoz <mebonilla9@gmail.com>
 */
public class AppDisponibilidadDao extends AppDisponibilidadCrud {

  public AppDisponibilidadDao(Connection cnn) {
    super(cnn);
  }

  public List<AppDisponibilidad> consultarInformacionDisponibilidad(String... valores) throws SQLException {
    PreparedStatement sentencia = null;
    List<AppDisponibilidad> lista = new ArrayList<>();
    try {
      int i = 1;
      String sql = "exec Consulta_Disponibilidad ?,?,?,?,?,?";
      sentencia = cnn.prepareStatement(sql);
      for (int j = 0; j < valores.length; j++) {
        if (j < 6) {
          sentencia.setObject(i++, valores[j]);
        }
      }
      ResultSet rs = sentencia.executeQuery();
      while (rs.next()) {
        AppDisponibilidad disponibilidad = new AppDisponibilidad();
        disponibilidad.setRegional(rs.getString("REGIONAL"));
        disponibilidad.setDepartamento(rs.getString("DEPARTAMENTO"));
        disponibilidad.setLocalidad(rs.getString("LOCALIDAD"));
        disponibilidad.setDistrito(rs.getString("DISTRITO"));
        disponibilidad.setArmario(rs.getString("ARMARIO"));
        disponibilidad.setCaja(rs.getString("CAJA"));
        disponibilidad.setCategoria(rs.getString("ITEM"));
        disponibilidad.setqOferta(rs.getString("Q"));
        lista.add(disponibilidad);
      }
    } finally {
      ConexionBD.desconectar(sentencia);
    }
    return lista;
  }

  public List<AppDisponibilidad> consultarDepartamentos() throws SQLException {
    PreparedStatement sentencia = null;
    List<AppDisponibilidad> lista = new ArrayList<>();
    try {
      String sql = "SELECT "
              + "Departamento "
              + "FROM APP_DISPONIBILIDAD "
              + "GROUP BY Departamento "
              + "ORDER BY Departamento";
      sentencia = cnn.prepareStatement(sql);
      ResultSet rs = sentencia.executeQuery();
      Map<String, Integer> columnas = new HashMap<>();
      columnas.put("APP_DISPONIBILIDAD_Regional", 1);
      while (rs.next()) {
        lista.add(getappDisponibilidad(rs, columnas));
      }
    } finally {
      ConexionBD.desconectar(sentencia);
    }
    return lista;
  }

  public List<AppDisponibilidad> consultarLocalidades(String departamento) throws SQLException {
    PreparedStatement sentencia = null;
    List<AppDisponibilidad> lista = new ArrayList<>();
    try {
      String sql = "SELECT "
              + "Localidad "
              + "FROM APP_DISPONIBILIDAD "
              + "WHERE Departamento like ? "
              + "GROUP BY Departamento,Localidad "
              + "ORDER BY Departamento,Localidad";
      sentencia = cnn.prepareStatement(sql);
      sentencia.setString(1, departamento);
      ResultSet rs = sentencia.executeQuery();
      Map<String, Integer> columnas = new HashMap<>();
      columnas.put("APP_DISPONIBILIDAD_Localidad", 1);
      while (rs.next()) {
        lista.add(getappDisponibilidad(rs, columnas));
      }
    } finally {
      ConexionBD.desconectar(sentencia);
    }
    return lista;
  }

  public List<AppDisponibilidad> consultarDistritos(String departamento, String localidad) throws SQLException {
    PreparedStatement sentencia = null;
    List<AppDisponibilidad> lista = new ArrayList<>();
    try {
      String sql = "SELECT MDF FROM APP_DISPONIBILIDAD WHERE Departamento = ? AND Localidad = ? GROUP BY MDF ORDER BY MDF;";
      System.out.println(sql);
      sentencia = cnn.prepareStatement(sql);
      sentencia.setString(1, departamento);
      sentencia.setString(2, localidad);
      ResultSet rs = sentencia.executeQuery();
      Map<String, Integer> columnas = new HashMap<>();
      columnas.put("APP_DISPONIBILIDAD_MDF", 1);
      while (rs.next()) {
        lista.add(getappDisponibilidad(rs, columnas));
      }
    } finally {
      ConexionBD.desconectar(sentencia);
    }
    return lista;
  }

  public List<AppDisponibilidad> consultarArmarios(String mdf) throws SQLException {
    PreparedStatement sentencia = null;
    List<AppDisponibilidad> lista = new ArrayList<>();
    try {
      String sql = "SELECT "
              + "Armario "
              + "FROM APP_DISPONIBILIDAD "
              + "WHERE MDF like ? "
              + "GROUP BY MDF,Armario "
              + "ORDER BY MDF,Armario;";
      sentencia = cnn.prepareStatement(sql);
      sentencia.setString(1, mdf);
      ResultSet rs = sentencia.executeQuery();
      Map<String, Integer> columnas = new HashMap<>();
      columnas.put("APP_DISPONIBILIDAD_Armario", 1);
      while (rs.next()) {
        lista.add(getappDisponibilidad(rs, columnas));
      }
    } finally {
      ConexionBD.desconectar(sentencia);
    }
    return lista;
  }

  public List<AppDisponibilidad> consultarCajas(String mdf, String armario) throws SQLException {
    PreparedStatement sentencia = null;
    List<AppDisponibilidad> lista = new ArrayList<>();
    try {
      String sql = "SELECT "
              + "Caja "
              + "FROM APP_DISPONIBILIDAD "
              + "where MDF like ? and Armario like ? "
              + "GROUP BY MDF,Armario,Caja "
              + "ORDER BY MDF,Armario,Caja";
      sentencia = cnn.prepareStatement(sql);
      sentencia.setString(1, mdf);
      sentencia.setString(2, armario);
      ResultSet rs = sentencia.executeQuery();
      Map<String, Integer> columnas = new HashMap<>();
      columnas.put("APP_DISPONIBILIDAD_Caja", 1);
      while (rs.next()) {
        lista.add(getappDisponibilidad(rs, columnas));
      }
    } finally {
      ConexionBD.desconectar(sentencia);
    }
    return lista;
  }

  public String[] consultarCajaDireccion(LatLng coordenadas) throws SQLException {
    PreparedStatement sentencia = null;
    String[] lista = new String[7];
    try {
      int i = 1;
      String sql = "exec Por_direccion ?,?";
      sentencia = cnn.prepareStatement(sql);
      sentencia.setObject(i++, coordenadas.lng);
      sentencia.setObject(i++, coordenadas.lat);
      ResultSet rs = sentencia.executeQuery();
      while (rs.next()) {
        lista[0] = rs.getString("reg");
        lista[1] = rs.getString("dpt");
        lista[2] = rs.getString("loc");
        lista[3] = rs.getString("dto");
        lista[4] = rs.getString("arm");
        lista[5] = rs.getString("caj");
        lista[6] = rs.getString("respuesta");
      }
    } finally {
      ConexionBD.desconectar(sentencia);
    }
    return lista;
  }

  public List<AppDisponibilidad> consultarLocalidadCoordenadas(String departamento) throws SQLException {
    PreparedStatement sentencia = null;
    List<AppDisponibilidad> lista = new ArrayList<>();
    try {
      String sql = "SELECT DISTINCT Localidad from APP_DISPONIBILIDAD WHERE Regional = ? AND X IS NOT NULL AND Y IS NOT NULL;";
      System.out.println(sql);
      sentencia = cnn.prepareStatement(sql);
      sentencia.setString(1, departamento);
      ResultSet rs = sentencia.executeQuery();
      Map<String, Integer> columnas = new HashMap<>();
      columnas.put("APP_DISPONIBILIDAD_Localidad", 1);
      while (rs.next()) {
        lista.add(getappDisponibilidad(rs, columnas));
      }
    } finally {
      ConexionBD.desconectar(sentencia);
    }
    return lista;
  }

}
