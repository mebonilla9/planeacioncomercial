package co.movistar.planeacioncomercial.modelo.dao.crud;

import co.movistar.planeacioncomercial.modelo.conexion.ConexionBD;
import co.movistar.planeacioncomercial.modelo.vo.AppArchivos;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class AppArchivosCrud implements IGenericoDAO<AppArchivos> {

  protected final int ID = 1;
  protected Connection cnn;

  public AppArchivosCrud(Connection cnn) {
    this.cnn = cnn;
  }

  public static AppArchivos getAppArchivos(ResultSet rs) throws SQLException {
    AppArchivos appArchivos = new AppArchivos();
    appArchivos.setId(rs.getLong("Id"));
    appArchivos.setNombreArchivo(rs.getString("Nombre_archivo"));
    appArchivos.setMes(rs.getLong("Mes"));
    appArchivos.setRuta(rs.getString("Ruta"));
    appArchivos.setModulo(rs.getString("Modulo"));
    appArchivos.setEstado(rs.getBoolean("Estado"));
    appArchivos.setCategoria(rs.getString("Estado"));

    return appArchivos;
  }

  public static AppArchivos getAppArchivos(ResultSet rs, Map<String, Integer> columnas) throws SQLException {
    AppArchivos appArchivos = new AppArchivos();
    Integer columna = columnas.get("APP_ARCHIVOS_Id");
    if (columna != null) {
      appArchivos.setId(rs.getLong(columna));
    }
    columna = columnas.get("APP_ARCHIVOS_Nombre_archivo");
    if (columna != null) {
      appArchivos.setNombreArchivo(rs.getString(columna));
    }
    columna = columnas.get("APP_ARCHIVOS_Mes");
    if (columna != null) {
      appArchivos.setMes(rs.getLong(columna));
    }
    columna = columnas.get("APP_ARCHIVOS_Ruta");
    if (columna != null) {
      appArchivos.setRuta(rs.getString(columna));
    }
    columna = columnas.get("APP_ARCHIVOS_Modulo");
    if (columna != null) {
      appArchivos.setModulo(rs.getString(columna));
    }
    columna = columnas.get("APP_ARCHIVOS_Estado");
    if (columna != null) {
      appArchivos.setEstado(rs.getBoolean(columna));
    }
    columna = columnas.get("APP_ARCHIVOS_Categoria");
    if (columna != null) {
      appArchivos.setCategoria(rs.getString("Categoria"));
    }
    return appArchivos;
  }

  @Override
  public void insertar(AppArchivos appArchivos) throws SQLException {
    PreparedStatement sentencia = null;
    try {
      int i = 1;
      String sql = "insert into APP_ARCHIVOS(Nombre_archivo,Mes,Ruta,Modulo,Estado,Categoria) values (?,?,?,?,?,?)";
      sentencia = cnn.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);
      sentencia.setObject(i++, appArchivos.getNombreArchivo());
      sentencia.setObject(i++, appArchivos.getMes());
      sentencia.setObject(i++, appArchivos.getRuta());
      sentencia.setObject(i++, appArchivos.getModulo());
      sentencia.setObject(i++, appArchivos.getEstado());
      sentencia.setObject(i++, appArchivos.getCategoria());

      sentencia.executeUpdate();
      ResultSet rs = sentencia.getGeneratedKeys();
      if (rs.next()) {
        appArchivos.setId(rs.getLong(ID));
      }
    } finally {
      ConexionBD.desconectar(sentencia);
    }
  }

  @Override
  public void editar(AppArchivos appArchivos) throws SQLException {
    PreparedStatement sentencia = null;
    try {
      int i = 1;
      String sql = "update APP_ARCHIVOS set Nombre_archivo=?,Mes=?,Ruta=?,Modulo=?,Estado=?,Categoria=? where Id=?";
      sentencia = cnn.prepareStatement(sql);
      sentencia.setObject(i++, appArchivos.getNombreArchivo());
      sentencia.setObject(i++, appArchivos.getMes());
      sentencia.setObject(i++, appArchivos.getRuta());
      sentencia.setObject(i++, appArchivos.getModulo());
      sentencia.setObject(i++, appArchivos.getEstado());
      sentencia.setObject(i++, appArchivos.getCategoria());
      sentencia.setObject(i++, appArchivos.getId());

      sentencia.executeUpdate();
    } finally {
      ConexionBD.desconectar(sentencia);
    }
  }

  @Override
  public List<AppArchivos> consultar() throws SQLException {
    PreparedStatement sentencia = null;
    List<AppArchivos> lista = new ArrayList<>();
    try {

      String sql = "select * from APP_ARCHIVOS";
      sentencia = cnn.prepareStatement(sql);
      ResultSet rs = sentencia.executeQuery();
      while (rs.next()) {
        lista.add(getAppArchivos(rs));
      }
    } finally {
      ConexionBD.desconectar(sentencia);
    }
    return lista;

  }

  @Override
  public AppArchivos consultar(Long id) throws SQLException {
    PreparedStatement sentencia = null;
    AppArchivos obj = null;
    try {

      String sql = "select * from APP_ARCHIVOS where Id=?";
      sentencia = cnn.prepareStatement(sql);
      sentencia.setLong(1, id);
      ResultSet rs = sentencia.executeQuery();
      if (rs.next()) {
        obj = getAppArchivos(rs);
      }
    } finally {
      ConexionBD.desconectar(sentencia);
    }
    return obj;
  }

}
