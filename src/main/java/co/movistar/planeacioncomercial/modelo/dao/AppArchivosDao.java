package co.movistar.planeacioncomercial.modelo.dao;

import co.movistar.planeacioncomercial.modelo.conexion.ConexionBD;
import co.movistar.planeacioncomercial.modelo.dao.crud.AppArchivosCrud;
import co.movistar.planeacioncomercial.modelo.vo.AppArchivos;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class AppArchivosDao extends AppArchivosCrud {

  public AppArchivosDao(Connection cnn) {
    super(cnn);
  }

  public List<String> consultarCategoriasModulo(String modulo) throws SQLException {
    PreparedStatement sentencia = null;
    List<String> lista = new ArrayList<>();
    try {
      String sql = "select DISTINCT Categoria from dbo.APP_ARCHIVOS where Modulo=?";
      sentencia = cnn.prepareStatement(sql);
      sentencia.setString(1, modulo);
      ResultSet rs = sentencia.executeQuery();
      while (rs.next()) {
        lista.add(rs.getString("Categoria"));
      }
    } finally {
      ConexionBD.desconectar(sentencia);
    }
    return lista;
  }

  public List<AppArchivos> consultarArchivosModuloCategoria(String modulo, String categoria) throws SQLException {
    PreparedStatement sentencia = null;
    List<AppArchivos> lista = new ArrayList<>();
    try {
      String sql = "select * from APP_ARCHIVOS where Modulo = ? AND Categoria = ? AND Estado = 1 AND Mes = (SELECT MAX(Mes) FROM APP_ARCHIVOS)";
      sentencia = cnn.prepareStatement(sql);
      sentencia.setString(1, modulo);
      sentencia.setString(2, categoria);
      ResultSet rs = sentencia.executeQuery();
      while (rs.next()) {
        lista.add(getAppArchivos(rs));
      }
    } finally {
      ConexionBD.desconectar(sentencia);
    }
    return lista;

  }
}
