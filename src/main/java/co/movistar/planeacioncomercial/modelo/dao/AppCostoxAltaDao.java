package co.movistar.planeacioncomercial.modelo.dao;

import co.movistar.planeacioncomercial.modelo.conexion.ConexionBD;
import co.movistar.planeacioncomercial.modelo.dao.crud.AppCostoxAltaCrud;
import co.movistar.planeacioncomercial.modelo.vo.AppCostoxAlta;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class AppCostoxAltaDao extends AppCostoxAltaCrud {

  public AppCostoxAltaDao(Connection cnn) {
    super(cnn);
  }

  public List<AppCostoxAlta> consultarPeriodos(Long idUsuario) throws SQLException {
    PreparedStatement sentencia = null;
    List<AppCostoxAlta> lista = new ArrayList<>();
    try {
      String sql = "SELECT DISTINCT PERIODO FROM APP_COSTOXALTA WHERE Id_Usuario = ? ORDER BY PERIODO DESC";
      sentencia = cnn.prepareStatement(sql);
      sentencia.setLong(1, idUsuario);
      ResultSet rs = sentencia.executeQuery();
      Map<String, Integer> columnas = new HashMap<>();
      columnas.put("APP_COSTOXALTA_PERIODO", 1);
      while (rs.next()) {
        lista.add(getAppCostoxAlta(rs, columnas));
      }
    } finally {
      ConexionBD.desconectar(sentencia);
    }
    return lista;
  }

  public List<AppCostoxAlta> consultarProductosDisponibles(Long idUsuario, String periodo) throws SQLException {
    PreparedStatement sentencia = null;
    List<AppCostoxAlta> lista = new ArrayList<>();
    try {
      String sql = "select DISTINCT PRODUCTO from APP_COSTOXALTA where ID_USUARIO = ? AND PERIODO = ?";
      sentencia = cnn.prepareStatement(sql);
      sentencia.setLong(1, idUsuario);
      sentencia.setString(2, periodo);
      ResultSet rs = sentencia.executeQuery();
      Map<String, Integer> columnas = new HashMap<>();
      columnas.put("APP_COSTOXALTA_PRODUCTO", 1);
      while (rs.next()) {
        lista.add(getAppCostoxAlta(rs, columnas));
      }
    } finally {
      ConexionBD.desconectar(sentencia);
    }
    return lista;
  }

  public List<AppCostoxAlta> consultarCostoxAltaCanalUsuario(Long idUsuario, String producto, String periodo, String canal) throws SQLException {
    PreparedStatement sentencia = null;
    List<AppCostoxAlta> lista = new ArrayList<>();
    try {
      String sql = "select * from APP_COSTOXALTA WHERE ID_USUARIO = ? AND PRODUCTO = ? AND PERIODO = ? AND CANAL = ? order by SEGMENTO ASC";
      sentencia = cnn.prepareStatement(sql);
      sentencia.setLong(1, idUsuario);
      sentencia.setString(2, producto);
      sentencia.setString(3, periodo);
      sentencia.setString(4, canal);
      ResultSet rs = sentencia.executeQuery();
      while (rs.next()) {
        lista.add(getAppCostoxAlta(rs));
      }
    } finally {
      ConexionBD.desconectar(sentencia);
    }
    return lista;

  }

  public List<AppCostoxAlta> consultarCanalesDisponibles(Long idUsuario, String periodo, String producto) throws SQLException {
    PreparedStatement sentencia = null;
    List<AppCostoxAlta> lista = new ArrayList<>();
    try {
      String sql = "SELECT DISTINCT CANAL FROM APP_COSTOXALTA WHERE ID_USUARIO = ? AND PERIODO = ? AND PRODUCTO = ?;";
      sentencia = cnn.prepareStatement(sql);
      sentencia.setLong(1, idUsuario);
      sentencia.setString(2, periodo);
      sentencia.setString(3, producto);
      ResultSet rs = sentencia.executeQuery();
      Map<String, Integer> columnas = new HashMap<>();
      columnas.put("APP_COSTOXALTA_CANAL", 1);
      while (rs.next()) {
        lista.add(getAppCostoxAlta(rs, columnas));
      }
    } finally {
      ConexionBD.desconectar(sentencia);
    }
    return lista;
  }

}
