package co.movistar.planeacioncomercial.modelo.dao;

import co.movistar.planeacioncomercial.modelo.dao.crud.AppCanalesCrud;

import java.sql.Connection;

/**
 * @author Manuel Ernesto Bonilla Muñoz <mebonilla9@gmail.com>
 */
public class AppCanalesDao extends AppCanalesCrud {

  public AppCanalesDao(Connection cnn) {
    super(cnn);
  }
}
