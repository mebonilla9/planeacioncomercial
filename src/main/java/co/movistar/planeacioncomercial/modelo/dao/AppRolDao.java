package co.movistar.planeacioncomercial.modelo.dao;

import co.movistar.planeacioncomercial.modelo.conexion.ConexionBD;
import co.movistar.planeacioncomercial.modelo.dao.crud.AppRolCrud;
import co.movistar.planeacioncomercial.modelo.vo.AppRol;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class AppRolDao extends AppRolCrud {

  public AppRolDao(Connection cnn) {
    super(cnn);
  }

  @Override
  public List<AppRol> consultar() throws SQLException {
    PreparedStatement sentencia = null;
    List<AppRol> lista = new ArrayList<>();
    try {

      String sql = "select * from APP_ROL ORDER BY Nombre_rol ASC";
      sentencia = cnn.prepareStatement(sql);
      ResultSet rs = sentencia.executeQuery();
      while (rs.next()) {
        lista.add(getAppRol(rs));
      }
    } finally {
      ConexionBD.desconectar(sentencia);
    }
    return lista;

  }
}
