package co.movistar.planeacioncomercial.modelo.dao.crud;

import co.movistar.planeacioncomercial.modelo.conexion.ConexionBD;
import co.movistar.planeacioncomercial.modelo.vo.AppNovedad;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class AppNovedadCrud implements IGenericoDAO<AppNovedad> {

  protected final int ID = 1;
  protected Connection cnn;

  public AppNovedadCrud(Connection cnn) {
    this.cnn = cnn;
  }

  public static AppNovedad getAppNovedad(ResultSet rs) throws SQLException {
    AppNovedad appNovedad = new AppNovedad();
    appNovedad.setIdNovedad(rs.getLong("Id_novedad"));
    appNovedad.setNombreNovedad(rs.getString("Nombre_novedad"));
    appNovedad.setModulos(rs.getString("modulos"));
    appNovedad.setTipo(rs.getString("tipo"));

    return appNovedad;
  }

  public static AppNovedad getAppNovedad(ResultSet rs, Map<String, Integer> columnas) throws SQLException {
    AppNovedad appNovedad = new AppNovedad();
    Integer columna = columnas.get("APP_NOVEDAD_Id_novedad");
    if (columna != null) {
      appNovedad.setIdNovedad(rs.getLong(columna));
    }
    columna = columnas.get("APP_NOVEDAD_Nombre_novedad");
    if (columna != null) {
      appNovedad.setNombreNovedad(rs.getString(columna));
    }
    columna = columnas.get("APP_NOVEDAD_modulos");
    if (columna != null) {
      appNovedad.setModulos(rs.getString(columna));
    }
    columna = columnas.get("APP_NOVEDAD_tipo");
    if (columna != null) {
      appNovedad.setNombreNovedad(rs.getString(columna));
    }
    return appNovedad;
  }

  @Override
  public void insertar(AppNovedad appNovedad) throws SQLException {
    PreparedStatement sentencia = null;
    try {
      int i = 1;
      String sql = "insert into APP_NOVEDAD(Nombre_novedad,modulos,tipo) values (?,?,?)";
      sentencia = cnn.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);
      sentencia.setObject(i++, appNovedad.getNombreNovedad());
      sentencia.setObject(i++, appNovedad.getModulos());
      sentencia.setObject(i++, appNovedad.getTipo());

      sentencia.executeUpdate();
      ResultSet rs = sentencia.getGeneratedKeys();
      if (rs.next()) {
        appNovedad.setIdNovedad(rs.getLong(ID));
      }
    } finally {
      ConexionBD.desconectar(sentencia);
    }
  }

  @Override
  public void editar(AppNovedad appNovedad) throws SQLException {
    PreparedStatement sentencia = null;
    try {
      int i = 1;
      String sql = "update APP_NOVEDAD set Nombre_novedad=?, modulos=?, tipo=? where Id_novedad=?";
      sentencia = cnn.prepareStatement(sql);
      sentencia.setObject(i++, appNovedad.getNombreNovedad());
      sentencia.setObject(i++, appNovedad.getModulos());
      sentencia.setObject(i++, appNovedad.getTipo());
      sentencia.setObject(i++, appNovedad.getIdNovedad());

      sentencia.executeUpdate();
    } finally {
      ConexionBD.desconectar(sentencia);
    }
  }

  @Override
  public List<AppNovedad> consultar() throws SQLException {
    PreparedStatement sentencia = null;
    List<AppNovedad> lista = new ArrayList<>();
    try {

      String sql = "select * from APP_NOVEDAD";
      sentencia = cnn.prepareStatement(sql);
      ResultSet rs = sentencia.executeQuery();
      while (rs.next()) {
        lista.add(getAppNovedad(rs));
      }
    } finally {
      ConexionBD.desconectar(sentencia);
    }
    return lista;

  }

  @Override
  public AppNovedad consultar(Long id) throws SQLException {
    PreparedStatement sentencia = null;
    AppNovedad obj = null;
    try {

      String sql = "select * from APP_NOVEDAD where Id_novedad=?";
      sentencia = cnn.prepareStatement(sql);
      sentencia.setLong(1, id);
      ResultSet rs = sentencia.executeQuery();
      if (rs.next()) {
        obj = getAppNovedad(rs);
      }
    } finally {
      ConexionBD.desconectar(sentencia);
    }
    return obj;
  }

}
