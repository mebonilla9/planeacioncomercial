package co.movistar.planeacioncomercial.modelo.dao;

import co.movistar.planeacioncomercial.modelo.dao.crud.AppEstadosCrud;

import java.sql.Connection;

/**
 * @author Manuel Ernesto Bonilla Muñoz <mebonilla9@gmail.com>
 */
public class AppEstadosDao extends AppEstadosCrud {

  public AppEstadosDao(Connection cnn) {
    super(cnn);
  }
}
