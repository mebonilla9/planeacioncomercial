package co.movistar.planeacioncomercial.modelo.dao;

import co.movistar.planeacioncomercial.modelo.conexion.ConexionBD;
import co.movistar.planeacioncomercial.modelo.dao.crud.AppCargoCrud;
import co.movistar.planeacioncomercial.modelo.vo.AppCargo;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 * @author Manuel Ernesto Bonilla Muñoz <mebonilla9@gmail.com>
 */
public class AppCargoDao extends AppCargoCrud {

  public AppCargoDao(Connection cnn) {
    super(cnn);
  }

  public List<AppCargo> consultarAsesor() throws SQLException {
    PreparedStatement sentencia = null;
    List<AppCargo> lista = new ArrayList<>();
    try {

      String sql = "select * from APP_CARGO where lower(NOM_CARGO) like lower('%asesor%');";
      sentencia = cnn.prepareStatement(sql);
      ResultSet rs = sentencia.executeQuery();
      while (rs.next()) {
        lista.add(getAppCargo(rs));
      }
    } finally {
      ConexionBD.desconectar(sentencia);
    }
    return lista;
  }
}
