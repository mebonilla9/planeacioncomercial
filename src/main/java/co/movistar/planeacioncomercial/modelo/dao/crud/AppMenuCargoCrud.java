package co.movistar.planeacioncomercial.modelo.dao.crud;

import co.movistar.planeacioncomercial.modelo.conexion.ConexionBD;
import co.movistar.planeacioncomercial.modelo.vo.AppCargo;
import co.movistar.planeacioncomercial.modelo.vo.AppMenu;
import co.movistar.planeacioncomercial.modelo.vo.AppMenuCargo;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * @author Manuel Ernesto Bonilla Muñoz <mebonilla9@gmail.com>
 */
public class AppMenuCargoCrud implements IGenericoDAO<AppMenuCargo> {

  protected final int ID = 1;
  protected Connection cnn;

  public AppMenuCargoCrud(Connection cnn) {
    this.cnn = cnn;
  }

  public static AppMenuCargo getAppMenuCargo(ResultSet rs) throws SQLException {
    AppMenuCargo appMenuCargo = new AppMenuCargo();
    appMenuCargo.setIdMenuCargo(rs.getLong("ID_MENU_CARGO"));
    appMenuCargo.setCargo(new AppCargo(rs.getLong("ID_CARGO")));
    appMenuCargo.setMenu(new AppMenu(rs.getLong("ID_MENU")));
    return appMenuCargo;
  }

  public static AppMenuCargo getAppMenuCargo(ResultSet rs, Map<String, Integer> columnas) throws SQLException {
    AppMenuCargo appMenuCargo = new AppMenuCargo();
    Integer columna = columnas.get("APP_MENU_CARGO_ID_MENU_CARGO");
    if (columna != null) {
      appMenuCargo.setIdMenuCargo(rs.getLong(columna));
    }
    columna = columnas.get("APP_MENU_CARGO_ID_CARGO");
    if (columna != null) {
      appMenuCargo.setCargo(new AppCargo(rs.getLong(columna)));
    }
    columna = columnas.get("APP_MENU_CARGO_ID_MENU");
    if (columna != null) {
      appMenuCargo.setMenu(new AppMenu(rs.getLong(columna)));
    }
    return appMenuCargo;
  }

  @Override
  public void insertar(AppMenuCargo appMenuCargo) throws SQLException {
    PreparedStatement sentencia = null;
    try {
      int i = 1;
      String sql = "insert into APP_MENU_CARGO(ID_CARGO,ID_MENU) values (?,?)";
      sentencia = cnn.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);
      sentencia.setObject(i++, appMenuCargo.getCargo().getIdCargo());
      sentencia.setObject(i++, appMenuCargo.getMenu().getIdMenu());

      sentencia.executeUpdate();
      ResultSet rs = sentencia.getGeneratedKeys();
      if (rs.next()) {
        appMenuCargo.setIdMenuCargo(rs.getLong(ID));
      }
    } finally {
      ConexionBD.desconectar(sentencia);
    }
  }

  @Override
  public void editar(AppMenuCargo appMenuCargo) throws SQLException {
    PreparedStatement sentencia = null;
    try {
      int i = 1;
      String sql = "update APP_MENU_CARGO set ID_CARGO=?,ID_MENU=? where ID_MENU_CARGO=?";
      sentencia = cnn.prepareStatement(sql);
      sentencia.setObject(i++, appMenuCargo.getCargo().getIdCargo());
      sentencia.setObject(i++, appMenuCargo.getMenu().getIdMenu());
      sentencia.setObject(i++, appMenuCargo.getIdMenuCargo());

      sentencia.executeUpdate();
    } finally {
      ConexionBD.desconectar(sentencia);
    }
  }

  @Override
  public List<AppMenuCargo> consultar() throws SQLException {
    PreparedStatement sentencia = null;
    List<AppMenuCargo> lista = new ArrayList<>();
    try {

      String sql = "select * from APP_MENU_CARGO";
      sentencia = cnn.prepareStatement(sql);
      ResultSet rs = sentencia.executeQuery();
      while (rs.next()) {
        lista.add(getAppMenuCargo(rs));
      }
    } finally {
      ConexionBD.desconectar(sentencia);
    }
    return lista;

  }

  @Override
  public AppMenuCargo consultar(Long id) throws SQLException {
    PreparedStatement sentencia = null;
    AppMenuCargo obj = null;
    try {

      String sql = "select * from APP_MENU_CARGO where ID_MENU_CARGO=?";
      sentencia = cnn.prepareStatement(sql);
      sentencia.setLong(1, id);
      ResultSet rs = sentencia.executeQuery();
      if (rs.next()) {
        obj = getAppMenuCargo(rs);
      }
    } finally {
      ConexionBD.desconectar(sentencia);
    }
    return obj;
  }

}
