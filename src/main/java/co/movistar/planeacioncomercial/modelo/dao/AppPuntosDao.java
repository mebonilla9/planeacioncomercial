package co.movistar.planeacioncomercial.modelo.dao;

import co.movistar.planeacioncomercial.modelo.conexion.ConexionBD;
import co.movistar.planeacioncomercial.modelo.dao.crud.AppPuntosCrud;
import co.movistar.planeacioncomercial.modelo.vo.AppPuntos;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class AppPuntosDao extends AppPuntosCrud {

  public AppPuntosDao(Connection cnn) {
    super(cnn);
  }

  public List<AppPuntos> consultarPuntosUsuario(Long id) throws SQLException {
    PreparedStatement sentencia = null;
    List<AppPuntos> lista = new ArrayList<>();
    try {

      String sql = "select * from APP_PUNTOS WHERE Id_usuario = ?";
      sentencia = cnn.prepareStatement(sql);
      sentencia.setLong(1, id);
      ResultSet rs = sentencia.executeQuery();
      while (rs.next()) {
        lista.add(getAppPuntos(rs));
      }
    } finally {
      ConexionBD.desconectar(sentencia);
    }
    return lista;

  }

  public List<AppPuntos> consultarPuntosNovedad(Long id) throws SQLException {
    PreparedStatement sentencia = null;
    List<AppPuntos> lista = new ArrayList<>();
    try {

      String sql = "select * from APP_PUNTOS WHERE Id_usuario = ? or Id_usuario is null";
      sentencia = cnn.prepareStatement(sql);
      sentencia.setLong(1, id);
      ResultSet rs = sentencia.executeQuery();
      while (rs.next()) {
        lista.add(getAppPuntos(rs));
      }
    } finally {
      ConexionBD.desconectar(sentencia);
    }
    return lista;

  }

  public AppPuntos consultarPuntoCoordinador(Long idUsuario, Long idCoordinador) throws SQLException {
    PreparedStatement sentencia = null;
    AppPuntos obj = null;
    try {
      String sql = "select * from APP_PUNTOS where Id_usuario=? and CC_coordinador=?";
      sentencia = cnn.prepareStatement(sql);
      sentencia.setLong(1, idUsuario);
      sentencia.setLong(2, idCoordinador);
      ResultSet rs = sentencia.executeQuery();
      if (rs.next()) {
        obj = getAppPuntos(rs);
      }
    } finally {
      ConexionBD.desconectar(sentencia);
    }
    return obj;
  }

  public List<AppPuntos> consultarPorCoordinador(Double cedula) throws SQLException {
    PreparedStatement sentencia = null;
    List<AppPuntos> lista = new ArrayList<>();
    try {

      String sql = "select * from APP_PUNTOS where CC_coordinador=? or lower(Nombre_punto) like lower('%pendiente%');";
      sentencia = cnn.prepareStatement(sql);
      sentencia.setDouble(1, cedula);
      ResultSet rs = sentencia.executeQuery();
      while (rs.next()) {
        lista.add(getAppPuntos(rs));
      }
    } finally {
      ConexionBD.desconectar(sentencia);
    }
    return lista;
  }


  public AppPuntos consultarPuntoPendiente() throws SQLException {
    PreparedStatement sentencia = null;
    AppPuntos obj = null;
    try {

      String sql = "select * from APP_PUNTOS where lower(Nombre_punto) like lower(?)";
      sentencia = cnn.prepareStatement(sql);
      sentencia.setString(1, "%Pendiente%");
      ResultSet rs = sentencia.executeQuery();
      if (rs.next()) {
        obj = getAppPuntos(rs);
      }
    } finally {
      ConexionBD.desconectar(sentencia);
    }
    return obj;
  }
}
