package co.movistar.planeacioncomercial.modelo.dao.crud;

import co.movistar.planeacioncomercial.modelo.conexion.ConexionBD;
import co.movistar.planeacioncomercial.modelo.vo.AppBajas;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class AppBajasCrud implements IGenericoDAO<AppBajas> {

  protected final int ID = 1;
  protected Connection cnn;

  public AppBajasCrud(Connection cnn) {
    this.cnn = cnn;
  }

  public static AppBajas getappBajas(ResultSet rs) throws SQLException {
    AppBajas appBajas = new AppBajas();
    appBajas.setIdBaja(rs.getLong("ID_BAJA"));
    appBajas.setProducto(rs.getString("PRODUCTO"));
    appBajas.setRegional(rs.getString("REGIONAL"));
    appBajas.setFechaVenta(rs.getLong("FECHA"));
    appBajas.setPromedio(rs.getString("PROM"));
    appBajas.setPaisProm(rs.getString("PAIS"));
    List<String> periodos = getCamposAdicionales(rs);
    for (int i = 0; i < periodos.size(); i++) {
      String periodo = periodos.get(i);
      appBajas.getPeriodos().put(periodo, rs.getString(periodo));
    }
    return appBajas;
  }

  public static AppBajas getappBajas(ResultSet rs, Map<String, Integer> columnas) throws SQLException {
    AppBajas appBajas = new AppBajas();
    Integer columna = columnas.get("APP_BAJAS_ID_BAJA");
    if (columna != null) {
      appBajas.setIdBaja(rs.getLong(columna));
    }
    columna = columnas.get("APP_BAJAS_PRODUCTO");
    if (columna != null) {
      appBajas.setProducto(rs.getString(columna));
    }
    columna = columnas.get("APP_BAJAS_REGIONAL");
    if (columna != null) {
      appBajas.setRegional(rs.getString(columna));
    }
    columna = columnas.get("APP_BAJAS_FECHA_VENTA");
    if (columna != null) {
      appBajas.setFechaVenta(rs.getLong(columna));
    }
    columna = columnas.get("APP_BAJAS_PROMEDIO");
    if (columna != null) {
      appBajas.setPromedio(rs.getString(columna));
    }
    columna = columnas.get("APP_BAJAS_PAIS_PROM");
    if (columna != null) {
      appBajas.setPaisProm(rs.getString(columna));
    }
    return appBajas;
  }

  protected static List<String> getCamposAdicionales(ResultSet rs) throws SQLException {
    List<String> camposDinamicos = new ArrayList<>();
    ResultSetMetaData metaData = rs.getMetaData();
    metaData.getColumnCount();
    for (int i = 1; i <= metaData.getColumnCount(); i++) {
      System.out.println("Columna: " + metaData.getColumnName(i));
      System.out.println("Label: " + metaData.getColumnLabel(i));
      if (i > 6) {
        camposDinamicos.add(metaData.getColumnName(i));
      }
    }
    return camposDinamicos;
  }

  @Override
  public void insertar(AppBajas appBajas) throws SQLException {
    PreparedStatement sentencia = null;
    try {
      int i = 1;
      String sql = "insert into APP_BAJAS(PRODUCTO,REGIONAL,FECHA_VENTA,201705,201706,201707,201708,201709,PROMEDIO,PAIS_PROM) values (?,?,?,?,?,?,?,?,?,?)";
      sentencia = cnn.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);
      sentencia.setObject(i++, appBajas.getProducto());
      sentencia.setObject(i++, appBajas.getRegional());
      sentencia.setObject(i++, appBajas.getFechaVenta());
      sentencia.setObject(i++, appBajas.getPromedio());
      sentencia.setObject(i++, appBajas.getPaisProm());

      sentencia.executeUpdate();
      ResultSet rs = sentencia.getGeneratedKeys();
      if (rs.next()) {
        appBajas.setIdBaja(rs.getLong(ID));
      }
    } finally {
      ConexionBD.desconectar(sentencia);
    }
  }

  @Override
  public void editar(AppBajas appBajas) throws SQLException {
    PreparedStatement sentencia = null;
    try {
      int i = 1;
      String sql = "update APP_BAJAS set PRODUCTO=?,REGIONAL=?,FECHA_VENTA=?,201705=?,201706=?,201707=?,201708=?,201709=?,PROMEDIO=?,PAIS_PROM=? where ID_BAJA=?";
      sentencia = cnn.prepareStatement(sql);
      sentencia.setObject(i++, appBajas.getProducto());
      sentencia.setObject(i++, appBajas.getRegional());
      sentencia.setObject(i++, appBajas.getFechaVenta());
      sentencia.setObject(i++, appBajas.getPromedio());
      sentencia.setObject(i++, appBajas.getPaisProm());
      sentencia.setObject(i++, appBajas.getIdBaja());

      sentencia.executeUpdate();
    } finally {
      ConexionBD.desconectar(sentencia);
    }
  }

  @Override
  public List<AppBajas> consultar() throws SQLException {
    PreparedStatement sentencia = null;
    List<AppBajas> lista = new ArrayList<>();
    try {
      String sql = "select * from APP_BAJAS";
      sentencia = cnn.prepareStatement(sql);
      ResultSet rs = sentencia.executeQuery();
      getCamposAdicionales(rs);
      while (rs.next()) {
        lista.add(getappBajas(rs));
      }
    } finally {
      ConexionBD.desconectar(sentencia);
    }
    return lista;
  }

  @Override
  public AppBajas consultar(Long id) throws SQLException {
    PreparedStatement sentencia = null;
    AppBajas obj = null;
    try {
      String sql = "select * from APP_BAJAS where ID_BAJA=?";
      sentencia = cnn.prepareStatement(sql);
      sentencia.setLong(1, id);
      ResultSet rs = sentencia.executeQuery();
      if (rs.next()) {
        obj = getappBajas(rs);
      }
    } finally {
      ConexionBD.desconectar(sentencia);
    }
    return obj;
  }
}
