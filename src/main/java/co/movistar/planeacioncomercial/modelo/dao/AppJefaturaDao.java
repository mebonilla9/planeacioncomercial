package co.movistar.planeacioncomercial.modelo.dao;

import co.movistar.planeacioncomercial.modelo.dao.crud.AppJefaturaCrud;

import java.sql.Connection;

/**
 * @author Manuel Ernesto Bonilla Muñoz <mebonilla9@gmail.com>
 */
public class AppJefaturaDao extends AppJefaturaCrud {

  public AppJefaturaDao(Connection cnn) {
    super(cnn);
  }
}
