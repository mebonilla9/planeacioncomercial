package co.movistar.planeacioncomercial.modelo.dao.crud;

import co.movistar.planeacioncomercial.modelo.conexion.ConexionBD;
import co.movistar.planeacioncomercial.modelo.vo.AppMetas;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class AppMetasCrud implements IGenericoDAO<AppMetas> {

  protected final int ID = 1;
  protected Connection cnn;

  public AppMetasCrud(Connection cnn) {
    this.cnn = cnn;
  }

  public static AppMetas getAppMetas(ResultSet rs) throws SQLException {
    AppMetas appMetas = new AppMetas();
    appMetas.setIdMeta(rs.getLong("Id_Meta"));
    appMetas.setNombre(rs.getString("Nombre"));

    return appMetas;
  }

  public static AppMetas getAppMetas(ResultSet rs, Map<String, Integer> columnas) throws SQLException {
    AppMetas appMetas = new AppMetas();
    Integer columna = columnas.get("APP_METAS_Id_Meta");
    if (columna != null) {
      appMetas.setIdMeta(rs.getLong(columna));
    }
    columna = columnas.get("APP_METAS_Nombre");
    if (columna != null) {
      appMetas.setNombre(rs.getString(columna));
    }
    return appMetas;
  }

  @Override
  public void insertar(AppMetas appMetas) throws SQLException {
    PreparedStatement sentencia = null;
    try {
      int i = 1;
      String sql = "insert into APP_METAS(Nombre) values (?)";
      sentencia = cnn.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);
      sentencia.setObject(i++, appMetas.getNombre());

      sentencia.executeUpdate();
      ResultSet rs = sentencia.getGeneratedKeys();
      if (rs.next()) {
        appMetas.setIdMeta(rs.getLong(ID));
      }
    } finally {
      ConexionBD.desconectar(sentencia);
    }
  }

  @Override
  public void editar(AppMetas appMetas) throws SQLException {
    PreparedStatement sentencia = null;
    try {
      int i = 1;
      String sql = "update APP_METAS set Nombre=? where Id_Meta=?";
      sentencia = cnn.prepareStatement(sql);
      sentencia.setObject(i++, appMetas.getNombre());
      sentencia.setObject(i++, appMetas.getIdMeta());

      sentencia.executeUpdate();
    } finally {
      ConexionBD.desconectar(sentencia);
    }
  }

  @Override
  public List<AppMetas> consultar() throws SQLException {
    PreparedStatement sentencia = null;
    List<AppMetas> lista = new ArrayList<>();
    try {

      String sql = "select * from APP_METAS";
      sentencia = cnn.prepareStatement(sql);
      ResultSet rs = sentencia.executeQuery();
      while (rs.next()) {
        lista.add(getAppMetas(rs));
      }
    } finally {
      ConexionBD.desconectar(sentencia);
    }
    return lista;

  }

  @Override
  public AppMetas consultar(Long id) throws SQLException {
    PreparedStatement sentencia = null;
    AppMetas obj = null;
    try {

      String sql = "select * from APP_METAS where Id_Meta=?";
      sentencia = cnn.prepareStatement(sql);
      sentencia.setLong(1, id);
      ResultSet rs = sentencia.executeQuery();
      if (rs.next()) {
        obj = getAppMetas(rs);
      }
    } finally {
      ConexionBD.desconectar(sentencia);
    }
    return obj;
  }

}
