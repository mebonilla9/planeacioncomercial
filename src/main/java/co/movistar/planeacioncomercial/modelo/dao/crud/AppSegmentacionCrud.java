package co.movistar.planeacioncomercial.modelo.dao.crud;

import co.movistar.planeacioncomercial.modelo.conexion.ConexionBD;
import co.movistar.planeacioncomercial.modelo.vo.AppSegmentacion;
import co.movistar.planeacioncomercial.negocio.util.DateUtil;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class AppSegmentacionCrud implements IGenericoDAO<AppSegmentacion> {

  protected final int ID = 1;
  protected Connection cnn;

  public AppSegmentacionCrud(Connection cnn) {
    this.cnn = cnn;
  }

  public static AppSegmentacion getAppSegmentacion(ResultSet rs) throws SQLException {
    AppSegmentacion appSegmentacion = new AppSegmentacion();
    appSegmentacion.setIdNit(rs.getLong("Id_nit"));
    appSegmentacion.setNit(rs.getString("nit"));
    appSegmentacion.setNombreCliente(rs.getString("Nombre_cliente"));
    appSegmentacion.setSegmento(rs.getString("Segmento"));
    appSegmentacion.setTipo(rs.getString("tipo"));
    appSegmentacion.setIngreso(rs.getTimestamp("Ingreso"));
    appSegmentacion.setConMarco(rs.getString("con_marco"));
    appSegmentacion.setIndicador(rs.getLong("indicador"));

    return appSegmentacion;
  }

  public static AppSegmentacion getAppSegmentacion(ResultSet rs, Map<String, Integer> columnas) throws SQLException {
    AppSegmentacion appSegmentacion = new AppSegmentacion();
    Integer columna = columnas.get("APP_SEGMENTACION_Id_nit");
    if (columna != null) {
      appSegmentacion.setIdNit(rs.getLong(columna));
    }
    columna = columnas.get("APP_SEGMENTACION_nit");
    if (columna != null) {
      appSegmentacion.setNit(rs.getString(columna));
    }
    columna = columnas.get("APP_SEGMENTACION_Nombre_cliente");
    if (columna != null) {
      appSegmentacion.setNombreCliente(rs.getString(columna));
    }
    columna = columnas.get("APP_SEGMENTACION_Segmento");
    if (columna != null) {
      appSegmentacion.setSegmento(rs.getString(columna));
    }
    columna = columnas.get("APP_SEGMENTACION_tipo");
    if (columna != null) {
      appSegmentacion.setTipo(rs.getString(columna));
    }
    columna = columnas.get("APP_SEGMENTACION_Ingreso");
    if (columna != null) {
      appSegmentacion.setIngreso(rs.getTimestamp(columna));
    }
    columna = columnas.get("APP_SEGMENTACION_con_marco");
    if (columna != null) {
      appSegmentacion.setConMarco(rs.getString(columna));
    }
    columna = columnas.get("APP_SEGMENTACION_indicador");
    if (columna != null) {
      appSegmentacion.setIndicador(rs.getLong(columna));
    }
    return appSegmentacion;
  }

  @Override
  public void insertar(AppSegmentacion appSegmentacion) throws SQLException {
    PreparedStatement sentencia = null;
    try {
      int i = 1;
      String sql = "insert into APP_SEGMENTACION(nit,Nombre_cliente,Segmento,tipo,Ingreso,con_marco,indicador) values (?,?,?,?,?,?,?)";
      sentencia = cnn.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);
      sentencia.setObject(i++, appSegmentacion.getNit());
      sentencia.setObject(i++, appSegmentacion.getNombreCliente());
      sentencia.setObject(i++, appSegmentacion.getSegmento());
      sentencia.setObject(i++, appSegmentacion.getTipo());
      sentencia.setObject(i++, DateUtil.balancearFecha(appSegmentacion.getIngreso()));
      sentencia.setObject(i++, appSegmentacion.getConMarco());
      sentencia.setObject(i++, appSegmentacion.getIndicador());

      sentencia.executeUpdate();
      ResultSet rs = sentencia.getGeneratedKeys();
      if (rs.next()) {
        appSegmentacion.setIdNit(rs.getLong(ID));
      }
    } finally {
      ConexionBD.desconectar(sentencia);
    }
  }

  @Override
  public void editar(AppSegmentacion appSegmentacion) throws SQLException {
    PreparedStatement sentencia = null;
    try {
      int i = 1;
      String sql = "update APP_SEGMENTACION set nit=?,Nombre_cliente=?,Segmento=?,tipo=?,Ingreso=?,con_marco=?,indicador=? where Id_nit=?";
      sentencia = cnn.prepareStatement(sql);
      sentencia.setObject(i++, appSegmentacion.getNit());
      sentencia.setObject(i++, appSegmentacion.getNombreCliente());
      sentencia.setObject(i++, appSegmentacion.getSegmento());
      sentencia.setObject(i++, appSegmentacion.getTipo());
      sentencia.setObject(i++, DateUtil.balancearFecha(appSegmentacion.getIngreso()));
      sentencia.setObject(i++, appSegmentacion.getConMarco());
      sentencia.setObject(i++, appSegmentacion.getIndicador());
      sentencia.setObject(i++, appSegmentacion.getIdNit());

      sentencia.executeUpdate();
    } finally {
      ConexionBD.desconectar(sentencia);
    }
  }

  @Override
  public List<AppSegmentacion> consultar() throws SQLException {
    PreparedStatement sentencia = null;
    List<AppSegmentacion> lista = new ArrayList<>();
    try {

      String sql = "select * from APP_SEGMENTACION";
      sentencia = cnn.prepareStatement(sql);
      ResultSet rs = sentencia.executeQuery();
      while (rs.next()) {
        lista.add(getAppSegmentacion(rs));
      }
    } finally {
      ConexionBD.desconectar(sentencia);
    }
    return lista;

  }

  @Override
  public AppSegmentacion consultar(Long id) throws SQLException {
    PreparedStatement sentencia = null;
    AppSegmentacion obj = null;
    try {

      String sql = "select * from APP_SEGMENTACION where Id_nit=?";
      sentencia = cnn.prepareStatement(sql);
      sentencia.setLong(1, id);
      ResultSet rs = sentencia.executeQuery();
      if (rs.next()) {
        obj = getAppSegmentacion(rs);
      }
    } finally {
      ConexionBD.desconectar(sentencia);
    }
    return obj;
  }

}
