package co.movistar.planeacioncomercial.modelo.dao;

import co.movistar.planeacioncomercial.modelo.conexion.ConexionBD;
import co.movistar.planeacioncomercial.modelo.dao.crud.AppPresupuestoComercialCrud;
import co.movistar.planeacioncomercial.modelo.vo.AppPresupuestoComercial;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class AppPresupuestoComercialDao extends AppPresupuestoComercialCrud {

  public AppPresupuestoComercialDao(Connection cnn) {
    super(cnn);
  }

  public List<AppPresupuestoComercial> consultarCategoriaUsuario(Long idUsuario) throws SQLException {
    PreparedStatement sentencia = null;
    List<AppPresupuestoComercial> lista = new ArrayList<>();
    try {

      String sql = "SELECT DISTINCT CATEGORIA "
              + "FROM APP_PRESUPUESTO_COMERCIAL "
              + "WHERE ID_USUARIO = ?;";
      sentencia = cnn.prepareStatement(sql);
      sentencia.setLong(1, idUsuario);
      ResultSet rs = sentencia.executeQuery();
      Map<String, Integer> columnas = new HashMap<>();
      columnas.put("APP_PRESUPUESTO_COMERCIAL_CATEGORIA", 1);
      while (rs.next()) {
        lista.add(getappPresupuestoComercial(rs, columnas));
      }
    } finally {
      ConexionBD.desconectar(sentencia);
    }
    return lista;
  }

  public List<AppPresupuestoComercial> consultarPeriodoUsuario(Long idUsuario) throws SQLException {
    PreparedStatement sentencia = null;
    List<AppPresupuestoComercial> lista = new ArrayList<>();
    try {

      String sql = "SELECT DISTINCT PERIODO "
              + "FROM APP_PRESUPUESTO_COMERCIAL "
              + "WHERE ID_USUARIO = ?;";
      sentencia = cnn.prepareStatement(sql);
      sentencia.setLong(1, idUsuario);
      ResultSet rs = sentencia.executeQuery();
      Map<String, Integer> columnas = new HashMap<>();
      columnas.put("APP_PRESUPUESTO_COMERCIAL_PERIODO", 1);
      while (rs.next()) {
        lista.add(getappPresupuestoComercial(rs, columnas));
      }
    } finally {
      ConexionBD.desconectar(sentencia);
    }
    return lista;
  }

  public List<AppPresupuestoComercial> consultarMetaUsuario(Long idUsuario, String categoria, String periodo) throws SQLException {
    PreparedStatement sentencia = null;
    List<AppPresupuestoComercial> lista = new ArrayList<>();
    try {

      String sql = "SELECT PRODUCTO,SUM(PRESUPUESTO)PRESUPUESTO "
              + "FROM APP_PRESUPUESTO_COMERCIAL "
              + "WHERE ID_USUARIO = ? AND CATEGORIA = ? AND PERIODO = ? "
              + "GROUP BY PRODUCTO, CATEGORIA;";
      sentencia = cnn.prepareStatement(sql);
      sentencia.setLong(1, idUsuario);
      sentencia.setString(2, categoria);
      sentencia.setString(3, periodo);
      ResultSet rs = sentencia.executeQuery();
      Map<String, Integer> columnas = new HashMap<>();
      columnas.put("APP_PRESUPUESTO_COMERCIAL_PRODUCTO", 1);
      columnas.put("APP_PRESUPUESTO_COMERCIAL_PRESUPUESTO", 2);
      while (rs.next()) {
        lista.add(getappPresupuestoComercial(rs, columnas));
      }
    } finally {
      ConexionBD.desconectar(sentencia);
    }
    return lista;
  }
}
