package co.movistar.planeacioncomercial.modelo.dao.crud;

import co.movistar.planeacioncomercial.modelo.conexion.ConexionBD;
import co.movistar.planeacioncomercial.modelo.vo.AppMetaUsuario;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class AppMetaUsuarioCrud implements IGenericoDAO<AppMetaUsuario> {

  protected final int ID = 1;
  protected Connection cnn;

  public AppMetaUsuarioCrud(Connection cnn) {
    this.cnn = cnn;
  }

  public static AppMetaUsuario getAppMetaUsuario(ResultSet rs) throws SQLException {
    AppMetaUsuario appMetaUsuario = new AppMetaUsuario();
    appMetaUsuario.setIdMetaUsuario(rs.getLong("Id_Meta_Usuario"));
    appMetaUsuario.setIdMeta(rs.getLong("Id_Meta"));
    appMetaUsuario.setValorMetaEsp(rs.getLong("Valor_Meta_Esp"));
    appMetaUsuario.setValorMetaAsig(rs.getLong("Valor_Meta_Asig"));
    appMetaUsuario.setIdUsuario(rs.getLong("Id_Usuario"));
    appMetaUsuario.setPeriodo(rs.getDate("Periodo"));
    appMetaUsuario.setIdCoordinador(rs.getLong("Id_Coordinador"));
    appMetaUsuario.setIdPunto(rs.getString("Id_Punto"));
    return appMetaUsuario;
  }

  public static AppMetaUsuario getAppMetaUsuario(ResultSet rs, Map<String, Integer> columnas) throws SQLException {
    AppMetaUsuario appMetaUsuario = new AppMetaUsuario();
    Integer columna = columnas.get("APP_META_USUARIO_Id_Meta_Usuario");
    if (columna != null) {
      appMetaUsuario.setIdMetaUsuario(rs.getLong(columna));
    }
    columna = columnas.get("APP_META_USUARIO_Id_Meta");
    if (columna != null) {
      appMetaUsuario.setIdMeta(rs.getLong(columna));
    }
    columna = columnas.get("APP_META_USUARIO_Valor_Meta_Esp");
    if (columna != null) {
      appMetaUsuario.setValorMetaEsp(rs.getLong(columna));
    }
    columna = columnas.get("APP_META_USUARIO_Valor_Meta_Asig");
    if (columna != null) {
      appMetaUsuario.setValorMetaAsig(rs.getLong(columna));
    }
    columna = columnas.get("APP_META_USUARIO_Id_Usuario");
    if (columna != null) {
      appMetaUsuario.setIdUsuario(rs.getLong(columna));
    }
    columna = columnas.get("APP_META_USUARIO_Periodo");
    if (columna != null) {
      appMetaUsuario.setPeriodo(rs.getDate(columna));
    }
    columna = columnas.get("APP_META_USUARIO_Id_Coordinador");
    if (columna != null) {
      appMetaUsuario.setIdCoordinador(rs.getLong(columna));
    }
    columna = columnas.get("APP_META_USUARIO_Id_Punto");
    if (columna != null) {
      appMetaUsuario.setIdPunto(rs.getString(columna));
    }
    return appMetaUsuario;
  }

  @Override
  public void insertar(AppMetaUsuario appMetaUsuario) throws SQLException {
    PreparedStatement sentencia = null;
    try {
      int i = 1;
      String sql = "insert into APP_META_USUARIO(Id_Meta,Valor_Meta_Esp,Valor_Meta_Asig,Id_Usuario,Periodo,Id_Coordinador,Id_Punto) values (?,?,?,?,?,?,?)";
      sentencia = cnn.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);
      sentencia.setObject(i++, appMetaUsuario.getIdMeta());
      sentencia.setObject(i++, appMetaUsuario.getValorMetaEsp());
      sentencia.setObject(i++, appMetaUsuario.getValorMetaAsig());
      sentencia.setObject(i++, appMetaUsuario.getIdUsuario());
      sentencia.setObject(i++, appMetaUsuario.getPeriodo());

      sentencia.executeUpdate();
      ResultSet rs = sentencia.getGeneratedKeys();
      if (rs.next()) {
        appMetaUsuario.setIdMetaUsuario(rs.getLong(ID));
      }
    } finally {
      ConexionBD.desconectar(sentencia);
    }
  }

  @Override
  public void editar(AppMetaUsuario appMetaUsuario) throws SQLException {
    PreparedStatement sentencia = null;
    try {
      int i = 1;
      String sql = "update APP_META_USUARIO set Id_Meta=?,Valor_Meta_Esp=?,Valor_Meta_Asig=?,Id_Usuario=?,Periodo=?,Id_Coordinador=?,Id_Punto=? where Id_Meta_Usuario=?";
      sentencia = cnn.prepareStatement(sql);
      sentencia.setObject(i++, appMetaUsuario.getIdMeta());
      sentencia.setObject(i++, appMetaUsuario.getValorMetaEsp());
      sentencia.setObject(i++, appMetaUsuario.getValorMetaAsig());
      sentencia.setObject(i++, appMetaUsuario.getIdUsuario());
      sentencia.setObject(i++, appMetaUsuario.getPeriodo());
      sentencia.setObject(i++, appMetaUsuario.getIdMetaUsuario());

      sentencia.executeUpdate();
    } finally {
      ConexionBD.desconectar(sentencia);
    }
  }

  @Override
  public List<AppMetaUsuario> consultar() throws SQLException {
    PreparedStatement sentencia = null;
    List<AppMetaUsuario> lista = new ArrayList<>();
    try {

      String sql = "select * from APP_META_USUARIO";
      sentencia = cnn.prepareStatement(sql);
      ResultSet rs = sentencia.executeQuery();
      while (rs.next()) {
        lista.add(getAppMetaUsuario(rs));
      }
    } finally {
      ConexionBD.desconectar(sentencia);
    }
    return lista;

  }

  @Override
  public AppMetaUsuario consultar(Long id) throws SQLException {
    PreparedStatement sentencia = null;
    AppMetaUsuario obj = null;
    try {

      String sql = "select * from APP_META_USUARIO where Id_Meta_Usuario=?";
      sentencia = cnn.prepareStatement(sql);
      sentencia.setLong(1, id);
      ResultSet rs = sentencia.executeQuery();
      if (rs.next()) {
        obj = getAppMetaUsuario(rs);
      }
    } finally {
      ConexionBD.desconectar(sentencia);
    }
    return obj;
  }

}
