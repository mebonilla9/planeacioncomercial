package co.movistar.planeacioncomercial.modelo.dao;

import co.movistar.planeacioncomercial.modelo.dao.crud.AppConsultaSegmentoCrud;

import java.sql.Connection;

public class AppConsultaSegmentoDao extends AppConsultaSegmentoCrud {

  public AppConsultaSegmentoDao(Connection cnn) {
    super(cnn);
  }
}
