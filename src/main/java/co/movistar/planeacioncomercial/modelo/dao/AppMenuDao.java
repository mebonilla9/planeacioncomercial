package co.movistar.planeacioncomercial.modelo.dao;

import co.movistar.planeacioncomercial.modelo.conexion.ConexionBD;
import co.movistar.planeacioncomercial.modelo.dao.crud.AppMenuCrud;
import co.movistar.planeacioncomercial.modelo.vo.AppMenu;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author Manuel Ernesto Bonilla Muñoz <mebonilla9@gmail.com>
 */
public class AppMenuDao extends AppMenuCrud {

  public AppMenuDao(Connection cnn) {
    super(cnn);
  }

  public List<AppMenu> consultarMenuUsuario(Long idUsuario, Long idPadre) throws SQLException {
    PreparedStatement sentencia = null;
    List<AppMenu> lista = new ArrayList<>();
    try {
      String sql = "SELECT "
              + "mnu.ID_MENU     APP_MENU_ID_MENU, "
              + "mnu.NOMBRE_MENU APP_MENU_NOMBRE_MENU, "
              + "mnu.Menu_padre APP_MENU_MENU_PADRE "
              + "FROM APP_MENU mnu INNER JOIN APP_MENU_CARGO amc ON mnu.ID_MENU = amc.ID_MENU "
              + "INNER JOIN APP_CARGO car ON amc.ID_CARGO = car.ID_CARGO "
              + "INNER JOIN APP_USUARIOS usu ON car.ID_CARGO = usu.ID_CARGO "
              + "WHERE usu.ID_USUARIO = ? AND mnu.Menu_padre = ? AND mnu.ESTADO LIKE 'VIGENTE' "
              + "ORDER BY mnu.NOMBRE_MENU ASC;";
      sentencia = cnn.prepareStatement(sql);
      sentencia.setLong(1, idUsuario);
      sentencia.setLong(2, idPadre);
      ResultSet rs = sentencia.executeQuery();
      Map<String, Integer> columnas = new HashMap<>();
      columnas.put("APP_MENU_ID_MENU", 1);
      columnas.put("APP_MENU_NOMBRE_MENU", 2);
      columnas.put("APP_MENU_MENU_PADRE", 3);
      while (rs.next()) {
        lista.add(getAppMenu(rs, columnas));
      }
    } finally {
      ConexionBD.desconectar(sentencia);
    }
    return lista;
  }

  public List<AppMenu> consultarCategoriaMenuUsuario(Long idUsuario) throws SQLException {
    PreparedStatement sentencia = null;
    List<AppMenu> lista = new ArrayList<>();
    try {
      String sql = "SELECT "
              + "mnu.ID_MENU     APP_MENU_ID_MENU, "
              + "mnu.NOMBRE_MENU APP_MENU_NOMBRE_MENU,"
              + "mnu.Menu_padre APP_MENU_MENU_PADRE "
              + "FROM APP_MENU mnu INNER JOIN APP_MENU_CARGO amc ON mnu.ID_MENU = amc.ID_MENU "
              + "INNER JOIN APP_CARGO car ON amc.ID_CARGO = car.ID_CARGO "
              + "INNER JOIN APP_USUARIOS usu ON car.ID_CARGO = usu.ID_CARGO "
              + "WHERE usu.ID_USUARIO = ? AND mnu.ESTADO LIKE 'VIGENTE' AND Menu_padre IS NULL "
              + "ORDER BY mnu.NOMBRE_MENU ASC;";
      sentencia = cnn.prepareStatement(sql);
      sentencia.setLong(1, idUsuario);
      ResultSet rs = sentencia.executeQuery();
      Map<String, Integer> columnas = new HashMap<>();
      columnas.put("APP_MENU_ID_MENU", 1);
      columnas.put("APP_MENU_NOMBRE_MENU", 2);
      columnas.put("APP_MENU_MENU_PADRE", 3);
      while (rs.next()) {
        lista.add(getAppMenu(rs, columnas));
      }
    } finally {
      ConexionBD.desconectar(sentencia);
    }
    return lista;

  }
}
