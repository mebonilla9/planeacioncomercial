package co.movistar.planeacioncomercial.modelo.dao.crud;

import co.movistar.planeacioncomercial.modelo.conexion.ConexionBD;
import co.movistar.planeacioncomercial.modelo.vo.AppCanales;
import co.movistar.planeacioncomercial.modelo.vo.AppEstados;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * @author Manuel Ernesto Bonilla Muñoz <mebonilla9@gmail.com>
 */
public class AppCanalesCrud implements IGenericoDAO<AppCanales> {

  protected final int ID = 1;
  protected Connection cnn;

  public AppCanalesCrud(Connection cnn) {
    this.cnn = cnn;
  }

  public static AppCanales getAppCanales(ResultSet rs) throws SQLException {
    AppCanales appCanales = new AppCanales();
    appCanales.setIdCanal(rs.getLong("ID_CANAL"));
    appCanales.setNombreCanal(rs.getString("NOMBRE_CANAL"));
    appCanales.setEstado(new AppEstados(rs.getLong("ID_ESTADO")));

    return appCanales;
  }

  public static AppCanales getAppCanales(ResultSet rs, Map<String, Integer> columnas) throws SQLException {
    AppCanales appCanales = new AppCanales();
    Integer columna = columnas.get("APP_CANALES_ID_CANAL");
    if (columna != null) {
      appCanales.setIdCanal(rs.getLong(columna));
    }
    columna = columnas.get("APP_CANALES_NOMBRE_CANAL");
    if (columna != null) {
      appCanales.setNombreCanal(rs.getString(columna));
    }
    columna = columnas.get("APP_CANALES_ID_ESTADO");
    if (columna != null) {
      appCanales.setEstado(new AppEstados(rs.getLong(columna)));
    }
    return appCanales;
  }

  @Override
  public void insertar(AppCanales appCanales) throws SQLException {
    PreparedStatement sentencia = null;
    try {
      int i = 1;
      String sql = "insert into APP_CANALES(NOMBRE_CANAL,ID_ESTADO) values (?,?)";
      sentencia = cnn.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);
      sentencia.setObject(i++, appCanales.getNombreCanal());
      sentencia.setObject(i++, appCanales.getEstado().getIdEstado());

      sentencia.executeUpdate();
      ResultSet rs = sentencia.getGeneratedKeys();
      if (rs.next()) {
        appCanales.setIdCanal(rs.getLong(ID));
      }
    } finally {
      ConexionBD.desconectar(sentencia);
    }
  }

  @Override
  public void editar(AppCanales appCanales) throws SQLException {
    PreparedStatement sentencia = null;
    try {
      int i = 1;
      String sql = "update APP_CANALES set NOMBRE_CANAL=?,ID_ESTADO=? where ID_CANAL=?";
      sentencia = cnn.prepareStatement(sql);
      sentencia.setObject(i++, appCanales.getNombreCanal());
      sentencia.setObject(i++, appCanales.getEstado().getIdEstado());
      sentencia.setObject(i++, appCanales.getIdCanal());

      sentencia.executeUpdate();
    } finally {
      ConexionBD.desconectar(sentencia);
    }
  }

  @Override
  public List<AppCanales> consultar() throws SQLException {
    PreparedStatement sentencia = null;
    List<AppCanales> lista = new ArrayList<>();
    try {

      String sql = "select * from APP_CANALES";
      sentencia = cnn.prepareStatement(sql);
      ResultSet rs = sentencia.executeQuery();
      while (rs.next()) {
        lista.add(getAppCanales(rs));
      }
    } finally {
      ConexionBD.desconectar(sentencia);
    }
    return lista;

  }

  @Override
  public AppCanales consultar(Long id) throws SQLException {
    PreparedStatement sentencia = null;
    AppCanales obj = null;
    try {

      String sql = "select * from APP_CANALES where ID_CANAL=?";
      sentencia = cnn.prepareStatement(sql);
      sentencia.setLong(1, id);
      ResultSet rs = sentencia.executeQuery();
      if (rs.next()) {
        obj = getAppCanales(rs);
      }
    } finally {
      ConexionBD.desconectar(sentencia);
    }
    return obj;
  }

}
