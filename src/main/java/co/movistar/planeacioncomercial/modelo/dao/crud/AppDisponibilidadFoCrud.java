package co.movistar.planeacioncomercial.modelo.dao.crud;

import co.movistar.planeacioncomercial.modelo.conexion.ConexionBD;
import co.movistar.planeacioncomercial.modelo.vo.AppDisponibilidadFo;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class AppDisponibilidadFoCrud implements IGenericoDAO<AppDisponibilidadFo> {

  protected final int ID = 1;
  protected Connection cnn;

  public AppDisponibilidadFoCrud(Connection cnn) {
    this.cnn = cnn;
  }

  public static AppDisponibilidadFo getAppDisponibilidadFo(ResultSet rs) throws SQLException {
    AppDisponibilidadFo appDisponibilidadFo = new AppDisponibilidadFo();
    appDisponibilidadFo.setIdDisponibilidadFo(rs.getLong("ID_DISPONIBILIDAD_FO"));
    appDisponibilidadFo.setBarrio(rs.getString("BARRIO"));
    appDisponibilidadFo.setLocalidad(rs.getString("LOCALIDAD"));
    appDisponibilidadFo.setDepartamento(rs.getString("DEPARTAMENTO"));
    appDisponibilidadFo.setRegional(rs.getString("REGIONAL"));
    appDisponibilidadFo.setxLibreCto(rs.getString("XLIBRE_CTO"));
    appDisponibilidadFo.setCto(rs.getString("CTO"));
    appDisponibilidadFo.setPrincipal(rs.getString("PRINCIPAL"));
    appDisponibilidadFo.setReferenciaPrincipal(rs.getString("REFERENCIA_PRINCIPAL"));
    appDisponibilidadFo.setCruce(rs.getString("CRUCE"));
    appDisponibilidadFo.setReferenciaCruce(rs.getString("REFERENCIA_CRUCE"));
    appDisponibilidadFo.setPlaca(rs.getString("PLACA"));
    appDisponibilidadFo.setComplemento(rs.getString("COMPLEMENTO"));
    appDisponibilidadFo.setDireccion(rs.getString("DIRECCION"));
    appDisponibilidadFo.setParesLibres(rs.getLong("PARESLIBRES"));
    appDisponibilidadFo.setIdParametrizada(rs.getBoolean("ID_PARAMETRIZADA"));

    return appDisponibilidadFo;
  }

  public static AppDisponibilidadFo getAppDisponibilidadFo(ResultSet rs, Map<String, Integer> columnas) throws SQLException {
    AppDisponibilidadFo appDisponibilidadFo = new AppDisponibilidadFo();
    Integer columna = columnas.get("APP_DISPONIBILIDAD_FO_ID_DISPONIBILIDAD_FO");
    if (columna != null) {
      appDisponibilidadFo.setIdDisponibilidadFo(rs.getLong(columna));
    }
    columna = columnas.get("APP_DISPONIBILIDAD_FO_BARRIO");
    if (columna != null) {
      appDisponibilidadFo.setBarrio(rs.getString(columna));
    }
    columna = columnas.get("APP_DISPONIBILIDAD_FO_LOCALIDAD");
    if (columna != null) {
      appDisponibilidadFo.setLocalidad(rs.getString(columna));
    }
    columna = columnas.get("APP_DISPONIBILIDAD_FO_DEPARTAMENTO");
    if (columna != null) {
      appDisponibilidadFo.setDepartamento(rs.getString(columna));
    }
    columna = columnas.get("APP_DISPONIBILIDAD_FO_REGIONAL");
    if (columna != null) {
      appDisponibilidadFo.setRegional(rs.getString(columna));
    }
    columna = columnas.get("APP_DISPONIBILIDAD_FO_XLIBRE_CTO");
    if (columna != null) {
      appDisponibilidadFo.setxLibreCto(rs.getString(columna));
    }
    columna = columnas.get("APP_DISPONIBILIDAD_FO_CTO");
    if (columna != null) {
      appDisponibilidadFo.setCto(rs.getString(columna));
    }
    columna = columnas.get("APP_DISPONIBILIDAD_FO_PRINCIPAL");
    if (columna != null) {
      appDisponibilidadFo.setPrincipal(rs.getString(columna));
    }
    columna = columnas.get("APP_DISPONIBILIDAD_FO_REFERENCIA_PRINCIPAL");
    if (columna != null) {
      appDisponibilidadFo.setReferenciaPrincipal(rs.getString(columna));
    }
    columna = columnas.get("APP_DISPONIBILIDAD_FO_CRUCE");
    if (columna != null) {
      appDisponibilidadFo.setCruce(rs.getString(columna));
    }
    columna = columnas.get("APP_DISPONIBILIDAD_FO_REFERENCIA_CRUCE");
    if (columna != null) {
      appDisponibilidadFo.setReferenciaCruce(rs.getString(columna));
    }
    columna = columnas.get("APP_DISPONIBILIDAD_FO_PLACA");
    if (columna != null) {
      appDisponibilidadFo.setPlaca(rs.getString(columna));
    }
    columna = columnas.get("APP_DISPONIBILIDAD_FO_COMPLEMENTO");
    if (columna != null) {
      appDisponibilidadFo.setComplemento(rs.getString(columna));
    }
    columna = columnas.get("APP_DISPONIBILIDAD_FO_DIRECCION");
    if (columna != null) {
      appDisponibilidadFo.setDireccion(rs.getString(columna));
    }
    columna = columnas.get("APP_DISPONIBILIDAD_FO_PARESLIBRES");
    if (columna != null) {
      appDisponibilidadFo.setParesLibres(rs.getLong(columna));
    }
    columna = columnas.get("APP_DISPONIBILIDAD_FO_ID_PARAMETRIZADA");
    if (columna != null) {
      appDisponibilidadFo.setIdParametrizada(rs.getBoolean(columna));
    }
    return appDisponibilidadFo;
  }

  @Override
  public void insertar(AppDisponibilidadFo appDisponibilidadFo) throws SQLException {
    PreparedStatement sentencia = null;
    try {
      int i = 1;
      String sql = "insert into APP_DISPONIBILIDAD_FO(BARRIO,LOCALIDAD,DEPARTAMENTO,REGIONAL,XLIBRE_CTO,CTO,PRINCIPAL,REFERENCIA_PRINCIPAL,CRUCE,REFERENCIA_CRUCE,PLACA,COMPLEMENTO,DIRECCION,PARESLIBRES,ID_PARAMETRIZADA) values (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
      sentencia = cnn.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);
      sentencia.setObject(i++, appDisponibilidadFo.getBarrio());
      sentencia.setObject(i++, appDisponibilidadFo.getLocalidad());
      sentencia.setObject(i++, appDisponibilidadFo.getDepartamento());
      sentencia.setObject(i++, appDisponibilidadFo.getRegional());
      sentencia.setObject(i++, appDisponibilidadFo.getxLibreCto());
      sentencia.setObject(i++, appDisponibilidadFo.getCto());
      sentencia.setObject(i++, appDisponibilidadFo.getPrincipal());
      sentencia.setObject(i++, appDisponibilidadFo.getReferenciaPrincipal());
      sentencia.setObject(i++, appDisponibilidadFo.getCruce());
      sentencia.setObject(i++, appDisponibilidadFo.getReferenciaCruce());
      sentencia.setObject(i++, appDisponibilidadFo.getPlaca());
      sentencia.setObject(i++, appDisponibilidadFo.getComplemento());
      sentencia.setObject(i++, appDisponibilidadFo.getDireccion());
      sentencia.setObject(i++, appDisponibilidadFo.getParesLibres());
      sentencia.setObject(i++, appDisponibilidadFo.getIdParametrizada());
      sentencia.executeUpdate();
      ResultSet rs = sentencia.getGeneratedKeys();
      if (rs.next()) {
        appDisponibilidadFo.setIdDisponibilidadFo(rs.getLong(ID));
      }
    } finally {
      ConexionBD.desconectar(sentencia);
    }
  }

  @Override
  public void editar(AppDisponibilidadFo appDisponibilidadFo) throws SQLException {
    PreparedStatement sentencia = null;
    try {
      int i = 1;
      String sql = "update APP_DISPONIBILIDAD_FO set BARRIO=?,LOCALIDAD=?,DEPARTAMENTO=?,REGIONAL=?,XLIBRE_CTO=?,CTO=?,PRINCIPAL=?,REFERENCIA_PRINCIPAL=?,CRUCE=?,REFERENCIA_CRUCE=?,PLACA=?,COMPLEMENTO=?,DIRECCION=?,PARESLIBRES=?,ID_PARAMETRIZADA=? where ID_DISPONIBILIDAD_FO=?";
      sentencia = cnn.prepareStatement(sql);
      sentencia.setObject(i++, appDisponibilidadFo.getBarrio());
      sentencia.setObject(i++, appDisponibilidadFo.getLocalidad());
      sentencia.setObject(i++, appDisponibilidadFo.getDepartamento());
      sentencia.setObject(i++, appDisponibilidadFo.getRegional());
      sentencia.setObject(i++, appDisponibilidadFo.getxLibreCto());
      sentencia.setObject(i++, appDisponibilidadFo.getCto());
      sentencia.setObject(i++, appDisponibilidadFo.getPrincipal());
      sentencia.setObject(i++, appDisponibilidadFo.getReferenciaPrincipal());
      sentencia.setObject(i++, appDisponibilidadFo.getCruce());
      sentencia.setObject(i++, appDisponibilidadFo.getReferenciaCruce());
      sentencia.setObject(i++, appDisponibilidadFo.getPlaca());
      sentencia.setObject(i++, appDisponibilidadFo.getComplemento());
      sentencia.setObject(i++, appDisponibilidadFo.getDireccion());
      sentencia.setObject(i++, appDisponibilidadFo.getParesLibres());
      sentencia.setObject(i++, appDisponibilidadFo.getIdParametrizada());
      sentencia.setObject(i++, appDisponibilidadFo.getIdDisponibilidadFo());
      sentencia.executeUpdate();
    } finally {
      ConexionBD.desconectar(sentencia);
    }
  }

  @Override
  public List<AppDisponibilidadFo> consultar() throws SQLException {
    PreparedStatement sentencia = null;
    List<AppDisponibilidadFo> lista = new ArrayList<>();
    try {

      String sql = "select * from APP_DISPONIBILIDAD_FO";
      sentencia = cnn.prepareStatement(sql);
      ResultSet rs = sentencia.executeQuery();
      while (rs.next()) {
        lista.add(getAppDisponibilidadFo(rs));
      }
    } finally {
      ConexionBD.desconectar(sentencia);
    }
    return lista;

  }

  @Override
  public AppDisponibilidadFo consultar(Long id) throws SQLException {
    PreparedStatement sentencia = null;
    AppDisponibilidadFo obj = null;
    try {

      String sql = "select * from APP_DISPONIBILIDAD_FO where ID_DISPONIBILIDAD_FO=?";
      sentencia = cnn.prepareStatement(sql);
      sentencia.setLong(1, id);
      ResultSet rs = sentencia.executeQuery();
      if (rs.next()) {
        obj = getAppDisponibilidadFo(rs);
      }
    } finally {
      ConexionBD.desconectar(sentencia);
    }
    return obj;
  }

}
